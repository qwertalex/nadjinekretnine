$(document).ready(function(){
    
     animate_moj_profil()
   
    $("#promeni-email").click(function(e){
          e.preventDefault();
     $("#modal-update-email").modal('show');
    })
    $("#promeni-username").click(function(e){
         e.preventDefault();
         $("#modal-update-username").modal();
    });
        $("#promeni-pass").click(function(e){
          e.preventDefault();
          $("#modal-update-pass").modal();
    })

    if (typeof MODAL_OK_EMAIL_PASS!=='undefined'&&MODAL_OK_EMAIL_PASS) {
        $("#modal-za-update-user-data").modal();
    }
    if (typeof MODAL_ZA_ERRORS!=='undefined'&&!MODAL_ZA_ERRORS) {
        $("#modal-update-errors").modal();
    }    
    
    
    
       $( "#mp_dob" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat:'dd/mm/yy',
      yearRange: '1910:'+new Date().getFullYear().toString(),
    });
    
    
    
  
    $(".mp_samobrojevi").popover({
        trigger:'focus'
    })

if ($.fn.select2) {


  config_mesto_options()

//-------------------------------------------------------------
      //  console.log(filter_counter_php.kategorija)
var data=[{id:0,title:"Sve kategorije"}]
//
for (var i in filter_counter_php.kategorija){
            data.push(filter_counter_php.kategorija[i])
    }
  //  console.log(data)
  $("#select_kategorija").select2({
       placeholder: "Izaberite kategoriju...",
  data: {
    results: data,
    text: "title"
  },
        formatSelection: function format(item) { return item.title; },
    formatResult: function format(item) { return item.title; },
  
   });
//--------------------------------------------------------------------------



 $("#s2id_select_kategorija").css({
     width:'100%',
 });
  /*
  $("#mesta_srbija").select2({
      placeholder:"Izaberite mesto...",
  });*/
   $("#s2id_mesta_srbija,#s2id_deo_mesta,#s2id_select_lokacija").css({
  width:'100%',
  'margin-left':"0"
 });
  
  
 // $('#deo_mesta').select2();
 // $("#lokacija").select2();
  
  //new Lokacija();
  //select_lokacija();
  
  
  
  }
     $('input').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue', 
    });
   
        $('.line-blue-icheck').each(function(){
     var text=$(this).data('label');
     
    $(this).iCheck({
      checkboxClass: 'icheckbox_line-blue add-tooltip',
      radioClass: 'iradio_line-blue',
      insert: '<div class="icheck_line-icon"></div>' + text
    });
  });
  
      $('.prodaja_izdavanje_filter').each(function(){
      var label_text= $("label[for='"+this.name+"']").text();
    $("label[for='"+this.name+"']").remove();
    
    
    $(this).iCheck({
      checkboxClass: 'icheckbox_line-blue add-tooltip',
      radioClass: 'iradio_line-blue',
      insert: '<div class="icheck_line-icon"></div>' + label_text
    });
  });
    
         $('.flat-icheck').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });
 
  $('.dropdown-menu').click(function (e) {
    e.stopPropagation();
  });
  
  

  //FILTERI
  new Form_call();
  
  
  new Oglasi_change();
  
  
  
   //modali za placene ponude
  new Lunch_modals();
  
  
  
  
  $('#top-tabs a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
})
  
   

  $('.standardni-oglas-popover').qtip({
      position:{
          my:"center left",
          at:"center right", 
      },
     content:"<div class=\"qtip-titlebar text-center\"><strong>Standardni oglas</strong></div>\n\
\n <p>Postavite besplatno jedan STANDARDNI oglas. Standardni oglas se ne istice posebno u listi oglasa!</p> \
<img   src='/img/baneri/standardponuda.png'/>",
     html:true,
     style: { classes: 'qtip-tipped ' },
     hide: {
        delay: 200,
        fixed: true, // <--- add this
     //   effect: function() { $(this).fadeOut(250); }
    },
   
      
  });
  

  $('.top-oglas-popover').qtip({
      position:{
          my:"center left",
          at:"center right",
          
      },
     content:"<div class=\"qtip-titlebar text-center\"><strong>Top ponuda</strong></div>\n\
\n                <p><strong>Povecajte vidljivost vaseg oglasa uz nasu TOP ponudu.</strong>    \n\
                      Vas oglas ce biti postavljen pri vrhu liste oglasa i imace oznaku <strong>TOP</strong>.                        </p>\
<img src='/img/baneri/topponuda.png'/>",
     html:true,
     style: { classes: 'qtip-rounded qtip-jtools' },
     hide: {
        delay: 200,
        fixed: true, // <--- add this
    //    effect: function() { $(this).fadeOut(250); }
    },  
      
  });
  
  
 
  $('.premium-oglas-popover').qtip({
      position:{
          my:"center left",
          at:"center right"
      },
     content:"<div class=\"qtip-titlebar text-center\"><strong>Premium ponuda</strong></div>\n\
\n AKO ZELITE NA NAJBRZI NACIN DA PRODATE ILI IZNAJMITE VASU NEKRETNINU IZABERITE PREMIUM PONUDU.</p>\<img src='/img/baneri/premiumponuda.png'/>",
     html:true,
     style: { classes: 'qtip-jtools qtip-rounded' },
     hide: {
        delay: 200,
        fixed: true, // <--- add this
     //   effect: function() { $(this).fadeOut(250); }
    },
  
      
  }); 
  
    if (typeof PRAVNA_LICA_PODATCI_MODAL!='undefined'&&PRAVNA_LICA_PODATCI_MODAL) {
        $("#pravna-lica_podatci").modal();
    }

    if (typeof PRAVNA_LICA_EMAIL_OBAVESTENJA_MODAL!='undefined'&&PRAVNA_LICA_EMAIL_OBAVESTENJA_MODAL) {
        $("#pravna-lica_email_obavestenja").modal();
    }  
  
  
  
  
    if (typeof pl_ads_info!=='undefined') {
      var pravna_lica_=new Pravna_lica(pl_ads_info);  
       new Produzi_oglas(pl_ads_info);
    }
  
 
  
  
  
  
  
    
  function day_plus_one(){
   var myDate = new Date(); 
   var date=new Date();
   date.setTime(myDate.setDate(myDate.getDate()+1));
   return date;
    }
 
           
           
    $( ".datepicker-oglas-pl-do" ).datepicker({
        dateFormat:"dd/mm/yy",
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      minDate:day_plus_one(),
    onClose:function(date){
           pravna_lica_.razlika_dana(date)
    }}).keydown(function(e){
        e.preventDefault();
    });
  

 
    if (typeof USER_NOT_VALIDE!=='undefined'&&USER_NOT_VALIDE) {
        $("#modal-deklaracija-required").modal();
    }
    
    
    
  
  
   //------------------------------------------------------------
   //-------------------------------------------------------------------
});

//custom fun
function create_el(el){return document.createElement(el);}
function ID(id){return document.getElementById(id);}
function app(parent,child){parent.appendChild(child);}
function create_opt(value,text,parent){
     var option=create_el("option");
     option.value=value;
     option.innerHTML=text;
     app(parent,option); 
}
function add_opts(obj,parent){for(var i in obj){create_opt(obj[i],i,parent); }}


/**
 * 
 * @param {Obj} pl_ads_info-> sve cene i stanje na racunu
 * @returns {Pravna_lica}
 * 
 * 
 * 
 * 
 * 
 * 
 */
function Pravna_lica(pl_ads_info){
     var tip_oglasa,id,oglas_payd,dana,cena,stanje,ukupna_cena,errors,oglas_trajanje_dana,
     nema_novca_na_racunu='Nemate dovoljno novca na racunu, da  bih ste uplatili kontaktirajte nas putem telefona ili poruke.<address><abbr title="Phone">Telefon:</abbr> (381) 65 831-5363<br/><abbr title="Phone">Email: </abbr><a href="mailto:info@nadjinekretnine.com">info@nadjinekretnine.com</a></address>';
            
       $("#modal-dopuna-racuna-lunch").click(function(){
        dopuna_racuna();
    });
    $("#preuzmi-vaucer").click(function(){
            create_forms('preuzmi-vaucer',{
                'vaucer':"1000",
                'submit-vaucer':"submit"
            });
    });
    $('#modal-istorija-uplate-lunch').click(function(){
        $("#modal-istorija-uplate").modal();
    })
    function __construct(){
        event_click();
        stanje=pl_ads_info.stanje;
      //  produzi_oglas();
       
    }
 /**
  * 
  * Racuna cenu * dana , jer su sve cene po danu
  */
 function izracunaj_cenu(cena_tip_oglasa,dana){
         if (dana) {
          cena=parseFloat(cena_tip_oglasa);
          var result=parseFloat(cena*dana);
          var to_fixed=result.toFixed(2);
          return to_fixed;
         }
     return false;
 }
 /**
  *proverava da li ima novca na racunu
  * 
  */
 function stanje_na_racunu(ukupna_cena){ 
     if ((stanje-ukupna_cena)<0) {
           return false;
        }
     return true;    
 }

 /**
  * 
  * @returns {undefined}
  * 
  * vraca razliku izmedju dva datuma
  * poziva se kada se izabere datum trajanja oglasa
  */
    this.razlika_dana=function(trajanje,start_date){ 
        if (trajanje) {
     var dash=trajanje.split('/');
    var date2=new Date(dash[1]+"/"+dash[0]+"/"+dash[2]);
    var date1 =start_date || new Date();
    var diffDays = date2.getTime() - date1.getTime(); 
     var razlika=Math.ceil(diffDays/86400000);
         if (razlika) {
            var rec_dana='dana';
            if (razlika.toString().length===1&&razlika.toString().indexOf("1")>-1||razlika.toString()[1]==1) {
                    rec_dana='dan';
                }
                oglas_trajanje_dana=razlika;
               modal_ready(razlika,rec_dana);
            return true;
         }
        return false;
        }
        return false;
    }
    
    
 /*
  * 
  * 
  * kada se odredi datum trajanja oglasa(this.razlika_dana=function(trajanje))
  * poziva se ova function koja ubacuje:
  * -koliko dana traje oglas
  * -cena za odredjeno trajanje oglasa
  * -ako nema novca onda ubacuje text iz variabile nema_novca_na_racunu , i postavlja var errors=true;
  * 
  */
 function modal_ready(razlika,rec_dana){
     $("#standard-nema-dovoljno-na-racunu").text("");
     $("#top-nema-dovoljno-na-racunu").text("");
     $("#premium-nema-dovoljno-na-racunu").text("");
     errors=false;
     
     switch(tip_oglasa){
         case "standard":
            var izracunaj_cenu_za_standard=izracunaj_cenu(pl_ads_info.standard.cena,razlika);
            $("#standard-trajanje-dana").text(razlika+' '+rec_dana);
             $("#standard-cena-dana").text(izracunaj_cenu_za_standard);
                if (!stanje_na_racunu(izracunaj_cenu_za_standard)) {
                     errors=true;
                     $("#standard-nema-dovoljno-na-racunu").html(nema_novca_na_racunu);                    
                }
             break;
         case "top": 
            var izracunaj_cenu_za_top=izracunaj_cenu(pl_ads_info.top.cena,razlika);
            $("#top-trajanje-dana").text(razlika+' '+rec_dana);
             $("#top-cena-dana").text(izracunaj_cenu_za_top);
                if (!stanje_na_racunu(izracunaj_cenu_za_top)) {
                     errors=true;
                     $("#top-nema-dovoljno-na-racunu").html(nema_novca_na_racunu);                    
                }
             break;    
         case "premium": 
            var izracunaj_cenu_za_top=izracunaj_cenu(pl_ads_info.premium.cena,razlika);
            $("#premium-trajanje-dana").text(razlika+' '+rec_dana);
             $("#premium-cena-dana").text(izracunaj_cenu_za_top);
                if (!stanje_na_racunu(izracunaj_cenu_za_top)) {
                     errors=true;
                     $("#premium-nema-dovoljno-na-racunu").html(nema_novca_na_racunu);                    
                }
             break;            
     }
     
     
 }
    
    /**
     * 
     * @returns {undefined}
     * 
     * aktivira event click za:
     * -dugme za aktivaciju modala
     * -potvrdi transakciju (na modalu)
     * 
     */
    function event_click(){
        $(".activate-ads-pl").click(function(){ 
            oglas_trajanje_dana=null;
            errors=false;            
            tip_oglasa=$(this).data('tip-oglasa');
          
            id=$(this).data('oglas');
            lunch_modals($(this).data());
         });
         
        $(".pl-potvrdi-transakciju").click(function(e){
            if (id&& oglas_trajanje_dana&&!errors) {
               create_forms('pl-racun',{'oglas_id':id,dana: oglas_trajanje_dana,'tip_oglasa':tip_oglasa,'pravna-lica':'pl'});
                return true;
            }
            e.preventDefault();   
            if (errors) {
                 alert('Nemate dovoljno novca na racunu, da izvrsite ovu transakciju, molimo vas dopunite racun!');
            }else{
                alert('Izaberite datum trajanja oglasa.')
            }
            return false;
       });
       $(".iskljuci-payd-ads").click(function(e){
           e.preventDefault();  
           $("#potvrdi-iskljucivanje-oglasa").data('id',$(this).data('oglas'))
           $("#modal-iskljuci-payd-ads").modal();
       });
       $("#potvrdi-iskljucivanje-oglasa").click(function(e){
           e.preventDefault(); 
          create_forms('pl-iskljuci-oglas',{'oglas_id':$(this).data('id'),'pl-iskljuci-oglas':'iskljuci'});
       });       
       $(".pl-osvezi-oglas").click(function(){
           $("#potvrdi-osvezi-ad").data('oglas_id',$(this).data('oglas'))
          $("#modal-osvezi-payd-ads").modal(); 
       });
       $("#potvrdi-osvezi-ad").click(function(){
          create_forms('pl-osvezi-oglas',{'oglas_id': $(this).data('oglas_id'),'pl-osvezi-oglas':'osvezi'});
       });       
    }
    
    /**
     * 
     * @param {type} data
     * @returns {undefined}
     * 
     * Izbacuje modale
     */
    function lunch_modals(data){
       clean_modals();
        var fixed_stanje=parseFloat(stanje).toFixed(2);
        switch(tip_oglasa){
           case "standard": 
                 $("#modul-pl-standard-stanje-na-racunu").text(fixed_stanje+" rsd");
                    $("#modal-standard-pl").modal();
               
               break;
           case "top":
               $("#modul-pl-top-stanje-na-racunu").text(fixed_stanje+" rsd");
               $("#modal-top-pl").modal();
                    break;
                case "premium":
                    $("#modul-pl-premium-stanje-na-racunu").text(fixed_stanje+" rsd");
                   $("#modal-premium-pl").modal();
                    break;
         }
      }
    
    
    function clean_modals(){
        $('.datepicker-oglas-pl-do').datepicker('setDate', null);
        
          $("#standard-nema-dovoljno-na-racunu").text("");
          $("#standard-cena-dana").text('0');
          $("#standard-trajanje-dana").text("");
          
          $("#top-nema-dovoljno-na-racunu").text("");
          $("#top-cena-dana").text('0');
          $("#top-trajanje-dana").text("");     
          
          $("#premium-nema-dovoljno-na-racunu").text("");
          $("#premium-cena-dana").text('0');
          $("#premium-trajanje-dana").text("");             
    }
    
    
    
    
    
    /*
     * 
     * @returns {undefined}
     * 
     * 
     * TRENUTNO ZA VAUCER!!!!!!!!!!!!
     */
    function dopuna_racuna(){
       $("#modal-dopuna-racuna").modal(); 
        
        
        
        
    }
    
    
    __construct();
}





/**
 * 
 * @returns {undefined}
 * 
 * 
 * 
 * 
 * 
 * OVA CLASS SLUZI SAMO ZA produzi oglas dugme
 */
 
    function Produzi_oglas(ads_info){
       var data,razlika_dana,stanje=ads_info.stanje,
               cena_standard=ads_info.standard.cena,
               cena_top=ads_info.top.cena,
               cena_premium=ads_info.premium.cena,
               selektovan_datum_isteka,
               dana,
               alert_nema_novca=false;
       
       
       
   $("#modul-pl-standard-stanje-na-racunu-produzi").text(stanje);
      $("#modul-pl-top-stanje-na-racunu-produzi").text(stanje);
      $("#modul-pl-premium-stanje-na-racunu-produzi").text(stanje);
        
        
         
         
        
        $(".produzi_oglas").click(function(){
            $( "#standardni-oglas-produzi" ).datepicker( "destroy" );
            $( "#top-oglas-produzi" ).datepicker( "destroy" );
            data=$(this).data();
           // console.log(data)
            //za standardni
        if (data.vrsta_oglasa==1) {
            $("#datum-isteka-standardni").text(data.datum_isteka);
            $("#modal-standard-produzi").modal();
            $( "#standardni-oglas-produzi" ).datepicker({
                        dateFormat:"dd/mm/yy",
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 3,
                        minDate:data.datumrow,
                        onClose:function(date){
                            selektovan_datum_isteka=date;
                            dana= razlika_dana(date);
                           $('#standard-nema-dovoljno-na-racunu-produzi').text('');
                           if (dana) {
                               var cena=izracunaj_cenu(cena_standard,dana);
                               if (cena) {
                                   alert_nema_novca=true;
                            $("#standard-cena-dana-produzi").text(cena);
                               $("#standard-trajanje-dana-produzi").text(dana+" "+rec_dana(dana))
                        }else{
                            alert_nema_novca=false;
                        $('#standard-nema-dovoljno-na-racunu-produzi').text('Nemate dovoljno novca na racunu!')
                        }  }
                        }}).keydown(function(e){
                        e.preventDefault(); });
        }     
            //---------------------------------------------------------
            else if(data.vrsta_oglasa==2){
             $("#datum-isteka-top").text(data.datum_isteka)
            $("#modal-top-produzi").modal();
            $( "#top-oglas-produzi" ).datepicker({
                        dateFormat:"dd/mm/yy",
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 3,
                        minDate:data.datumrow,
                        onClose:function(date){
                            selektovan_datum_isteka=date;
                            dana= razlika_dana(date);
                           $('#top-nema-dovoljno-na-racunu-produzi').text('');
                           if (dana) {
                               var cena=izracunaj_cenu(cena_top,dana);
                               if (cena) {
                                   alert_nema_novca=true;
                            $("#top-cena-dana-produzi").text(cena);
                                 $("#top-trajanje-dana-produzi").text(dana+" "+rec_dana(dana));
                        }else{
                            alert_nema_novca=false;
                         $('#top-nema-dovoljno-na-racunu-produzi').text('Nemate dovoljno novca na racunu!')
                        }
                    }
                        }}).keydown(function(e){
                        e.preventDefault(); });               
                
            }
            else if(data.vrsta_oglasa==3){
             $("#datum-isteka-premium").text(data.datum_isteka)
            $("#modal-premium-produzi").modal();
            $( "#premium-oglas-produzi" ).datepicker({
                        dateFormat:"dd/mm/yy",
                        defaultDate: "+1w",
                        changeMonth: true,
                        numberOfMonths: 3,
                        minDate:data.datumrow,
                        onClose:function(date){
                            selektovan_datum_isteka=date;
                            dana= razlika_dana(date);
                            $('#premium-nema-dovoljno-na-racunu-produzi').text('')
                           if (dana) {
                               var cena=izracunaj_cenu(cena_premium,dana);
                               if (cena) {
                                   alert_nema_novca=true;
                              $("#premium-cena-dana-produzi").text(cena);
                                 $("#premium-trajanje-dana-produzi").text(dana+" "+rec_dana(dana))
                        }else{
                            alert_nema_novca=false;
                                $('#premium-nema-dovoljno-na-racunu-produzi').text('Nemate dovoljno novca na racunu!')
                           }
                             
                           }
                        }}).keydown(function(e){
                        e.preventDefault(); });               
                
            }            
            
            
            
        });
        
         
         
         $(".oglas-potvrdi-produzi").click(function(){
             if (!alert_nema_novca) {
            alert("Nemate dovoljno novca na racunu!");
            return false;
        }
            create_forms('produzi_oglas',{
                'id':data.oglas,
                 datum:selektovan_datum_isteka,
                 'oglas-potvrdi-produzi':'produzi',
                 dana:dana,
                 
                
            }); 
         });
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         function rec_dana(razlika){
               var rec_dana='dana';
            if (razlika.toString().length===1&&razlika.toString().indexOf("1")>-1||razlika.toString()[1]==1) {
                    rec_dana='dan';
                }
                return rec_dana;
         }
        
         
         
         
         
       /**
  * 
  * Racuna cenu * dana , jer su sve cene po danu
  */
 function izracunaj_cenu(cena_tip_oglasa,dana){
         if (dana) {
             var cena=parseFloat(cena_tip_oglasa),
                  result=parseFloat(cena*dana),
                  to_fixed=result.toFixed(2);
             if (stanje_na_racunu(to_fixed)) {
                return to_fixed;
             }
         
         }
     return false;
 }
 /**
  *proverava da li ima novca na racunu
  * 
  */
 function stanje_na_racunu(ukupna_cena){ 
     if ((stanje-ukupna_cena)<0) {
           return false;
        }
     return true;    
 }
   
         
      function razlika_dana(trajanje){
        if (trajanje) {
            var dash=trajanje.split('/'),
              date2=new Date(dash[1]+"/"+dash[0]+"/"+dash[2]),
              dash1=data.datum_isteka_row.split("/"),
              date1 = new Date(dash1[1]+"/"+dash1[0]+"/"+dash1[2]),
              diffDays = date2.getTime() - date1.getTime(), 
              razlika=Math.ceil(diffDays/86400000);
           if (razlika) { return razlika; }
           return false;
        }
        return false;
      }    
        
        
        
        
        
    }
    
    
    
    
    



































//redirekcija na site uplatnica.rs
function uplatnica_rs(){
    var myWindow = window.open("http://uplatnica.rs/", "", "width=980, height=600");
}


/**
 * 
 * @returns {undefined}
 * 
 * 
 * 
 * startuje modale za fizicka lica
 */
function Lunch_modals(){
   var tip_oglasa,state,id,oglas_payd;osvezi_oglasi();
    
    function __construct(){
        event_click();iskljuci_placen_oglas();
    }
    
    
    function event_click(){
          $(".activate-ads").click(function(){
        //console.log(this)
      
      var tip=$(this).data();
          //  console.log(tip)
       switch(tip.tip){
           case "standard":
               tip_oglasa='standard';
               oglas_payd=tip.adsPayed;
             //  state=tip.state;
              //      console.log('standard')
               break;
           case "top":
               tip_oglasa='top';
                   // console.log('top');
                    break;
                case "premium":
                    tip_oglasa='premium';
                //    console.log('premium')
                    break;
       }
       state=tip.state;
      id=tip.oglas;
       modals(); 
  });

}
      
  
    
    function modals(){
        switch(tip_oglasa){
            case "standard": 
                if (oglas_payd==true) {
                    $('#modal-standard').modal('show');
                }else{ 
                switch(state){
                    case 'off':  
                            if (typeof CHECK_ACTIVE_ADS!='undefined'&&CHECK_ACTIVE_ADS<1) {
                                create_forms('oglas-standardni',{'standard-id':id,'state':state});
                            }else{
                               $("#modal-standard-activate").modal();$("#standard-id-oglasa").text(id);
                            } 
                            
                            break;
                    case 'on': $('#modal-standard-off').modal('show');
                     break;    
                }
                //    create_forms('oglas-standardni',{'standard-id':id,'state':state});
                }
                break;
            case "top":
                switch(state){
                    case 'off': $('#modal-top').modal('show');$("#top-id-oglasa").text(id);break;
                    case 'on': $('#modal-off').modal('show');
                       // $("#potvrdi-iskljucivanje-oglasa").data("id-oglasa",id)
                        
                        break;    
                }
               // 
                break;
            case "premium":
                switch(state){
                    case 'off':$('#modal-premium').modal('show');$("#premium-id-oglasa").text(id);break;
                    case 'on': $('#modal-off').modal('show');break;    
                }
                break;
        }
     }
    
    function iskljuci_placen_oglas(){
        $("#potvrdi-iskljucivanje-oglasa").click(function(){
           create_forms('oglas-standardni',{'oglas_id':id,'iskljuci-placeni-oglas':state,'tip-oglasa':tip_oglasa});
         });
            $("#potvrdi-iskljucivanje-standardnog-oglasa").click(function(){
             create_forms('oglas-standardni',{'standard-id':id,'state':state});
         })   ; 
    }
    
    function osvezi_oglasi(){
        $(".osvezi-oglas").click(function(){
            var osvezi_id=$(this).data().oglas;
            $("#oo-standard-id-oglasa,#oo-top-id-oglasa,#oo-premium-id-oglasa").text(osvezi_id);
            $("#modal-osvezi-oglas").modal('show')
            
            
        })
    }
    
    __construct();
}



function create_forms(name,fields){
      var form = $("<form/>",  { action:'',method:'post',name:name } );
      for(var i in fields){
           form.append($("<input/>",{ type:'hidden',value:fields[i],name:i} ));
      }
   
         $('body').append(form);
        $("form[name='"+name+"']").submit(); 
    
    
    
}











function animate_moj_profil(){
    var img;
    $(".thumbnails .span3").hover(function(){
        var th=$(this).children();
          img=$(th).children().first();
        $(img).addClass('animated5 pulse');
    },function(){
        $(img).removeClass('animated5 pulse');
    });
  
  var random=Math.floor(Math.random() * 3) + 1;
  setTimeout(function(){
  $(".thumbnails .span3").each(function(i,e){
        if (random==i) {
            $(e).addClass('animated tada');
            return false;
        }
  })     
  },1000)
 
  
  
}




/**
 * 
 * @returns {undefined}
 * 
 * FILTERI
 */
function Form_call(){
   
    (function kategorija(){
        $("#select_kategorija").change(function(){
           $("#mesta_srbija").select2('val',[]);
            $("#deo_mesta").select2('val',[]);
            $("#select_lokacija").select2('val',[]);
            $('.sub').iCheck('uncheck');
            $("#filteri-form").trigger('click');
            
        })
     })();
    (function mesto(){
         $("#mesta_srbija").change(function(){
             $("#deo_mesta").select2('val',[]);
             $("#select_lokacija").select2('val',[])
             $("#filteri-form").trigger('click');
        })      
    })();   
    
    (function deo_mesta(){
         $("#deo_mesta").change(function(){
            $("#filteri-form").trigger('click');
        })      
    })();    
     (function lokacija(){
         $("#select_lokacija").change(function(){
            $("#filteri-form").trigger('click');
        })      
    })();     
     (function active(){
    $('.sub').on('ifChecked', function(event){
    $("#filteri-form").trigger('click');
    });
    
      $('.sub').on('ifUnchecked', function(event){
        $("#filteri-form").trigger('click');
    
    });  
    })();   
    
    (function submit_button(){
        $(".submit-form-filter").click(function(e){
            e.preventDefault(); 
          
             $("#filteri-form").trigger('click');
        })
        
    })();
    
    
    
}








function Oglasi_change(){
    
    function __construct(){
        /*active();*/delete_ad();
    }
    
    
    function delete_ad(){
        $(".delete-ad").click(function(){
          var id=$(this).data('oglas');
	$('#modal-delete-ad').modal()
 $("#modal-delete-ad-definitivno").click(function(){
     var form = $("<form/>",  { action:'',method:'post',name:'oglasi-delete-ad' } );
        form.append($("<input/>",{ type:'text',style:'width:65%',value:id,name:"delete-ad"} ));
         $('body').append(form);
        $("form[name='oglasi-delete-ad']").submit();    
      
 })
     
         })   
    }
    
    
    
    
    
    
    __construct();
}






 




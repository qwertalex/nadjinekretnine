
       
 $(document).ready(function() {
     uplodify_call(new Sortable_manipulation()); 
       
    });
       
  


 
      
        function Sortable_manipulation(){
           
          
            /**
             * 
             * ovaj metod ubacuje uplodovane slike u 
             * listu sa drag and drop
             * 
             */
            this.set_img=function(dir,img,update){
              var image=this.create_el('img');
              image.src=dir+img;
              this.add_tolist(image);
             if (!update) {this.hidden_fields_add(img);}
          }
            /**
             * 
             * pravi elemente i ubacuje ih u parent element (#sortable)
             * ovaj metod se poziva u metodu this.set_img(dir,img)
             * 
             */
            this.add_tolist=function(img){
                var that=this;
                var parent=document.getElementById('sortable');
                var li=this.create_el('li');
                li.className="ui-state-default";
                
                var delete_img=this.create_el('img');
                delete_img.src="/img/tmp/cancel.png";
                delete_img.className='delete_image';
                var a=this.create_el('a');
                a.href="#";
                a.onclick=function(e){
                    e.preventDefault();
                    var el=$(this).parent()[0];
                      that.delete_img(el);
            
                  }
                a.appendChild(delete_img);
                
                
             
                li.appendChild(img);
                 li.appendChild(a);
                 li.appendChild(this.glavna_slika());
                 parent.appendChild(li);
                
    };
    
    
    /**
     * 
     * pravi elemente span ili a u zavisnosti da li je 
     * slika glavna ili ne!
     * 
     * 
     */
    this.postavi_glavnu=function(img){
        var that=this;
        if (img=='glavna') {
        var span=this.create_el("span");       
    span.style.color='red';
    span.innerHTML="Glavna slika";
    return span;
}
else{
        var a=this.create_el("a");
    a.href="#";
    a.innerHTML='Postavite kao glavnu';
    a.className="postavi_kao_glavnu";
   a.onclick=function(e){
       e.preventDefault();
       var el=$(this).parent()[0];
       $('#sortable').prepend(el)
       that.reset_glavna_slika();
       
       
    }
    return a;
        }
    }
    
    
    
    /*
     * 
     * postavlja glavnu sliku ako 
     * nema ni jednog elementa u parent postavlja
     * prvi da bude glavna slika
     * 
     * 
     */
    
    this.glavna_slika=function(){
        var len=$("#sortable").children().length;
        if (len==0) {
 return this.postavi_glavnu('glavna')
}
    return this.postavi_glavnu();
        
    }
    
    
    
    
    
    /**
     * 
     * kada dodje do promene prilikom drag and drop ili 
     * kada se klikne postavi kao glavnu
     * aktivira se ovaj method tako sto 
     * resetuje sve slike i prvu oznacava kao glavnu
     * 
     * 
     */
    
    this.reset_glavna_slika=function(){
        if ($("#sortable").children().length==0) {
    return false;
}
        var that=this;
        var first=$("#sortable").children(":first")[0];
        
        $("#sortable").children().each(function(i,e){    
            if (i==0) {
               if ($(e).children('a.postavi_kao_glavnu').length) {
     $(e).children('a').remove('.postavi_kao_glavnu');
     $(e).append(that.postavi_glavnu('glavna'));
}
}
else{
    if ($(e).children('span').length) {
     $(e).children('span').remove();
     $(e).append(that.postavi_glavnu());
}
 
        }
  });
        this.hidden_fields_reset();
        
    }
    
    
    /**
     * 
     * brise sliku 
     * salje ajax i brise sliku
     * i element u listi #sortable koji predstavlja sliku
     * 
     * 
     */
    this.delete_img=function(el){
       var that=this;
       var img=$(el).children('img')[0];
       var src=img.src;
 
       var name=src.slice(src.lastIndexOf("/")+1);
       
     $(el).slideUp(400, function(){
          that.hidden_fields_delete(name)
          $(el).remove(); 
       that.reset_glavna_slika()
    });
  /* 
$.post( "http://nadjinekretnine.com/ostavi-oglas/ajax-delete-img", 
{ name: name })
  .done(function( data ) {
  if (data=='ok') {
   
}
else if(data=='false'){
    alert("Doslo je do greske, pokusajte ponovo!")
        }
  });
     **/   
        
        
    };
    
    
    
    
    /**
     * vraca array sa id od input polja 
     * 
     * 
     * 
     */
    
    this.hidden_fields=function(){
        var range=[];
        for(var i=1;i<=10;i++){range.push("#slika_"+i);}
        return range;
       };
    
    
    /**
     * ubacuje value u prvo input polje kojem je value 
     * prazan; value je ime slike
     * 
     * 
     * 
     */
    this.hidden_fields_add=function(img){
        var fields=this.hidden_fields();
              for(var x=0;x<fields.length;x++){
     if ($(fields[x]).val()==="") {
    $(fields[x]).val(img);
    return true;
}       
    }
        return false;
    };
    
    
    
    /**
     * 
     * brise value iz hidden inputa
     * 
     * 
     * 
     */
    this.hidden_fields_delete=function(name){ 
        
        var fields=this.hidden_fields();
        for(var i=0;i<fields.length;i++){
           
         if ($(fields[i]).val().indexOf(name)) {
            $(fields[i]).val("");
             }   
        }
       
    };
    
    
    /**
     * 
     * resetuje redosled u hidden fields
     * koji moze da se poremeti sa drag and drop,
     * linkom,delete img;
     * 
     * 
     * 
     * 
     */
    this.hidden_fields_reset=function(){
         var fields=this.hidden_fields();
         for(var x=0;x<fields.length;x++){
         $(fields[x]).val("")    
        }
        $("#sortable").children().each(function(i,e){ 
        var first=$(e).children(":first")[0];
        var src=first.src;
        var lok=src.split("/");
           // console.log(lok)
        var name=lok[lok.length-2]+"/"+lok[lok.length-1];
           // console.log(name)
        $("#slika_"+(i+1)).val(name);
        
    });
     };
    /**
             * 
             * 
             * pravi element i vraca ga
             * 
             */
          this.create_el=function(el){
              var el=document.createElement(el);
              return el;
    }
            
            
            
            
            
    }
   
       
       
       



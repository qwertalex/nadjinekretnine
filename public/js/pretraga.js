$(document).ready(function() {
   
   //resize map on page load or resize eveent
     var map_size= new Map_auto_size();
     map_size.resize();
   //---------------------------------------
   //load google map
     google.maps.event.addDomListener(window, 'load', initialize);
    //--------------------------------------
    //limit lline in title and opis; ovde najvise 2 reda
    var newlines=new New_lines();
    newlines.newline_size('.title-size',2)
    //--------------------------------------
    
    //bootstrap  menu fix
     $('.dropdown-menu').click(function (e) {
        e.stopPropagation();
        });
      //--------------------------------------  
        
     
        
     //icheck   
     $('form[name="filteri"] input').iCheck({
         checkboxClass: 'icheckbox_minimal',
         radioClass: 'iradio_minimal',
         labelHover:true,
         cursor: true
     });
     //icheck in modal
     $('#modal-dodatno').on('shown', function () {
         $('form[name="dodatni-filteri"] input').iCheck({
             checkboxClass: 'icheckbox_minimal',
             radioClass: 'iradio_minimal',
             labelHover:true,
             cursor: true
         });
     });
     //--------------------------------------------------   
       
       
       
       
      //hide list in modul dodatno  
     $("#grejanje-toogle").css('display','none')  
     $("#dodatni-kriterijumi-toogle").css('display','none')  
     $("#spratovi-toogle").css('display','none')
     $("#dodatno-toogle").css('display','none')
     $("#opis-objekta-toogle").css('display','none')
     $("#vrste-kuca-toogle").css('display','none')
     $("#vrsta-plac-toogle").css('display','none')
    //--------------------------------------       
       
       //show hide dodatne opcije u modal dodatno
      var dodatno_class= new show_hide_dodatno_options_CLASS();
      dodatno_class.event_click();
      //-------------------------------------- 
      //regulise width and height modal dodatno
       modal_position_dodatno();
   //--------------------------------------


      //mobile menu param map_size object
       mobile_menu_toogle(map_size)
     //--------------------------------------
        
     
  //select za sortiranje
   $("#sortiranje").select2({
        data:[
            {id:1,text:'Najnoviji oglasi'},
            {id:2,text:'Najstariji oglasi'},
            {id:3,text:'Najniza cena'},
            {id:4,text:'Najvisa cena'}
        ]   ,
        width:250,
   }).select2('val', ["1"]);    
    //--------------------------------------   
       
     
    
    
    
    //sakriva oglas kada se klikne na hide oglas
    hide_oglas()
    //--------------------------------------
    
    //kada se u filter menu klikne na prodaja ili izdavanje
    // samo checkira checkbox  samo vizuelno
    var style_filter_js=new Filter_style_CLASS();
    style_filter_js.izdavanje_prodaja();
    //--------------------------------------
    
    
    
   var filter_cena=new Filter_cena();
 filter_cena.fill_cena_sugestions();
    
    
    
    new Kvadratura_filter();
 
    
    
    
    
   new Broj_soba_filter()
    
    
    
    
    
    
    
    
    
    
    
    
});
/*
 * 
 * End document.ready()
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */




/**
 * 
 * @param {object} map_size
 * @returns {void}
 * 
 * show hide mobile menu
 * 
 */
     function mobile_menu_toogle(map_size){
         $("#nav-mobile-map").click(function(e){
             e.preventDefault();
             var width=parseInt($('#menu-nav-map').css('width'));
             if (width<=0) {
                 $('#wrapper').animate({width:'80%'},350,function(){
                     map_size.resize(); 
                     $("#nav-mobile-img").attr('src','/img/icons/cross@2x.png');
                 });
                 $('#menu-nav-map').animate({width:'20%'},350)
                     return true;
             }
             $('#wrapper').animate({width:'100%'},350,function(){
                 map_size.resize(); 
                 $("#nav-mobile-img").attr('src','/img/btn-nav.png')
             })
             $('#menu-nav-map').animate({width:'0%'},350)
    
         });
     }
//---------------------------------------------------------------



/**
 * 
 * @returns {void}
 * 
 * namesta position modal u zavisnosti od velicine ekrana
 * 
 */
     function modal_position_dodatno(){
         var heig=$('body').outerHeight(true)-100;
         $("#modal-dodatno").on('show',function(){
             $("#modal-body").css({
                 'max-height':800,
                  height:heig,
             });
         }).css(
           {
             'max-height':800,
              height:heig,
             'margin-left': function () {
                return -($(this).width() / 2);
             }
          })
      
         $("#modal-body").height(+$("#modal-dodatno").height())
     }
//---------------------------------------------------------------






/**
 * 
 * @returns {void}
 * 
 * radi u modal dodatno(filteri) 
 * radi toogle za vise opcija ili manje opcija
 * 
 * 
 */
function show_hide_dodatno_options_CLASS(){
   
     this.event_click=function(){
         var that=this;
         $(".manje-vise-opcija").click(function(){ 
             that.toogle($(this).data('dodatno'),this);
         });  
     };
 
     this.toogle=function(data,el){
          var toogle=data+"-toogle";
          var that=this;
          var display=$("#"+toogle).css('display');
          $( "#"+toogle ).toggle( 400, function() {
              that.toogle_text(display,el);
          });
     } ; 
        
     this.toogle_text=function(display,el){
           if (display==='none') {
               $(el).text('Manje opcija ▲');
            }
           else if(display==='block'){
              $(el).text('Vise opcija ▼');
           }  
     } 
   
};























/**
 * 
 * @returns {Map_auto_size}
 * 
 * 
 * Opis:
 * resize width and heigth za #map-canvas i #svi-oglasi-mapa
 * 
 * #svi-oglasi-mapa ima fixni width(600px) a mapa ima ostalo
 * sto se izracunava u procentima
 * 
 * heigth se izracunava automatski 
 * 
 * koristi jquery $(window).resize event 
 * --------------------------------------------------------------
 * 
 * 
 */
function Map_auto_size(){
  
     //construct
    this.resize=function(){
         this.width();
         this.heigth();
         this.resize_event();
    }
     //------------------
    
    this.heigth=function(){
         var container = ($(".breadcrumb-wrapper").outerHeight(true) / $("body").height()) * 100;
         var filter= ($("#filter").outerHeight(true) / $("body").height()) * 100;
         var per=100-container-filter;
         $("#map-canvas").css('height',per+"%");
         $("#svi-oglasi-mapa").css('height',per+"%");
          
    }
    
    
    
    this.width=function(){
         var svi_oglasi=$("#svi-oglasi-mapa").width();
         var svi_oglasi_percent= (svi_oglasi / $('#map-con').width()) * 100;
         var map_can=100-svi_oglasi_percent;
         $("#map-canvas").css('width',map_can+"%");
         var mm=parseInt(($("#map-canvas").width() / $('#map-con').width()) * 100);
      
    }
    
    
    this.resize_event=function(){
        var that=this;
        $( window ).resize(function() {
            that.heigth();
            that.width();
            modal_position_dodatno()
        });
    }
    
    
    
    
    
}


















 var map;
function initialize() {
  var mapOptions = {
    zoom:14,
    center: new google.maps.LatLng(44.821583, 20.457372),
    //scrollwheel: false,
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
  
  




/*
 * 
 * @type google.maps.LatLng
 * 
 * 
 * kada kliknes na marker onda izadje text
 * 
 * 
 */
var position=new google.maps.LatLng(44.743855, 20.506252)
var marker=new google.maps.Marker({
    position:position,
    map:map
})


var infowindow=new google.maps.InfoWindow({
    content:"this is some text potok 14",
    size:new google.maps.Size(100,100)
})
google.maps.event.addListener(marker,'click',function(){
    infowindow.open(map,marker)
})



  /* 

  google.maps.event.addListener(map, 'center_changed', function() {
    // 3 seconds after the center of the map has changed, pan back to the
    // marker.
 window.setTimeout(function() {
      map.panTo(marker.getPosition());
    }, 3000);
    
  });
*/




/*
google.maps.event.addListener(map,'click',function(e){
    placeMarker(e.latLng)
})
 
 
 
 function placeMarker(location){
     var marker =new google.maps.Marker({
         position:location,
         map:map
     });
 }
  */
  
  
  
  google.maps.event.addDomListener(window, "resize", function() {
 var center = map.getCenter();
 google.maps.event.trigger(map, "resize");
 map.setCenter(center); 
});
  
  
}



/**
 * 
 * @returns {New_lines}
 * 
 * ogranicava linije text naprimer ako text u divu ima
 * pet linija a ja bih da ima max dve linije ondat pozivam ovo
 * koristi se za title i opis u listi oglasa
 * PRIMER:
 * -------
 *     //limit lline in title and opis; ovde najvise 2 reda
 *      var newlines=new New_lines();
 *      newlines.newline_size('.title-size',2)
 * 
 */
function New_lines(){
    this.newline_size=function (el,count) {
         var that=this;
         if (el.indexOf(".")==-1) {
            throw  new Error('samo jquery class je dozvoljeno (.imeclasse)')
        }
         $(el).each(function(i,e){
             var lines= that.count_newlines(e);
            
             if (lines>count) {
                 var text=$(e).text();
                 while(lines>count){
                     text = text.slice(0, - 1);
                     $(e).text(text);
                     lines=that.count_newlines(e);
                 }
                 text = text.slice(0, -4);
                 $(e).text(text+'...');
             }
         });
     }
    //private
    this.count_newlines=function(el){
         var divheight = $(el).height(); 
         var lineheight = $(el).css('line-height').replace("px","");
         return Math.round(divheight/parseInt(lineheight)); 
} 
}
//-------------------------------------------------------



/**
 * 
 * @returns {undefined}
 * 
 */
function Filter_style_CLASS(){
   
    this.all_ids={
                "#izdavanje-all":[
            '#izdavanje-agencija',
            '#izdavanje-vlasnik'
        ],
               "#prodaja-all":[
            '#prodaja-agencija',
            '#prodaja-vlasnik',
            '#prodaja-banka',
            '#prodaja-investitor'
        ] 
    }
    
     this.izdavanje_prodaja=function(){
         var that=this, 
         main_id=['#izdavanje-all','#prodaja-all'],
         ids=[],
         num=0;
         
         for(var main=0;main<main_id.length;main++){
             $(main_id[main]).on('ifClicked', function(){
                 switch(this.id){
                     case 'izdavanje-all':ids=that.all_ids['#izdavanje-all'];
                         break;
                     case 'prodaja-all':ids=that.all_ids['#prodaja-all'];
                         break;
                 }
                 
                 if (!that.get_events(this,'ifChecked')) {
                     $(this).on('ifChecked', function(){
                         for(var i=0;i<ids.length;i++){
                             $(ids[i]).iCheck('check');             
                         } 
                     })
                 }
            
                if (!that.get_events(this,'ifUnchecked')) {
                     $(this).on('ifUnchecked', function(){
                         for(var i=0;i<ids.length;i++){
                             $(ids[i]).iCheck('uncheck');             
                         }
                     })                 
                 };
             }) ;
     
             var ids=that.all_ids[main_id[main]];
          
             for(var i=0;i<ids.length;i++){
                 var parent;   
                 $(ids[i]).on('ifChecked', function(){
                     if (this.id.indexOf('prodaja-')>-1) {
                         parent='#prodaja-all';
                     }   
                     else{parent='#izdavanje-all';}
                     $(parent).unbind('ifChecked');
                     $(parent).iCheck('check');  
                     num++;
                 });
    
                $(ids[i]).on('ifUnchecked', function(){
                     num--;
                     if (num===0) {
                         $(parent).iCheck('uncheck');    
                     }
                 });     
             }
         }   
     }
    
    
    
    
    
    this.get_events=function(el,event){
         var events=jQuery._data(el, "events" );
         for(var key in events){if (key===event) {return true; } }
         return false;
    }
    
    
}









/**
 * 
 * @returns {undefined}
 * hide jedan oglas kada se klikne na sliku hide
 */
function hide_oglas(){
    $('.hide-oglas').click(function(e){
        e.preventDefault();
       var parent=$(this).parents()[3];
        $(parent).hide(300);
     });
 }




/**
 * 
 * @returns {Filter_cena}
 * OPIS:
 * -----
 * Sluzi za range cena, sa sugesijama i dinamicki menja cene u navigacijonom meniju.
 * Prvo uzima GET parametre da bih se znalo da li se radi o prodaji ili izdavanju
 * i drugi param da bh znalo da li je u pretrazi vec ukljucen filter cena_do(ako jeste onda ga ispisuje u nav menu)
 * 
 * Ima vec sugestije za input min ispisane u objectu this.cene;
 * sugestije za input max rade dinamicki(za detalje pogledati functions: this.append_list_all,this.apppend_li_min,this.apppend_li_max)
 * 
 * 
 * 
 * 
 * 
 */


function Filter_cena(){
    
    /**
     * 
     * @returns {Boolean|Filter_cena.cene.izdavanje|Filter_cena.cene.prodaja}
     * u zavistosti da li je prodaja ili izdavanje ili u izgradnji vraca 
     * array this.cene
     */
    this.url_params=function(){
             var pro_izd=parseInt(getUrlVars()['prodaja_izdavanje']);
              var cena_arr;
             switch(pro_izd){
                 case 1:cena_arr=this.cene.prodaja;
                     break;
                 case 2:cena_arr=this.cene.izdavanje;
                     break;
                 case 3:cena_arr=this.cene.izdavanje;
                     break;
                 default:return false;
             }
             return cena_arr;
    }
    
    
    //default cene za min input
    this.cene={
        'prodaja':{
             'min':['30000','50000','75000','100000','150000','200000','250000','300000','400000','500000']
        },
        izdavanje:{
            min:['100','200','300','400','500','750','1000','1250','1500','1750','2000','2500','3000']
            
        }
    }
    
 /**
  * 
  *1.Formatira input unos na keyup
  *2.Ubacuje text u nav menu
  * 
  * 
  * 
  */
 this.input_events=function(){
     var that=this;
     
      $('input[name="cena-min"]').keyup(function(e){
          $(this).val(that.format_number($(this).val()));
          that.format_nav_text();
       })
       $('input[name="cena-max"]').keyup(function(){
            $(this).val(that.format_number($(this).val()));
            that.format_nav_text();
       })    
       
       


        $('input[name="cena-min"]').keypress(function(e){return  numbersonly(e); });
        $('input[name="cena-max"]').keypress(function(e){return  numbersonly(e);});
     
     
     
       /*
        * http://www.javascriptkit.com/javatutors/javascriptkey3.shtml
        */     
         function numbersonly(e){
             var unicode=e.charCode? e.charCode : e.keyCode
                if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
                if (unicode<48||unicode>57) //if not a number
                return false //disable key press
                }
         }
                
  }
 
 
 
 
 
 
 /**
  * 
  * @param {ing} min
  * @returns {@var;first}
  * 
  * Brise zarez i broja i pertvara ga u string
  * 
  * 
  */
 
    this.remove_comma=function(min){
        min=min.toString();
             while(min.indexOf(",")>-1) {
           var first=min.slice(0,min.indexOf(","));
            min=first+min.slice(min.indexOf(",")+1);
         }
         return min; 
    }
    
    
    /**
     * 
     * @param {int} num
     * @returns {String|@this;@call;remove_comma}
     * 
     * Formatira broj: 1000 u 1,000 || 250000 u 250,000 || 33000000 u 33,000,000
     * 
     */
    this.format_number=function(num){
        num=num.toString();
        num=this.remove_comma(num)
        if (num.length>3) {
            var count=0,str="",return_string="";
            for(var i=num.length-1;i>=0;i--){
               if (count===3) {str+=",";count=0;}
                     str+=num[i];
                      count++;
             }
            for(var i=str.length-1;i>=0;i--){
                 return_string+=str[i]
             }            
             return return_string;
         }
        return num
       }
    
    
    /**
     * 
     * 
     * @returns {Boolean|String}
     * 
     * Formatira broj: 11,000 u 1.1K || 50,000 u 50K za nav menu
     */
    this.format_button_price=function(cena){ 
         cena=this.remove_comma(cena); 
         $('#button-cena').text("")
         if (cena[0]=="0"||cena=="") {return false;}
         if (cena==="Max") {return "max";}
         var str="";
        switch(cena.length){
             case 1:case 2:case 3:str='€'+cena; break;
             case 4:str='€'+cena[0];if (cena[1]!="0") { str+="."+cena[1]+"K";}else{str+="K";} break;
             case 5:str='€'+cena[0]+cena[1]+"K";break;
             case 6:str='€'+cena[0]+cena[1]+cena[2]+"K";break;
             case 7: str='€'+cena[0]; if (cena[1]!="0"){str+=cena[1]+"M";}  else{str+="M";} break;
             case 8:  str='€'+cena[0]+cena[1]+"M";  break;
             case 9: str='€'+cena[0]+cena[1]+cena[2]+"M";  break
         }
         return str;
    }
    
    
    /**
     * 
     * @returns {undefined}
     * 
     * Ubacuje text u nav menu: 20K - 30K
     */
     this.format_nav_text=function(){
             var min=$.trim($('input[name="cena-min"]').val()),            
             max=$.trim($('input[name="cena-max"]').val());
          
          min=this.format_button_price(min);
          max=this.format_button_price(max);
         if (min&&!max&&max!="max") {
             set_(min,"+","")
        }
         
          else if (!min&&max!="max"&&max) {
              set_("","do ",max);
        }        
         else if(min&&max&&max!="max"){
              set_(min," - ",max);
         }
         
         else if (max=='max') {
            if (min) {set_(min,"+","");}
            else set_("","","Cena")
            
        }
         else if(!min&&!max){
             $('#button-cena').text("Cena")
             
         }
          
          
     function set_(min,sep,max){
             $('#button-cena-min').text(min);
             $("#cena-sep").text(sep);
             $('#button-cena-max').text(max);
     }
     
     }
    
    
    
    
    /**
     * 
     * @param {type} id
     * @param {type} cena
     * @returns {undefined}
     * 
     * Ubacuje sugestije u listu ispod inputa(min ili max)
     *  ovu funkiju pozivaju:  this.apppend_li_min,  this.apppend_li_max
     * 
     */
     this.append_list_all=function(id,cena){
             var that=this,text='€'+this.format_number(cena)+"+";
             switch(id){case "#lista-cena-right":text='€'+this.format_number(cena);}
             $(id).append(
                 $('<li>').append(
                     $('<a>').attr({'href':'#','data-cena':cena})
                     .text(text)
                     .click(click_fun)
                 ));                       
          function click_fun(){
                         if (id=='#lista-cena-left') {
                             $('input[name="cena-min"]').val(that.format_number($(this).data('cena')))  ;
                             $(id).hide();
                             that.add_li('#lista-cena-right');
                             $('input[name="cena-max"]').focus();
                              that.format_nav_text()
                             
                             
                         }
                         else if (id=='#lista-cena-right') {
                             $('input[name="cena-max"]').val(that.format_number($(this).data('cena')));
                             $('#cena-dropdown').parent().removeClass('open');
                            that.format_nav_text()
                         }                        
                     }     
     }
    
    
    /**
     * 
     * Ubacuje sugestije u listu min
     * 
     * 
     */
     this.apppend_li_min=function(){
         var id='#lista-cena-left',
         cena_arr=this.url_params(),
         that=this;
         if ( $(id).children().length === 0 ) {
                 $(id).append( $('<li>').append($('<a>')
                     .attr('href','#').text('0')
                     .click(function(){$('input[name="cena-min"]').val('0')
                         $(id).hide();
                         that.add_li('#lista-cena-right',false,'max');
                         $('input[name="cena-max"]').focus();
                         that.format_nav_text('0')
                     })
                 ));   
         }
         for(var i=0;i<cena_arr['min'].length;i++){     
           this.append_list_all(id,cena_arr['min'][i])  
       }
                            
     }
    
    
    
    /**
     * 
     *  Ubacuje sugestije u listu max
     * 
     * 
     */
    this.append_li_max=function(){
         var min=this.remove_comma($('input[name="cena-min"]').val()),
         pro_izd=parseInt(getUrlVars()['prodaja_izdavanje']),
         num,start_num,that=this;
       switch(parseInt(pro_izd)){
           case 1:num=max_prodaja(min);break;
           case 2:num=max_izdavanje(min);break;
       }
       // if (!min) {min=0;}
       start_num=parseInt(num)+(parseInt(min)||0);
        
        for(var i=0;i<10;i++){
           
            this.append_list_all('#lista-cena-right',start_num);
            start_num=start_num+num;
        }
       var id='#lista-cena-right';
                     $(id).append(
                 $('<li>').append(
                     $('<a>').attr({'href':'#'})
                     .text("Max")
                     .click(click_fun)
                 ));                       
          function click_fun(){
                        $('input[name="cena-max"]').val('Max');
                             $('#cena-dropdown').parent().removeClass('open');
                             that.format_nav_text("max")
                         }                        
                        
        
        
        
        function max_izdavanje(str){
            var izd;str=str.toString(); 
             switch(str.length){
                 case 0: case 1:case 2:case 3:izd=100;break;
                 case 4: 
                     switch(str[0]){
                      case '1':izd=250;
                      case '2':izd=250;break;
                      default:izd=500;break;
                  }
                   break;
              default:izd=500; break;
                  
          }
          return izd;
        }
        
                function max_prodaja(str){
            var pro;str=str.toString();
             switch(str.length){
                 case 0:case 1:case 2:case 3:case 4: case 5:pro=20000;break;
                 case 6:
                     switch(str[0]){
                      case '1':pro=25000;break;
                      default:pro=50000;break;
                  }
                   break;
              default:pro=250000; break;
                  
          }
          return pro
        }   
        
    }
    
    
    
    
    /**
     * 
     * @param {type} id
     * @returns {undefined}
     * 
     * 
     * 
     */
     this.add_li=function(id){
         $(id).children().remove();
         $(id).show();
         
         switch(id){
             case '#lista-cena-left':this.apppend_li_min(); break;
             case '#lista-cena-right':this.append_li_max(); break;
         }
     }
    
    
    
    
    /**
     * 
     * PUBLIC METHOD
     * 
     * 
     * 
     * 
     * 
     */
    
    this.fill_cena_sugestions=function(){
        var that=this;
        var cena=parseInt(getUrlVars()['cena_do'])
        if (cena) {$("#button-cena-max").text("do "+this.format_button_min_max(cena))}
        else{$("#button-cena").text("Cena")}
     this.input_events();
        $('#cena-dropdown').click(function(){
         var parent=$(this).parent()
         if (!$(parent).hasClass('open')) { 
                 // set autofocus
                setTimeout(function() { $('input[name="cena-min"]').focus() }, 100);
                 $("#lista-cena-right").hide();
                 //auto focus
                 $('input[name="cena-min"]').focus(function(){
                     that.add_li('#lista-cena-left');
                     $("#lista-cena-right").hide();
                      
                 });
                 that.add_li('#lista-cena-left');
                
                 $('input[name="cena-max"]').focus(function(){
                     that.add_li('#lista-cena-right');
                     $("#lista-cena-left").hide();
                 });
         }
     })  
    
     }
  
  
  
  
  
}






 


function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}





function Kvadratura_filter(){
    
    this.min="input[name='kvadratura-min']";
    this.max="input[name='kvadratura-max']";
    
    this.cena=new Filter_cena();
    
    this._construct=function(){
         this.input_events();
         var that=this;
         $('#kvadratura-dropdown').click(function(){
             var parent=$(this).parent()
             if (!$(parent).hasClass('open')) { 
             setTimeout(function() { $(that.min).focus() }, 100);
             }
         });
    }
    
  
    
    
    this.ajax=function(min,max){
        console.log('Ajax call')
    }
    
    
    
    this.input_events=function(){
         var that=this;
         $("#button-kvadratura").click(function(e){
             e.preventDefault();
             var min=that.cena.remove_comma($(that.min).val());
             var max=that.cena.remove_comma($(that.max).val());
             that.ajax(min,max)
         });
        
     
         $(that.min).keyup(function(e){
             $(this).val(that.cena.format_number($(this).val()));
             that.cena.format_nav_text();
         }) ;
         $(that.max).keyup(function(){
             $(this).val(that.cena.format_number($(this).val()));
             that.cena.format_nav_text();
         }) ;   
       
         $(that.min).keypress(function(e){return  numbersonly(e); });
         $(that.max).keypress(function(e){return  numbersonly(e);});
     
     
     
       /*
        * http://www.javascriptkit.com/javatutors/javascriptkey3.shtml
        */     
         function numbersonly(e){
             var unicode=e.charCode? e.charCode : e.keyCode
                if (unicode!=8){ //if the key isn't the backspace key (which we should allow)
                if (unicode<48||unicode>57) //if not a number
                return false //disable key press
                }
         }
        
    }
   
    
    
    this._construct();
}










function Broj_soba_filter(){
    
    
    
    
    this._construct=function(){
        this.events()
    }
    this.ajax_call=function(data){
        console.log('ajax call')
    }
    
    this.events=function(){
         var that=this
         $('#list-broj-soba li a').click(function(e){
            e.preventDefault();
            $(this).parent().parent().parent().removeClass('open')
            var data=$(this).data('sobe');
            $('#broj-soba-nav').text(data+"+");
            that.ajax_call(data)
            
         }) 
        
        
    }

    
    
    
    
    this._construct();
}





























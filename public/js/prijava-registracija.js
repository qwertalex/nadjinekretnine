 $(document).ready(function() {
 
	$('.tabs a').click(function (e) {
  		e.preventDefault();
  		$(this).tab('show');
                var data=$(this).data('focus');
                $("input[name='"+data+"']").focus();
	});
 
 
     if (getUrlVars()['reg']) {$("input[name='reg_email']").focus(); }
     else $("input[name='login_email']").focus();

    if (getUrlVars()['newpass']) {
        $("#modal-lozinka-zamenjena").modal();
    }
    if (getUrlVars()['zaboravljena_lozinka']) {
         $("#modal-porukajeposlata").modal();
    }
 
 $('#modal-porukajeposlata').on('hidden', function () {
 $("input[name='login_email']").focus();
})
 
 
 

 //js form validation 
new Form_validation();
 
 
 
 
 $("#zaboravljena-lozinka").submit(function(){
     $('#zab-lozinka-error').hide();
        if (validateEmail($("#input-zaboravljena-lozinka").val())) {
            return true;
        }
        $('#zab-lozinka-error').show(100);
        return false;
 })
 
 /*
   $('.uslovi-koriscenja').iCheck({
    checkboxClass: 'icheckbox_square',
    radioClass: 'iradio_square',
    increaseArea: '20%' // optional
  });
 */
 
 
       $('.icheck-line').each(function(){
           if (this.name=='oglasivac_nekretnine') {
            var label_text=$(this).data("oglasivac")
            
            
        }else{
      var label_text= $("label[for='"+this.name+"']").text();
    $("label[for='"+this.name+"']").remove();            
        }

    
 
    $(this).iCheck({
      checkboxClass: 'icheckbox_line-blue add-tooltip',
      radioClass: 'iradio_line-blue',
      insert: '<div class="icheck_line-icon"></div>' + label_text
    });
  });
  
 
   create_username_pop("Korisnicko ime moze da sadrzi karektere a-z,A-Z,0-9 i mora da sadrzi najmanje dva karaktera.",false,true)
 
 $("#reg_username").bind("keyup paste",function(){
       // console.log(this.value);
        if (!this.value.match(/^[0-9a-zA-Z]+$/)&& $.trim(this.value)!="") {
              create_username_pop('<p class="text-error">Dozvoljeni karakteri su slova i brojevi, bez razmaka.</p>',true)
              return false;
         }
        if (this.value.length>1) {
              create_username_pop( ' <p class="text-center"><i class="fa fa-refresh fa-spin"></i></p>',true)
                 
              $.post( "http://nadjinekretnine.com/prijava-registracija/check-username", {username:this.value })
                 .done(function( data ) {
                   
                    create_username_pop(data,true)
                 });            
        }else{
 create_username_pop('<p class="text-error">Korisnicko ime mora da sadrzi najmanje dva karaktera!</p>',true)
  
        }

 });
 
 
 
 
 
 
 
 
 });

function create_username_pop(content,html,show){
      $('#reg_username').popover('destroy');
      $('#reg_username').popover({
     placement:'top',
     trigger:'focus', 
      content:content,
     html:html, 
 });
    if (!show) {
   $('#reg_username').popover('show')      
    }
 
  
}




function Form_validation(){
    var that=this;
    
    this.__construct=function(){
      //   login_submit_event();
         register_submit_event()
    }
    
    //ISKLJUCENO!
     function login_submit_event(){
        
        $("form[name='login'").submit(function(e){return true;
             //e.preventDefault();
             var error=[];
             var e=false,p=false;
             
             var email=$("#input-login-email").val();
             var pass=$("#input-login-password").val();
             if (!validateEmail(email)) {error.push('email');}
             if (pass.length<5) {error.push('pass');}
           
             if (error.length>0) {
               // console.log('ima gresaka');
                for(var i=0;i<error.length;i++){
                     switch(true){
                         case error[i]==='email':e=true;break;
                         case error[i]==='pass':p=true;break;
                     }                   
                }
                show_errors('#error-login-text',e,p,false,false);
                return false;
             }
             return true;
         });
     };
    
    
    
     function register_submit_event(){
        
         $("form[name='registracija_novi_korisnik']").submit(function(e){return true;
             //e.preventDefault();
             var error=[];
             var e=false,p=false,r=false,z=false,u=false;
             var username=$('#reg_username').val();
             var email=$("#reg_email").val();
             var pass=$("#reg_password").val();
             var retupe=$("#reg_retupepass").val();
             var zapamti=$("#reg_uslovi_koriscenja").is(':checked');
             
              if (!username.match(/^[0-9a-zA-Z]+$/)||username.length<1){
                error.push('username') 
              }
             if (!validateEmail(email)) {error.push('email');}
             if (pass.length<5) {error.push('pass');}
             if (pass!==retupe){error.push('retupe');}
             if (!zapamti){error.push('zapamti')}
             if (error.length>0) {
               // console.log('ima gresaka');
                for(var i=0;i<error.length;i++){
                     switch(true){
                         case error[i]==='email':e=true;break;
                         case error[i]==='pass':p=true;break;
                         case error[i]==='retupe':r=true;break;
                         case error[i]==='zapamti':z=true;break;
                         case error[i]==='username':u=true;break;
                     }                   
                }
                show_errors('#error-register-text',u,e,p,r,z);
                return false;
             }
             return true;
         });       
     }

    function show_errors(tabID,username,email,lozinka,potvrdi,uslovi){
        var username_text="Korisnicko ime nije validno!"
         var email_text='Email nije validan.';
         var lozinka_text='Lozinka nije validna.'
        var potvrdi_text='Unesene lozinke se ne podudaraju. Lozinka mora da ima najmanje 5 karaktera.';
        var uslovi_text='Morate prihvatiti uslove koriscenja!'
        $(tabID+" ul").children().remove();
        if (username===true) {
           $(tabID+" ul").append('<li>'+username_text+'</li>'); 
        }
                
        if (email===true) {
           $(tabID+" ul").append('<li>'+email_text+'</li>'); 
        }
        
         if (lozinka===true) {
            $(tabID+" ul").append('<li>'+lozinka_text+'</li>');
        }
               
        if ( potvrdi===true) {
           $(tabID+" ul").append('<li>'+potvrdi_text+'</li>');
        }
               
        if ( uslovi===true) {
           $(tabID+" ul").append('<li>'+uslovi_text+'</li>');
        }        
        $(tabID).show();
           $('html, body').animate({
        scrollTop: $(tabID).offset().top
    }, 200);
    }
    
 
    
    this.__construct();
}










<?php

namespace LoginRegister\Controller;

use Zend\Mvc\Controller\AbstractActionController,
        Zend\View\Model\ViewModel;
//local
use LoginRegister\Models\Login\Login,
        LoginRegister\Models\Register\RegisterNewUser,
        LoginRegister\Models\ZaboravljenaLozinka\ZaboravljenaLozinka,
        LoginRegister\Models\ResetPassword\ZaboravljenaLozinkaForm;

 

class LoginRegisterController extends AbstractActionController {

    
    
    
    
    public function indexAction() {
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
         $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');   
         $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
         $renderer->headLink()->appendStylesheet('/libraries/icheck/blue.css'); 
         $renderer->headLink()->appendStylesheet('/libraries/icheck/line/blue.css'); 
         $renderer->headLink()->appendStylesheet('/libraries/font-awesome-4.1.0/css/font-awesome.min.css');
         $renderer->headLink()->appendStylesheet('/css/bootstrap-social.css');
          $renderer->headScript()->appendFile( '/js/prijava-registracija.js');
         
         //user class
         $User=$this->getServiceLocator()->get('User');
         //login class
         $login=new Login();
         $login_not_valide_message=false;
         //register class
         $register=new RegisterNewUser($User);
         
         //zaboravljena lozinka
         $zaboravljena_lozinka=new ZaboravljenaLozinka();
         //--------------------------------------------------------
         
         $request = $this->getRequest();
         $remoteAddr = $request->getServer('REMOTE_ADDR');
              
           
         
         if ($request->isPost()) {
             $post = $request->getPost()->toArray() ;
            
             //zaboravljena lozinka
             if (isset($post['submit-zaboravljena-lozinka'])) {
                 $zaboravljena_lozinka->setData($post);
                 if ($zaboravljena_lozinka->isValid($post)) {
                     if ($get_data_by_email=$User->get_by_email($post['zaboravljena-lozinka'])) {
                         $User->reset_pass($get_data_by_email['id_users_nk'],$get_data_by_email['email_nk']);
                     }
                 }
             }
             
             
             
              //register
             if (isset($post['reg_submit'])) {
                 $register->setData($post);  
                 if ($register->isValid($post)) {
                        $register->getData($post);
                        $post['user_ip']=$remoteAddr; 
                    $create_user= $User->create_user($post);
                     if ($create_user) {
                     $User->createSession($User->login_validation_email_or_username_pass($post['reg_username'],$post['reg_password']));
                     $this->redirect()->toUrl('http://nadjinekretnine.com/moj-profil');                      
                    }
   
                 }
             }
             
  
            //login
             if (isset($post['login_submit'])) {
                 $login->setData($post);
                if ($login->isValid($post)) {
                                        
                     $check_validation=$User->login_validation_email_or_username_pass($post['login_email_username'],$post['login_pass']);
                      if ($check_validation) {
                          #set zapamti me ----------------------------------------
                         if ($post['login_zapamtime']=='1') {
                             $User->zapamti_me($check_validation->id_users_nk);
                         } 
                         $User->createSession($check_validation);
                         $this->redirect()->toUrl('http://nadjinekretnine.com/moj-profil');                         
                     }else{
                              $login_not_valide_message='Uneseni podatci su netacni!';
                       } 
                
                     }
             }
             
                 
         }
         
         
        
        
      
        return new ViewModel([
            'login'=>$login,
            'login_error'=>$login_not_valide_message,
            'register_form'=>$register,
            'zab_lozinka'=>$zaboravljena_lozinka,
            
        ]);
        
        
    }
    
  
    
    
  
    
    
    public function resetPasswordAction() {
         $token=$this->getEvent()->getRouteMatch()->getParam('id');
         $form=false; 
         if ($token) {
             $User=$this->getServiceLocator()->get('User');
             if ($data=$User->reset_pass_serach_token($token)) {
                     $form=new ZaboravljenaLozinkaForm();
                     $request = $this->getRequest();
                     if ($request->isPost()) {
                             $post = $request->getPost()->toArray();               
                             if (isset($post['new_submit'])) {
                                     $form->setData($post);
                                     if ($form->isValid($post)) {
                                        $User->new_password((int)$data['id_users_nk'],$post['new_password']);
                                        $this->redirect()->toUrl('http://nadjinekretnine.com/prijava-registracija?newpass=true'); 
                                     }
                             }
                     }
             }
         }
         return new ViewModel([
             'form'=>$form,
         ]);
        
    }
    
    
    
    
    
    public function checkUsernameAction() {
         $viewModel=new ViewModel();
        $ajax_return=false;
        $request = $this->getRequest();
        if ($request->isPost()) {
             $post = $request->getPost()->toArray();
             if (isset($post['username'])&&  strlen($post['username'])>1) {
                 $User=$this->getServiceLocator()->get('User');
                 if ($User->check_username_availability($post['username'])) {
                     $ajax_return='<p class="text-success">Korisnicko ime je slobodno!</p>';
                 }else {
                        $ajax_return='<p class="text-error">Korisnicko ime je zauzeto!</p>';
                    }
                     
             }
         }
        $viewModel->setTerminal($request->isPost());
        return $viewModel->setVariables([ 'ajax_response'=>  $ajax_return, ]);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

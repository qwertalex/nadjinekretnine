<?php
namespace LoginRegister\Models\ResetPassword;


use Zend\InputFilter\InputFilter;




class Filter extends InputFilter{

    
    public function __construct() {
        
               
     #password    
        
            $this->add(array(
            'name'=>'new_password',
            'requeried'=>true,
            'validators'=>array(
                array(
                   'name'=>'not_empty',
                    'break_chain_on_failure' => true,
                     'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\NotEmpty::IS_EMPTY=>
                'Polje Lozinka je obavezno!'
                        )
                    )
                ),
                array(
                    'name'=>'string_length',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'min'=>5,
                         'max' => 32,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Polje lozinka mora da sadrzi minimum 5 karaktera!', 
                                 'stringLengthTooLong' => 'Polje lozinka mora da sadrzi najvise 32 karaktera!' 
                            ),
                    ),
                ),
            )
            
        ));    
        
      #---------------------------------------------------------------------     
             
        
        
       #confirm password   
        
            $this->add(array(
            'name'=>'new_retupepass',
            'requeried'=>true,
            'validators'=>array(
                 array(
                   'name'=>'not_empty',
                    'break_chain_on_failure' => true,
                     'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\NotEmpty::IS_EMPTY=>
                'Polje Potvrdi Lozinku je obavezno!'
                        )
                    )
                ),               
             array(
            'name' => 'Identical',
            'options' => array(
                'token' => 'new_password', // name of first password field self::NOT_SAME
                 'messages'=>array(
                     \Zend\Validator\Identical::NOT_SAME=>
                'Unesene lozinke se ne podudaraju!'
                        )                
            ),
        ),
       
            )
            
        ));    
        
        
        
        
    }
 
    
    
    
    
    
    
    
    
    
    
}
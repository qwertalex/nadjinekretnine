<?php
namespace LoginRegister\Models\ResetPassword;


use Zend\Form\Form;



class ZaboravljenaLozinkaForm extends Form {
 
    
    
    
    public function __construct() {
        
        
         parent::__construct("reset-password");
         $this->setAttribute('action', '');
         $this->setAttribute('method', 'post');
         $this->setAttribute("class", 'form-horizontal');
         $this->setInputFilter(new \LoginRegister\Models\ResetPassword\Filter());
        
        
    
                 #------------------------------------------------  
        
              #password        
       $this->add(array(
            'name'=>'new_password',
            'attributes'=>array(
                'type'=>'password',
                
            ),
           ));
    #------------------------------------------------     
       #repeat pass
      $this->add(array(
            'name'=>'new_retupepass',
            'attributes'=>array(
                'type'=>'password',
                
            ),
           ));
      #------------------------------------------------                                 
         
            
      #submit
      $this->add(array(
            'name'=>'new_submit',
          'type'=>'submit',
            'attributes'=>array(
                
                      'class'=>'btn   btn-primary arrow-right',
                'value'=>'Sacuvaj',
            ),
           ));
        
        
         
         
         
         
         
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
<?php
namespace LoginRegister\Models\Register;

use Zend\InputFilter\InputFilter;

/**
 * filter za RegisterNewUser
 * 
 * 
 * 
 */


use Application\Models\User\User;




class FilterRegisterNewUser extends InputFilter
{
   
  /**
    * 
    * @param class $obj | Application\Ajax\AjaxCheckUserEmail
    * 
    * 
    * 
    */
     public function __construct( User $obj) {
        
  
         
         
          #username   
        $this->add(array(
            'name'=>'reg_username',
            'requeried'=>true,
            'allow_empty'=>false,
               'validators' => array(
                      [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Username je obavezno!' 
                            ],
                        ],
                     ],  
                   array(
                    'name'=>'string_length',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'min'=>2,
                         'max' => 15,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Korisnicko ime mora da sadrzi najmanje dva karaktera!', 
                                 'stringLengthTooLong' => 'Korisnicko ime mora da sadrzi najvise 15 karaktera!' 
                            ),
                    ),
                ),
                 array(
                  'name'=>'Regex',
                     'break_chain_on_failure' => true,
                     'options'=>
                     array(
                     'pattern' => '/^[a-zA-Z\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Korisnicko ime nije validno!'
                     ))),
                                [
                    'name'=>'\Application\CustomValidators\Checkusername',
                    'break_chain_on_failure' => true,
                   'options'=>['user_class'=> $obj]
                    ],
            
                   
            ),
                 ));
   #---------------------------------------------------------------------   
           
        
      #email  
             $this->add(array(
            'name'=>'reg_email',
            'requeried'=>true,
            'filters'=>[
                [ 'name'=>'StringTrim',]
                
             ],
            'validators'=>array(
                     [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Email je obavezno!' 
                            ],
                        ],
                     ], 
                array(
                    'name'=>'EmailAddress',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\EmailAddress::INVALID_FORMAT=>
                'Email adresa nije validna!'
                        )
                    )
                ),
                [
                    'name'=>'\Application\CustomValidators\CheckEmailInDB',
                    'break_chain_on_failure' => true,
                   'options'=>['user_class'=> $obj]
                    ],
            )
            
        ));
        
     #---------------------------------------------------------------------      
             
        
        
     #password    
        
            $this->add(array(
            'name'=>'reg_password',
            'requeried'=>true,
            'validators'=>array(
                array(
                   'name'=>'not_empty',
                    'break_chain_on_failure' => true,
                     'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\NotEmpty::IS_EMPTY=>
                'Polje Lozinka je obavezno!'
                        )
                    )
                ),
                array(
                    'name'=>'string_length',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'min'=>5,
                         'max' => 32,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Polje lozinka mora da sadrzi minimum 5 karaktera!', 
                                 'stringLengthTooLong' => 'Polje lozinka mora da sadrzi najvise 32 karaktera!' 
                            ),
                    ),
                ),
            )
            
        ));    
        
      #---------------------------------------------------------------------     
             
        
        
       #confirm password   
        
            $this->add(array(
            'name'=>'reg_retupepass',
            'requeried'=>true,
            'validators'=>array(
                 array(
                   'name'=>'not_empty',
                    'break_chain_on_failure' => true,
                     'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\NotEmpty::IS_EMPTY=>
                'Polje Potvrdi Lozinku je obavezno!'
                        )
                    )
                ),               
             array(
            'name' => 'Identical',
            'options' => array(
                'token' => 'reg_password', // name of first password field self::NOT_SAME
                 'messages'=>array(
                     \Zend\Validator\Identical::NOT_SAME=>
                'Unesene lozinke se ne podudaraju!'
                        )                
            ),
        ),
       
            )
            
        ));    
        
        
        
        
         
               
#reg_zapamtime  
        $this->add(array(
            'name'=>'reg_uslovi_koriscenja',
            'requeried'=>true,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
            array(
                'name' => 'Identical',
                'options' => array(
                    'token' => 1,
                    'messages' => array(
                        \Zend\Validator\Identical::NOT_SAME => 'Morate prihvatiti uslove koriscenja.',
                    ),
                ),
            ),
            ),
                 ));
   #---------------------------------------------------------------------       
 
        

    
#oglasivac_nekretnine
        $this->add(array(
            'name'=>'oglasivac_nekretnine',
            'requeried'=>true,
            'allow_empty'=>false,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje oglasivac je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 2,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------  
        
        
        
    }
    
    
    
    
    
    
    
    
    
}



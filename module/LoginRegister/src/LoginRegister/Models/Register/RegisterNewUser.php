<?php


namespace LoginRegister\Models\Register;

use Zend\Form\Form;

//local
use Application\Models\User\User;

class RegisterNewUser extends Form 
{
   /**
    * 
    * @param class $obj | Application\Ajax\AjaxCheckUserEmail
    * 
    * 
    * 
    */
    public function __construct( User $user) {
        
        parent::__construct("registracija_novi_korisnik");
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new \LoginRegister\Models\Register\FilterRegisterNewUser($user));
        
        
        
        
        #email-----------------------------------       
        $this->add(array(
            'name'=>'reg_username',
            'attributes'=>array(
                'type'=>'text',
                'id'=>'reg_username',
                'class'=>'span5'
            ),
           ));
        #------------------------------------------------         
        
        
        
        
        

        #email-----------------------------------       
        $this->add(array(
            'name'=>'reg_email',
            'attributes'=>array(
                'type'=>'text',
               // 'placeholder'=>'E-mail',
               // 'class'=>'input-orange-border',
                'id'=>'reg_email',
                'class'=>'span5'
            ),
           ));
        #------------------------------------------------  
        
              #password        
       $this->add(array(
            'name'=>'reg_password',
            'attributes'=>array(
                'type'=>'password',
           //     'placeholder'=>'Lozinka',
               // 'class'=>'input-orange-border',
                'id'=>'reg_password',
                'class'=>'span5'
            ),
           ));
    #------------------------------------------------     
       #repeat pass
      $this->add(array(
            'name'=>'reg_retupepass',
            'attributes'=>array(
                'type'=>'password',
               // 'placeholder'=>'Ponovi lozinku',
              //  'class'=>'input-orange-border',
                'id'=>'reg_retupepass',
                'class'=>'span5'
                
            ),
           ));
      #------------------------------------------------                                 
         
      
      
            
     #reg_uslovi_koriscenja--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'reg_uslovi_koriscenja',
            'attributes'=>[
                'id'=>'reg_uslovi_koriscenja',
                'checked'=>'checked',
                'class'=>'uslovi-koriscenja'
            ],
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
           
     ));
      #-----------------------------------------   
      
      
       
          #  radio buttons---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'oglasivac_nekretnine',
                 'options' => array(
                  'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'Fizicko lice',
                            2=> 'Pravno lice',
                     ),
             ),
                 'attributes' => array(
               'value' => '1', //set default checked value to key 1;
               'class'=>'icheck-line',
            )
           
     ));
      #-----------------------------------------  
       
       
      #submit
      $this->add(array(
            'name'=>'reg_submit',
          'type'=>'Zend\Form\Element\Button',
            'attributes'=>array(
                
                      'class'=>'btn pull-right btn-primary arrow-right',
                'value'=>'Registruj se',
               // 'style'=>'margin-top:10px',
               // 'id'=>'reg_submit',
                
            ),
           ));
        
        
    }
    

    
    
    
    
    
    
    
    
    
    
}


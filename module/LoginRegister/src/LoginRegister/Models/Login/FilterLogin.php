<?php

namespace LoginRegister\Models\Login;

//use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class FilterLogin extends InputFilter
{
   
    public function __construct() {
        
        
        
              
         
          #username   
        $this->add(array(
            'name'=>'login_email_username',
            'requeried'=>true,
            'allow_empty'=>false,
               'validators' => array(
                  [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Korisnicko ime ili email je obavezno!' 
                            ],
                        ],
                     ],                   
                    [
                    'name'=>'\Application\CustomValidators\UsernameOrPassword',
                    'break_chain_on_failure' => true,
                    ],
                         
                   
            ),
                 ));
   #---------------------------------------------------------------------      
        
        
         
        
            $this->add(array(
            'name'=>'login_pass',
            'required'=>true,
            'validators'=>array(
                  [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Lozinka je obavezno!' 
                            ],
                        ],
                     ], 
                array(
                    'name'=>'string_length',
                    'options'=>array(
                         'encoding' => 'UTF-8',
                         'min'=>5,
                         'max' => 32,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Polje lozinka mora da sadrzi minimum 5 karaktera!', 
                                 'stringLengthTooLong' => 'Polje lozinka mora da sadrzi najvise 32 karaktera!' 
                            ),
                    ),
                ),
            )
            
        ));    
        
        
        
                       
         #podkategorija    
         $this->add(array(
             'name'=>'login_zapamtime',
             'required'=>false,
             'allowEmpty'=>true, 
             
             'validators' => array(
                  array(
                    'name' => 'Identical',
                    'options' => array(
                    'token' => '1',
                     ),
                 ),
             ),
         ));
   #---------------------------------------------------------------------  
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
}




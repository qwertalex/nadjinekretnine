<?php

namespace LoginRegister\Models\Login;

use Zend\Form\Form;




class Login extends Form
{
   
   
    
    public function __construct() {
        
        parent::__construct("login");
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setInputFilter(new \LoginRegister\Models\Login\FilterLogin());
     
        
      
        
        
        
        
        
          $this->add(array(
            'name'=>'login_email_username',
            'attributes'=>array(
                'type'=>'text',
                'id'=>'input-login-email',
                'value'=>'',
                'class'=>'span5'
            ),
           ));
        
        
      
        
        $this->add(array(
            'name'=>'login_pass',
            'attributes'=>array(
                'type'=>'password',
                'id'=>'input-login-password',
               'class'=>'span5'
            ),
     
            
        ));
        
               
        
                    
     #login_zapamti--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'login_zapamtime',
      
            'attributes'=>[
                'id'=>'login_zapamtime',
                'class'=>'icheck-line'
            ],
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => '1',
                     'unchecked_value' => "",
             ),
           
           
     ));
      #-----------------------------------------   
        
        
        
      
     #login_submit           
        $this->add(array(
            'name'=>'login_submit',
            'type'=>'submit',
            'attributes'=>array(
                
                'class'=>'btn btn-primary arrow-right pull-right',
             //   'value'=>'Prijavi se',
                'style'=>'font-weight:bolder;font-style:italic;',
                'id'=>'login_submit',
            ),
             'options' => array(
        'label' => 'Prijavi se', 
    )
       
        ));
        
        
      
         
  
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}


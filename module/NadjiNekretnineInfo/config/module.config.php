<?php





return array(
  
'controllers' => array(
     'invokables' => array(
'NadjiNekretnineInfo\Controller\Index' => 'NadjiNekretnineInfo\Controller\NadjiNekretnineInfoController',# <------HERE
          // <----- Module Controller
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'info' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/info[/:action][/:id]',# <------HERE
            // <---- url format module/action/id
           'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              'id'     => '[0-9]+',
            ),
            'defaults' => array(
               'controller' => 'NadjiNekretnineInfo\Controller\Index',# <------HERE
                // <--- Defined as the module controller
               'action'     => 'index',
                // <---- Default action
            ),
        ),
     ),
  ),
),
    
    'view_manager' => array(
       
        'template_map' => array( 
            
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
 
);













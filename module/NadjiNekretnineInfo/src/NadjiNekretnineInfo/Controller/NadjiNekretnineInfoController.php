<?php

namespace NadjiNekretnineInfo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

//local
use Application\Models\Oglasi\AdsInfo as Info;

class NadjiNekretnineInfoController extends AbstractActionController {

    public function indexAction() {
        return new ViewModel([]);
        
    }
    
        public function oNamaAction() {
        return new ViewModel([]);
     }
    
    
    
        public function marketingAction() {
            
            
            
            
        return new ViewModel([
            'Info'      =>new Info()
        ]);
    }
    
    
        public function kontaktAction() {
        return new ViewModel([]);
    }
    
 
    public function cestaPitanjaAction() {
         return new ViewModel([]);
      }
    
     
    
      public function usloviKoriscenjaAction() {
         return new ViewModel([]);
      }
      
      public function partnerskiSajtoviAction() {
         return new ViewModel([]);
      }   
    
       public function mapaSajtaAction() {
          $this->getResponse()->getHeaders()->addHeaders(array('Content-type' => 'text/xml')); 
         
          $viewModel=new ViewModel();
          $viewModel->setTerminal(true);
          return $viewModel;
      }    
    
       public function impressumAction() {
         return new ViewModel([]);
      }    
        
    public function odjavaAction(){
        $get=$this->params()->fromQuery();
        $msg='Vec ste odjavljeni sa servisa!';
       // var_dump($get);
        if (isset($get['id_mitrazimo'],$get['hash'])) {
        $adapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $sql=new \Zend\Db\Sql\Sql($adapter);
        $select=$sql->select('mi_trazimo');
        $select->where([
            'idmi_trazimo'      =>$get['id_mitrazimo'],
            'hash'              =>$get['hash']
        ]);
        $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $delete=$sql->delete('mi_trazimo');
             $delete->where([
                'idmi_trazimo'      =>$get['id_mitrazimo'],
                'hash'              =>$get['hash'] 
             ]);
             $sql->prepareStatementForSqlObject($delete)->execute();
             $msg='Uspesno ste se odjavili sa servisa';
         }
         }
        
        
        return new ViewModel([
            'msg'       =>$msg
        ]);
    }
    
}



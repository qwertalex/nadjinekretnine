<?php





return array(
  
'controllers' => array(
     'invokables' => array(
'Banners\Controller\Index' => 'Banners\Controller\BannersController',# <------HERE
          // <----- Module Controller
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'banners' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/banners[/:action][/:id]',# <------HERE
            // <---- url format module/action/id
           'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              'id'     => '[0-9]+',
            ),
            'defaults' => array(
               'controller' => 'Banners\Controller\Index',# <------HERE
                // <--- Defined as the module controller
               'action'     => 'index', 
            ),
        ),
     ),
  ),
),
     
           'view_manager' => array(
    'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);













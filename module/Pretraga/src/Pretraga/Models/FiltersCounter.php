<?php

namespace Pretraga\Models;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Db\Sql\Sql;


//lokal
use Application\Models\Oglasi\Render\RenderView as Render;


/**
 * Description of FiltersCounter:
 * ova class se nasledjuje od Pretraga\Model\Pretraga;
 * 
 * Pretraga mu slaje filter na osnovu cega vraca cunter(brojac za filtere);
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class FiltersCounter extends \Ajax\Model\AjaxLocationSearch {
    
    protected $adapter;
    protected $filter;


    public function __construct($adapter,$filter) {
        $this->adapter=$adapter;
        $this->filter=$filter;
      //  parent::__construct($filter, $adapter);
    }
    
       
         
         public function min_max_sum_rows() {
             
             $sql=new Sql($this->adapter);
             $select = $sql->select();
             $select->from('oglasi_nk');
             $select->columns([]);
             
             $select->join('dodatno','dodatno.id_oglasi_nk=oglasi_nk.id_oglasi_nk',[
              /*   'min_cena'=>new \Zend\Db\Sql\Expression("min(cena)"),
                 'max_cena'=>new \Zend\Db\Sql\Expression("max(cena)"),
                 'min_sobe'=>new \Zend\Db\Sql\Expression("min(broj_soba)"),
                 'max_sobe'=>new \Zend\Db\Sql\Expression("max(broj_soba)"),
                 'min_kvadratura'=>new \Zend\Db\Sql\Expression("min(kvadratura)"),
                 'max_kvadratura'=>new \Zend\Db\Sql\Expression("max(kvadratura)"),
                 'min_spratovi'=>new \Zend\Db\Sql\Expression("min(spratovi)"),
                 'max_spratovi'=>new \Zend\Db\Sql\Expression("max(ukupno_spratovi)"),        */            
             ]);
             
             
             $select->join('dodatni_podatci','dodatni_podatci.id_oglasi_nk=dodatno.id_oglasi_nk',[
                 'interfon'=>new \Zend\Db\Sql\Expression("sum(interfon)"),
                 'klima'=>new \Zend\Db\Sql\Expression("sum(klima)"),
                 'kablovska'=>new \Zend\Db\Sql\Expression("sum(kablovska)"),
                 'internet'=>new \Zend\Db\Sql\Expression("sum(internet)"),
                 'depozit'=>new \Zend\Db\Sql\Expression("sum(depozit)"),
                 'zamena'=>new \Zend\Db\Sql\Expression("sum(zamena)"),
                 'odmah_useljiv'=>new \Zend\Db\Sql\Expression("sum(odmah_useljiv)"),
                 'za_pusace'=>new \Zend\Db\Sql\Expression("sum(za_pusace)"),
                 'za_nepusace'=>new \Zend\Db\Sql\Expression("sum(za_nepusace)"),
                 'uknjizen'=>new \Zend\Db\Sql\Expression("sum(uknjizen)"),
                 'lift'=>new \Zend\Db\Sql\Expression("sum(lift)"),
                 'podrum'=>new \Zend\Db\Sql\Expression("sum(podrum)"),
                 'topla_voda'=>new \Zend\Db\Sql\Expression("sum(topla_voda)"),
                 'dodatni_podatci_telefon'=>new \Zend\Db\Sql\Expression("sum(dodatni_podatci_telefon)"),
                 'dodatno_graza'=>new \Zend\Db\Sql\Expression("sum(dodatno_graza)"),
                 'parking'=>new \Zend\Db\Sql\Expression("sum(parking)"),
                 'terasa'=>new \Zend\Db\Sql\Expression("sum(terasa)"),
               ],'left');
             
                       $select->join('lokacija','lokacija.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[]);
          $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[]);
          $select->join('slike','slike.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[], 'left');
       
       
          $select->join('kuce','kuce.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[],'left');
          $select->join('plac','plac.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[],'left');
            if ($filteri=$this->filter) {
                $select->where($filteri);
             }
             
            // $select->group($column); 
             $statement = $sql->prepareStatementForSqlObject($select);
              $results = $statement->execute();
              return array_values(iterator_to_array($results));              
             
         }         
         
         
         
         
         
         
         
         
         
         
         
         
         
        public function count_kategorija(){
            
            
        $sql=new Sql($this->adapter);
             $select = $sql->select();
             $select->from('oglasi_nk');
             $select->columns(['kategorija','count'=>new \Zend\Db\Sql\Expression('COUNT(*)')]);
             
        
             $select->group('kategorija'); 
             $statement = $sql->prepareStatementForSqlObject($select);
             $results = $statement->execute();
            
             $kat=array_values(iterator_to_array($results)); 
         //    var_dump($kat);
             foreach($kat as $k=>$value){
             $kat[$k]['id']=$kat[$k]['kategorija'];
             $kat[$k]['title']=Render::kategorija($kat[$k]['kategorija'])." (".$kat[$k]['count'].")";
             unset($kat[$k]['count'],$kat[$k]['kategorija']);
              }
             return $kat;
        }
         
    
    
    
    
  
    
    
    
    public function count_slika(){
             $sql=new Sql($this->adapter);
             $select = $sql->select();
             $select->from('oglasi_nk');
             $select->columns([]);
             
          $select->join('lokacija','lokacija.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[]);
          $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[]);
          $select->join('slike','slike.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[
              'count'=>new \Zend\Db\Sql\Expression('COUNT(*)')]);
          $select->join('dodatno','dodatno.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[],'left');
          $select->join('dodatni_podatci','dodatni_podatci.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[],'left');
          $select->join('kuce','kuce.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[],'left');
          $select->join('plac','plac.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[],'left');
            if ($filteri=$this->filter) {
                $select->where->isNotNull('slika_1');
                $select->where($filteri);
             }
 
             $statement = $sql->prepareStatementForSqlObject($select);
             $results = $statement->execute();
             return array_values(iterator_to_array($results))[0]['count'];       
        
    }
    
    
 
    
    
    
    
         
                
             
             
                public function count_all($table,$col) {
                $sql=new Sql($this->adapter);
                $select = $sql->select();
                $select->from('oglasi_nk');
                
                $do=$k=$p=$l=$v=$s=$d=[];
                $oglasi_nk=['count'=>new \Zend\Db\Sql\Expression('COUNT(*)')];
                switch($table):
                    case "dodatno": $do[]=$col;break;
                    case "kuce":$k[]=$col;break;
                    case "plac":$p[]=$col;break;
                    case "vrsta_oglasa":$v[]=$col;break;
                    case "slike":$s[]=$col;break;
                    case "dodatn_podatci":$d[]=$col;break;
                    case "oglasi_nk":$oglasi_nk[]=$col;break;
                endswitch;
                $select->columns($oglasi_nk);
                $select->join('dodatno','dodatno.id_oglasi_nk=oglasi_nk.id_oglasi_nk',$do);
                $select->join('kuce','kuce.id_oglasi_nk=oglasi_nk.id_oglasi_nk',$k,'left');
                $select->join('plac','plac.id_oglasi_nk=oglasi_nk.id_oglasi_nk',$p,'left');             
                $select->join('lokacija','lokacija.id_oglasi_nk = oglasi_nk.id_oglasi_nk',$l);
                $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk = oglasi_nk.id_oglasi_nk',$v);
                $select->join('slike','slike.id_oglasi_nk = oglasi_nk.id_oglasi_nk',$s, $select::JOIN_LEFT);
                $select->join('dodatni_podatci','dodatni_podatci.id_oglasi_nk = oglasi_nk.id_oglasi_nk',$d, $select::JOIN_LEFT);
                $filteri=  $this->filter;
                unset($filteri[$col]);
                $select->where($filteri);
                $select->group([$col]); 
                $statement = $sql->prepareStatementForSqlObject($select);
                $results = $statement->execute();
                return array_values(iterator_to_array($results));              
             }             
             
             
             
             
             
             
             
             
             
         
         
         
    
}

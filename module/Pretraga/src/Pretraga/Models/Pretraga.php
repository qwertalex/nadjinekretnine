<?php
namespace Pretraga\Models;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate;
use Zend\Db\Sql\Predicate\Operator;
//local
use Application\Models\Url\UrlRewrite as Url;
use Application\Models\Oglasi\Render\RenderToDb as Render_db;

/**
 * PARENT: \Pretraga\Models\FiltersCounter
 * GRAND PARENT: \Ajax\Model\AjaxLocationSearch
 * 
 * 
 * OPIS:
 * -----
 * Ova class zapravo vraca sve oglase (koji su filtrirani), zatim stavlja default values u formu koja se koristi za 
 * pretragu i filtere zato sto se forma ne validira na standardni nacin nego preko module.config; 
 * 
 * 
 * creira i filere koji se koriset u parent and grandparent
 * 
 * 
 * 
 * 
 * 
 */

class Pretraga extends \Pretraga\Models\FiltersCounter{
    
     protected $get;
     protected $adapter;
     protected $data;
     protected $filter=[];
     protected $where;
     public function __construct(\Zend\Db\Adapter\Adapter $adapter,$get="") {
         $this->get=$get;
         $this->adapter=$adapter;
         $this->where=new \Zend\Db\Sql\Where();
         $this->add_filters();//kreirenje filtera za pretragu
         parent::__construct($adapter,$this->filter);
       // var_dump($get);
       // var_dump($this->filter);

     }
   

     
     
     
  /*
   * 
   * 
   * 
   * vraca sve oglase u zavisnosti od pretrage(od filtera koji user koristi)
   * 
   * 
   * 
   * 
   * 
   */
    
     public function get_ads() {
         $sql=new Sql($this->adapter);
         $select=$sql->select(); 
         
         $select->from( 'oglasi_nk');
         $select->join('lokacija','lokacija.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['mesto','lokacija','deo_mesta','ulica','mapa','e_stan_frame']);
         $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[
                      'datum_postavljanja_oglasa','datum_isteka','vrsta_oglasa']);
         $select->join('slike','slike.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['slika_1'], $select::JOIN_LEFT);
         
        $select->join('dodatno','dodatno.id_oglasi_nk = oglasi_nk.id_oglasi_nk',
                     ['grejanje','spratovi','ukupno_spratovi','kvadratura','cena','broj_soba','tip_objekta','opis_objekta',
                         'nacin_placanja','namestenost','valuta'],
                     $select::JOIN_LEFT);
        
        $select->join('dodatni_podatci','dodatni_podatci.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[
                'internet','kablovska','interfon','klima','depozit','zamena','odmah_useljiv','za_pusace','za_nepusace','uknjizen',
            'lift','podrum','topla_voda','dodatni_podatci_telefon','dodatno_graza','terasa','parking'
                ], $select::JOIN_LEFT);
        
         $select->join('kuce','kuce.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['spratnost','vrsta_kuce'], $select::JOIN_LEFT);
        
         $select->join('plac','plac.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[
                 'ostalo_plac','vrsta_plac','povrsina_plac','jedinica_mere_plac'
             ], $select::JOIN_LEFT);
        
         $select->order("vrsta_oglasa.vrsta_oglasa DESC");
        $select->order("vrsta_oglasa.datum_postavljanja_oglasa DESC");          
        $select->order("oglasi_nk.datum_kreiranja_oglasa DESC");   
     
         if (count($this->filter)>0) {
             $select->where($this->filter);
         }
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $res = array_values(iterator_to_array($results));//var_dump($res);
             return $res; 
          }
          return [];
       }
       
       
       
       
       
       
     /**
      * 
      * @return type
      * 
      * 
      * 
      * 
      * kreira filtere u zavisnosti od $this->get varaibile tj. od parametara 
      * u routes.
      * 
      * 
      */
     
     public function add_filters() {
         $get=$this->get;
     
      //  var_dump($get);
       $this->filter[]=$this->where->greaterThanOrEqualTo('vrsta_oglasa',1);
          $kategorija=Url::get_kategorija_id($get['kategorija']);
          if ($kategorija===14) {
              $this->filter['kategorija']=$kategorija;
              $this->filter_mesto();
              return false;
          }
         $this->filter['kategorija']=$kategorija;
         $this->filter['prodaja_izdavanje']=$get['prodaja_izdavanje']==='prodaja'?1:2;
      /*   $this->filter=[
             'kategorija'               =>$kategorija,
             'prodaja_izdavanje'        =>$get['prodaja_izdavanje']==='prodaja'?1:2,
            ];*/
         //set mesto
         $this->filter_mesto();
         //sobe
         $this->filter_sobe();
         //cena
         $this->filter_cena();
         //kvadratura
         $this->filter_kvadratura();
         //set slika
         $this->filter_slika();
        //svi checkboxovi
        $this->filter_chbx();
         //spratovi
         $this->filter_spratovi();
       //povrsina plac
       $this->filter_plac_povrsina();
         
       //  var_dump($this->filter);   
      //   var_dump($get);
         return $this->filter;
         
       }
       
       
       
       
       
       
       /**
        *
        * @var type 
        * 
        * 
        * lista svih chebxova;
        * ako hocu da dodam novi onda samo dodam u bazi broj(comentaru) ili
        * samo za dodatno napravim novi row;
        * svaki array['values'] pocinje sa key 1 zato sto je to vrednost u bazi tj.  u bazi
        * su samo brojevi npr. namesten je u bazi 1 , prazan je 3;
        * ZA DODATNO VAZE DRUGA PRAVILA;
        * Za dodatno se poziva druga function
        * filter_dodatno_chbx($str)
        * 
        */
       
       protected $chbx_list=[
            'namestenost'=>[
               'values'=>[1=>'namesten','polunamesten','prazan'],
               'table' =>'namestenost'
               ],
            
            'opis_objekta'    =>[
                    'values'    =>[1=>'izvorno_stanje','renoviran','lux','salonski','duplex','za_renoviranje'],
                    'table'     =>'opis_objekta'
            ],
             'tip_objekta'      =>[
                    'values'    =>[1=>'stara_gradnja','novogradnja','u_izgradnji'],
                    'table'     =>'tip_objekta'
             ],
           'dodatno'            =>[
                    'tables'        =>['depozit','odmah_useljiv','uknjizen','zamena','za_pusace','za_nepusace','klima','kablovska','internet','lift','podrum','topla_voda',
                        'dodatni_podatci_telefon','interfon','dodatno_graza','parking','terasa'],
            ],
           'oglasivac_nekretnine'   =>[
               'values'         =>[1=>'vlasnik','agencija','investitor','banka'],
               'table'          =>'oglasivac'
           ],
           'vrsta_kuce'         =>[
               'values'         =>['1'=>'cela_kuca','duplex_viseporodicne_kuce','vila','montazna_kuca','stambeno_poslovna_kuca','horizontala_kuce','vertikala_kuce'],
               'table'          =>'vrsta_kuce'
           ],
           'spratnost_kuce'     =>[
               'values'         =>[1=>'prizemna_kuca','spratna_kuca'],
               'table'         =>'spratnost'
           ],
           'ostalo_plac'        =>[
               'values'         =>[1=>'dozvoljena_gradnja','zapoceta_gradnja','deljiv'],
               'table'          =>'ostalo_plac'
           ],
           'vrsta_plac'        =>[
               'values'         =>[1=>'plac','njiva','vocnjak','vinograd','suma'],
               'table'          =>'vrsta_plac'
           ]
               
       ];
       
       
       /**
        * 
        * @param type $str
        * @return boolean
        * 
        * pravi filter za dodatno tako sto ako je isset(this->$get['dodatno'];
        * vrti u loop da vidi da li string(param)postoji u array i ako postoji daje mu 
        * vrednost 1
        */
       protected function filter_dodatno_chbx($str){
           $dodatno=$this->chbx_list['dodatno']['tables'];
           $arr= array_map(function($v){
               if ($v=='garaza') { return 'dodatno_graza'; }
               elseif($v==='telefon')return "dodatni_podatci_telefon";
               return $v;
           },explode('-',$str));
           foreach($dodatno as $k=>$v){
               if (in_array($v,$arr)) {
                   $this->filter[$v]=1;
               }
           }
           return true;
       }
       
       
       
       /**
        * 
        * @param type $str
        * @param array $chbx_list
        * 
        * kreira filtere za chbx sve sem dodatno
        */
       protected function filter_set_chbx($str,array $chbx_list){
           $nam=explode('-',$str);
            $num=[];
             foreach($chbx_list['values'] as $k=>$v){
                 
                 if (in_array($v,$nam)) {
                   $num[]=$k;
               }
             }
             if (count($num)>0) {
                 $this->filter[$chbx_list['table']]=$num;
             }
               
       }
       
       
       






       protected function grejanje_filter($str){
           $gr=  explode("-", $str);
           $arr=[];
           foreach ($gr as $v):
              $render= Render_db::grejanje($v);
               if ($render) {
                   $arr[]=$render;
                   
               }
           endforeach;
           
           if (count($arr)>0) {
               $this->filter['grejanje']=$arr;
           }
       }
               

       /**
        * 
        * @return boolean
        * 
        * 
        * 
        * ako su postoje get params(routes) poziva odgovarajucu funkciju sa odgovarajucim parametrima
        */
       public function filter_chbx(){
           $get=$this->get; 
             if (isset($get['namestenost-val'])) {
                 $this->filter_set_chbx($get['namestenost-val'],$this->chbx_list['namestenost']);
               }if (isset($get['grejanje-val'])) {
                   $this->grejanje_filter($get['grejanje-val']);
                   
               }if (isset($get['opis-objekta-val'])) { 
                 $this->filter_set_chbx($get['opis-objekta-val'],$this->chbx_list['opis_objekta']);
               }if (isset($get['tip-objekta-val'])) {
                 $this->filter_set_chbx($get['tip-objekta-val'],$this->chbx_list['tip_objekta']);
               }if (isset($get['dodatno-val'])) {
                 $this->filter_dodatno_chbx($get['dodatno-val']);
               }if (isset($get['oglasivac-val'])) {
                 $this->filter_set_chbx($get['oglasivac-val'],$this->chbx_list['oglasivac_nekretnine']);
               }if (isset($get['vrsta-kuce-val'])) {
                 $this->filter_set_chbx($get['vrsta-kuce-val'],$this->chbx_list['vrsta_kuce']);
               }if (isset($get['spratnost-kuce-val'])) {
                 $this->filter_set_chbx($get['spratnost-kuce-val'],$this->chbx_list['spratnost_kuce']);
               }if (isset($get['ostalo-plac-val'])) {
                 $this->filter_set_chbx($get['ostalo-plac-val'],$this->chbx_list['ostalo_plac']);
               }if (isset($get['vrsta-plac-val'])) {
                 $this->filter_set_chbx($get['vrsta-plac-val'],$this->chbx_list['vrsta_plac']);
               }
               
             return false;
       }
       
       
       
       
       
       
       
       
       
       
       
       
       /*
        * short cut za range values npr. 
        * cena od i cena do,kvadratura od do...
        * params:
        * $name=ime kolone iz baze;
        * $mm=je zapravo explode('-','10-2000')(npr. cena od 10 do 2000) ali zbog
        * mnogih konbinacija npr(0-0,100-0) tj. nemogucih kombinacija
        * ovaj paramertar se je uglavnom function $this->format_range_values($name,$od,$do);
        * 
        * 
        */
     protected function range_predicate($name,array $mm){
         if (count($mm)===1) {
            return $this->where->greaterThanOrEqualTo($name,$mm['min']);
         }elseif(count($mm)===2){
            return  $this->where->between($name, $mm['min'],$mm['max']);  
         }
         return false;
     }
       
     /**
      * 
      * @return boolean
      * 
      * 
      * 
      * 
      * 
      * OVIH NEKOLIKO FUNKCIJA SU ZAPRAVO GRESKA (treba da bude jedna funkcija ili da se prikljuce nekoj drujoj fun.)
      * samo proveravaju da li je setovana $get variabila i pozivaju odgovarajuce function sa odg. params
      * i puni globalnu variabiliu filer;
      * 
      * 
      */
       
       protected function filter_sobe(){
           if (isset($this->get['sobe-val'])) {
               $this->filter[]=  $this->range_predicate('broj_soba', $this->format_range_values('sobe-val','min','max'));
           }
           return false;
       }
       
       
       protected function filter_cena(){
           if (isset($this->get['cena-val'])) { 
               $this->filter[]=  $this->range_predicate('cena',$this->format_range_values('cena-val','min','max'));
           }
           return false;
       }
              
       protected function filter_kvadratura(){
           if (isset($this->get['kvadratura-val'])) { 
               $this->filter[]=  $this->range_predicate('kvadratura',$this->format_range_values('kvadratura-val','min','max'));
           }
           return false;
       }       
       
       protected function filter_spratovi(){
           if (isset($this->get['spratovi-val'])) {             
               $this->filter[]=  $this->range_predicate('spratovi',$this->format_range_values('spratovi-val','min','max'));
           }
           return false;
       }           
       
        protected function filter_plac_povrsina(){
           if (isset($this->get['povrsina-plac-val'])) { 
               $this->filter[]=  $this->range_predicate('povrsina_plac',$this->format_range_values('povrsina-plac-val','min','max'));
           }
           return false;
       }       

       
        protected function filter_slika(){
               if (isset($this->get['sa-slikom'])) {
                    $this->filter[]=$this->where->like('slika_1', '%%');
                }
        }       
          /**
        * 
        * 
        * 
        * 
        * 
        */
       
        
        /**
         * 
         * pravi filter za mesto...
         * posto mesto u bazi moze da ima tri kolone mesto,deo_mesta  i lokacija
         * variabila $mesto moze da se dobije u vise formata:
         * $mesto=1; u tom slucaju pravi filer za bazu: mesto=1;
         * $mesto=1-2000; mesto=1 and deo_mesta=2000;
         * $mesto=1-2000-10003;  mesto=1 and deo_mesta=2000 and lokacija=10003;
         * to je to...
         * 
         * 
         */
       protected function filter_mesto(){
         //  var_dump($this->get['lokacija']);
           $mesto= $this->get_location_id($this->get['lokacija']);
           if (count($mesto)===0) {
               return false;
           }
               $mes=[];$deo=[];$lok=[];
       //   if ($mesto&&count($mesto)>0) {
          //   if ($mesto['id']!=0) {
                 foreach ($mesto as $k => $v) {
                     if ($v!=0) {
                     if (strpos($v, '-')===false) {
                         $mes[]=$v;
                       /// $this->filter['mesto']=$v;
                    }elseif(count(explode('-', $v))===2){
                        $deo[]=explode('-', $v)[1];
                     //  $this->filter['deo_mesta']=explode('-', $v)[1];
                    }elseif(count(explode('-', $v))===3){
                        $lok[]=explode('-', $v)[2];
                        // $this->filter['lokacija']=explode('-', $v)[2];
                    }     
                 }
             }
        //}   
 
               
$all_predicates=[];

       if (count($mes)>0) { 
           foreach($mes as $k=>$v):
               $all_predicates[]=new Predicate\Operator('mesto', Operator::OPERATOR_EQUAL_TO, $v);
           endforeach;
       }if (count($deo)>0) {
           foreach($deo as $k=>$v):
               $all_predicates[]=new Predicate\Operator('deo_mesta', Operator::OPERATOR_EQUAL_TO, $v);
           endforeach;
        }if (count($lok)>0) {
           foreach($lok as $k=>$v):
              $all_predicates[]=new Predicate\Operator('lokacija', Operator::OPERATOR_EQUAL_TO, $v);
           endforeach;
        }
             
        
        
                    $where = new \Zend\Db\Sql\Where(array(
    // Other conditions...
    new Predicate\PredicateSet(
        $all_predicates,Predicate\PredicateSet::COMBINED_BY_OR
    ),
));
             
             $this->filter[]=$where;
        
        
        
        
        
        
        
        
        
        
        
        
             //  var_dump($this->filter); 
             
             
       }
       
       
       
     
     
     
     
       public function count_kategorija(){
           return parent::count_kategorija();
       }
     

       
       
       
       
     /**
      * 
      * OVO JE ZA FORMU FILER
      * 
      *
      * OPIS:
      * -----
      * Dinamicki setuje vrednosti u formu(jer se nevrsi validacija forme nego se validira u module.config)
      * 
      * 
      * 
      * 
      */ 
       
       public function set_form_values() {
           $get=$this->get;
                  //    var_dump($get);
                  
   
           $data=[
               'kategorija'         =>Url::get_kategorija_id($get['kategorija']),
               'prodaja_izdavanje'  =>$get['prodaja_izdavanje']==="izdavanje"?2:1,
               'mesta_srbija'       =>implode(',',array_filter(array_map(function($v){if ($v) { return $v;}},$this->get_location_id($get['lokacija'])))),
               'samo-sa-slikom'     =>isset($get['sa-slikom'])?1:"",
                         
               
           ];
 
         
          
          
           if ($pp_v=$this->format_range_values('povrsina-plac-val','plac_povrsina_od','plac_povrsina_do')) {
              if (isset($pp_v['plac_povrsina_od'])) {$data['plac_povrsina_od']=$pp_v['plac_povrsina_od'];}
              if (isset($pp_v['plac_povrsina_do'])) {$data['plac_povrsina_do']=$pp_v['plac_povrsina_do'];}
           }         
          if ($cena=$this->format_range_values('cena-val','cena_od','cena_do')) {
              if (isset($cena['cena_od'])) {$data['cena_od']=$cena['cena_od'];}
              if (isset($cena['cena_do'])) {$data['cena_do']=$cena['cena_do'];}
           }
          if ($kvadratura=$this->format_range_values('kvadratura-val','kvadratura_od','kvadratura_do')) {
              if (isset($kvadratura['kvadratura_od'])) {$data['kvadratura_od']=$kvadratura['kvadratura_od'];}
              if (isset($kvadratura['kvadratura_do'])) {$data['kvadratura_do']=$kvadratura['kvadratura_do'];}
           }          
           if ($sobe=$this->format_range_values('sobe-val','sobe_od','sobe_do')) {
              if (isset($sobe['sobe_od'])) {$data['sobe_od']=$sobe['sobe_od'];}
              if (isset($sobe['sobe_do'])) {$data['sobe_do']=$sobe['sobe_do'];}
           }         
          if ($spratovi=$this->format_range_values('spratovi-val','sprat','ukupno_spratova')) {
              if (isset($spratovi['sprat'])) {$data['sprat']=$spratovi['sprat'];}
              if (isset($spratovi['ukupno_spratova'])) {$data['ukupno_spratova']=$spratovi['ukupno_spratova'];}
           }          
           
           $c_arr=['namestenost','grejanje','opis-objekta','tip-objekta','dodatno',
         'vrsta-kuce','spratnost-kuce','ostalo-plac','vrsta-plac','spratovi','oglasivac'];
           foreach($c_arr as $n){
           if ($chbx=$this->chbx($n.'-val')) {
               foreach($chbx as $k=>$v){
                   $data[$k]=$v;
               }
           }               
           }

           
           
           
           
          
          //var_dump($data);
           return $data;
       }
       
       
       
       
       
       
       
       protected function chbx($name){
           if (!isset($this->get[$name])) {
               return false;
           }
           $ret=[];
           $name=explode('-',$this->get[$name]);
           foreach($name as $v){
               $ret[$v]=1;
           }
            return $ret;
       }
       
       
      
       
       
       
       
       /**
        * 
        * @param type $name
        * @param type $od
        * @param type $do
        * @return boolean
        * 
        * 
        * FORMATIRA VREDNOSTI  za od i do;
        * npr:
        * cena_od i cena_do(dva imput polja u route izgledaju ovako:20-30;
        * posto mogu da dodju u mnogim formatima onda ih ova funkicja na neki
        * nacin validira
        * nepozeljini formati su:
        * 100-0(od 100 do 0)
        * 0-0 ...itd
        * 
        * 
        * 
        * 
        */
       

   protected   function format_range_values($name,$od,$do){
          if (!isset($this->get[$name])) {
             return false;
          }
          $ret=[];
          $name=$this->get[$name];
          $name=explode("-",$name);
          if (count($name)===1) {
              $ret[$od]=$name[0];
          }elseif(count($name)===2){
              if ($name[0]==0&&$name[1]==0) {
                  return false;
              }elseif($name[1]==0){
                  $ret[$od]=$name[0];
              }else{
                  $ret=[
                    $od=>(int)$name[0],
                    $do=>(int)$name[1]
                  ];
              }
          }
          
          if (count($ret)>0) {
              return $ret;
          }
          return false;
          
      }
       
       
       
       
       /**
        * 
        * @return type
        * 
        * poziva odredjene countere  u zavisnosti od trazene kategorije i prodaje i izdavanja
        * stednja na resursima
        * 
        * 
        * 
        */
       
       public function call_counter(){
           $kat=$this->get['kategorija'];
           $pro_izd=$this->get['prodaja_izdavanje'];
           $counter=[
               'oglasivac'           =>$this->count_all('oglasi_nk','oglasivac'),
               'dodatni_podatci'     =>  $this->min_max_sum_rows(),
               'slike'               =>  $this->count_slika()
           ];
           if ($pro_izd==='izdavanje') {
               $counter['namestenost']=$this->count_all('dodatno','namestenost');
           }
           switch($kat):
               case 'stanovi':
               case 'sobe':case 'lokali':case "poslovni-prostori":
               case 'vikendice':case "apartmani":
                   $counter['grejanje']=$this->count_all('dodatno','grejanje');
                   $counter ['tip_objekta']=$this->count_all('dodatno','tip_objekta');
                   $counter['opis_objekta']=$this->count_all('dodatno','opis_objekta');                   
                  break; 
                   
                   case 'kuce':
                       $counter['vrsta_kuce']=$this->count_all('kuce','vrsta_kuce');
                        $counter['spratnost_kuce']=$this->count_all('kuce','spratnost');
                   $counter['grejanje']=$this->count_all('dodatno','grejanje');
                   $counter ['tip_objekta']=$this->count_all('dodatno','tip_objekta');                       
                    break;    
               case 'placevi':
                  $counter['ostalo_plac']= $this->count_all('plac','ostalo_plac');
                   $counter['vrsta_plac']= $this->count_all('plac','vrsta_plac');
                  break;
                       
           endswitch;
           
               
           
           
           
           
           
           return $counter;
       }
      
      
      
      
      
      
       
       
       
       
    
}







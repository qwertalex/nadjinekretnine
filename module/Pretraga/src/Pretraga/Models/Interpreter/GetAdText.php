<?php
namespace Pretraga\Models\Interpreter;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of GetText
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class GetAdText {
    
    
    
    
    
    
    
    
    public static function get_kategorija($int) {
        $int=(int)$int;
        if (!is_int($int)) { return false; }
        $col='kategorija';$text=false;$table="oglasi_nk";
        
             switch($int){
                     case 1: $text='Stan';break;
                     case 2:$text='Soba';break;
                     case 3:$text='Kuća';break;
                     case 4:$text='Garaža';break;
                     case 5:$text='Plac';break;
                     case 6:$text='Lokal';break;
                     case 7:$text='PP';break;
                     case 8:$text='Hala'; break;
                     case 9:$text='Kiosk'; break;
                     case 10:$text='Stovariste';break;
                     case 11:$text='Magacin';break;
                     case 12:$text='Poljoprivredno zemljiste';break;
                     case 13:$text='Gradjevinsko zemljiste';break;                    
                     case 14:$text='Vikendica';break;
                     case 15:$text='Apartmani'; break;
                     case 16:$text='Soba'; break;
                     case 17:$text='Selidbe'; break; 
             }
        
             if ($text) {
             return ['table'=>$table,'col'=>$col,'text'=>$text];    
             }
        
        return false;
        
        
    }
    
    
    
    
    
    public static function get_prodaja_izdavanje($int) {
        $col='prodaja_izdavanje';$text=false;$table="oglasi_nk";
        switch((int)$int){
            case 1:$text='Prodaja';break;
            case 2:$text='Izdavanje';break;
         }
         if ($text) {
             return ['table'=>$table,'col'=>$col,'text'=>$text];
         }
         return false;
    }
    
    
    public static function get_oglasivac($int) {
        $col="oglasivac";$text=false;$table="oglasi_nk";
        
         switch((int)$int){
            case 1:$text='Vlasnik';break;
            case 2:$text='Agencija';break;
            case 3:$text='Investitor';break;
            case 4:$text='Banka';break;
         }
         if ($text) {
             return ['table'=>$table,'col'=>$db,'text'=>$text];
         }
         return false;       
        
        
        
    }
    
    
    
    /**
     * 
     * @param type $int
     * @return boolean
     * 
     * 
     *  
     * 
     * 
    */  
    public static function get_grejanje($int){
         $table="dodatno";$text=false;$col='grejanje';
        
         switch((int)$int){
            case 1:$text='cg';break;
            case 2:$text='eg';break;
            case 3:$text='ta';break;
            case 4:$text='gas';break;
            case 5:$text='podno';break;
            case 6:$text='kaljeva pec';break;
            case 7:$text='norveski radiatori';break;
            case 8:$text='mermerni radiatori';break;            
         }
         if ($text) {
               return ['table'=>$table,'col'=>$col,'text'=>$text];
         }
         return false;       
       }
    
    
    
    public static function get_spratovi($int) {
        $table="dodatno";$text=false;$col='spratovi';
             switch ($int){
                  case 'suteren':$text="Suteren";break;
                  case 'polu_suteren':$text='Polusuteren';break;
                  case 'prizemlje':$text='Prizemlje';break;
                  case 'visoko_prizemlje':$text='Visoko prizemlje';break;
                  case 'potkrovlje':$text='Potkrovlje';break;
                  case 'penthouse':$text='Penthouse';break;
                  case in_array($int, range(1,30)):$text=$int;break;
             }
             
          if ($text) {
             return ['table'=>$table,'text'=>$text,'col'=>$col];
         }
         return false;           
        
    }
    
    
     public static function get_spratovi_ukupno($int) {
        $table="dodatno";$text=false;$col='ukupno_spratovi';
             switch ($int){
                   case in_array($int, range(1,30)):$text=$int;break;
             }
             
          if ($text) {
             return ['table'=>$table,'text'=>$text,'col'=>$col];
         }
         return false;           
        
    }   
    
    
    
    
      /*            
     protected function vrsta_placa() {
         $table='vrsta_placa';
         if ($na=  $this->is_set('vrsta_plac')) {
             switch ($na){
                  case 1:$col="plac";break;
                  case 2:$col="njiva";break;
                  case 3:$col="vocnjak";break;     
                  case 4:$col="vinograd";break;
                  case 5:$col="suma";break;
             }
             $this->db_data[$table]=[$col=>1,];
         }
     }       
            
     protected function ostalo_plac() {
         $post=  $this->post;
         $table='ostalo_plac';
         $this->db_data[$table]=["povrsina"=>$post['plac_povrsina'],"jedinica_mere"=>$post['ha_ar']];
         if ($na=  $this->is_set('ostalo_plac')) {
             switch ($na){
                  case 1:$col="dozvoljena_gradnja";break;
                  case 2:$col="zapoceta_gradnja";break;
                  case 3:$col="deljiv";break;     
             }
            $this->db_data[$table][$col]=1;
          }
     }  
      
     
     
     /*
                 
      protected function spratovi() {
         $table='spratovi';
         $val=1;
         $ukupno=0;
         if ($na=  $this->is_set('sprat')) {
             switch ($na){
                  case 'suteren':$col="suteren";break;
                  case 'polu_suteren':$col='polusuteren';break;
                  case 'prizemlje':$col='prizemlje';break;
                  case 'visoko_prizemlje':$col='visoko_prizemlje';break;
                  case 'potkrovlje':$col='potkrovlje';break;
                  case 'penthouse':$col='penthouse';break;
                  default : $col="sprat";$val=$na; break; 
             }
             
             if ($uk=  $this->is_set('ukupno_spratova')) {
                 $ukupno=$uk;
                 
             }
             
              $this->db_data[$table]=[$col=>$val, 'ukupan_br_spratova'=>$ukupno,];
         }
     }  
  */
     /*
     
           protected function tip_objekta() {
         $table='tip_objekta';
         if ($na=  $this->is_set('tip_objekta')) {
             switch ($na){
                  case 1:$col="stara_gradnja";break;
                  case 2:$col="novogradnja";break;
                  case 3:$col="u_izgradnji";break;     
             }
             $this->db_data[$table]=[
                 //'id_oglasi_nk'=>$this->oglasi_id,
                 $col=>1,
             ];
         }
     }  
     
     */
        /*     
      protected function kuce_spratnost() {
          $table='kuce_spratnost';
          $pri=NULL;
          $spratna=NULL;
          
          if ($spr=$this->is_set('kuce_sparatnost')) {
              switch ($spr){
                  case 1:$pri=1;break;
                  case 2:$spratna=1;break;
              }
          }
          
          $this->db_data[$table]=['prizemna'=>$pri,'spratna'=>$spratna,];
          
          
      }
       protected function kuce_vrsta_objekta() {
          $table='kuce_vrsta_objekta';
          if ($vrs=$this->is_set('vrsta_objekta')) {
              switch ($vrs){
                  case 1:$col="cela_kuca";break;
                  case 2:$col="duplex";break;
                  case 3:$col="apartmanske_kuce";break;
                  case 4:$col="vila";break;
                  case 5:$col="montazna_kuca";break;
                  case 6:$col="stambeno_poslovna";break;
                  case 7:$col="horizontala_kuce";break;
                  case 8:$col="vertikala_kuce";break;
                  case 9:$col="seoska_kuca";break;
                  case 10:$col="salas";break;
              }
              
             $this->db_data[$table]=[ $col=>1,];
         }
     }
      
      */
      
      /*
     protected function nacin_placanja() {
         $table='nacin_placanja_izdavanje';
         if ($np=  $this->is_set('nacin_placanja')) {
             switch ($np){
                  case 1:$col="jedan";break;
                  case 2:$col="tri";break;
                  case 3:$col="sest";break;
                  case 4:$col="dvanest";break;                
             }
             $this->db_data[$table]=[
                // 'id_oglasi_nk'=>$this->oglasi_id,
                 $col=>1,
             ];
         }
         
     }
      
         protected function namestenost() {
         $table='namestenost';
         if ($na=  $this->is_set('namestenost')) {
             switch ($na){
                  case 1:$col="namesten";break;
                  case 2:$col="polunamesten";break;
                  case 3:$col="prazan";break;                
             }
             $this->db_data[$table]=[
                // 'id_oglasi_nk'=>$this->oglasi_id,
                 $col=>1,
             ];
         }
         
     }  
      */
      
      /*
      protected function opis_objekta() {
         $table='opis_objekta';
         if ($na=  $this->is_set('opis_objekta')) {
             switch ($na){
                  case 1:$col="izvorno_stanje";break;
                  case 2:$col="renoviran";break;
                  case 3:$col="lux";break;     
                  case 4:$col="salonski";break;
                  case 5:$col="duplex";break;
                  case 6:$col="za_renoviranje";break;
             }
             $this->db_data[$table]=[
                 //'id_oglasi_nk'=>$this->oglasi_id,
                 $col=>1,
             ];
         }
     }  
      */
      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
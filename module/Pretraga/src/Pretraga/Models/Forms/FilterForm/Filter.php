<?php

namespace Pretraga\Models\Forms\FilterForm;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Application\Models\LokacijaSr\Lokacija as Lok;
/**
 * Description of Filter
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class Filter extends \Zend\InputFilter\InputFilter{
    
    
    
    
    
    public function __construct() {
 

         
          #kategorija    
        $this->add(array(
            'name'=>'kategorija',
            'allow_empty' => true, 
            'requeried'=>true,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
                                       [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Katgorija je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 18,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------       
        
        
    $this->add(array(
            'name'=>'prodaja_izdavanje',
            'requeried'=>true,
            //'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 2,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------        
        
    #mesta_srbija    
        $this->add(array(
            'name'=>'mesta_srbija',
            'requeried'=>true,
          'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => [
                   [
                      'name' => 'Between',
                      'options' => [
                         'min' => 0,
                         'max' => count(Lok::get_mesta_srbija()),
                       ]
                   ]
               ]
                 ));
   #---------------------------------------------------------------------  
  /*      
         #---------------------------------------------------------------------    
        
           #deo_mesta     
        $this->add(array(
            'name'=>'deo_mesta',
           'requeried'=>true,
          'allow_empty' => true, 
            'filters'  => array(
                        array('name' => 'Int'),
                    ),
               'validators' => [
                   [
                      'name' => 'Between',
                      'options' => [
                         'min' => Lok::get_deo_mesta_range()['min'],
                         'max' => Lok::get_deo_mesta_range()['max'] ,
                       ]
                   ]
               ]
                 ));
   #---------------------------------------------------------------------   
        
             
        
 #lokacija    
        $this->add(array(
            'name'=>'lokacija',
           'requeried'=>true,
          'allow_empty' => true, 
            'filters'  => array(
                     array('name' => 'Int'),
                    ),
               'validators' => [
                   [
                      'name' => 'Between',
                      'options' => [
                         'min' => Lok::get_deo_lokacija_range()['min'],
                         'max' => Lok::get_deo_lokacija_range()['max'] ,
                       ]
                   ]
               ]
                 ));
   #---------------------------------------------------------------------       
        
        
   #ulica   
        $this->add(array(
            'name'=>'ulica',
        'required'=> true,
        'allow_empty' => true,
            
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------      
       
        
        */
               
        $this->add(array(
            'name'=>'samo-sa-slikom',
            'requeried'=>true,
            //'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
        
         
        
                $this->add(array(
            'name'=>'cena-od',
            'requeried'=>true,
            'allow_empty'=>TRUE,
       'validators'=>[
           [
               'name'=>'Zend\Validator\Digits',
           ]
       ]
                 ));
   #---------------------------------------------------------------------  
        
        
              $this->add(array(
            'name'=>'cena-do',
            'requeried'=>true,
            'allow_empty'=>TRUE,
       'validators'=>[
           [
               'name'=>'Zend\Validator\Digits',
           ]
       ]
                 ));
   #---------------------------------------------------------------------    
        
        
                $this->add(array(
            'name'=>'kvadratura-od',
            'requeried'=>true,
            'allow_empty'=>TRUE,
       'validators'=>[
           [
               'name'=>'Zend\Validator\Digits',
           ]
       ]
                 ));
   #---------------------------------------------------------------------  
        
        
        
                $this->add(array(
            'name'=>'kvadratura-do',
            'requeried'=>true,
            'allow_empty'=>TRUE,
       'validators'=>[
           [
               'name'=>'Zend\Validator\Digits',
           ]
       ]
                 ));
   #---------------------------------------------------------------------  
        
        
   $this->add([
       'name'=>'sobe_od',
       'required'=>true,
       'allow_empty'=>true,
       'validators'=>[
           [
               'name'=>'\Zend\Validator\InArray',
               'options'=>[
                   'haystack'=>['0.5','1','1,5','2','2.5','3']
               ]
           ]
       ]
    ]);
   $this->add([
       'name'=>'sobe_do',
       'required'=>true,
       'allow_empty'=>true,
       'validators'=>[
           [
               'name'=>'InArray',
               'options'=>[
                   'haystack'=>['0.5','1','1,5','2','2.5','3'],
                  // 'strict'   => true,
               ]
           ]
       ],

    ]);             
                
        
            
           function validate_chbx($name){
               return [
                        'name'=>$name,
                       'requeried'=>true, 
                       'allow_empty'=>true,
                       'filters'  => [
                         // ['name' => 'Int']
                        ],
                       'validators' => [
                           [
                                'name' => 'Between',
                                'options' => [
                                    'min' => 0,
                                    'max' => 1,
                                ]
                           ]
                       ]
                             
               ];
           }
   
           
           $chbx=[
               'namesten','polunamesten','prazan','banka','mesecno','tri_meseca','sest_meseci','godisnje','cela_kuca',
               'duplex_viseporodicne_kuce','vila','montazna_kuca','stambeno_poslovna_kuca', 'horizontala_kuce',
                'vertikala_kuce','prizemna_kuca','spratna_kuca', 'dozvoljena_gradnja','zapoceta_gradnja', 'deljiv',
                'ha_ar','plac' ,'njiva','vocnjak','vinograd','suma',
               ];
           foreach ( $chbx as $v) {
               $this->add(validate_chbx($v));
           }
           
           
           
    
              
 #sprat
    $this->add(array(
            'name'=>'sprat',
            'requeried'=>true,
            'allow_empty' => true, 
             'validators' => array(
              array('name'=>'OstaviOglasForm\Models\ValidatorSprat'),
              ),
            ));
   #---------------------------------------------------------------------  
    
     
             
        
#ukupno_spratova
    $this->add(array(
            'name'=>'ukupno_spratova',
            'requeried'=>true,
            'allow_empty' => true, 
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 30,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
    
    
   
                
                
                
                
                
                
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

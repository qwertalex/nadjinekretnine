<?php

namespace Pretraga\Models\Forms\FilterForm;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of Form
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class Form extends \Zend\Form\Form{
    
    
    
      public function __construct() {
       
      
        parent::__construct('oglasi-filter');//ime forme
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'get');
        $this->setAttribute('id', 'form-fiter');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setInputFilter(new \Pretraga\Models\Forms\FilterForm\Filter());//filter
        
         

                
#kategorija--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'kategorija',
             'attributes' => array(
               'id' => 'select_kategorija',
              'multiple'=>'multiple',
               'class'=>'select-submit',
                 'data-detect'          =>'kategorija'
            ),
   
             
     ));
      #----------------------------------------- 
     #-----------------------------------------   
        
                         
                
        #  radio buttons---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'prodaja_izdavanje',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje',
              //  "id"=>"radio_prodaja_izdavanje"
                  ),
                 'options' => array(
                  'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'Prodaja',
                            2=> 'Izdavanje',
                     ),
             ),
                 'attributes' => array(
               // 'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #----------------------------------------- 
        
    
       
#mesto--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'mesta_srbija',
           'attributes'=>array(
               "id"=>"mesta_srbija",
               'class'=>'select-submit',
               'style'=>'width:100%',
               'data-detect'            =>'mesto',
               //'value'=>'1'
           ),
           
     ));
      #-----------------------------------------        
   
  #sa slikom--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'samo-sa-slikom',
            'attributes'=>array(
          'id'=>'samo-sa-slikom'
               
                  ),
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
           
     ));
      #-----------------------------------------     
       
       
       
      $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'sobe_od',
             'attributes'=>[
               'style'=>'width:45%'  ,
               //  'class'=>'select-submit'
             ],
             'options' => array(
                     'label' => 'Which is your mother tongue?',
                     'empty_option'=>'od',
                     'value_options' => ['1'=>1,2,3,4,'5'=>'5+'],
             )
     ));
       
       
          $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'sobe_do',
              'attributes'=>[
               'style'=>'width:45%'  ,
                 // 'class'=>'select-submit'
             ],             
             'options' => array(
                     'label' => 'Which is your mother tongue?',
                     'empty_option'=>'do',
                     'value_options' => ['1'=>1,2,3,4,'5'=>'5+'],
             )
     ));   
                
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'cena_od',
            'attributes'=>array(
                'type'=>'text',
           'placeholder'=>"od",
                'style'=>"width:45%;height:30px;border:1px solid silver;"
            ),
           
            
        ));
     #-----------------------------------------   
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'cena_do',
            'attributes'=>array(
                'type'=>'text',
            'placeholder'=>"do",
               'style'=>"width: 45%;height:30px;border:1px solid silver;"          
            ),
           
            
        ));
     #-----------------------------------------   
       
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'kvadratura_od',
            'attributes'=>array(
                'type'=>'text',
            'placeholder'=>"od",
               'style'=>"width: 45%;height:30px;border:1px solid silver;"       
            ),
           
            
        ));
     #-----------------------------------------   
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'kvadratura_do',
            'attributes'=>array(
                'type'=>'text',
             'placeholder'=>"do",
             'style'=>"width: 45%;height:30px;border:1px solid silver;"         
            ),
           
            
        ));
     #-----------------------------------------   
       
        
        
        
        
        
        

        function create_chbx($name,$v=1,$data=false){
            $attributes=[
               'class'=>'chbx-submit' 
            ];
            if ($data) {
                $attributes['data-detect']=$data;
            }
                    
            return [
              'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> $name,
             'attributes'=>$attributes,
                 'options' => [
                     'use_hidden_element' => true,
                     'checked_value' =>$v,
                     'unchecked_value' => 0
                 ]               
            ];
        }
        
        
        
        
        
        /*
         * 
         * 
         * 
         * oglasivac
         * 
         *                             1=>'Vlasnik',
                            2=>'Agencija',
                            3=>'Investitor',
                            4=>"Banka"
         * 
         * 
         */
    
        
          #cg--------------------------------        
       $this->add(create_chbx('vlasnik',1,'oglasivac'));
      #----------------------------------------- 
       $this->add(create_chbx('agencija',1,'oglasivac'));
      #-----------------------------------------        
              $this->add(create_chbx('investitor',1,'oglasivac'));
      #-----------------------------------------   
                     $this->add(create_chbx('banka',1,'oglasivac'));
      #-----------------------------------------   
        
        
        /*
         * 
         * IZDAVANJE
         * 
         * 
         */
        $n=1;
        //namestenost
        $this->add(create_chbx('namesten',1,'namestenost'));
        #----------------------------------------
                $this->add(create_chbx('polunamesten',1,'namestenost'));
        #----------------------------------------
                        $this->add(create_chbx('prazan',1,'namestenost'));
        #----------------------------------------
        $n=1;
        //nacin placanja
         $this->add(create_chbx('mesecno'));
        #----------------------------------------
                $this->add(create_chbx('tri_meseca'));
        #----------------------------------------
                        $this->add(create_chbx('sest_meseci'));
        #----------------------------------------       
                                                $this->add(create_chbx('godisnje'));
        #----------------------------------------       
        
        
        
        
        
        
          
       /*
        * 
        * 
        * Grejanje
        * 
        * 
        * 
        * 
        * 
        */       
     $n=1;
        
          #cg--------------------------------        
       $this->add(create_chbx('cg',1,'grejanje'));
      #----------------------------------------- 
       $this->add(create_chbx('eg',1,'grejanje'));
      #----------------------------------------- 
              $this->add(create_chbx('ta',1,'grejanje'));
      #----------------------------------------- 
                     $this->add(create_chbx('gas',1,'grejanje'));
      #----------------------------------------- 
                            $this->add(create_chbx('podno',1,'grejanje'));
      #----------------------------------------- 
                                   $this->add(create_chbx('kaljeva',1,'grejanje'));
      #----------------------------------------- 
                                          $this->add(create_chbx('mermerniradiatori',1,'grejanje'));
      #----------------------------------------- 
                                                 $this->add(create_chbx('norveskiradiatori',1,'grejanje'));
      #----------------------------------------- 
   
        
        
        
        /*
         * 
         * opis objekta
         *      
         */ $n=1;
        
       $this->add(create_chbx('izvorno_stanje',1,'opis-objekta'));
      #-----------------------------------------  
              $this->add(create_chbx('renoviran',1,'opis-objekta'));
      #-----------------------------------------   
                     $this->add(create_chbx('lux',1,'opis-objekta'));
      #-----------------------------------------   
                            $this->add(create_chbx('salonski',1,'opis-objekta'));
      #-----------------------------------------   
                                   $this->add(create_chbx('duplex',1,'opis-objekta'));
      #-----------------------------------------   
                                          $this->add(create_chbx('za_renoviranje',1,'opis-objekta'));
      #-----------------------------------------   
                                          
                                          
          /*
           * 
           * tip objekta
           * 
           * 
           */                                
                                          
              $n=1;
        
       $this->add(create_chbx('stara_gradnja',1,'tip-objekta'));                            
       #-----------------------------------------      
              $this->add(create_chbx('novogradnja',1,'tip-objekta'));                            
       #-----------------------------------------        
                     $this->add(create_chbx('u_izgradnji',1,'tip-objekta'));                            
       #-----------------------------------------        
                                          
             
                     
                     
             /*
              * 
              * 
              * dodatno
              * 
              * 
              */        
                     $n=1;                              
                                          
                                          
         $this->add(create_chbx('depozit',1,'dodatno'));                            
       #-----------------------------------------    
                  $this->add(create_chbx('odmah_useljiv',1,'dodatno'));                            
       #-----------------------------------------  
                           $this->add(create_chbx('uknjizen',1,'dodatno'));                            
       #-----------------------------------------  
                                    $this->add(create_chbx('zamena',1,'dodatno'));                            
       #-----------------------------------------  
                                             $this->add(create_chbx('za_pusace',1,'dodatno'));                            
       #-----------------------------------------  
                                                      $this->add(create_chbx('za_nepusace',1,'dodatno'));                            
       #-----------------------------------------  
                                                               $this->add(create_chbx('klima',1,'dodatno'));                            
       #-----------------------------------------  
                                                                        $this->add(create_chbx('kablovska',1,'dodatno'));                            
       #-----------------------------------------  
                                                                                 $this->add(create_chbx('internet',1,'dodatno'));                            
       #-----------------------------------------  
        $this->add(create_chbx('lift',1,'dodatno'));                            
       #-----------------------------------------  
             $this->add(create_chbx('podrum',1,'dodatno'));                            
       #-----------------------------------------  
                 $this->add(create_chbx('topla_voda',1,'dodatno'));                            
       #-----------------------------------------  
                    $this->add(create_chbx('telefon',1,'dodatno'));                            
       #-----------------------------------------  
                        $this->add(create_chbx('interfon',1,'dodatno'));                            
       #-----------------------------------------  
                             $this->add(create_chbx('garaza',1,'dodatno'));                            
       #-----------------------------------------  
                                 $this->add(create_chbx('parking',1,'dodatno'));                            
       #-----------------------------------------  
                                    $this->add(create_chbx('terasa',1,'dodatno'));                            
       #-----------------------------------------  
                                          
                   
                                    
                 /*
                  * 
                  * 
                  * KUCE
                  * 
                  * 
                  *                  
                  */                   
                                    
                      //vrsta kuce
                      $this->add(create_chbx('cela_kuca',1,'vrsta-kuce'));
                      #-----------------------------------------
                     $this->add(create_chbx('duplex_viseporodicne_kuce',1,'vrsta-kuce'));
                      #-----------------------------------------
                       $this->add(create_chbx('vila',1,'vrsta-kuce'));
                      #-----------------------------------------
                        $this->add(create_chbx('montazna_kuca',1,'vrsta-kuce'));
                      #-----------------------------------------
                       $this->add(create_chbx('stambeno_poslovna_kuca',1,'vrsta-kuce'));
                      #-----------------------------------------
                        $this->add(create_chbx('horizontala_kuce',1,'vrsta-kuce'));
                      #-----------------------------------------
                        $this->add(create_chbx('vertikala_kuce',1,'vrsta-kuce'));
                      #-----------------------------------------
                         //spratnost kuce           
                                    
                         $this->add(create_chbx('prizemna_kuca',1,'spratnost-kuce'));
                      #-----------------------------------------
                        $this->add(create_chbx('spratna_kuca',1,'spratnost-kuce'));
                      #-----------------------------------------                                   
                    /*
                     * /kuce
                     */                
                                    
                  /*
                   * 
                   * 
                   * PLAC
                   * 
*/                 
                      //ostalo plac
                   $this->add(create_chbx('dozvoljena_gradnja',1,'ostalo-plac'));
                    $this->add(create_chbx('zapoceta_gradnja',1,'ostalo-plac'));
                     $this->add(create_chbx('deljiv',1,'ostalo-plac'));
                                    
        #plac povrsina-------------------------------------         
        $this->add(array(
            'name'=>'plac_povrsina_od',
            'attributes'=>array(
                'type'=>'text',
             "style"=>"width:45%;border:1px solid silver;height:30px",
                'placeholder'=>'povrsina do',
                
            ),
       ));
     #----------------------------------------- 
         #plac povrsina-------------------------------------         
        $this->add(array(
            'name'=>'plac_povrsina_do',
            'attributes'=>array(
                'type'=>'text',
             "style"=>"width:45%;border:1px solid silver;height:30px",
                'placeholder'=>'povrsina do',
                
            ),
       ));
     #-----------------------------------------  
    
        
        
        
        
        
      #-----------------------------------------                            
                    //vrsta_plac
       /*
        *                         1=> 'plac',
                            2=> 'njiva',
                            3=>'vocnjak',
                            4=> 'vinograd',
                            5=>'suma',
        */
                                    
            $this->add(create_chbx('plac',1,'vrsta-plac'));
            $this->add(create_chbx('njiva',1,'vrsta-plac'));
            $this->add(create_chbx('vocnjak',1,'vrsta-plac'));
            $this->add(create_chbx('vinograd',1,'vrsta-plac'));
            $this->add(create_chbx('suma',1,'vrsta-plac'));
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
                                    
               
#sprat--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'sprat',
             'attributes'=>[
                 'style'=>"width:100px;height:30px;border:1px solid silver;",
              //   'class'=>'select-submit'
             ],
             'options' => array(
                     'label' => 'Which is your mother tongue?',
                    'empty_option' => '--od--',
                     'value_options' => array('1'=>1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                         21,22,23,24,25,26,27,28,29,30
                      ),
             )
     ));
      #-----------------------------------------   
       
       
 #ukupan broj spratova-------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'ukupno_spratova',
             'attributes'=>[
                 'style'=>"width:100px;height:30px;border:1px solid silver;",
                // 'class'=>'select-submit'
             ],           
             'options' => array(
                  //   'label' => 'Which is your mother tongue?',
                    'empty_option' => '--do--',
                     'value_options' => ['1'=>1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                         21,22,23,24,25,26,27,28,29,30],
             )
     ));
      #-----------------------------------------    
       
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
                

       
        
        #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit-filters',
            'attributes'=>array(
                'type'=>'submit',
               'class'=>'btn btn-primary  arrow-right pull-right',
                'value'=>'Objavi oglas',
               'id'=>'filteri-form',
              // 'style'=>'display:none',
                
            ),
           ));
        
        #-----------------------------------------   
          
       
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}

<?php

namespace Pretraga\Controller;
error_reporting(E_ALL|E_STRICT);
ini_set('display_errors', 'on');
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

//local
use Pretraga\Models\Pretraga;
use Pretraga\Models\Forms\FilterForm\Form as FilterForm;
use Application\Models\AssetsVersion\AutoVersion as Ver;


class PretragaController extends AbstractActionController {

    
 



    public function indexAction() {var_dump('test');
        //this is for performance
        $this->layout()->setTemplate('layout/layout');
     # -------------------------------------------------------
     $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');  
     $renderer->headLink()->appendStylesheet('/libraries/font-awesome-4.1.0/css/font-awesome.min.css');
     $renderer->headLink()->appendStylesheet('/css/bootstrap-social.css');
     $renderer->headScript()->appendFile( '/libraries/select2/select2.min.js');
     $renderer->headLink()->appendStylesheet('/libraries/select2/select2.css');
     $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
     $renderer->headLink()->appendStylesheet('/libraries/icheck/line/blue.css');
     $renderer->headLink()->appendStylesheet('/libraries/icheck/blue.css');
     $renderer->headScript()->appendFile( '/libraries/notify/jquery.noty.packaged.js');
     $renderer->headScript()->appendFile(Ver::pretraga_js()/*'/js/pretraga.lista.js'*/);
   #-----------------------------------------------------------------------------------------------------       
        $request=  $this->getRequest();
         $get=$this->params()->fromRoute();
        //var_dump($get);
        $Pretraga=new Pretraga($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'),$get);
       // $Filteri=new Filter($get);
        $FilterForm=new FilterForm();
    
               /*----------------------------------
          * paginacija
          ----------------------------------*/
      if ($this->params()->fromRoute('po-strani')) {
    if ( $this->params()->fromRoute('po-strani')==10||
                $this->params()->fromRoute('po-strani')==20||
                        $this->params()->fromRoute('po-strani')==50){
                        $po_strani=$this->params()->fromRoute('po-strani'); 
            }
           else{ $po_strani=10; }
         }else{$po_strani=10;}
          
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($Pretraga->get_ads()));
        $paginator->setCurrentPageNumber($this->params()->fromRoute('strana'))
                 ->setItemCountPerPage($po_strani);
      
       /*-------------------------------------------------
        * end paginaicja
       -------------------------------------------------- */     
          //dinamically set form data; zato sto se ne validira sa
        // inoputFilter nego se validira u module.config(routes)
           $FilterForm->setData($Pretraga->set_form_values());
          
           
         // var_dump($Pretraga->call_counter()['slike']);
           
           
           
           
           
           
           
           
 //  $counter=$Pretraga->call_counter();
      /**
       * 
       * 
       * 
       * 
       * HELPER AND VIEWMODEL
       * 
       * 
       * 
       * 
       */  
      unset($get['controller']);
      $get['action']='pretraga-oglasa';
      
         $viewModel = new ViewModel();
    
         $lista_oglasa=new ViewModel([
            'paginator' =>$paginator,
             'get'      =>$get,
             'render_html'  => new \Application\Models\Oglasi\Render\RenderView(),
             'cookie'       =>!isset($request->getCookie()->favoriti)?false:$request->getCookie()->favoriti,
            
        ]);
        $lista_oglasa->setTemplate("listaOglasa");        
        $viewModel->addChild($lista_oglasa,"lista_oglasa");
        //var_dump($get);
         $sidebar=new ViewModel([
           'form'   =>$FilterForm,
           'get'    =>$get,
           'counter'=> $Pretraga->call_counter(),
         ]);
        $sidebar->setTemplate("sidebar");        
        $viewModel->addChild($sidebar,"sidebar");         
        return $viewModel;
    }
    
     
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function mapaAction() {
               
          
     $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');   
     $renderer->headScript()->appendFile( '/libraries/select2/select2.min.js');
     $renderer->headLink()->appendStylesheet('/libraries/select2/select2.css');
     $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
     $renderer->headLink()->appendStylesheet('/libraries/icheck/minimal/_all.css');       
     $renderer->headScript()->appendFile( '/js/pretraga.js');  
        
        
        
         $request =$this->getRequest();
        
      
         $oglasi=  $this->db_oglasi();
        // $oglasi->get_oglasi();
         
         $oglasi->set_params([
             'kategorija'=>$this->params('kategorija_val'),
             'prodaja_izdavanje'=>$this->params('prodaja_izdavanje_val'),
             'lokacija'=>$this->params('lokacija_val'),
             'sa_slikom'=>$this->params('sa_slikom_val'),
             'cena_do'=>$this->params('cena_do_val'),
             'kvadratura_do'=>$this->params('kvadratura_do_val'),
             'po_strani'=>$this->params('po_strani_val'),
             
         ]);

         
        var_dump($this->params('prodaja_izdavanje_val'));
         
         $viewModel = new ViewModel();
    
               //change layout
         $layout = $this->layout();
         $layout->setTemplate('layout/pretraga');
         //--------------------------------------      
        return $viewModel;
    }
    
    
    
    
    
    
}



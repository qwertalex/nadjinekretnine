<?php
return array(
  
'controllers' => array(
     'invokables' => array(
'Pretraga\Controller\Index' => 'Pretraga\Controller\PretragaController',
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'oglasi-mapa' => array(
        'type'    => 'segment',
        'options' => array(
           'route'    => '/pretraga-oglasa'
            . '/:kategorija'
            . '/:prodaja_izdavanje'
            . '/:lokacija'
            . '[/:sa-slikom]'
            . '[/cena/:cena-val]'
            . '[/kvadratura/:kvadratura-val]'
            . '[/broj-soba/:sobe-val]'
            . '[/namestenost/:namestenost-val]'
            . '[/grejanje/:grejanje-val]'
            . '[/opis-objekta/:opis-objekta-val]'
            . '[/tip-objekta/:tip-objekta-val]'
            . '[/dodatno/:dodatno-val]'
            . '[/vrsta-kuce/:vrsta-kuce-val]'
            . '[/spratnost-kuce/:spratnost-kuce-val]'
            . '[/ostalo-plac/:ostalo-plac-val]'
            . '[/vrsta-plac/:vrsta-plac-val]'
            . '[/povrsina-plac/:povrsina-plac-val]'
            . '[/spratovi/:spratovi-val]'
            . '[/oglasivac/:oglasivac-val]'
            . '[/strana/:strana]'
            . '[/po-strani/:po-strani]',
 
          
           'constraints' => [
              'kategorija'              => 'stanovi|kuce|sobe|garaze|placevi|dnevno-izdavanje|lokali|poslovni-prostori|hale|kiosci|stovarista|magacin|poljoprivredno-zemljiste|gradjevinsko-zemljiste|vikendice|apartmani|sobe|selidbe',
              'prodaja_izdavanje'       => 'prodaja|izdavanje',
              'lokacija'                => '[a-z\-\_0-9]+',
              'sa-slikom'               => 'samo-sa-slikom',
              'cena'                    => 'cena',
              'cena-val'                =>'[0-9\-]+',
              'kvadratura'              => 'kvadratura',
              'kvadratura-val'          => '[0-9\-]+',
              'broj-soba'               =>'broj-soba',
               'sobe-val'               =>'[\d\-]+',
               'namestenost'            =>'naemstenost',
               'namestenost-val'        =>'[namesten|polunamesten|prazan\-]+',
               'grejanje'               =>'grejanje',
               'grejanje-val'           =>'[a-z\-]+',
               'opis-objekta'           =>'opis-objekta',
               'opis-objekta-val'       =>'[a-z\-\_]+',
               'tip-objekta'            =>'tip-objekta',
               'tip-objekta-val'        =>'[a-z\-\_]+',
               'dodatno'                =>'dodatno',
               'dodatno-val'            =>'[a-z\-\_]+',
               'vrsta-kuce'             =>'vrsta-kuce',
               'vrsta-kuce-val'         =>'[a-z\-\_]+',
               'spratnost-kuce'         =>'spratnost-kuce',
               'spratnost-kuce-val'     =>'[a-z\-\_]+',
               'ostalo-plac'            =>'ostalo-plac',
               'ostalo-plac-val'        =>'[a-z\-\_]+',
               'vrsta-plac'             =>'vrsta-plac',
               'vrsta-plac-val'         =>'[a-z\-\_]+',
               'povrsina-plac'          =>'povrsina-plac',
               'povrsina-plac-val'      =>'[\d\-]+',
               'spratovi'               =>'spratovi',
               'spratovi-val'           =>'[\d\-]+',
               'oglasivac'              =>'oglasivac',
               'oglasivac-val'          =>'[vlasnik|agencija|investitor|banka\-]+',
                'strana'                  => '[0-9]+',
              'po-strani'               => '[a-zA-Z0-9_-]+',
            ],
            'defaults' => array(
               'controller' => 'Pretraga\Controller\Index',
               'action'     => 'index',
               'strana' => 1,
                
            ),
        ),
     ),
  ),
        
        
        
        
        
        
        
        
        
),
    
    'view_manager' => array(
        'template_map'=>[
            'listaOglasa'             =>__DIR__ . '/../view/layout/helpers/lista-oglasa.phtml',
            'sidebar'                 =>__DIR__ . '/../view/layout/helpers/sidebar.phtml',
            'paginacija'                 =>__DIR__ . '/../view/layout/helpers/paginacija.phtml',
            'form_filters_lista_oglasa'                 =>__DIR__ . '/../view/layout/helpers/form-filters-lista-oglasa.phtml',
            
            
        ],
         'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    
);













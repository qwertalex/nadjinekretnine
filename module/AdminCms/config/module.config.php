<?php





return array(
  
'controllers' => array(
     'invokables' => array(
'AdminCms\Controller\Index' => 'AdminCms\Controller\AdminCmsController',# <------HERE
          // <----- Module Controller
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'admincms' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/admin-cms[/:action][/:id][/page/:page]',# <------HERE
           'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              'id'     => '[0-9]+',
               'page'     => '[0-9]+',
            ),
            'defaults' => array(
               'controller' => 'AdminCms\Controller\Index',# <------HERE
               'action'     => 'index',
               'page' => 1,
            ),
        ),
     ),
  ),
),

    
    
           'view_manager' => array(
    'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
     'template_map' => array( 
             'admin/layout'                => __DIR__ . '/../view/layout/layout.phtml',
          'admin/error/404'               => __DIR__ . '/../view/error/404.phtml',//nazalost ovaj admin panels 404 se ne koristi
         'bulk_paginacija_buttons'                 =>__DIR__ . '/../view/admin-cms/admin-cms/helpers/paginator_buttons.phtml',
        ),
    ),
    
    
    
    
    
    
    
    
);













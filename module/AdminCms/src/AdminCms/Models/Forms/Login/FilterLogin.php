<?php

namespace AdminCms\Models\Forms\Login;

//use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class FilterLogin extends InputFilter
{
   
    public function __construct() {
        
        
        
              
         
          #username   
        $this->add(array(
            'name'=>'user_id',
            'requeried'=>true,
            'allow_empty'=>false,
               'validators' => array(
                   [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'ID korisnika nije validan.' 
                            ],
                        ],
                     ], 
                   
                   [
                       'name'       =>'Zend\Validator\EmailAddress',
                       'break_chain_on_failure' => true,
                       'options'    =>[
                           'messages'   =>[
                               \Zend\Validator\EmailAddress::INVALID_FORMAT=>'ID korisnika nije validan.'
                           ]
                       ]
                   ],
                   
                   [
                       'name'       =>'Zend\Validator\InArray',
                       'break_chain_on_failure' => true,
                       'options'            =>[
                           'haystack'   =>['alexa.manasijevic85@gmail.com','goxy85@yahoo.com']
                       ]
                   ]
                   
                   
                   
            ),
                 ));
   #---------------------------------------------------------------------      
        
        
         
        
            $this->add(array(
            'name'=>'password',
            'required'=>true,
            'validators'=>array(
                  [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Lozinka je obavezno!' 
                            ],
                        ],
                     ], 
                array(
                    'name'=>'string_length',
                    'options'=>array(
                         'encoding' => 'UTF-8',
                         'min'=>5,
                         'max' => 32,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Polje lozinka mora da sadrzi minimum 5 karaktera!', 
                                 'stringLengthTooLong' => 'Polje lozinka mora da sadrzi najvise 32 karaktera!' 
                            ),
                    ),
                ),
            )
            
        ));    
        
        
      
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
}




<?php

namespace AdminCms\Models\Forms\Login;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Form\Form;
/**
 * Description of Login
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class Login extends Form{
    
    public function __construct() {
        
          parent::__construct('adminLogin');//ime forme
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setInputFilter(new \AdminCms\Models\Forms\Login\FilterLogin());//filter
        
        
        
                     
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'user_id',
            'attributes'=>array(
                'type'=>'text',
                'class'     =>'form-control',
                'placeholder'   =>'User ID'
            ),
           
            
        ));
     #-----------------------------------------   
                
                         
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'password',
            'attributes'=>array(
                'type'=>'password',
             'class'     =>'form-control',
                'placeholder'   =>'Password'
            ),
           
            
        ));
     #-----------------------------------------       
                
                
                #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'submit',
                'class'=>'btn bg-olive btn-block',
                'value'=>'Login',
            
               
            ),
           ));
        
        #----------------------------------------- 
                
                
    }
    
    
    
    
}

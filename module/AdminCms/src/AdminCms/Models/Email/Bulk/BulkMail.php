<?php

namespace AdminCms\Models\Email\Bulk;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
 use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;

//local
use Application\Newsletters\Templates\BulkMail as Template;
use AdminCms\Models\Email\SendMailAdmin as Conf;

/**
 * Description of BulkMail
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class BulkMail {
    
    protected $adapter;
    protected $template="bulk-template-V1.0";
    protected $exclude_email=[
      'mojkvadrat.rs','b92.net','nekretnine.rs',  
    ];
    protected $exclude_time="7 DAYS";
    
    
    
    
    
    public function __construct($adapter) {
        $this->adapter=$adapter;
      //  var_dump($this->get_emails());
      //var_dump($this->cont_sent_messages());
    }
    
    
    
    protected function load_template($tmp){
        
        switch($tmp):
            case "bulk-template-V1.0":return Template::$templates['1.0'];
        endswitch;
        
        return false;
        
        
    }
    
    
    
    
    public function send_bulk_messages($template){
        $email =array_filter($this->get_emails(),'trim');
    
       //  $email=['qwertalexa@yahoo.com','alexa.manasijevic85@gmail.com'];
         $template= $this->load_template($template);
         $subject="Besplatno postavite vas oglas!";
         $email_validator = new \Zend\Validator\EmailAddress();    
        
         foreach($email as $v):
            if (!$email_validator->isValid($v)) { continue; }
            
            
          //  var_dump($v);
            
         $html = new MimePart($template);
         $html->type = "text/html";
         $body = new MimeMessage();
         $body->setParts(array($html));
         
         $message = new Message();
         $message->setEncoding("UTF-8");
         $message->setBody($body);

         $message->addTo($v)
        ->addFrom(Conf::INFO)
        ->setSubject($subject);     



    
// Setup SMTP transport using LOGIN authentication
$transport = new SmtpTransport();
 $options   = new SmtpOptions(array(
    'name'              => Conf::HOST,
    'host'              => Conf::SMTP,
    'port' => 80,
    'connection_class'  => 'login',
    'connection_config' => array(
        'username' => Conf::INFO,
        'password' => Conf::PASS,
    ),
)); 
    $transport->setOptions($options);
    $transport->send($message);
   $this->sent_emails($v);

 endforeach;

        
        
        
        
    }
    
    
    
    protected function get_emails(){ 
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select('crawler_bulk_mails');
        $select->columns(['email']);
        $where=new \Zend\Db\Sql\Where();
        $where->lessThan('datetime',date('Y-m-d H:i:s',(strtotime ( '-7 days' , strtotime ( date("Y-m-d H:i:s")) ) )));
        $select->where([
             $where,
            'subscribe' =>1
        ]);
        
        $statement=$sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {     
          return $this->remove_unwonted_emails(array_values(iterator_to_array($results)));
         }
         return [];
        
    }
    
    
    
    /**
     * 
     * @param array $emails
     * @return type
     * 
     * 
     * Radi remove emails koji su nepozeljni kao npr.
     * ako spamujem mojkvadrat.rs necu da saljem njima email na podrska@mojkvadrat.rs
     * 
     * 
     */
    protected function remove_unwonted_emails(Array $emails){
        $return=array_map(function($v){
            return $v['email'];
        },$emails);
            
         foreach($return as $k=>$v):
             foreach ($this->exclude_email as $exc):
                 if (strpos($v,$exc)!==false) {
                     unset($return[$k]);
                 }
             endforeach;
         endforeach;
      return $return;
        
    }
    
    
    
    
    protected function sent_emails($email){
        $date=date("Y-m-d H:i:s");
        $sql = "UPDATE `NodeBot`.`crawler_bulk_mails` SET `datetime` ='$date' , `send_number` =send_number+1 WHERE `email` = '$email';";
       $resultSet = $this->adapter->query($sql,\Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);

       }
    
        public function get_send_emails(){ 
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select('crawler_bulk_mails');
         $statement=$sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {     
          return array_values(iterator_to_array($results));
         }
         return [];
        
    }
    
    
    
    
    public function cont_sent_messages(){
        $sql = "select count(send_number) as count from crawler_bulk_mails where send_number>0;";
       $resultSet = $this->adapter->query($sql,\Zend\Db\Adapter\Adapter::QUERY_MODE_EXECUTE);
       return array_values(iterator_to_array($resultSet ))[0];
     }
    
    
    
    
    
    
    
 
    
    
    
    
    
    
    
}

<?php

namespace AdminCms\Models\Email;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
 use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;

//local
use Application\Newsletters\Templates\IndexTemplate;
/**
 * Description of SendMail
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class SendMailAdmin {
    
    protected $adapter;
    protected $post;
    
    
    //email setings
    const INFO="info@nadjinekretnine.com";
    const PODRSKA="podrska@nadjinekretnine.com";
    const MARKETNING="marketing@nadjinekretnine.com";
    const PASS='qwert';
    const SMTP='smtpout.secureserver.net';
    const HOST="nadjinekretnine.com";
    //------------------------------------------
    
    
    
    public function __construct($adapter,$post) {
        $this->adapter=$adapter;
        $this->post=$post;
    }
    
    protected function load_template($template){
        
        switch($template):
            case "prvi-kupac":return IndexTemplate::template();
            default:return false;
        endswitch;
        
    }
    
    public function send_mail($post){
       $email =array_filter(explode(",",$post['emailto']),'trim');
            
      
       $template=$this->load_template($post['select-template']);
       $msg=$post['message'];
       if (!$template&&$msg) {
           $template=$msg;
       }elseif(!$template&&!$msg){
           return false;
       }
         $email_validator = new \Zend\Validator\EmailAddress();    
    foreach($email as $v):
       
            if (!$email_validator->isValid($v)) {
               continue;
            }
            
            
            var_dump($v);
            
         $html = new MimePart($template);
         $html->type = "text/html";
         $body = new MimeMessage();
         $body->setParts(array($html));
         
         $message = new Message();
         $message->setEncoding("UTF-8");
         $message->setBody($body);

 $message->addTo($v)
        ->addFrom(self::INFO)
        ->setSubject($post['subject']);     



    
// Setup SMTP transport using LOGIN authentication
$transport = new SmtpTransport();
 $options   = new SmtpOptions(array(
    'name'              => self::HOST,
    'host'              => self::SMTP,
    'port' => 80,
    'connection_class'  => 'login',
    'connection_config' => array(
        'username' => self::INFO,
        'password' => self::PASS,
    ),
)); 
    $transport->setOptions($options);
    $transport->send($message);


 endforeach;

      
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
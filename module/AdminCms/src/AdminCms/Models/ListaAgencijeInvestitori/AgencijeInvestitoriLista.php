<?php

namespace AdminCms\Models\ListaAgencijeInvestitori;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of GetAll
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class AgencijeInvestitoriLista {
    
    protected $adapter;
    
    public function __construct($adapter) {
        $this->adapter=$adapter;
        
        
    }
    
    
    
    public function get_all(){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select('agencije_investitori_lista');
        
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
            return array_values(iterator_to_array($results));
            
         }
         return false;
        
        
    }
    
    
    public function insert_new($data){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $insert=$sql->insert('agencije_investitori_lista');
        $data['datetime']=date("Y-m-d H:i:s");
        unset($data['dodaj-novu-agenciju']);
        $insert->values($data);
        $statement = $sql->prepareStatementForSqlObject($insert);
         $results = $statement->execute();
        if ($results->count()>0) {
            return true;
            
         }
         return false;
    }
    
    
    
    public function delete_from_list($id){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $delete=$sql->delete('agencije_investitori_lista');
        $delete->where([
           'id_agencije_investitori_lista'=>$id 
        ]);
        $statement = $sql->prepareStatementForSqlObject($delete);
         $results = $statement->execute();
        if ($results->count()>0) {
            return true;
            
         }
         return false;
    }
    
    
        
    public function update_data($data){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $update=$sql->update('agencije_investitori_lista');
        $id=$data['id_agencije_investitori_lista_update'];
        unset($data['id_agencije_investitori_lista_update'],$data['agencija_update']);
          
         foreach($data as $k=>$v): 
           $data[str_replace("_update","", $k)]=$v;
           unset($data[$k]);
         endforeach;
      
        
        $update->set($data);
        $update->where([
           'id_agencije_investitori_lista'=>$id 
        ]);
        $statement = $sql->prepareStatementForSqlObject($update);
         $results = $statement->execute();
        if ($results->count()>0) {
            return true;
            
         }
         return false;
    }
    
    
    
    
    
    
    
    
     public function get_all_where($table,Array $where=[],$columns=false){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select($table);
        if ($columns) {
            $select->columns([$columns]);
        }
        if (count($where)>0) {
            $select->where($where);
        }
        
        
        
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
            return array_values(iterator_to_array($results));
            
         }
         return false;
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

<?php

namespace AdminCms\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
 

//local
use AdminCms\Models\Forms\Login\Login;
use AdminCms\Models\ListaAgencijeInvestitori\AgencijeInvestitoriLista as Lista;
use AdminCms\Models\Email\SendMailAdmin as Mail;
use AdminCms\Models\Email\Bulk\BulkMail;

class AdminCmsController extends AbstractActionController {

    



    public function indexAction() {
        $viewModel=new ViewModel();
        $layout=$this->layout();
        $layout->setTemplate('admin/layout');
      // $_SESSION=[];
       // var_dump($_SESSION);
      $get=$this->params()->fromQuery();
      if (isset($get['logout'])) {
          $auth = new AuthenticationService();
         $auth->setStorage(new SessionStorage('auth_admin_login'));
         if ($auth->hasIdentity()) {
            $auth->clearIdentity();
            $this->redirect()->toUrl('http://nadjinekretnine.com/admin-cms/login');
          
        }
       }
      return $viewModel;
      }
    
    
    
    
    public function loginAction(){
       $login=new Login();
     //   $_SESSION=[];
        $request=$this->getRequest(); 
         if ($request->isPost()){           
             $post= $request->getPost()->toArray();
           //  if (isset($post['user_id'],$post['password'])) {
                 $login->setData($post);
                 if ($login->isValid($post)) {
                     $User=  $this->getServiceLocator()->get('User');
                 	$post=$login->getData();
                          if ($data=$User->login_validation_email_or_username_pass($post['user_id'],$post['password'],true)) {
                             //   $User->createSession($data,'ac_login');
                             //    $this->redirect()->toUrl('http://nadjinekretnine.com/admin-cms');
                              
                              
                              $auth = new AuthenticationService();
                              $auth->setStorage(new SessionStorage('auth_admin_login'));
                              $result = $auth->authenticate($data);
                              $this->redirect()->toUrl('http://nadjinekretnine.com/admin-cms');
                                  
                             
                         }else{
                             $login->get('user_id')->setMessages(array('ID korisnika nije validan.'));
                         }
                   
                 }
           //  }
         }
         
        $viewModel=new ViewModel([
           'form'       =>$login
        ]);
        $viewModel->setTerminal(true);
        return $viewModel;
    }
    
     
    
    
    
    
            public function mailAction(){
                error_reporting(E_ALL);
ini_set('display_errors', true);
                $layout=$this->layout();
                $layout->setTemplate('admin/layout');
                $adapter=  $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $request=  $this->getRequest();
                
                $Lista=new Lista($adapter);
                $data=[
                  'agencije-lista'      =>$Lista->get_all_where('agencije_investitori_lista', [
                      'investitor_agencija'    =>'agencija'
                  ],'email'),
                  'investitor-lista'      =>$Lista->get_all_where('agencije_investitori_lista', [
                      'investitor_agencija'    =>'investitor'
                  ],'email'),  
                  'fizicka-lica'            =>$Lista->get_all_where('users_nk', ['user_oglasivac'=>1], 'email_nk'),
                  'pravna-lica'             =>$Lista->get_all_where('users_nk', ['user_oglasivac'=>2], 'email_nk'),
                ];
                
                $SendMail=new Mail($adapter,"");
                
                if ($request->isPost()) {
                    $post=$request->getPost()->toArray();
                    if (isset($post['send_mail'])) {
                         $SendMail->send_mail($post);
                    // var_dump($post);
                    }
                }
                
                
                
               
                
                 $viewModel=new ViewModel([
                   'data'       =>$data
               ]); 
                
                return $viewModel;
                
            }



            
            
            
            /**
             * 
             * @return ViewModel
             * 
             * 
             * 
             * 
             * 
             * 
             */
   public function agencijeInvestitoriAction(){
                $layout=$this->layout();
                $layout->setTemplate('admin/layout');
            
                $adapter=  $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
                $Lista=new Lista($adapter);
                $modals=[];
                $request=  $this->getRequest();
                if ($request->isPost()) {  
                  $post= $request->getPost()->toArray();  
                  if (isset($post['dodaj-novu-agenciju'])) {
                       if ($Lista->insert_new($post)) {
                           $modals['dodaj_agenciju']=true;
                       }else{
                           $modals['dodaj_agenciju']=false;
                       }   
                  }elseif(isset($post['id-oglas-delete'])&&is_numeric($post['id-oglas-delete'])){
                      $Lista->delete_from_list($post['id-oglas-delete']);
                  }elseif(isset($post['agencija_update'])){
                      $Lista->update_data($post);
                    
                  }
                  
                
                }
               $viewModel=new ViewModel([
                   'lista'      =>$Lista->get_all(),
                   'modals'     =>$modals,
                   
               ]); 
                
                return $viewModel;
                
            }





            public function bulkMailAction(){
                $layout=$this->layout();
                $layout->setTemplate('admin/layout');
                $adapter=  $this->getServiceLocator()->get('adapter2');
                $request=  $this->getRequest();
                $Bulk=new BulkMail($adapter);
                if ($request->isPost()) {
                    $post= $request->getPost()->toArray();
                    $Bulk->send_bulk_messages($post['bulk-template']);
                }
                
                
                $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($Bulk->get_send_emails()));
                $paginator->setCurrentPageNumber($this->params()->fromRoute('page'))->setItemCountPerPage(20);
                
                
                $viewModel=new ViewModel([
                    'paginator'      =>$paginator,
                    'count'          =>$Bulk->cont_sent_messages()
                   
               ]); 
                
                return $viewModel;
                
            }













}
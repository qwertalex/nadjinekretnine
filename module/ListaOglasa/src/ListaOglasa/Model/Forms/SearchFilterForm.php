<?php

namespace ListaOglasa\Model\Forms;





use Zend\Form\Form;




class SearchFilterForm extends Form 
{
   
    
    
      
    public function __construct() {
        parent::__construct('filterSearch');
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
       // $this->setInputFilter(new \Application\Models\Form\FilterRegisterFormModels());
        
        
        
        
    #kategorija--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Select',
             'name'=> 'kategorija',
               'attributes'=>array(
                'class'=>'option-color',
              //     'style'=>'color:red;'
                  
                 ),
             'options'=> array(
                  'value_options'=> array(
                         'Stanovi',
                         'Kuce',
                         'Sobe',
                         'Pomocni Objekti',
                         'Poslovni Prostori',
                         'Lokali i Skladista',
                         'Ugostiteljski Objekti',
                         'Proizvodni Objekti',
                         'Sportski Objekti',
                         'Poljoprivredna Zemljista',
                         'Gradjevinska Zemljista',
                         'Vikendice',
                         'Apartmani'),
             )
     ));
      #-----------------------------------------   
        
        
   #zemlja--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Select',
             'name'=> 'zemlja',
               'attributes'=>array(
                'class'=>'small_input',
                 ),
             'options'=> array(
                  'value_options'=> array(
                     1=>'Srbija',
                     2=>'Crna Gora',
                     3=>'Makedonija',
                     4=>'BiH'),
             )
     ));
      #-----------------------------------------   
        $veci_gradovi=array( 'Beograd'=>11000  , 'Novi Sad'=>21000 ,'Nis'=>18000 ,);
        $gradovi=array(
                   'Ada'=>24430,  'Adasevci'=>22244,  'Luzane'=>18228, 'Adorjan'=>24425, 'Lužnice'=>34325, 'Adrani'=>36203,  'Mala Mostanica'=>11261 ,'Aleksinacki Rudnik'=>18226  ,'Mačkat'=>31312 ,'Aleksa Santic'=>25212  ,'Mačvanska Mitrovica'=>22202 ,'Aleksandrovac'=>37230  ,'Mačvanski Pričinović'=>15211  ,'Aleksandrovac'=>12370  ,'Maglić'=>21473 ,'Aleksandrovo'=>23217  ,'Majdan'=>23333 ,'Aleksinac'=>18220  ,'Majdanpek'=>19250 ,'Alibunar'=>26310  ,'Majilovac'=>12221 ,'Aljinovici'=>31307  ,'Majur'=>35270 ,'Apatin'=>25260  ,'Majur'=>15353 ,'Aradac'=>23207  ,'Mala Bosna'=>24217 ,'Arandjelovac'=>34303  ,'Mala Krsna'=>11313 ,'Arandjelovac'=>34300  ,'Mala Plana'=>18423 ,'Arilje'=>31230  ,'Malča'=>18207 ,'Asanja'=>22418  ,'Male Pčelice'=>34216 ,'Azanja'=>11423  ,'Male Pijace'=>24416 ,'Babin Kal'=>18315  ,'Mali Beograd'=>24309 ,'Babusnica'=>18330  ,'Mali Idjos'=>24321 ,'Bač'=>21420  ,'Mali Izvor'=>19347 ,'Bačevci'=>31258  ,'Mali Jasenovac'=>19209 ,'Bačina'=>37265  ,'Mali Požarevac'=>11235 ,'Bacinci'=>22225   ,'Mali Zvornik'=>15318 ,'Bačka Palanka'=>21400  ,'Malo Crniće'=>12311 ,'Bačka Topola'=>24300  ,'Malo Krcmare'=>34212 ,'Bački Breg'=>25275  ,'Malosiste'=>18415 ,'Bački Brestovac'=>25242  ,'Mandjelos'=>22208 ,'Bački Gracac'=>25252  ,'Manojlovce'=>16201 ,'Bački Jarak'=>21234   ,'Maradik'=>22327 ,'Bački Monostor'=>25272  ,'Margita'=>26364 ,'Bački Petrovac'=>21470  ,'Markovac'=>11325 ,'Bački Sokolac'=>24343  ,'Markovica'=>32243 ,'Bački Vinogradi'=>24415  ,'Marsic'=>34209 ,'Bačko Dobro Polje'=>21465  ,'Martinci'=>22222 ,'Bačko Gradiste'=>21217  ,'Martonos'=>24417 ,'Bačko Novo Selo'=>21429  ,'Mataruska Banja'=>36201 ,'Bačko Petrovo Selo'=>21226  ,'Medja'=>23234 ,'Badnjevac'=>34226  ,'Medosevac'=>18209 ,'Badovinci'=>15358  ,'Medjurečje'=>32255 ,'Bagrdan'=>35204  ,'Medvedja'=>35224 ,'Bajina Basta'=>31250  ,'Medvedja'=>37244 ,'Bajmok'=>24210  ,'Medvedja'=>16240 ,'Bajsa'=>24331  ,'Melenci'=>23270 ,'Balajinac'=>18257  ,'Meljak'=>11426 ,'Baljevac Na Ibru'=>36344  ,'Melnica'=>12305 ,'Banatska Palanka'=>26324  ,'Merćez'=>18436 ,'Banatsko Arandjelovo'=>23332  ,'Merdare'=>18445 ,'Banatski Brestovac'=>26234  ,'Merosina'=>18252 ,'Banatski Despotovac'=>23242  ,'Metlić'=>15233 ,'Banatsko Karađorđevo'=>23216   ,'Metovnica'=>19204 ,'Banatsko Novo Selo'=>26314  ,'Mihajlovac'=>11312 ,'Banatska Subotica'=>26327  ,'Mihajlovac'=>19322 ,'Banatsko Visnjićevo'=>23237  ,'Mihajlovo'=>23202 ,'Banatska Dubica'=>23251  ,'Mijatovac'=>35236 ,'Banatska Topola'=>23315  ,'Milentija'=>37227 ,'Banatski Dvor'=>23213  ,'Milesevo'=>21227 ,'Banatski Karlovac'=>26320  ,'Miletićevo'=>26373 ,'Banatsko Veliko Selo'=>23312  ,'Milosevac'=>11318 ,'Banicina'=>11424  ,'Milosevo'=>35268 ,'Banja'=>34304  ,'Milutovac'=>37246 ,'Banja Kod Priboja'=>31337  ,'Minićevo'=>19340 ,'Banja Koviljača'=>15316  ,'Mionica'=>14242 ,'Banjani'=>14214  ,'Mirosaljci'=>11567 ,'Banjska'=>38216  ,'Mirosevce'=>16204 ,'Banostor'=>21312  ,'Misićevo'=>24211 ,'Banovci Dunav'=>22303  ,'Mitrovac'=>31251 ,'Banovo Polje'=>15362  ,'Mladenovac'=>11400 ,'Barajevo'=>11460  ,'Mladenovo'=>21422 ,'Baranda'=>26205  ,'Mojsinje'=>32211 ,'Barbatovac'=>18426  ,'Mokra Gora'=>31243 ,'Bare'=>34205  ,'Mokranja'=>19317 ,'Barič'=>11504  ,'Mokrin'=>23305 ,'Barice'=>26367  ,'Mol'=>24435 ,'Barlovo'=>18432  ,'Molovin'=>22256 ,'Barosevac'=>11565  ,'Morović'=>22245 ,'Basaid'=>23316  ,'Mosorin'=>21245 ,'Batočina'=>34227  ,'Mozgovo'=>18229 ,'Batrovci'=>22251  ,'Mramor'=>18251 ,'Bavaniste'=>26222  ,'Mramorak'=>26226 ,'Bečej'=>21220  ,'Mrčajevci'=>32210 ,'Bečmen'=>11279   ,'Muhovac'=>17529 ,'Begaljica'=>11308  ,'Muzlja'=>23206 ,'Begeč'=>21411  ,'Nadalj'=>21216  ,'Begejci'=>23232  ,'Nakovo'=>23311 ,'Bela Crkva'=>26340  ,'Natalinci'=>34313 ,'Bela Crkva'=>15313  ,'Negotin'=>19300 ,'Bela Palanka'=>18310  ,'Neresnica'=>12242 ,'Bela Voda'=>37253  ,'Nestin'=>21314 ,'Bela Zemlja'=>31311  ,'Neuzina'=>23245 ,'Belanovica'=>14246  ,'Nikinci'=>22422 ,'Bele Vode'=>32259  ,'Nikolićevo'=>19311 ,'Belegis'=>22306  ,'Nikolinci'=>26322 ,'Beli Potok'=>11223  ,'Beli Potok'=>19366  ,'Niska Banja'=>18205 ,'Beljina'=>11461  ,'Nisevac'=>18366 ,'Belo Blato'=>23205  ,'Njegosevo'=>24311 ,'Beloljin'=>18424  ,'Noćaj'=>22203 ,'Belosavci'=>34312  ,'Nova Crnja'=>23218 ,'Belotinac'=>18411  ,'Nova Crvenka'=>25224 ,'Belusic'=>35263  ,'Nova Gajdobra'=>21431 ,'Beočin'=>21300  ,'Nova Pazova'=>22330 ,'Nova Varos'=>31320 ,'Novi Banovci'=>22304 ,'Novi Bečej'=>23272 ,'Novi Itebej'=>23236 ,'Novi Karlovci'=>22322 ,'Novi Kneževac'=>23330 ,'Novi Kozarci'=>23313 ,'Novi Kozjak'=>26353 ,'Novi Pazar'=>36300  ,'Novi Slankamen'=>22323 ,'Berkasovo'=>22242  ,'Novi Zednik'=>24223 ,'Bersici'=>32305  ,'Novo Laniste'=>35271 ,'Besenovo'=>22212  ,'Novo Milosevo'=>23273 ,'Beska'=>22324  ,'Novo Orahovo'=>24351 ,'Bezdan'=>25270  ,'Novo Selo'=>36216 ,'Bigrenica'=>35238 ,'Novo Selo'=>18250   ,'Bikić Do'=>22254                  ,'Obrenovac'=>11500   ,'Bikovo'=>24206                  ,'Obrez'=>37266   ,'Biljaca'=>17522                  ,'Obrez'=>22417   ,'Bingula'=>22253                  ,'Obrovac'=>21423   ,'Bioska'=>31241                  ,'Odzaci'=>25250   ,'Bistar'=>17546                  ,'Ogar'=>22416   ,'Bistrica'=>31325                  ,'Omoljica'=>26230   ,'Blace'=>18420                  ,'Oparić'=>35267   ,'Blazevo'=>37226                  ,'Opovo'=>26204   ,'Bobovo'=>35217                  ,'Orane'=>16233   ,'Bocar'=>23274                  ,'Oraovica'=>17557   ,'Bodjani'=>21427                  ,'Orasac'=>34308   ,'Bogaras'=>24408                  ,'Oresković'=>24344   ,'Bogatić'=>15350                  ,'Orid'=>15213   ,'Bogojevo'=>25245                  ,'Orlovat'=>23263   ,'Bogovadja'=>14225                  ,'Orom'=>24207   ,'Bogovina'=>19372                  ,'Osanica'=>12317   ,'Bogutovac'=>36341                  ,'Osečina'=>14253   ,'Bojnik'=>16205                  ,'Osipaonica'=>11314   ,'Boka'=>23252                  ,'Osnić'=>19378   ,'Boleč'=>11307                  ,'Osreci'=>37281   ,'Boljevac'=>19370                  ,'Ostojićevo'=>23326   ,'Boljevci'=>11275                  ,'Ostrovica'=>18312   ,'Boljkovci'=>32312                  ,'Ostružnica'=>11251   ,'Bor'=>19210                  ,'Ovča'=>11212   ,'Borča'=>11211                  ,'Ovčar Banja'=>32242   ,'Borski Brestovac'=>19229                  ,'Pačir'=>24342    ,'Borsko Bucje'=>19231                  ,'Padinska Skela'=>11213    ,'Bosilegrad'=>17540                  ,'Padej'=>23325    ,'Bosnjace'=>16232                  ,'Padež'=>37257    ,'Bosnjane'=>37262                  ,'Padina'=>26215    ,'Bosut'=>22217                  ,'Palić'=>24413    ,'Botos'=>23243                  ,'Palilula'=>18363    ,'Bozetici'=>31322                  ,'Pambukovica'=>14213    ,'Bozevac'=>12313                  ,'Pančevo'=>26000   ,'Bozica'=>17537                  ,'Panonija'=>24330   ,'Bracevac'=>19315                  ,'Paraćin'=>35250   ,'Bradarac'=>12206                  ,'Parage'=>21434   ,'Braničevo'=>12222                  ,'Parunovac'=>37201   ,'Brankovina'=>14201                  ,'Pasjane'=>38266   ,'Bratinac'=>12225                  ,'Pavlis'=>26333   ,'Bratljevo'=>32256                  ,'Pecanjevce'=>16251   ,'Brđani'=>32303                  ,'Pećinci'=>22410   ,'Brekovo'=>31234                  ,'Pećka'=>14207   ,'Bresnica'=>32213                  ,'Pejkovac'=>18413   ,'Brestac'=>22415                  ,'Pepeljevac'=>37231   ,'Brestovačka Banja'=>19216                  ,'Perlez'=>23260   ,'Brestovac'=>16253                  ,'Perućac'=>31256   ,'Brezane'=>12205                  ,'Petlovača'=>15304   ,'Brezde'=>14244                  ,'Petrovac Na Mlavi'=>12300   ,'Brezjak'=>15309                  ,'Petrovaradin'=>21131   ,'Brezna'=>32307                  ,'Petrovcic'=>11282   ,'Brezova'=>32253                  ,'Pinosava'=>11226   ,'Brezovica'=>38157                   ,'Pirot'=>18300   ,'Brgule'=>14212                  ,'Pivnice'=>21469   ,'Brnjica'=>12230                  ,'Plana'=>35247   ,'Brodarevo'=>31305                  ,'Plandiste'=>26360   ,'Brus'=>37220                  ,'Planinica'=>19207   ,'Brusnik'=>19313                  ,'Platičevo'=>22420   ,'Brvenik'=>36346                  ,'Plavna'=>21428   ,'Brza Palanka'=>19323                  ,'Plažane'=>35212   ,'Brzan'=>34228                  ,'Ples'=>37238   ,'Brzeće'=>37225                  ,'Ploča'=>37237   ,'Bucje'=>19369                  ,'Pločica'=>26229   ,'Budjanovci'=>22421                  ,'Pobeda'=>24313   ,'Budisava'=>21242                  ,'Počekovina'=>37243   ,'Bujanovac'=>17520                  ,'Poćuta'=>14206   ,'Bujanovac'=>17567                  ,'Podunavci'=>36215   ,'Bukovac'=>21209                  ,'Podvis'=>19362   ,'Bukovica'=>32251                  ,'Podvrska'=>19321   ,'Bukovik'=>34301                    ,'Poganovo'=>18326   ,'Bukurovac'=>34217                  ,'Pojate'=>37214   ,'Bumbarevo Brdo'=>34242                  ,'Poljana'=>12372   ,'Bunar'=>35273                  ,'Popinci'=>22428   ,'Burdimo'=>18368                  ,'Popovac'=>35254   ,'Burovac'=>12307                  ,'Popovac'=>18260   ,'Busilovac'=>35249                  ,'Popučke'=>14221   ,'Čačak'=>32000                  ,'Porodin'=>12375   ,'Čajetina'=>31310                  ,'Potočac'=>35207   ,'Čalma'=>22231                  ,'Požarevac'=>12000   ,'Čantavir'=>24220                  ,'Požega'=>31210   ,'Cecina'=>18417                  ,'Prahovo'=>19330   ,'Čelarevo'=>21413                  ,'Pranjani'=>32308   ,'Čenej'=>21233                  ,'Predejane'=>16222   ,'Ćenta'=>23266                  ,'Prekonoga'=>18365   ,'Čerević'=>21311                  ,'Preljina'=>32212   ,'Cerovac'=>15224                  ,'Presevo'=>17523   ,'Cestereg'=>23215                  ,'Prevest'=>35264   ,'Ćićevac'=>37210                  ,'Prhovo'=>22442   ,'Čitluk'=>37208                  ,'Priboj'=>31330   ,'Čitluk'=>18232                  ,'Priboj Vranjski'=>17511   ,'Čoka'=>23320                  ,'Pricevic'=>14251   ,'Conoplja'=>25210                  ,'Prigrevica'=>25263   ,'Čortanovci'=>22326                  ,'Prijepolje'=>31300   ,'Crepaja'=>26213                  ,'Prilicki Kiseljak'=>32252   ,'Crkvine'=>36321                  ,'Prilužje'=>38213   ,'Crna Bara'=>23328                  ,'Privina Glava'=>22257   ,'Crna Bara'=>15355                  ,'Prnjavor Mačvanski'=>15306   ,'Crna Trava'=>16215                  ,'Progar'=>11280   ,'Crnokliste'=>18304                  ,'Prokuplje'=>18400   ,'Crvena Crkva'=>26323                  ,'Prolom'=>18433   ,'Crvena Reka'=>18313                  ,'Provo'=>15215   ,'Crvenka'=>25220                  ,'Pružatovac'=>11413   ,'Čukojevac'=>36220                  ,'Pukovac'=>18255   ,'Čumić'=>34322                  ,'Putinci'=>22404   ,'Ćuprija'=>35230                  ,'Rabrovo'=>12254   ,'Čurug'=>21238                  ,'Rača'=>18440   ,'Đala'=>23335                  ,'Rača Kragujevačka'=>34210   ,'Darosava'=>34305                   ,'Radalj'=>15321   ,'Dasnica'=>37271                  ,'Radenković'=>22206   ,'Debeljača'=>15214                  ,'Radičević'=>21225   ,'Dec'=>22441                  ,'Radinac'=>11311   ,'Deliblato'=>26225                  ,'Radljevo'=>14211   ,'Delimede'=>36307                  ,'Radojevo'=>23221   ,'Deronje'=>25254                  ,'Radujevac'=>19334   ,'Desimirovac'=>34321                  ,'Rajac'=>19314   ,'Despotovac'=>35213                  ,'Rajković'=>14202   ,'Despotovo'=>21468                  ,'Rakinac'=>11327   ,'Devojački Bunar'=>26316                  ,'Rakovac'=>21299   ,'Dezeva'=>36305                  ,'Ralja'=>11233   ,'Dimitrovgrad'=>18320                  ,'Ranilovic'=>34302   ,'Divci'=>14222                  ,'Ranilug'=>38267   ,'Divčibare'=>14204                  ,'Ranovac'=>12304   ,'Divljaka'=>31236                  ,'Rasanac'=>12315   ,'Divos'=>22232                  ,'Rasevica'=>35206   ,'Divostin'=>34204                  ,'Raska'=>36350   ,'Dobanovci'=>11272                  ,'Rastina'=>25283   ,'Dobra'=>12224                  ,'Rataje'=>37236   ,'Dobri Do'=>18408                  ,'Ratari'=>11411   ,'Dobrić'=>15235                  ,'Ratina'=>36212   ,'Dobrica'=>26354                  ,'Ratkovo'=>25253   ,'Dobrinci'=>22412                  ,'Ravna Dubrava'=>18246   ,'Dolac'=>18314                  ,'Ravna Reka'=>35235   ,'Doljevac'=>18410                  ,'Ravni'=>31206   ,'Dolovo'=>26227                  ,'Ravni Topolovac'=>23212   ,'Donja Bela Reka'=>19213                  ,'Ravnje'=>22205   ,'Donja Borina'=>15317                  ,'Ravno Selo'=>21471   ,'Donja Kamenica'=>19352                  ,'Ražana'=>31265   ,'Donja Livadica'=>11326                  ,'Ražanj'=>37215   ,'Donja Ljubata'=>17544                  ,'Razbojna'=>37223   ,'Donja Mutnica'=>35255                  ,'Razgojna'=>16252   ,'Donja Orovica'=>15323                  ,'Rekovac'=>35260   ,'Donja Rečica'=>18404                  ,'Reljan'=>17556   ,'Donja Satornja'=>34314                  ,'Resavica'=>35237   ,'Donja Trnava'=>18421                  ,'Resnik'=>34225   ,'Donje Vidovo'=>35258                  ,'Rgotina'=>19214   ,'Donje Crnatovo'=>18414                  ,'Ribare'=>35220   ,'Donje Crniljevo'=>15227                  ,'Ribare'=>37259   ,'Donje Medurovo'=>18254                  ,'Ribari'=>15310   ,'Donje Tlamino'=>17547                  ,'Ribarice'=>36309   ,'Donje Zuniće'=>19345                  ,'Ribarska Banja'=>37205   ,'Donji Dusnik'=>18242                  ,'Ridjica'=>25280   ,'Donji Krcin'=>37258                  ,'Ripanj'=>11232   ,'Donji Milanovac'=>19220                  ,'Ristovac'=>17521   ,'Donji Stajevac'=>17526                  ,'Ritisevo'=>26331   ,'Doroslovo'=>25243                  ,'Roćevci'=>36205   ,'Dračić'=>14203                  ,'Rogača'=>11453   ,'Draginac'=>15311                  ,'Rogačica'=>31255   ,'Draginje'=>15226                  ,'Roge'=>31237   ,'Draglica'=>31317                  ,'Rogljevo'=>19318   ,'Dragobraca'=>34231                  ,'Rožanstvo'=>31208   ,'Dragocvet'=>35272                  ,'Rudna Glava'=>19257   ,'Dragosevac'=>35262                  ,'Rudnica'=>36353   ,'Dragovo'=>35265                  ,'Rudnik'=>32313   ,'Drajkovce'=>38239                  ,'Rudno'=>36222   ,'Draževac'=>11506                  ,'Rudovci'=>11566   ,'Draževac(kod Aleksinca)'=>18223                  ,'Ruma'=>22400   ,'Drenovac'=>35257                  ,'Rumenka'=>21201   ,'Drenovac'=>15212                    ,'Ruplje'=>16223   ,'Drlace'=>15324                  ,'Rusanj'=>11194   ,'Drugovac'=>11432                  ,'Ruski Krstur'=>25233   ,'Dublje'=>15359                  ,'Rusko Selo'=>23314   ,'Duboka'=>12255                  ,'Rutevac'=>18224   ,'Dubovac'=>26224                  ,'Šabac'=>15000   ,'Dubovo'=>18406                  ,'Sajan'=>23324   ,'Dubravica'=>12207                  ,'Šajkas'=>21244   ,'Dudovica'=>11561                  ,'Sakule'=>26206   ,'Duga Poljana'=>36312                  ,'Salas'=>19224   ,'Dugo Polje'=>18237                  ,'Salas Noćajski'=>22204   ,'Djunis'=>37202                  ,'Samaila'=>36202   ,'Dupljaja'=>26328                  ,'Samos'=>26350   ,'Đurdjevo'=>34215                  ,'Sanad'=>23331   ,'Đurdjevo'=>21239                  ,'Saraorci'=>11315   ,'Đurđin'=>24213                  ,'Šarbanovac'=>18235   ,'Dusanovac'=>19335                  ,'Šarbanovac'=>19373   ,'Dvorane'=>37206                  ,'Šarbanovac Timok'=>19379   ,'Džep'=>17514                  ,'Šasinci'=>22425   ,'Džigolj'=>18405                  ,'Sastav Reka'=>16213   ,'Ečka'=>23203                  ,'Sastavci'=>31335   ,'Elemir'=>23208                  ,'Savinac'=>19377   ,'Erdec'=>34207                  ,'Savino Selo'=>21467   ,'Erdevik'=>22230                  ,'Seča Reka'=>31262   ,'Farkaždin'=>23264                  ,'Sečanj'=>23240   ,'Feketić'=>24323                  ,'Sedlare'=>35211   ,'Futog'=>21410                  ,'Sefkerin'=>26203   ,'Gornji Milanovac'=>32300                  ,'Selenča'=>21425   ,'Gadžin Han'=>18240                  ,'Seleus'=>26351   ,'Gaj'=>26223                  ,'Selevac'=>11407   ,'Gajdobra'=>21432                  ,'Senje'=>35233   ,'Gakovo'=>25282                  ,'Senjski Rudnik'=>35234   ,'Gamzigradska Banja'=>19228                  ,'Senta'=>24400   ,'Gardinovci'=>21247                  ,'Šepsin'=>11433   ,'Gibarac'=>22258                  ,'Šetonje'=>12309   ,'Glavinci'=>35261                  ,'Sevojno'=>31205   ,'Globoder'=>37251                  ,'Sibač'=>22440   ,'Glogonj'=>26202                  ,'Sibnica'=>11454   ,'Glogovac'=>35222                  ,'Sićevo'=>18311   ,'Gložan'=>21412                  ,'Šid'=>22240   ,'Glusci'=>15356                  ,'Sijarinska Banja'=>16246   ,'Golobok'=>11316                  ,'Sikirica'=>35256   ,'Golubac'=>12223                  ,'Sikole'=>19225   ,'Golubinci'=>22308                  ,'Silbas'=>21433   ,'Goračići'=>32232                  ,'Šilopaj'=>32311   ,'Goričani'=>32225                  ,'Šimanovci'=>22310   ,'Gornja Dobrinja'=>31214                  ,'Simićevo'=>12373   ,'Gornja Dragusa'=>18425                  ,'Siokovac'=>35203   ,'Gornja Lisina'=>17538                  ,'Sip'=>19326   ,'Gornja Sabanta'=>34206                  ,'Sirča'=>36208   ,'Gornja Toplica'=>14243                  ,'Sirig'=>21214   ,'Gornja Toponica'=>18202                   ,'Sirogojno'=>31207   ,'Gornja Trepca'=>32215                  ,'Sivac'=>25223   ,'Gornji Banjani'=>32306                  ,'Sjenica'=>36310   ,'Gornji Barbes'=>18241                  ,'Skela'=>11509   ,'Gornji Breg'=>24406                  ,'Skobalj'=>11322   ,'Gornji Brestovac'=>16244                  ,'Skorenovac'=>26228   ,'Gornji Matejevac'=>18204                  ,'Slatina'=>32224   ,'Gornji Stepos'=>37221                  ,'Slatina'=>19324   ,'Gornji Stupanj'=>37234                  ,'Slavkovica'=>14245   ,'Gospođinci'=>21237                  ,'Šljivovica'=>31244   ,'Gostilje'=>31313                  ,'Šljivovo'=>37239   ,'Grabovac'=>11508                  ,'Slovac'=>14223   ,'Grabovci'=>22423                  ,'Smederevska Palanka'=>11420   ,'Gracanica'=>38205                  ,'Smederevo'=>11300   ,'Gradina'=>18321                  ,'Smilovci'=>18323   ,'Gradskovo'=>19205                  ,'Smoljinac'=>12312   ,'Grasevci'=>37229                  ,'Sočanica'=>38217   ,'Grdelica'=>16220                  ,'Soko Banja'=>18230   ,'Grebenac'=>26347                  ,'Sombor'=>25000   ,'Gredetin'=>18213                  ,'Sonta'=>25264   ,'Grejac'=>18219                  ,'Sopoćani'=>36308   ,'Grgurevci'=>22213                  ,'Sopot'=>11450   ,'Grliste'=>19342                  ,'Sot'=>22243   ,'Grljan'=>19341                  ,'Sremska Mitrovica'=>22000   ,'Grocka'=>11306                  ,'Srbobran'=>21480   ,'Grosnica'=>34202                  ,'Srednjevo'=>12253   ,'Gruza'=>34230                  ,'Sremski Mihaljevci'=>22413   ,'Guberevac'=>34232                  ,'Sremčica'=>11253   ,'Guča'=>32230                  ,'Sremska Kamenica'=>21208   ,'Gudurica'=>26335                  ,'Sremska Rača'=>22247   ,'Gunaros'=>24312                  ,'Sremski Karlovci'=>21205   ,'Gusevac'=>18208                  ,'Srpska Crnja'=>23220   ,'Hajdučica'=>26370                  ,'Srpski Itebej'=>23233   ,'Hajdukovo'=>24414                  ,'Srpski Krstur'=>23334   ,'Halovo'=>19236                  ,'Srpski Miletić'=>25244   ,'Hetin'=>23235                  ,'Stajićevo'=>23204   ,'Horgos'=>24410                  ,'Stalać'=>37212   ,'Horgos Granični Prelaz'=>24411                  ,'Stanisić'=>25284   ,'Hrtkovci'=>22427                  ,'Stapar'=>25240   ,'Iđos'=>23323                  ,'Stara Moravica'=>24340   ,'Idvor'=>26207                  ,'Stara Pazova'=>22300   ,'Ilandža'=>26352                  ,'Starčevo'=>26232   ,'Ilićevo'=>34203                  ,'Stari Žednik'=>24224   ,'Ilinci'=>22250                  ,'Stari Banovci'=>22305   ,'Inđija'=>22320                  ,'Stari Lec'=>26371   ,'Irig'=>22406                  ,'Stari Ledinci'=>21206   ,'Ivanjica'=>32250                  ,'Stari Slankamen'=>22329   ,'Ivanovo'=>26233                  ,'Staro Selo'=>11324   ,'Izbiste'=>26343                  ,'Štavalj'=>36311   ,'Jablanica'=>31314                  ,'Stave'=>14255   ,'Jabučje'=>14226                  ,'Stejanovci'=>22405   ,'Jabuka'=>26201                  ,'Stenjevac'=>35215   ,'Jabuka'=>31306                  ,'Stepanovićevo'=>21212   ,'Jabukovac'=>19304                  ,'Stepojevac'=>11564   ,'Jadranska Lesnica'=>15308                  ,'Štitar'=>15354   ,'Jagnjilo'=>11412                  ,'Stojnik'=>34307   ,'Jagodina'=>35000                  ,'Stopanja'=>37242   ,'Jakovo'=>11276                  ,'Stragari'=>34323   ,'Jamena'=>22248                  ,'Straža'=>26345   ,'Janosik'=>26362                  ,'Strelac'=>18332   ,'Jarkovac'=>23250                  ,'Strižilo'=>35269   ,'Jarmenovci'=>34318                  ,'Štrpce'=>38236   ,'Jasa Tomić'=>23230                  ,'Stubal'=>17512   ,'Jasenovo'=>35241                  ,'Štubik'=>19303   ,'Jasenovo'=>26346                  ,'Stubline'=>11507   ,'Jasenovo '=>31319                  ,'Studenica'=>36343   ,'Jasika'=>37252                  ,'Subotica Kod Svilajnca'=>35209   ,'Jazak'=>22409                  ,'Subotica'=>24000   ,'Jazovo'=>23327                  ,'Subotinac'=>18227   ,'Jelasnica'=>18206                  ,'Subotiste'=>22414   ,'Jelasnica'=>17531                  ,'Sukovo'=>18322   ,'Jelen Do'=>31215                  ,'Sumrakovac'=>19376   ,'Jelovik'=>34309                  ,'Supska'=>35228   ,'Jermenovci'=>26363                  ,'Surčin'=>11271   ,'Ježevica'=>32222                  ,'Surduk'=>22307   ,'Ježevica'=>31213                  ,'Surdulica'=>17530   ,'Josanica'=>18234                  ,'Surjan'=>23254   ,'Josanicka Banja'=>36345                  ,'Susek'=>21313   ,'Jovac'=>35205                  ,'Sutjeska'=>23244   ,'Jovanovac'=>34211                  ,'Suvi Do'=>12322   ,'Jovanovac'=>18258                  ,'Sveti Ilija'=>17508   ,'Jugbogdanovac'=>18253                  ,'Svetozar Miletić'=>25211   ,'Junkovac'=>11562                  ,'Svilajnac'=>35210   ,'Kać'=>21241                  ,'Svileuva'=>15221   ,'Kačarevo'=>26212                  ,'Svilojevo'=>25265   ,'Kajtasovo'=>26329                  ,'Svođe'=>16212   ,'Kalna'=>19353                  ,'Svojnovo'=>35259   ,'Kaluđerske Bare'=>31257                  ,'Svrljig'=>18360   ,'Kaluđerica'=>11130                  ,'Takovo'=>32304   ,'Kamenica'=>32206                  ,'Taras'=>23209   ,'Kamenica'=>15222                  ,'Tavankut'=>24214   ,'Kamenica'=>14252                  ,'Tekeris'=>15234   ,'Kamenica'=>18324                  ,'Tekija'=>19325   ,'Kanjiža'=>24420                  ,'Telečka'=>25222   ,'Kaona'=>32234                  ,'Temerin'=>21235   ,'Kaonik'=>37256                  ,'Temska'=>18355   ,'Karađorđevo'=>21421                  ,'Tesica'=>18212   ,'Karadjordjevo'=>24308                  ,'Titel'=>21240   ,'Karan'=>31204                  ,'Toba'=>23222   ,'Karavukovo'=>25255                  ,'Tomasevac'=>23262   ,'Karlovčić'=>22443                  ,'Topola'=>34310   ,'Katun'=>18225                  ,'Topolovnik'=>12226   ,'Kelebija'=>24104                  ,'Toponica'=>34243   ,'Kelebija-granični prelaz'=>24205                  ,'Torda'=>23214   ,'Kevi'=>24407                  ,'Tornjos'=>24352   ,'Kikinda'=>23300                  ,'Totovo Selo'=>24427   ,'Kisač'=>21211                  ,'Tovarisevo'=>21424   ,'Kladovo'=>19320                  ,'Trbusani'=>32205   ,'Klek'=>23211                  ,'Tresnjevac'=>24426   ,'Klenak'=>22424                  ,'Tresnjevica'=>35248   ,'Klenike'=>17524                  ,'Trgoviste'=>17525   ,'Klenje'=>12258                  ,'Trnava'=>32221   ,'Klenje'=>15357                  ,'Trnavci'=>37235   ,'Kličevac'=>12209                  ,'Trnjane'=>19306   ,'Klisura'=>17535                  ,'Trsić'=>15303   ,'Kljajićevo'=>25221                  ,'Trstenik'=>37240   ,'Klokočevac'=>19222                  ,'Trupale'=>18211   ,'Knić'=>34240                  ,'Tulare'=>16247   ,'Knićanin'=>23265                  ,'Turekovac'=>16231   ,'Knjaževac'=>19350                  ,'Turija'=>21215   ,'Kobisnica'=>19316                  ,'Tutin'=>36320   ,'Koceljeva'=>15220                  ,'Ub'=>14210   ,'Kokin Brod'=>31318                  ,'Ugao'=>36313   ,'Kolare'=>35242                  ,'Ugrinovci'=>32314   ,'Kolari'=>11431                  ,'Ugrinovci'=>11277   ,'Kolut'=>25274                  ,'Uljma'=>26330   ,'Komirić'=>14254                  ,'Umčari'=>11430   ,'Konak'=>23253                  ,'Umka'=>11260   ,'Konarevo'=>36340                  ,'Urovica'=>19305   ,'Končarevo'=>35219                  ,'Usće'=>36342   ,'Konjuh'=>37254                  ,'Utrine'=>24437   ,'Kopaonik'=>36354                  ,'Uzdin'=>26216   ,'Koprivnica'=>19223                  ,'Užice'=>31000   ,'Koraćica'=>11415                  ,'Uzovnica'=>15319   ,'Korbevac'=>17545                  ,'Velika Mostanica'=>11262   ,'Korbovo'=>19329                  ,'Vajska'=>21426   ,'Korenita'=>15302                  ,'Valjevo'=>14000   ,'Korman'=>34224                  ,'Varda'=>31263   ,'Korman'=>18216                  ,'Varna'=>15232   ,'Kosovska Mitrovica'=>38220                  ,'Varvarin'=>37260   ,'Kosančić'=>16206                  ,'Vasica'=>22241   ,'Kosjerić'=>31260                  ,'Vasilj'=>19367   ,'Kosovo Polje'=>38210                  ,'Vatin'=>26337   ,'Kostojevići'=>31254                  ,'Velika Grabovnica'=>16221   ,'Kostolac'=>12208                  ,'Velesnica'=>19328   ,'Kotraža'=>32235                  ,'Velika Drenova'=>37245   ,'Kovačevac'=>11409                  ,'Velika Greda'=>26366   ,'Kovačica'=>26210                  ,'Velika Ivanča'=>11414   ,'Kovilj'=>21243                  ,'Velika Jasikova'=>19235   ,'Kovilje'=>32257                  ,'Velika Krsna'=>11408   ,'Kovin'=>26220                  ,'Velika Lomnica'=>37209   ,'Kragujevac'=>34000                  ,'Velika Plana'=>18403   ,'Krajisnik'=>23231                  ,'Velika Plana'=>11320   ,'Kraljevci'=>22411                  ,'Velika Reka'=>15322   ,'Kraljevo'=>36000                  ,'Velika Vrbnica'=>37233   ,'Krčedin'=>22325                  ,'Veliki Borak'=>11462   ,'Kremna'=>31242                  ,'Veliki Crljeni'=>11563   ,'Krepoljin'=>12316                  ,'Veliki Gaj'=>26365   ,'Kriva Feja'=>17543                  ,'Veliki Izvor'=>19206   ,'Kriva Reka'=>37282                  ,'Veliki Popović'=>35223   ,'Krivaja'=>24341                  ,'Veliki Radinci'=>22211   ,'Krivelj'=>19219                  ,'Veliki Siljegovac'=>37204   ,'Krivi Vir'=>19375                  ,'Veliko Bonjince'=>18215   ,'Krnjesevci'=>22314                  ,'Veliko Golovode'=>37207   ,'Krnjevo'=>11319                  ,'Veliko Gradiste'=>12220   ,'Krupač'=>18307                  ,'Veliko Krčmare'=>34214   ,'Krupanj'=>15314                  ,'Veliko Laole'=>12306   ,'Krusar'=>35227                  ,'Veliko Orasje'=>11323   ,'Krusčić'=>25225                  ,'Veliko Selo'=>12314   ,'Krusčica'=>26380                  ,'Veliko Srediste'=>26334   ,'Krusčica'=>31233                  ,'Vencane'=>34306   ,'Krusedol'=>22328                  ,'Veternik'=>21203   ,'Krusevac'=>37000                  ,'Vilovo'=>21246   ,'Krusevica'=>18409                  ,'Vinča'=>11351   ,'Kučevo'=>12240                  ,'Vinci'=>12229   ,'Kucura'=>21466                  ,'Vionica'=>32254   ,'Kukljin'=>37255                  ,'Visnjevac'=>24222   ,'Kukujevci'=>22224                  ,'Visnjićevo'=>22246   ,'Kula'=>25230                  ,'Visoka Rzana'=>18306   ,'Kulina'=>18214                  ,'Vitanovac'=>36206   ,'Kulpin'=>21472                  ,'Vitkovac'=>36207   ,'Kumane'=>23271                  ,'Vitojevci'=>22431   ,'Kupci'=>37222                  ,'Vitosevac'=>37213   ,'Kupinik'=>26368                  ,'Vladičin Han'=>17510   ,'Kupinovo'=>22419                  ,'Vladimirci'=>15225   ,'Kupusina'=>25262                  ,'Vladimirovac'=>26315   ,'Kursumlijska Banja'=>18435                  ,'Vlajkovac'=>26332   ,'Kursumlija'=>18430                  ,'Vlase'=>17507   ,'Kusadak'=>11425                  ,'Vlasina Okruglica'=>17532   ,'Kusić'=>26349                  ,'Vlasina Rid'=>17533   ,'Kusići'=>32258                  ,'Vlasina Stojkovićevo'=>17534   ,'Kusiljevo'=>35226                  ,'Vlaska'=>11406   ,'Kustilj'=>26336                  ,'Vlaski Do'=>12371   ,'Kuzmin'=>22223                  ,'Vlasotince'=>16210   ,'Laćarak'=>22221                  ,'Vodanj'=>11328   ,'Laćisled'=>37232                  ,'Voganj'=>22429   ,'Lađevci'=>36204                  ,'Vojka'=>22313   ,'Lajkovac'=>14224                  ,'Vojska'=>35208   ,'Lalić'=>25234                  ,'Vojvoda Stepa'=>23219   ,'Lalinac'=>18201                  ,'Vojvodinci'=>26338   ,'Lapovo'=>34220                  ,'Voluja'=>12256   ,'Lapovo Selo'=>34223                  ,'Vračev Gaj'=>26348   ,'Lazarevac'=>11550                  ,'Vračevsnica'=>32315   ,'Lazarevo'=>23241                  ,'Vranić'=>11427   ,'Laznica'=>12321                  ,'Vranje'=>17500   ,'Lebane'=>16230                  ,'Vranjska Banja'=>17542   ,'Lece'=>16248                  ,'Vranovo'=>11329   ,'Ledinci'=>21207                  ,'Vratarnica'=>19344   ,'Lelić'=>14205                  ,'Vražogrnac'=>19312   ,'Lenovac'=>19343                  ,'Vrba'=>36214   ,'Lepenac'=>37224                  ,'Vrbas'=>21460   ,'Lepenica'=>17513                  ,'Vrbica'=>23329   ,'Leposavić'=>38218                  ,'Vrčin'=>11224   ,'Lesak'=>38219                  ,'Vrdnik'=>22408   ,'Leskovac'=>16000                  ,'Vreoci'=>11560   ,'Lesnica'=>15307                  ,'Vrnjačka Banja'=>36210   ,'Lestane'=>11309                  ,'Vrnjci'=>36217   ,'Ležimir'=>22207                  ,'Vrsac'=>26300   ,'Lički Hanovi'=>18245                  ,'Vučje'=>16203   ,'Lipar'=>25232                  ,'Vukovac'=>12318   ,'Lipe'=>11310                  ,'Žabalj'=>21230   ,'Lipolist'=>15305                  ,'Žabari'=>12374   ,'Ljig'=>14240                  ,'Zablaće'=>32223   ,'Ljuba'=>22255                  ,'Zabojnica'=>34244   ,'Ljuberadja'=>18217                  ,'Zabrežje'=>11505   ,'Ljubis'=>31209                  ,'Žagubica'=>12320   ,'Ljubovija'=>15320                          ,'Zajaca'=>15315    ,'Ljukovo'=>22321                       ,'Zaječar'=>19000    ,'Ljutovo'=>24215                       ,'Zaplanjska Toponica'=>18244    ,'Lok'=>21248                        ,'Zasavica'=>22201    ,'Lokve'=>26361                        ,'Zavlaka'=>15312    ,'Lovćenac'=>24322                        ,'Zdravinje'=>37203    ,'Loznica'=>15300                        ,'Zemun'=>11080    ,'Lozovik'=>11317                        ,'Zica'=>36221    ,'Lubnica'=>19208                        ,'Zirovnica'=>34229    ,'Lučani'=>32240                        ,'Žitiste'=>23210    ,'Lug'=>21315                        ,'Žitkovac'=>18210    ,'Lugavcina'=>11321                        ,             'Žitni Potok'=>18407    ,'Luka'=>19234                       ,              'Žitoradja'=>18412    ,'Lukare'=>36306                       ,              'Zlatari'=>37228    ,'Lukićevo'=>23261                       ,              'Zlatibor'=>31315    ,'Lukino Selo'=>23224                       ,              'Zlatica'=>23255    ,'Lukovo'=>18437                       ,              'Zlodol'=>31253    ,'Lukovo'=>19371                        ,             'Zlot'=>19215    ,'Zmajevo'=>21213    ,'Zminjak'=>15352    ,'Zrenjanin'=>23000    ,'Zubin Potok'=>38228    ,'Zuc'=>18438    ,'Zuce'=>11225    ,'Zvecan'=>38227   ,'Zvezdan'=>19227   ,'Zvonce'=>18333  
                        );
      ksort($gradovi); 
     $gradovi=  array_flip($gradovi);
      $veci_gradovi=  array_flip($veci_gradovi);
      $gradovi=  array_merge($veci_gradovi,$gradovi);
  #grad--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Select',
             'name'=> 'grad',
            'attributes'=>array(
                'class'=>'small_input',
                 ),
             'options'=> array(
                  'value_options'=> $gradovi,
             )
     ));
      #-----------------------------------------   
        
        


  #deo_grada--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Select',
             'name'=> 'deo_grada',
             'attributes'=>array(
                'id'=>'deo-grada',
                 'disabled'=>'disabled',
                 'class'=>'small_input',
                 ),
             'options'=> array(
                  'value_options'=> array(
                      
                  )
             )
     ));
      #-----------------------------------------  


        
      
  #prodaja--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'prodaja',
             'attributes'=>array(
                'id'=>'inputRent',
                
                  ),
           
     ));
      #-----------------------------------------  
       
       
       
       
             
  #izdavanje--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'izdavanje',
             'attributes'=>array(
                'id'=>'inputSale',
                
                  ),
           
     ));
      #-----------------------------------------  
       
       
       
       
       
            
  #sa slikom--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'sa_slikom',
             'attributes'=>array(
                'id'=>'sa-slikom',
                
                  ),
           
     ));
      #-----------------------------------------   
       
       
       
       
                      
     #cena od-------------------------------------------
       $this->add(array(
            'name'=>'cena_od',
            'attributes'=>array(
                'type'=>'text',
               //  'class'=>'small_input',
                'placeholder'=>'od',
                'style'=>'color:#e17862',
                
            ),
         
        ));
        
       #-----------------------------------------  
       
       
                          
     #cena do-------------------------------------------
       $this->add(array(
            'name'=>'cena_do',
            'attributes'=>array(
                'type'=>'text',
            //    'class'=>'small_input',
                 'placeholder'=>'do',
                   'style'=>'color:#e17862',
            ),
         
        ));
        
       #-----------------------------------------     
       
       
   #kvadratura od-------------------------------------------
       $this->add(array(
            'name'=>'kvadratura_od',
            'attributes'=>array(
                'type'=>'text',
            //     'class'=>'small_input',
                'placeholder'=>'od',
                'style'=>'color:#e17862',
                
            ),
         
        ));
        
       #-----------------------------------------  
       
       
                          
     #kvadratura do-------------------------------------------
       $this->add(array(
            'name'=>'kvadratura_do',
            'attributes'=>array(
                'type'=>'text',
            //    'class'=>'small_input',
                 'placeholder'=>'do',
                'style'=>'color:#e17862',
                
            ),
         
        ));
        
       #----------------------------------------- 
       
       
          
      #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'submit',
               'class'=>'btn btn-primary btn-large list-your-property arrow-right',
                'value'=>'Nadjite Oglas',
              //  'id'=>'cmdSubmit',
               
            ),
           ));
        
        #-----------------------------------------   
          
          

          
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
}







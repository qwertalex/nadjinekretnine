<?php





return array(
  
'controllers' => array(
     'invokables' => array(
'ListaOglasa\Controller\Index' => 'ListaOglasa\Controller\IndexController',# <------HERE
         
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'listaoglasa' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/stambeni-objekti[/:action][/:id]',# <------HERE
           'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              'id'     => '[0-9]+',
            ),
            'defaults' => array(
               'controller' => 'ListaOglasa\Controller\Index',# <------HERE
               'action'     => 'index',
               
            ),
        ),
     ),
  ),
),
    
    'view_manager' => array(
    'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    
);













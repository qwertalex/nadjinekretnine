<?php

namespace MojProfil;


use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

//local
use MojProfil\Models\MojiOglasi\MojiOglasi,
Application\Models\User\SessionManipulation;



class Module {
    
    
    
    
    
       public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    
    
    
    
    
 #default   includuje Helloworld/config/module.config.php  
public function getConfig() {return include __DIR__ . '/config/module.config.php';}
      
#autoload by the ModuleManager;
public function getAutoloaderConfig(){
return array(
   'Zend\Loader\StandardAutoloader' => array(
      'namespaces' => array( __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,))
         );
     }
      
  
     
     
     
        
     
     /*
 * this method add do manu things!
 * 
 * key invokables sadrzi key value madels. value je lokacija modela
 */
    public function getServiceConfig(){
        
        return array(
              'abstract_factories' => array(),
            'aliases' => array(),
            #add new Model with callback function
            'factories' => array(
             'Get_moji_oglasi' => function($serviceLocator) {
               $dbAdapter=$serviceLocator->get('Zend\Db\Adapter\Adapter');
                       $sess=SessionManipulation::get_user_session(); 
                       $id=$sess['id'];
                       $oglasi=$serviceLocator->get("Oglasi");
                       $count=$oglasi->count_ads();
               return new MojiOglasi($dbAdapter,$id,$count);
            },                           
               
                
                
                
            ),
        
   #add new models---------------------------------------------------------
            'invokables' => array(
               
                
                
            ), 
    #----------------------------------------------------------------------        
            
            'services' => array(),
            'shared' => array(),  
            
            
            
        );
        
        
        
        
    }
     
     
     
     
     
     
     
     
     
     
     
}














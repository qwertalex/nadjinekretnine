<?php

namespace MojProfil\Controller;

use Zend\Mvc\Controller\AbstractActionController,
Zend\View\Model\ViewModel;
//Zend\Mail\Message;
 //use Zend\Mail\Transport\Sendmail as SendmailTransport;

//local
use MojProfil\Models\MojiPodatci\ChangeEmail\ChangeEmail,
    MojProfil\Models\MojiPodatci\ChangePass\ChangePassword,
MojProfil\Models\MojiPodatci\ChangeUsername\ChangeUsername,
MojProfil\Models\MojiPodatci\FormMojiPodatci,
MojProfil\Models\MojiOglasi\FormFilter,
Application\Models\Oglasi\AdsInfo,
Application\Models\User\SessionManipulation,
Application\Models\Form\KontaktPodrska\KontaktPodrska,
MojProfil\Models\PravnaLica\MojiPodatci\PravniPodatciForm,
MojProfil\Models\EmailObavestenja\EmailObavestenja,
Application\Models\Images\ImageValidation as ImgVal,
MojProfil\Models\PravnaLica\MojiPodatci\PlMojiPodatci;

class MojProfilController extends AbstractActionController {

    protected $form_moji_podatci_var;
    protected $user_data_manipulation_var;
    protected $change_email_var;
    protected $form_change_password_var;

 

    public function indexAction() {
         
        $request =$this->getRequest();
        $get=$request->getQuery()->toArray();        
         if (isset($get['action'])&&$get['action']==="logout") {
            SessionManipulation::logout('login');
              $this->redirect()->toUrl('http://nadjinekretnine.com/');
                  
         }
         
          $User=$this->getServiceLocator()->get('User');
         
          
          
          
         $viewModel=new ViewModel([
             'user_type'        =>$User->user_type(),
             'pl_validation'=>$User->get_pravna_lica_data(),
             
         ]);
       
        return $viewModel;
        
    }
    
    
    
    public function deklaracijaAction(){
         $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface'); 
         $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
         $renderer->headLink()->appendStylesheet('/libraries/icheck/flat/blue.css');     
         $renderer->headScript()->appendFile( '/js/moj-profil.js');   
         //-----------------------------------------------------
        $User=$this->getServiceLocator()->get('User');
        $request=$this->getRequest();
        
        $pravni_podatci=new PravniPodatciForm($User->get_pravna_lica_data());
         $pravna_lica_podatci_modal=false;
        //$User->on_off_user_validation(false);
         $post= $request->getPost()->toArray();
          //PRAVNA LICA PODATCI
         if (isset($post['submit_pravna_lica_podatci'])) {
             $pravni_podatci->setData($post);
                        //  var_dump($post);
             if ($pravni_podatci->isValid($post)) {
                 $post=$pravni_podatci->getData();
                 $User->pl_deklaracija($post);
                  $pravni_podatci->insert_into_db($post, $User->get_user_id(),$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
                  $pravna_lica_podatci_modal=true;
                  $User->on_off_user_validation(true);
                   $this->redirect()->toUrl('http://nadjinekretnine.com/moj-profil?deklaracija=ok');
             }
         }
         
        
        
        $user_not_valid=false;
         if ($this->params()->fromQuery('user_not_valid')) {
            $user_not_valid=true;
         }
        
        
        
        $viewModel=new ViewModel([
            "form"             =>$pravni_podatci, 
            "user_valid_obavesenje"=>$user_not_valid,
            
        ]);
        
        
        
         
        
        return $viewModel;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
   /**
    * 
    * @return ViewModel
    * OPIS:
    * Vadi podatke iz baze i menja podatke o useru u tabelama users_nk i newsletter
    * postoje tri forme:
    * 1.menja email
    * 2.menja password
    * 3.menja dodtne podatke
    * 
    */
    public function mojiPodatciAction() {

         $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface'); 
         $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
         $renderer->headLink()->appendStylesheet('/libraries/icheck/flat/blue.css');     
         $renderer->headScript()->appendFile( '/js/moj-profil.js');            
     
         //forma-polja za formu sa user podatcima
         $form_dodatni_podatci=  new FormMojiPodatci();
         //---------------------------------------------------
        $modal_ok=FALSE;
        
         //user class
         $User=$this->getServiceLocator()->get('User');
       
        
         $form_email_pravna_lica=new EmailObavestenja($User->get_newsletter());
         $modal_pl_email_obavestenja=false;
         
        $request=$this->getRequest();
        
         $change_email=new ChangeEmail();
         $email_errors=false;
         
         $change_pass=new ChangePassword();
         $pass_errors=FALSE;
         
         $change_username=new ChangeUsername($User);
         $username_errors=false;
         
         
         $pl_mp_form=new PlMojiPodatci();
         
         
         
         
         
         
         
         
         
         
         
         if ($request->isPost()){            
             $post= $request->getPost()->toArray();
             
             //CHANGE EMAIL
             if (isset($post['change-email-submit'])) {
                 $change_email->setData($post);
                 if ($change_email->isValid($post)) {
                     $post=$change_email->getData();
                     if ($User->update_email($post['email'],$post['password'])) {
                         $modal_ok=TRUE;
                     }else{
                           $email_errors="Polje Lozinka ili Email nije validno!";
                      }
                  }
             }
              //CHANGE Username
             if (isset($post['change-username-submit'])) {
                 $change_username->setData($post);
                 if ($change_username->isValid($post)) {
                     $post=$change_username->getData();
                     if ($User->update_username($post['new_username'],$post['password'])) {
                         $modal_ok=TRUE;
                     }else{
                           $username_errors="Polje Lozinka ili Korisnicko ime nije validno!";
                      }
                  }
             }            
             //--------------------------------------------------
             //PROMENI LOZINKU
             if (isset($post['change-pass-submit'])) {
                 $change_pass->setData($post);
                 if ($change_pass->isValid($post)) {
                     $post=$change_pass->getData();
                     if ($User->update_password($post['password'],$post['old_password'])) {
                         $modal_ok=TRUE;
                     }else{
                         $pass_errors="Polje Lozinka nije ispravno!";
                     }
                 }
             }
            
             
             
             
             
             if (isset($post['pl_mp_submit'])) {
                 $post=array_merge_recursive(
            $this->getRequest()->getPost()->toArray(),
            $this->getRequest()->getFiles()->toArray()
            );
          //  var_dump($post);
                 
                 $pl_mp_form->setData($post);
                 if ($pl_mp_form->isValid($post)) {
                     $post=$pl_mp_form->getData();
                       if ($post['mp_logo']['error']===4) {
                        // var_dump($post);
                         $User->update_pl_data($post);
                         $modal_ok=TRUE;
                     }else{
                 $ImgVal=new ImgVal($post['mp_logo']);
                 if (!$ImgVal->extension('jpg,png,jpeg')) {
                     $pl_mp_form->get("mp_logo")->setMessages(['Dozvoljni formati su .jpg,png,jpeg!']);
                 }elseif (!$ImgVal->size('10MB')) {
                         $pl_mp_form->get("mp_logo")->setMessages(['Maximalna velicina file je 10MB!']);
                    }elseif(count($pl_mp_form->getMessages())==0){ 
                   $file_name_logo= $ImgVal->rename_move('/var/www/nadjinekretnine.com/public/img/logo');
                   $post['mp_logo']=$file_name_logo;
                    $User->update_pl_data($post);
                  //  var_dump($post);
                   //     $modal_ok=TRUE;
                 }
              } 
            }else{
                     //form not valide
                     
                 }
                 
                 
                 
                 
                 
                 
                 
                 
                 
             }
             
             
   
         //PROMENI DODATNE PODATKE
         if (isset($post['mp_submit'])) {
             $post=$this->getRequest()->getPost()->toArray();
           //  var_dump($post);
          $form_dodatni_podatci->setData($post);
             //  $form_dodatni_podatci->setData($post);
             if ($form_dodatni_podatci->isValid($post)) {
                 $post=$form_dodatni_podatci->getData();
                 $User->update_pl_data($post);
                $form_email_pravna_lica->inser_into_db($post,$User->get_user_id(),$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
                
                        $modal_ok=TRUE;
                }
            // var_dump($form_dodatni_podatci->getMessages());
         }
         
         
       
         
         //PRAVNA LICA EMAIL OBAVESTENJA
         if (isset($post['submit_pravna_lica_email_obavestenja'])) {
             $form_email_pravna_lica->setData($post);
             if ($form_email_pravna_lica->isValid($post)) {
                 $post=$form_email_pravna_lica->getData();
                 //var_dump($post);
                $form_email_pravna_lica->inser_into_db($post,$User->get_user_id(),$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
                 $modal_pl_email_obavestenja=true;
             }
         }
         
         
         
    }  
        
        
       
        
        
     //vadi iz baze podatke o useru i podatke o newsltteru         
         $user_data=$User->get_by_id();
         $newsletter=$User->get_newsletter();
         

         
         
         
         $viewModel= new ViewModel([
             'db_data'          => $user_data,
            ]);
         
        //GET USER TYPE:fizicko ili pravno lice
         $user_type=$User->user_type();
            $menu=new ViewModel([
              'user_type'=>$user_type,
               'action'=>$this->params()->fromRoute('action'),
         ]);
         $menu->setTemplate("menu");       
        $viewModel->addChild($menu,"menu");           
         
         
         /*
          * if is fizicko lice
          */
        if ($user_type==1) {
           $moji_podatci=[
                 "form"             =>$form_dodatni_podatci,  
                 'newsletter'       =>$newsletter,
                 'db_data'          => $user_data,               
           ];
           $moji_podatci_template='dodatni_podatci';
           
        }elseif($user_type==2){//PRAVNA LICA
            
            
           $moji_podatci=[
               "form_dodatno"             =>$pl_mp_form,
               'db_data'          => $user_data, 
              
                'form_email'       =>$form_email_pravna_lica,
                'newsletter'       =>$newsletter,              
             
               'modal_email_obavestenja'=>$modal_pl_email_obavestenja,
               
               
           ];
           $moji_podatci_template='dodatni_podatci_pravna_lica';            
        }else{
           // return new ViewModel();
         //   throw new \Exception("Korisnik nije validan!");
        }
        
        //LOAD helpers
         $moji_podatciHelper=new ViewModel($moji_podatci);
          $moji_podatciHelper->setTemplate( $moji_podatci_template);
          $viewModel->addChild($moji_podatciHelper,"moji_podatci");
         
          
          //helper for change email password and username
          $modalsHelper=new ViewModel([
             'change_username'  => $change_username,
             'change_email'     => $change_email,
             'change_pass'      => $change_pass,
             'modal_ok'         => $modal_ok,
             'email_errors'     => $email_errors,
             'pass_errors'      => $pass_errors,
             'username_errors'  => $username_errors,     
             
                 ]);
          $modalsHelper->setTemplate("dodatni_podatci_modals");        
                         
         
         
         $viewModel->addChild($modalsHelper,"dodatni_podatci_modals");
         
        return $viewModel;
    
        
    }
    
    
  
    
    public function mojiOglasiAction() {
      // var_dump($_GET);
     // var_dump($_POST);
 
         $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');   
         $renderer->headLink()->appendStylesheet('/libraries/font-awesome-4.1.0/css/font-awesome.min.css');
         $renderer->headLink()->appendStylesheet('/css/bootstrap-social.css');
         $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
         $renderer->headLink()->appendStylesheet('/libraries/icheck/line/blue.css');  
         $renderer->headLink()->appendStylesheet('/libraries/icheck/minimal/blue.css'); 
         $renderer->headLink()->appendStylesheet('/libraries/select2/select2.css');
         $renderer->headScript()->appendFile( '/libraries/select2/select2.min.js');      
         $renderer->headScript()->appendFile( '/js/moj-profil.js');        
        
    
         $User=$this->getServiceLocator()->get('User');
         $MojiOglasi=$this->getServiceLocator()->get('Get_moji_oglasi');  
         
         $filter=new FormFilter();
         //class PlAccounts nasledjuje class Oglasi
         $PlAccounts=$this->getServiceLocator()->get('PlAccounts');// $Oglasi=$this->getServiceLocator()->get('Oglasi');
         //----------------------------------------------------------------ddd
      
     
        
        $request=$this->getRequest(); 
        
        /*
         * 
         * POST za razne stvari:menjanje stanja oglasa(on off) za standardni oglas i iskljucivanje placenih oglasa
         */
         if ($request->isPost()) {
           $post= $request->getPost()->toArray();//var_dump($post);
           if (isset($post['standard-id'],$post['state'])&&  is_int((int)$post['standard-id'])&&($post['state']=='on'||$post['state']=='off')) {
             // $MojiOglasi->vrsta_oglasa($post['standard-id'],$post['state']);
               $PlAccounts->on_off_free_standardni_oglas($post['standard-id'],$post['state']);
           }elseif(isset($post['delete-ad'])&&is_int((int)$post['delete-ad'])){
               $MojiOglasi->delete_oglas($post['delete-ad']);
           }elseif(isset($post['oglas_id'],$post['tip-oglasa'])&&is_int((int)$post['oglas_id'])&&($post['tip-oglasa']=='top'||$post['tip-oglasa']=='premium')){
              // var_dump($post);
               $MojiOglasi->iskljuci_placeni_oglas($post['oglas_id'],$post['tip-oglasa']);
           } elseif(isset($post['pravna-lica'])){
               $PlAccounts->set_data($post);
            }elseif(isset($post['pl-iskljuci-oglas'],$post['oglas_id'])){
                $PlAccounts->iskljuci_oglas($post['oglas_id']);
            }elseif(isset($post['pl-osvezi-oglas'],$post['oglas_id'])){
                $PlAccounts->osvezi_oglas($post['oglas_id']);
            }elseif(isset($post['submit-vaucer'])){
                 $PlAccounts->vaucer();
            }elseif(isset($post['oglas-potvrdi-produzi'])){
               // var_dump($post);
                $PlAccounts->produzi_oglas($post);
            }
         }
         /*
         * end post
         */
        
 $send_get_paginator=false;

 
        /*---------------------------------
         * FILTERI ZA PRETRAGU
         -----------------------------------*/
        if ($request->getQuery()){
                             
             $get= $request->getQuery()->toArray();
             
             if (isset($get['submit'])) {  
                 $filter->setData($get);//var_dump($get); 
                 if ($filter->isValid($get)) {
                     $get=$filter->getData($get); 
                     $MojiOglasi->set_get($get);
                     $send_get_paginator=http_build_query($get, '', '&');
                  }else{
                   //   var_dump( $filter->getMessages());
                  }
          } 
         }
       
       
         /*----------------------------------
          * paginacija
          ----------------------------------*/
      if ($this->params()->fromRoute('po-strani')) {
    if ( $this->params()->fromRoute('po-strani')==10||
                $this->params()->fromRoute('po-strani')==20||
                        $this->params()->fromRoute('po-strani')==50){
                        $po_strani=$this->params()->fromRoute('po-strani'); 
            }
           else{ $po_strani=10; }
         }else{$po_strani=10;}
        
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($MojiOglasi->get_oglasi()));
         $paginator->setCurrentPageNumber($this->params()->fromRoute('strana'))
                 ->setItemCountPerPage($po_strani);
       /*-------------------------------------------------
        * end paginaicja
       -------------------------------------------------- */   
         
          
         $user_type=$User->user_type();
       
         /*
          * HELPERI
          */
        $viewModel=new ViewModel([
           
         ]);

           $menu=new ViewModel([
             'user_type'=>$user_type,
               'action'=>$this->params()->fromRoute('action'),
         ]);
         $menu->setTemplate("menu");       
        $viewModel->addChild($menu,"menu");       
        
          $placena_ponuda=new ViewModel([
             'user_type'=>$user_type,
              'pravna_lica_data'=>$User->pravna_lica_all_data(),
              'ads_info'=>new AdsInfo(),
              'ads_top_ten_counter'=>$MojiOglasi->topTenAds(),
              
         ]);
         $placena_ponuda->setTemplate("ponuda_tip_oglasa");       
        $viewModel->addChild($placena_ponuda,"placena_ponuda");
        
        
        
         $user_helper=new ViewModel([
             'user_type'=>$user_type,
           'paginator'  =>$paginator,
            'po_strani' =>$po_strani,
             'free'     =>$PlAccounts->count_active_ads(),
             'get_paginator'=>$send_get_paginator,
             
        ]);
         $user_helper->setTemplate("user_helper");
         
         
         
         
         $user_filters=new ViewModel([
             'filter'=>$filter,
             'moji_oglasi'=>$MojiOglasi->filter_counter()
         ]);
         $user_filters->setTemplate("user_filter_helper");
         
         $modals=new ViewModel([ 
             'Ads_info'=>new AdsInfo(),
             'user_type'=>$user_type,
             'istorija_uplate'=>$PlAccounts->get_istorija_racuna()
             ]);
         $modals->setTemplate("modals");
         
         $viewModel->addChild($user_helper,"user_helper");
         $viewModel->addChild($user_filters,"user_filter");
         $viewModel->addChild($modals,"modals");
        return $viewModel;
    
       }
     
     
     
     
     
     
     
    
     public function favoritiAction() {
    
                $User=$this->getServiceLocator()->get('User');
                  $viewModel=new ViewModel([
              
         ]);
         $user_type=$User->user_type();
            $menu=new ViewModel([
              'user_type'=>$user_type,
               'action'=>$this->params()->fromRoute('action'),
         ]);
         $menu->setTemplate("menu");       
        $viewModel->addChild($menu,"menu");           

         return $viewModel;
          
         
         
         
         return new ViewModel();
         
     }
    
    
     public function porukeAction() {
        
          $User=$this->getServiceLocator()->get('User');
                  $viewModel=new ViewModel([
              
         ]);
         $user_type=$User->user_type();
            $menu=new ViewModel([
              'user_type'=>$user_type,
               'action'=>$this->params()->fromRoute('action'),
         ]);
         $menu->setTemplate("menu");       
        $viewModel->addChild($menu,"menu");           

         return $viewModel;
          
         
     }
    
     public function pomocPodrskaAction() {
            $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');   
         $renderer->headLink()->appendStylesheet('/libraries/font-awesome-4.1.0/css/font-awesome.min.css');    
         $User=$this->getServiceLocator()->get('User');

         $form=new KontaktPodrska();
         $request=  $this->getRequest();
         $modal_ok=false;
         if ($request->isPost()) {
             $post=$request->getPost()->toArray(); 
             $form->setData($post);
             if ($form->isValid($post)) {
                 $valid_data=$form->getData();
           
                  $User->podrska($valid_data['name'],$valid_data['message']);
                 
                   $modal_ok=TRUE;
                   $form->setData([
                       'name'   =>"",
                       'message'=>"",
                   ]);
             }
             
         }          
         $viewModel=new ViewModel([
             'data'         =>$User->get_by_id(),
             'form'         =>$form,
             'modal'        =>$modal_ok,
             
         ]);
         $user_type=$User->user_type();
            $menu=new ViewModel([
              'user_type'=>$user_type,
               'action'=>$this->params()->fromRoute('action'),
         ]);
         $menu->setTemplate("menu");       
        $viewModel->addChild($menu,"menu");           

         return $viewModel;
         
     }
    
    /**
     * 
     * @return ViewModel
     * 
     * REFRESH CAPTCHA
     */
     public function reloadCaptchaAction() {
         
         $form=new KontaktPodrska();
         $captcha = $form->get('captcha')->getCaptcha();
           $data = array();

    $data['id']  = $captcha->generate();
    $data['src'] = $captcha->getImgUrl() .
                   $captcha->getId() .
                   $captcha->getSuffix();

         $viewModel=new ViewModel();
         $viewModel->setTerminal(TRUE);
         $viewModel->setVariable('json',  json_encode($data));
         return $viewModel;
     }   
   
    
}



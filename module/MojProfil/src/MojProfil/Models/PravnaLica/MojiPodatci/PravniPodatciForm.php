<?php

namespace MojProfil\Models\PravnaLica\MojiPodatci;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of PravniPodatci
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
use Application\Models\FormGenerator\Filter\FilterGenerator;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;

class PravniPodatciForm extends \Zend\Form\Form {
    protected $db_data;

     public function __construct($db_data) {
         parent::__construct('pravna-lica');//ime forme
         $this->setAttribute('action', '');
         $this->setAttribute('method', 'post');
         $this->setAttribute('class', 'form-horizontal');
         $this->setAttribute('enctype', 'multipart/form-data');
         $this->setInputFilter(new Filter());
         $this->db_data=$db_data;
       
             #naziv_firme-------------------------------------         
        $this->add(array(
             'name'=>'naziv_firme',
             'attributes'=>array(
                  'type'=>'text',
                 'value'=>  $this->set_default_values('naziv_firme'),
            ),
        ));
     #-----------------------------------------   
        
     #ulica-------------------------------------         
        $this->add(array(
             'name'=>'ulica',
             'attributes'=>array(
                  'type'=>'text',
                 'value'=>  $this->set_default_values('ulica'),
            ),
        ));
     #-----------------------------------------     
        
        
          #Maticni broj-------------------------------------         
        $this->add(array(
             'name'=>'maticni_broj',
             'attributes'=>array(
                  'type'=>'text',
                 'value'=>  $this->set_default_values('maticni_broj'),
            ),
        ));
     #----------------------------------------- 
                
          #Maticni broj-------------------------------------         
        $this->add(array(
             'name'=>'pib',
               'attributes'=>array(
                  'type'=>'text',
                   'value'=>  $this->set_default_values('pib'),
            ),
        ));
     #----------------------------------------- 
               $this->add(array(
             'name'=>'ime_prezime',
               'attributes'=>array(
                  'type'=>'text',
                   'value'=>  $this->set_default_values('ime_prezime'),
            ),
        ));
     #-----------------------------------------   
  
                   $this->add(array(
             'name'=>'telefon',
             'attributes'=>array(
                  'type'=>'text',
                   'title'=>'U ovo polje unesite samo brojeve.',
                 'value'=>  $this->set_default_values('telefon'),
            ),
        ));
     #-----------------------------------------    
                   
                   
                #CHECKBOX--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'saglasnost',
             'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           'attributes'=>[
               'class'=>'flat-icheck',
               'value' => 1
           ]
         ));
      #-----------------------------------------       
     
                   
       #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit_pravna_lica_podatci',
            'attributes'=>array(
                'type'=>'submit',
               'class'=>'btn btn-primary list-your-property arrow-right',
                'value'=>'Sacuvaj',
           ),
           ));
        
        #-----------------------------------------    
       
        }
    
    
    
    
 public function set_default_values($name) {
        return isset($this->db_data[$name])?$this->db_data[$name]:"";
    }
     
   
     
     public function insert_into_db($post,$user_id,$adapter) {
           
         return false;
         
         
         
         /*
         
         $gateway = new TableGateway('pravna_lica', $adapter);     
     
           $select= $gateway->select([
               'id_users_nk'      =>  $user_id, 
            ]);
           $select_data= $select->current();
           if ($select_data) {
               $gateway->update([
              'naziv_firme'            =>  $post['naziv_firme'],
              'ulica'         =>  $post['ulica'],
              'pib'=>  $post['pib'],
              'maticni_broj'=>  $post['maticni_broj'],
              'ime_prezime'=>  $post['ime_prezime'],
              'telefon'=>  $post['telefon'],                  
               ]);
           }else{
                  $gateway->insert([
             'id_users_nk'      =>  $user_id,
              'naziv_firme'            =>  $post['naziv_firme'],
              'ulica'         =>  $post['ulica'],
              'pib'=>  $post['pib'],
              'maticni_broj'=>  $post['maticni_broj'],
              'ime_prezime'=>  $post['ime_prezime'],
              'telefon'=>  $post['telefon'],
           ]);              
           }
           */ 
  
      }
   
     
     
     
     
     
     
     
     
     
     
     
     
    
}




 

class Filter extends \Zend\InputFilter\InputFilter {
    
    
    public function __construct() {
        $filter=new FilterGenerator();
         $this->add($filter->string_length( [ 'name'=>'naziv_firme'],['min'=>1,'max'=>100,],'Naziv firme'));   
   #---------------------------------------------------------------------   
   $this->add($filter->string_length([ 'name'=>'ulica'],['min'=>1,'max'=>255,],'Ulica'));
   #---------------------------------------------------------------------     
   $this->add($filter->digits(['name'=> 'maticni_broj'],'Maticni broj'));
   #---------------------------------------------------------------------       
   $this->add($filter->digits(['name'=> 'pib'],'PIB'));
   #---------------------------------------------------------------------          
   $this->add($filter->string_length( [ 'name'=>'ime_prezime'],['min'=>1,'max'=>100,],'Ime i Prezime'));      
   #---------------------------------------------------------------------                            
    $this->add($filter->digits(['name'=> 'telefon'],'Telefon'));
   #---------------------------------------------------------------------           
   $this->add($filter->identical(['name'=>'saglasnost'], '1', 'Morate garantovati da su uneti podaci tačni.'));
        
    }
    
    
    
    
    
}














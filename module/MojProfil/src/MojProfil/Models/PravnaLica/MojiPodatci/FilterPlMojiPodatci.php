<?php

namespace MojProfil\Models\PravnaLica\MojiPodatci;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\InputFilter\InputFilter;
/**
 * Description of FilterPlMojiPodatci
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class FilterPlMojiPodatci  extends InputFilter {
    
    
     public function __construct() {
      
        $this->add(array(
            'name'=>'mp_ime',
        'required'=> TRUE,
        'allow_empty' => true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------   
        
   
        
        
           $this->add(array(
            'name'=>'mp_ulica',
        'required'=> TRUE,
        'allow_empty' => true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------       
        
        
        
            $this->add(array(
            'name'=>'mp_mesto',
        'required'=> TRUE,
        'allow_empty' => true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
        
                
        
            $this->add(array(
            'name'=>'mp_website',
        'required'=> false,
        'allow_empty' => true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '250',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
                   
 #mp_telefon    
        $this->add(array(
            'name'=>'mp_telefon',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'U polje telefon mozete uneti samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------    
        
        
        
              
                   
 #mp_mobilni   
        $this->add(array(
            'name'=>'mp_mobilni',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                 'U polje mobilni mozete uneti samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------     
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
}

<?php

namespace MojProfil\Models\PravnaLica\MojiPodatci;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Form\Form;
/**
 * Description of PlMojiPodatci
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class PlMojiPodatci extends Form  {
    
    
    
    
    public function __construct(){
         parent::__construct("pl-moji_podatci");
         $this->setAttribute('action', '');
         $this->setAttribute('method', 'post');
         $this->setAttribute('enctype','multipart/form-data');
         $this->setInputFilter(new \MojProfil\Models\PravnaLica\MojiPodatci\FilterPlMojiPodatci());
         
         
         
           
          #ime-----------------------------------       
        $this->add(array(
             'name'=>'mp_ime',
             'attributes'=>array(
                 'type'=>'text',
              //   'class'=>'input-orange-border input-medium search-query',
             ),
            
           ));
        #------------------------------------------------  
           #ime-----------------------------------       
        $this->add(array(
             'name'=>'mp_ulica',
             'attributes'=>array(
                 'type'=>'text',
             
             ),
            
           ));
        #------------------------------------------------         
         $this->add(array(
             'name'=>'mp_mesto',
             'attributes'=>array(
                 'type'=>'text',
            
             ),
            
           ));
        #------------------------------------------------   
    
         
         $this->add(array(
             'name'=>'mp_logo',
             'attributes'=>array(
                 'type'=>'file',
            
             ),
            
           ));
        #------------------------------------------------           
         
      #tel-----------------------------------   
         
        $this->add(array(
            'name'=>'mp_telefon',
            'attributes'=>array(
                 'type'=>'text',
                 'data-toggle'=>"popover",
                'data-placement'=>"right",
                'data-content'=>"U ovo polje unesite samo brojeve.",
                'data-original-title'=>"",               
                'class'=>'mp_samobrojevi'
             ),
           ));
        #------------------------------------------------       
        
    
        #mob-----------------------------------       
        $this->add(array(
            'name'=>'mp_mobilni',
            'attributes'=>array(
                 'type'=>'text',
                'data-toggle'=>"popover",
                'data-placement'=>"right",
                'data-content'=>"U ovo polje unesite samo brojeve.",
                'data-original-title'=>"",
                'class'=>'mp_samobrojevi'
             ),
           ));
        #------------------------------------------------     
   
        
        
                 $this->add(array(
             'name'=>'mp_website',
             'attributes'=>array(
                 'type'=>'text',
            
             ),
            
           ));
        #------------------------------------------------  
        
        
        
        
        #ime-----------------------------------       
        $this->add(array(
             'name'=>'pl_mp_submit',
             'attributes'=>array(
                 'type'=>'submit',
                 'class'=>'btn pull-right btn-primary arrow-right',
                 'value'=>'Sacuvaj izmene',
             ),
            
           ));
        #------------------------------------------------  
       
       
       
       
       
       
       
       
       
     
         
         
         
         
     
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

<?php
namespace MojProfil\Models\EmailObavestenja;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of EmailObavestenja
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
use Application\Models\FormGenerator\Filter\FilterGenerator;
use Zend\Db\TableGateway\TableGateway;


class EmailObavestenja  extends \Zend\Form\Form {
    
    protected $db_data;
    public function __construct($data) {
         parent::__construct('email-obavestenja');//ime forme
         $this->setAttribute('action', '');
         $this->setAttribute('method', 'post');
         $this->setAttribute('class', 'form-horizontal');
         $this->setAttribute('enctype', 'multipart/form-data');
         $this->setInputFilter(new Filter());
         $this->db_data=isset($data[0])?$data[0]:[];
      
                         #CHECKBOX--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'noviteti',
             'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           'attributes'=>[
               'class'=>'flat-icheck',
               'value' =>(int) $this->set_default_values('noviteti')
           ]
         ));
      #-----------------------------------------    
                       #CHECKBOX--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'istekli_oglasi',
             'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           'attributes'=>[
               'class'=>'flat-icheck',
               'value' => $this->set_default_values('istekli_oglasi')
           ]
         ));
      #-----------------------------------------    
                       #CHECKBOX--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'poruke',
             'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           'attributes'=>[
               'class'=>'flat-icheck',
               'value' => $this->set_default_values('poruke')
           ]
         ));
      #-----------------------------------------    
         
              #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit_pravna_lica_email_obavestenja',
            'attributes'=>array(
                'type'=>'submit',
               'class'=>'btn btn-primary list-your-property arrow-right',
                'value'=>'Sacuvaj',
           ),
           ));
        
        #-----------------------------------------     
    
     }
    
    
 public function set_default_values($name) {
        return isset($this->db_data[$name])?$this->db_data[$name]:"";
    }
         
    
        
     public function inser_into_db($post,$user_id,$adapter) {
            $gateway = new TableGateway('newsletter', $adapter);     
          $gateway->update([
             
             'noviteti'         =>$post['noviteti'],
              'istekli_oglasi'  =>$post['istekli_oglasi'],
              'poruke'          =>$post['poruke']
           ],[
               'id_users_nk'      =>  $user_id,
           ]);
      }
   
     
      
      
      
    
}


class Filter extends \Zend\InputFilter\InputFilter {
    
    
    public function __construct() {
        $filter=new FilterGenerator();
        
       $filter->bettwen(['name'=>'noviteti'], [
           'min'=>0,
           'max'=>1
       ], '');
       
       
       
    }
    
    
    
    
    
}

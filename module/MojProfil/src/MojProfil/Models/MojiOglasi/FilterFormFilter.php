<?php
namespace MojProfil\Models\MojiOglasi;


use Zend\InputFilter\InputFilter;

//local
 


class FilterFormFilter extends InputFilter {
    
    
    
    
    public function __construct() {
 
        
   $this->add([
             'name'=>'id_oglasa',
             'required'=>true,
             'allow_empty' => true,
            'filters'  => array(
                 ['name' => 'StripTags'],
                 ['name' => 'StringTrim']
                    ),
             'validators' => array(
                  [
                      'name'=>'Zend\Validator\Digits',
                      'break_chain_on_failure' => true,
                      'options'=>[
                          'messages'=>[
                              \Zend\Validator\Digits::NOT_DIGITS =>"Polje id prima samo brojeve!",
                              \Zend\Validator\Digits::STRING_EMPTY => 'Polje id je obavezno!',
                              \Zend\Validator\Digits::INVALID => "Polje id prima samo brojeve!" ,
                          ]
                      ]
                      ],
                  
                
                ), 
   ]);
        
        
        
        
        
        
        
        
        
        
        
        
        
          #kategorija    
        $this->add(array(
            'name'=>'kategorija',
            'allow_empty' => true, 
            'requeried'=>true,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
                                       [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Katgorija je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 14,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------       
        
        
         
        #mesta_srbija    
        $this->add(array(
            'name'=>'mesta_srbija',
            'requeried'=>true,
          'allow_empty' => true, 
            'filters'  => array(
                     //  array('name' => 'Int'),
                    ),
               'validators' => array(     
                 [
                  'name'=>'Regex',
                     'options'=>[
                     'pattern' => '/^[\d\,]+$/',
                     'messages'=>[
                     \Zend\Validator\Regex::NOT_MATCH=>
                       'Polje kvadratura prima samo brojeve!'
                     ]]
                 ]
            ),
                 ));
   #---------------------------------------------------------------------  
        
         #---------------------------------------------------------------------    
        
           #deo_mesta     
        $this->add(array(
            'name'=>'deo_mesta',
           'requeried'=>true,
          'allow_empty' => true, 
            'filters'  => array(
                     //  array('name' => 'Int'),
                    ),
               'validators' => array(     
                  [
                  'name'=>'Regex',
                     'options'=>[
                     'pattern' => '/^[\d\,]+$/',
                     'messages'=>[
                     \Zend\Validator\Regex::NOT_MATCH=>
                       'Polje kvadratura prima samo brojeve!'
                     ]]
                 ]        
       
            ),
                 ));
   #---------------------------------------------------------------------   
        
             
        
 #lokacija    
        $this->add(array(
            'name'=>'lokacija',
           'requeried'=>true,
          'allow_empty' => true, 
            'filters'  => array(
                     //  array('name' => 'Int'),
                    ),
               'validators' => array(     
                  [
                  'name'=>'Regex',
                     'options'=>[
                     'pattern' => '/^[\d\,]+$/',
                     'messages'=>[
                     \Zend\Validator\Regex::NOT_MATCH=>
                       'Polje kvadratura prima samo brojeve!'
                     ]]
                 ]            
            ),
                 ));
   #---------------------------------------------------------------------       
        
        
   #ulica   
        $this->add(array(
            'name'=>'ulica',
        'required'=> true,
        'allow_empty' => true,
            
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------      
          
   #klima
    $this->add(array(
            'name'=>'aktivni',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------       
        
        
        $this->add(array(
            'name'=>'isticu',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------    
        
        
                $this->add(array(
            'name'=>'cena-od',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje kvadratura je obavezno!' 
                            ],
                        ],
                     ], 
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Polje kvadratura prima samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------  
        
        
              $this->add(array(
            'name'=>'cena-do',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje kvadratura je obavezno!' 
                            ],
                        ],
                     ], 
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Polje kvadratura prima samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------    
        
        
                $this->add(array(
            'name'=>'kvadratura-od',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje kvadratura je obavezno!' 
                            ],
                        ],
                     ], 
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Polje kvadratura prima samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------  
        
        
        
                $this->add(array(
            'name'=>'kvadratura-do',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje kvadratura je obavezno!' 
                            ],
                        ],
                     ], 
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Polje kvadratura prima samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------  
        
        
        
          
        $this->add(array(
            'name'=>'prodaja',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------                
                
                
                
                
            
        $this->add(array(
            'name'=>'izdavanje',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------              
                
                
                
                
                
                
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
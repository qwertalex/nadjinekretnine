<?php
namespace MojProfil\Models\MojiOglasi;

//use Zend\Db\TableGateway\TableGateway;
//use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Zend\Db\Sql\Sql;

//local
//use Application\Models\LokacijaSr\Lokacija,
 //Application\Search\Interpreter,
//Pretraga\Models\Interpreter\GetAdText;
use Application\Models\Oglasi\AdsManipulaton;
use Application\Models\Oglasi\Render\RenderView as Html;

/*Description:
 * 
 * Ova class-a se koristi u route mojprofil
 * 
 * Methods:
 * -__construct(adapter,user_id,$ad_count)
 * -----------------------------------------------
 * -get_oglasi($row_data=false,$view=true)
 * vraca sve oglase iz gomile tabela od odredjenog usera
 * bez parametara return method  [[return $this->prepare_for_view($res);]]
 * moze da vrati i bez method prepare_for_view(); ako se stavi $row_data=true;
 * ----------------------------------------------------------------------------
 * public set_get(get)-uzima $_GET variabilu i stavlja u globalnu $this->get
 * -------------------------------------------------------------------------
 *  protected function prepare_for_view(Array $res){}
 * ovu function poziva get_oglasi() i ona vraca array sa izmenjenim vrednostima
 * koji kasnije olaksavaju izbacivanje u view
 * npr. iz baze se vracaju brojevi za razne stvari npr. za lokaicja.mesto [[ Lokacija::get_mesta_srbija_text($database_value);]]
 * ------------------------------------------------------------------------------------------------------------------------------
 * protected function applay_filters($get)
 * poziva se u method public function get_oglasi($row_data=false,$view=true)
 * vraca filtrirane ogase. AKo korisnik hoce da filtrira svoje oglase onda se aktivira ova funkcija i 
 * u method public function get_oglasi($row_data=false,$view=true) aktivira filtere
 * -----------------------------------------------------------------------------
 *  public function delete_oglas($oglas_id)
 * ----------------------------------------------
 * public function iskljuci_placeni_oglas($oglas_id,$vrsta_oglasa)
 * Koristi se za fizicka lica  da iskljuce placeni oglas
 * -------------------------------------------------------------------
 * public function count_oglasi_test($column,$join_table=false,array $where=[])
 * broji jedan oglas iz tabele sa optional where couse
 *     $c_kat= $this->count_oglasi_test('kategorija');
 *     $c_mesto=$this->count_oglasi_test('mesto','lokacija',$mesto_filter);
 * -------------------------------------------------------------------
 *  public function filter_counter(){
 * broji oglase i radi query za svaki row (uzas)
 * takodje ako se ukljuci filter pretraga onda broji u skladu sa filterom
 * U svakom slucaju broji oglase 
 * ---------------------------------------------
 *  public function topTenAds()
 * vraca top 10 ogalsa po broji pregleda
 * 
 * 
 * 
 * 
 */



class MojiOglasi{

    protected $adapter;
    protected $get;
    protected $user_id;
    protected $ad_count;
    protected $oglas_id;
    public function __construct(\Zend\Db\Adapter\Adapter $adapter,$id,$ad_count) {
        $this->adapter=$adapter;
        $this->user_id=$id;
        $this->ad_count=$ad_count;
    }
    
    
    public function get_oglasi($row_data=false,$view=true) {
        $sql=new Sql($this->adapter);
        $select = $sql->select();
        $select->columns(['id_oglasi_nk','naslov','broj_pregleda','kategorija','prodaja_izdavanje']);
        $select->from( 'oglasi_nk')->where(["oglasi_nk.id_users_nk=$this->user_id"]);
        
        $select->join('lokacija','lokacija.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['mesto','lokacija','deo_mesta']);
        $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[
                      'datum_postavljanja_oglasa','datum_isteka','vrsta_oglasa'     
                        ], $select::JOIN_LEFT);
        $select->join('dodatno','dodatno.id_oglasi_nk = oglasi_nk.id_oglasi_nk',
                     ['grejanje','spratovi','ukupno_spratovi','kvadratura','cena','broj_soba','tip_objekta','opis_objekta','nacin_placanja','namestenost'],
                     $select::JOIN_LEFT);
        $select->join('slike','slike.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['slika_1'], $select::JOIN_LEFT);
     
        $select->order("oglasi_nk.datum_kreiranja_oglasa DESC"); 
        $select->order("vrsta_oglasa.datum_postavljanja_oglasa DESC");          
          
     
           if (!$row_data&&isset($this->get)&&$applay=$this->applay_filters($this->get)) {
              //var_dump($applay);
                $select->where($applay);
            }
          
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $res = array_values(iterator_to_array($results));//var_dump($res);
             if ($row_data||!$view) {
                 return $res;
             }
             return $this->prepare_for_view($res);
          
         }
          return [];
    }
    
    
    
    public function set_get($get) {
        $this->get=$get;
    }
    
    
    protected function prepare_for_view(Array $res) {
        
        foreach ($res as $key => $v) {
          //  $res[$key]=array_filter($res[$key]);
            unset($v['id_lokacija']);
          $res[$key]['mesto']=Html::get_mesto($v['mesto']);
          $res[$key]['deo_mesta']=Html::get_deo_mesta($v['mesto'],$v['deo_mesta']);
          $res[$key]['lokacija']=Html::get_lokacija($v['mesto'],$v['deo_mesta'],$v['lokacija']); 
           
          $res[$key]['link']=Html::create_link($res[$key]['kategorija'], $res[$key]['id_oglasi_nk'], $res[$key]['naslov']);
          
          
          
            $res[$key]['kategorija']=Html::kategorija($res[$key]['kategorija']);
            $res[$key]['prodaja_izdavanje']=Html::get_pro_izd($res[$key]['prodaja_izdavanje']);
            $slika=isset($v['slika_1'])?$v['slika_1']:false;
           $res[$key]['slika_1']=Html::image($slika);
            $cena=isset($v['cena'])?$v['cena']:false;
          $res[$key]['cena']=Html::cena_valuta($cena);
            
       
            
        }
      // var_dump($res);
        return $res;
        
        
    }
    
    
    
    
    protected function applay_filters($get) {
          $ready=[];
         // var_dump(array_filter($get));
          if (isset($get['id_oglasa'])&&trim($get['id_oglasa'])!="") {
               $ready['oglasi_nk.id_oglasi_nk']=$get['id_oglasa'];
               return $ready;
          }
          $kat=$get['kategorija'];
          if (in_array($kat, range(1,14))) {
              $ready['kategorija']=$kat;
          }
  
          $mesta=isset($get['mesta_srbija'])&&  trim($get['mesta_srbija'])!=""?$get['mesta_srbija']:false;
        if ($mesta) {
             if (strpos($mesta,',')===false) {
               $ready['mesto']=(int)$mesta; 
            }else{ 
                $me=explode(',', $mesta);
                foreach ($me as $mm) {
                    $ready['mesto'][]=(int)$mm;
                }
            }
            
        }
        if (isset($get['deo_mesta'])&&trim($get['deo_mesta'])!="") {
            if (strpos($get['deo_mesta'], ',')===false) {
               $ready['deo_mesta']=(int)$get['deo_mesta']; 
            }else{
                $deo_d=explode(',',$get['deo_mesta']);
                foreach ($deo_d as $dd) { 
                    $ready['deo_mesta'][]=(int)$dd;
                }
            }
            
        }
        if (isset($get['lokacija'])&&trim($get['lokacija'])!="") {
            if (\strpos($get['lokacija'], ',')===false) {
               $ready['lokacija']=(int)$get['lokacija']; 
            }else{
                $lokacija_d=explode(',',$get['lokacija']);
                foreach ($lokacija_d as $dlokacija) {
                    $ready['lokacija'][]=(int)$dlokacija;
                }
            }            
         }        
        
        
        
        
        $aktivni=$get['aktivni']==1?true:false;
        if ($aktivni) {
             $predicate = new  \Zend\Db\Sql\Where(); 
             $ready[]=$predicate->greaterThan('vrsta_oglasa.vrsta_oglasa',0 );
        }
        
        if ($get['isticu']!=0) {
            $date = strtotime("+2 day");
            $date_2= date('Y-m-d H:i:s', $date);    
            $current=date("Y-m-d H:i:s");
            $ready[]=new \Zend\Db\Sql\Predicate\Between("vrsta_oglasa.datum_isteka",$current,$date_2);
         }
        
         
         if ( trim($get['cena-od'])!=""&&trim($get['cena-do'])!="") { 
             $ready[]=new \Zend\Db\Sql\Predicate\Between("dodatno.cena",$get['cena-od'],$get['cena-do']); 
             
         }else if(trim($get['cena-od'])!=""&&trim($get['cena-do'])==""){
             $predicate = new  \Zend\Db\Sql\Where(); 
             $ready[]=$predicate->greaterThanOrEqualTo('dodatno.cena',$get['cena-od'] );
            // $ready[]=$predicate->greaterThanOrEqualTo('selidbe.selidbe_cena',$get['cena-od'] );
         }else if(trim($get['cena-do'])!=""&&trim($get['cena-od'])==""){
             $predicate = new  \Zend\Db\Sql\Where(); 
             $ready[]=$predicate->lessThanOrEqualTo('dodatno.cena',$get['cena-do'] );
         }
         
         
          
         if ( trim($get['kvadratura-od'])!=""&&trim($get['kvadratura-do'])!="") {  
             $ready[]=new \Zend\Db\Sql\Predicate\Between("dodatno.kvadratura",$get['kvadratura-od'],$get['kvadratura-do']); 
             
         }else if(trim($get['kvadratura-od'])!=""&&trim($get['kvadratura-do'])==""){   
             $predicate = new  \Zend\Db\Sql\Where(); 
             $ready[]=$predicate->greaterThanOrEqualTo('dodatno.kvadratura',$get['kvadratura-od'] );
         }else if(trim($get['kvadratura-do'])!=""&&trim($get['kvadratura-od'])==""){
                                
             $predicate = new  \Zend\Db\Sql\Where(); 
             $ready[]=$predicate->lessThanOrEqualTo('dodatno.kvadratura',$get['kvadratura-do'] );
            // $ready[]=$predicate->isNotNull('dodatno.kvadratura');
         }        
         
         
         
          if ($get['prodaja']=='1'&&$get['izdavanje']=='1') {
          /*   $where = new \Zend\Db\Sql\Where();
            $ready[]=$where->nest() ->equalTo('oglasi_nk.prodaja_izdavanje', 1)->or->equalTo('oglasi_nk.izdavanje', 1);
           * 
           */
         }elseif($get['prodaja']!='0'){
             $ready['prodaja_izdavanje']=1;
         }elseif($get['izdavanje']!='0'){
             $ready['prodaja_izdavanje']=2;
         }
         
           if (count($ready)>0) {
            return $ready;
        }
        return false;
        
    }
    
    
    
    
    /**
     * 
     * @param type $oglas_id
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    public function delete_oglas($oglas_id) {
       $AdsManipulation= new AdsManipulaton($this->adapter,  $this->user_id);
       $AdsManipulation->delete_oglas($oglas_id);
    }
    
    
    

    
    
    
    
     public function iskljuci_placeni_oglas($oglas_id,$vrsta_oglasa) {
         
         
         switch($vrsta_oglasa){
          case 'top':$num=2;break;
          case 'premium':$num=3;break;
     }
         
          $sql=new Sql($this->adapter);
          //check if user exsists
           $select=$sql->select();
           $select->from('oglasi_nk');
            $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['vrsta_oglasa']);
           $select->where([
                      'oglasi_nk.id_oglasi_nk'=>$oglas_id,
                     'oglasi_nk.id_users_nk'=>$this->user_id,              
           ]);
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();$res = array_values(iterator_to_array($results));
         if ($results->count()==1&&$res[0]['vrsta_oglasa']==$num) { 
                 $update = $sql->update();
                 $update->table('vrsta_oglasa');
                 $update->set(['vrsta_oglasa'=>0,'datum_postavljanja_oglasa'=>NULL,'datum_azuriranja_oglasa'=>NULL]);
                 $update->where([
                     'id_oglasi_nk'=>$oglas_id,
                 ]);
               $statement = $sql->prepareStatementForSqlObject($update);
               $results = $statement->execute();
               if ($results->count()==1) {
                  return true;
             
               }          
         }
         
         return false;
     }
    
    
     
     
     
     
     
     
     
        
    public function count_oglasi_test($column,$join_table=false,array $where=[]) {
        $columns=[$column,'count'=>new \Zend\Db\Sql\Expression('COUNT(*)')];
        $columns_lokacija=[];
        $columns_vrsta_oglasa=[];
        $columns_dodatno=[];
               $sql=new Sql($this->adapter);
                  $select = $sql->select();
                  if (!$join_table) {
                    $select->columns($columns);   
                  }else{
                      $select->columns([]);
                  }
                   
                    $select->from('oglasi_nk')->where([
                        "id_users_nk"=>$this->user_id,
                    ]);
                    switch($join_table){
                        case "lokacija":$columns_lokacija=$columns;break;
                        case "vrsta_oglasa":$columns_vrsta_oglasa=$columns;$columns_vrsta_oglasa[]='vrsta_oglasa'; break;
                        case 'dodatno':$columns_dodatno=$columns;break;
                    }
                   
                    
                    $select->join('lokacija','lokacija.id_oglasi_nk = oglasi_nk.id_oglasi_nk',$columns_lokacija); 
                    $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk = oglasi_nk.id_oglasi_nk',$columns_vrsta_oglasa); 
                    $select->join('dodatno','dodatno.id_oglasi_nk = oglasi_nk.id_oglasi_nk',$columns_dodatno);
                    
                    if (count($where)>0) {
                        $select->where($where);
                    }
                    
                    $select->group($column); 
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         return array_values(iterator_to_array($results));     
     
    }
    
    
    public function filter_counter(){
   //     \Application\Models\LokacijaSr\Lokacija::get_lm(10030);
        $get=$this->get;
       // var_dump($get);
        $get_isticu=false;
        $filter=[];
        
        $kategorija_filter=[];
        if (isset($get['kategorija'])&&in_array($get['kategorija'],range(1,14))) {
            $kategorija_filter=['kategorija'=>$get['kategorija']];
            $filter['kategorija']=$get['kategorija'];
        }if(isset($get['prodaja'])&&$get['prodaja']===1){
            $filter['prodaja_izdavanje']=1;
        }if(isset($get['izdavanje'])&&$get['izdavanje']===1){
            $filter['prodaja_izdavanje']=2;
        }if(isset($get['mesta_srbija'])&&$get['mesta_srbija']>0){
            $m_e=  \explode(',', $get['mesta_srbija']);
                foreach ($m_e as $m_e_v) {
                    $filter['mesto'][]=$m_e_v;
                }
        }if(isset($get['deo_mesta'])&&$get['deo_mesta']>0){
            if (strpos($get['deo_mesta'],",")==false) {
             $filter['deo_mesta']=$get['deo_mesta'];
            }else{
                $d_e=  \explode(',', $get['deo_mesta']);
                foreach ($d_e as $d_e_v) {
                    $filter['deo_mesta'][]=$d_e_v;
                }
            } 
        }if(isset($get['lokacija'])&&$get['lokacija']>0){ 
            if (strpos($get['lokacija'],",")==false) {
             $filter['lokacija']=$get['lokacija'];
            }else{
                $d_e=  \explode(',', $get['lokacija']);
                foreach ($d_e as $d_e_v) {
                    $filter['lokacija'][]=$d_e_v;
                }
            }                
        }if (isset($get['aktivni'])&&$get['aktivni']==1) {
            $predicate = new  \Zend\Db\Sql\Where(); 
             $filter[]=$predicate->greaterThan('vrsta_oglasa.vrsta_oglasa',0 );
        }if (isset($get['isticu'])&&$get['isticu']==1) { 
            $date_2= date('Y-m-d H:i:s', strtotime("+2 day"));    
            $current=date("Y-m-d H:i:s");
            $filter[]=new \Zend\Db\Sql\Predicate\Between("vrsta_oglasa.datum_isteka",$current,$date_2);
                      $get_isticu=true;  
         }if (isset($get['cena-od'])&&trim($get['cena-od'])!=""&&trim($get['cena-do'])=="") {
               $predicate = new  \Zend\Db\Sql\Where(); 
             $filter[]=$predicate->greaterThanOrEqualTo('dodatno.cena',$get['cena-od'] );
        }elseif(trim($get['cena-od'])!=""&&trim($get['cena-do'])==""){
             $predicate = new  \Zend\Db\Sql\Where(); 
             $filter[]=$predicate->greaterThanOrEqualTo('dodatno.cena',$get['cena-od'] );
            // $ready[]=$predicate->greaterThanOrEqualTo('selidbe.selidbe_cena',$get['cena-od'] );
         }else if(trim($get['cena-do'])!=""&&trim($get['cena-od'])==""){
             $predicate = new  \Zend\Db\Sql\Where(); 
             $filter[]=$predicate->lessThanOrEqualTo('dodatno.cena',$get['cena-do'] );
         }
         
         
          
         if ( trim($get['kvadratura-od'])!=""&&trim($get['kvadratura-do'])!="") {  
             $filter[]=new \Zend\Db\Sql\Predicate\Between("dodatno.kvadratura",$get['kvadratura-od'],$get['kvadratura-do']); 
             
         }else if(trim($get['kvadratura-od'])!=""&&trim($get['kvadratura-do'])==""){   
             $predicate = new  \Zend\Db\Sql\Where(); 
             $filter[]=$predicate->greaterThanOrEqualTo('dodatno.kvadratura',$get['kvadratura-od'] );
         }else if(trim($get['kvadratura-do'])!=""&&trim($get['kvadratura-od'])==""){
                                
             $predicate = new  \Zend\Db\Sql\Where(); 
             $filter[]=$predicate->lessThanOrEqualTo('dodatno.kvadratura',$get['kvadratura-do'] );
                        
         }       
       //   var_dump($filter);
        $c_kat= $this->count_oglasi_test('kategorija');
        
        $isticu=$vrsta_oglasa_filter= $mesto_filter=$deo_mesta_filter=$lokacija_filter=$pro_izd_filter=$filter;
        
      
        unset($mesto_filter['mesto']);
        unset($deo_mesta_filter['deo_mesta']);
        unset($lokacija_filter['lokacija']);
        unset($pro_izd_filter['prodaja_izdavanje']);
     //   unset($vrsta_oglasa_filter[0]);
         $c_mesto=$this->count_oglasi_test('mesto','lokacija',$mesto_filter);
        $c_deo_mesta=$this->count_oglasi_test('deo_mesta','lokacija',$deo_mesta_filter);
        $c_lokacija=$this->count_oglasi_test('lokacija','lokacija',$lokacija_filter); 
        $c_aktivni_oglasi=$this->count_oglasi_test('vrsta_oglasa','vrsta_oglasa',$vrsta_oglasa_filter);
        $c_pro_izd=$this->count_oglasi_test('prodaja_izdavanje',false,$pro_izd_filter);
       // var_dump($c_mesto);
        if ($get_isticu) {
           $c_isticu=$this->count_oglasi_test('vrsta_oglasa','vrsta_oglasa',$isticu);
         
        }else{
            
           $c_isticu= $this->count_oglasi_test('vrsta_oglasa','vrsta_oglasa',[
               new \Zend\Db\Sql\Predicate\Between("vrsta_oglasa.datum_isteka",date("Y-m-d H:i:s"),date('Y-m-d H:i:s', strtotime("+2 day")))
           ]);
        }
       
          $return=[
              'kategorija'  =>$c_kat,
              'mesto'       =>$c_mesto,
              'deo_mesta'   =>$c_deo_mesta,
              'lokacija'    =>$c_lokacija,
              
          ];
          
    foreach ($return as $key => $value) { 
        foreach($value as $kk => $v){
             if (isset($v['kategorija'])) {//var_dump($v);
                 $return[$key][$kk]['id']= $v['kategorija'];                        
                 $return[$key][$kk]['title']=Html::kategorija($v['kategorija'])." (".$v['count'].")";
                 unset($return[$key][$kk]['kategorija'],$return[$key][$kk]['count']);
             }elseif(isset($v['mesto'])){
                 $return[$key][$kk]['id']= $v['mesto'];                        
                 $return[$key][$kk]['title']=Html::get_mesto($v['mesto'])." (".$v['count'].")";
                 unset($return[$key][$kk]['mesto'],$return[$key][$kk]['count']);                 
             }elseif(isset($v['deo_mesta'])){
                 $return[$key][$kk]['id']= $v['deo_mesta'];                        
                 $return[$key][$kk]['title']=Html::get_dm($v['deo_mesta'])." (".$v['count'].")";
                 unset($return[$key][$kk]['deo_mesta'],$return[$key][$kk]['count']);                 
             }elseif(isset($v['lokacija'])){
                 $return[$key][$kk]['id']= $v['lokacija'];                        
                 $return[$key][$kk]['title']=Html::get_lm($v['lokacija'])." (".$v['count'].")";
                 unset($return[$key][$kk]['lokacija'],$return[$key][$kk]['count']);                 
             }
       }
     }            
           $return['prodaja_izdavanje']=$c_pro_izd;
           $return['aktivni']=$c_aktivni_oglasi;
           $return['isticu']=$c_isticu;
       // var_dump($return);
        return $return; 
    }
    

    
    
    public function topTenAds() {
        $sql=new Sql($this->adapter);
        $select=$sql->select('oglasi_nk');
        $select->columns(['id_oglasi_nk','max_count'=>new \Zend\Db\Sql\Expression("max(broj_pregleda)")]);
        $select->where([
           "id_users_nk"=>$this->user_id, 
        ]);
        $select->group("id_oglasi_nk");
        $select->order("max_count DESC");
        $select->limit(10);
          $statement = $sql->prepareStatementForSqlObject($select);
          $results = $statement->execute();
          return array_values(iterator_to_array($results));          
    }
    
    
    
    
    
    

    
    
}
<?php
namespace MojProfil\Models\MojiOglasi;


use Zend\Form\Form;

//local
use Application\Models\LokacijaSr\Lokacija;

class FormFilter extends Form {
     
    public function __construct() {
       
      
        parent::__construct('oglasi-filter');//ime forme
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'get');
        $this->setAttribute('id', 'form-fiter');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setInputFilter(new \MojProfil\Models\MojiOglasi\FilterFormFilter());//filter
        
         



#TEXT-------------------------------------         
        $this->add(array(
            'name'=>'id_oglasa',
            'attributes'=>array(
                'type'=>'text',
           'placeholder'=>"id oglasa",
               'style'=>"border:1px solid silver;width:80%;height: 34px;",
                'class'=>' search-query',
                
            ),
           
            
        ));
     #-----------------------------------------   
                
#kategorija--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'kategorija',
             'attributes' => array(
               'id' => 'select_kategorija',
              'multiple'=>'multiple'
            ),
   
             
     ));
      #----------------------------------------- 
     #-----------------------------------------   
        
                              
#mesto--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'mesta_srbija',
           'attributes'=>array(
               "id"=>"mesta_srbija",
             //  "class"=>"span3",
           ),
           
     ));
      #-----------------------------------------        
        
           
       
       
    #deo mesta--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'deo_mesta',
           'attributes'=>array(
              // "disabled"=>"disabled",
               'id'=>'deo_mesta',
               "class"=>"span3",
           ),
             
     ));
      #-----------------------------------------        
       
       
       
        #lokacija--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'lokacija',
           'attributes'=>array(
               //"disabled"=>"disabled",
               'id'=>'select_lokacija',
               "class"=>"span3",
           ),
            
     ));
      #-----------------------------------------        
       
       
                
     #Ulica-------------------------------------         
        $this->add(array(
            'name'=>'ulica',
            'attributes'=>array(
                'type'=>'text',
              'style'=>"border:1px solid silver;width:80%;height: 34px;",
                "id"=>"input_ulica",
                'class'=>'input-sto',
                
            ),
           
            
        ));
     #-----------------------------------------   
        
        
        
            #CHECKBOX--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'aktivni',
            'attributes'=>array(
                'id'=>'aktivni',
               'class'=>'prodaja_izdavanje_filter sub'
                  ),
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
           
     ));
      #-----------------------------------------  
        
       
       
             #CHECKBOX--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'isticu',
            'attributes'=>array(
          'class'=>'prodaja_izdavanje_filter sub'
               
                  ),
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
           
     ));
      #-----------------------------------------     
       
       
       
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'cena-od',
            'attributes'=>array(
                'type'=>'text',
           'placeholder'=>"od",
                'style'=>"width:100px;height:30px;border:1px solid silver;"
            ),
           
            
        ));
     #-----------------------------------------   
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'cena-do',
            'attributes'=>array(
                'type'=>'text',
            'placeholder'=>"do",
               'style'=>"width: 100px;height:30px;border:1px solid silver;"          
            ),
           
            
        ));
     #-----------------------------------------   
       
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'kvadratura-od',
            'attributes'=>array(
                'type'=>'text',
            'placeholder'=>"od",
               'style'=>"width: 100px;height:30px;border:1px solid silver;"       
            ),
           
            
        ));
     #-----------------------------------------   
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'kvadratura-do',
            'attributes'=>array(
                'type'=>'text',
             'placeholder'=>"do",
             'style'=>"width: 100px;height:30px;border:1px solid silver;"         
            ),
           
            
        ));
     #-----------------------------------------   
       
        
                
        
        
                     #CHECKBOX--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'izdavanje',
            'attributes'=>array(
         'class'=>'prodaja_izdavanje_filter sub'
               
                  ),
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
           
     ));
      #-----------------------------------------    
        
        
        
        
             #CHECKBOX--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'prodaja',
            'attributes'=>array(
         'class'=>'prodaja_izdavanje_filter sub'
               
                  ),
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
           
     ));
      #-----------------------------------------          
            
       
        
        #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'submit',
               'class'=>'btn btn-primary  arrow-right pull-right',
                'value'=>'Objavi oglas',
               'id'=>'filteri-form',
               'style'=>'display:none',
                
            ),
           ));
        
        #-----------------------------------------   
          
       
        
    }
    
    
    
}
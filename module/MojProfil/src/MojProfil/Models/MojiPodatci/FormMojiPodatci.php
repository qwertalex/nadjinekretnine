<?php
namespace MojProfil\Models\MojiPodatci;

use Zend\Form\Form;



/**
 * 
 * forma za moj-profil/moji-podatci
 * 
 * 
 */
class FormMojiPodatci extends Form {
 
    
    public function __construct() {
        
         parent::__construct("moji_podatci");
         $this->setAttribute('action', '');
         $this->setAttribute('method', 'post');
         $this->setAttribute('enctype','multipart/form-data');
         $this->setInputFilter(new \MojProfil\Models\MojiPodatci\FilterMojiPodatci());
     
         
          #ime-----------------------------------       
        $this->add(array(
             'name'=>'mp_ime',
             'attributes'=>array(
                 'type'=>'text',
              //   'class'=>'input-orange-border input-medium search-query',
             ),
            
           ));
        #------------------------------------------------  
           #ime-----------------------------------       
        $this->add(array(
             'name'=>'mp_ulica',
             'attributes'=>array(
                 'type'=>'text',
             
             ),
            
           ));
        #------------------------------------------------         
         $this->add(array(
             'name'=>'mp_mesto',
             'attributes'=>array(
                 'type'=>'text',
            
             ),
            
           ));
        #------------------------------------------------   
    
         
         $this->add(array(
             'name'=>'mp_logo',
             'attributes'=>array(
                 'type'=>'file',
            
             ),
            
           ));
        #------------------------------------------------           
         
         
         
         
         
         
         
         
         
         
      #tel-----------------------------------   
         
        $this->add(array(
            'name'=>'mp_telefon',
            'attributes'=>array(
                 'type'=>'text',
                 'data-toggle'=>"popover",
                'data-placement'=>"right",
                'data-content'=>"U ovo polje unesite samo brojeve.",
                'data-original-title'=>"",               
                'class'=>'mp_samobrojevi'
             ),
           ));
        #------------------------------------------------       
        
    
        #mob-----------------------------------       
        $this->add(array(
            'name'=>'mp_mobilni',
            'attributes'=>array(
                 'type'=>'text',
                'data-toggle'=>"popover",
                'data-placement'=>"right",
                'data-content'=>"U ovo polje unesite samo brojeve.",
                'data-original-title'=>"",
                'class'=>'mp_samobrojevi'
             ),
           ));
        #------------------------------------------------     
       
        #dob-----------------------------------       
        $this->add(array(
            'name'=>'mp_dob',
            'attributes'=>array(
                 'type'=>'text',
              //   'class'=>'input-orange-border input-medium search-query',
                 'id'=>'mp_dob',
                
             ),
           ));
        #------------------------------------------------ 
        
        
        
                 
        #pol---------------------------------        
     $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'mp_pol',
             'attributes'=>[
                 'class'=>'flat-icheck'
             ],
             'options' => array(
                     'value_options' => array(
                            1=> ' muski',
                            2=> ' zenski',
                     ),
             ),
            
     ));
      #-----------------------------------------  
        
      
     
           
     #mp_noviteti--------------------------------        
       $this->add(array(
             'type'=> 'checkbox',
             'name'=> 'noviteti',
              'attributes'=>[
                 'class'=>'flat-icheck'
             ],         
               'options' => array(
                //     'use_hidden_element' => true,
                     'checked_value' =>1,
                     'unchecked_value' => '',
             ),
           
           
     ));
      #-----------------------------------------   
     
     
     
           
     #mp_istekli_oglasi--------------------------------        
       $this->add(array(
             'type'=> 'checkbox',
             'name'=> 'istekli_oglasi',
              'attributes'=>[
                 'class'=>'flat-icheck'
             ],          
                   'options' => array(
                  //   'use_hidden_element' => true,
                     'checked_value' =>1,
                     'unchecked_value' => '',
             ),
           
           
     ));
      #----------------------------------------- 
     
     
               
     #mp_poruke--------------------------------       
            $this->add(array(
             'type' => 'checkbox',
             'name' => 'poruke',
              'attributes'=>[
                 'class'=>'flat-icheck'
             ],               
                   'options' => array(
                  //   'use_hidden_element' => true,
                     'checked_value' =>1,
                     'unchecked_value' => '',
             ),
     ));
       
       
       
        #ime-----------------------------------       
        $this->add(array(
             'name'=>'mp_submit',
             'attributes'=>array(
                 'type'=>'submit',
                 'class'=>'btn pull-right btn-primary arrow-right',
                 'value'=>'Sacuvaj izmene',
             ),
            
           ));
        #------------------------------------------------  
       
       
       
       
       
       
       
       
       
     
     
     
        
        
        
    }
    
    
    
}





<?php
namespace MojProfil\Models\MojiPodatci\ChangeUsername;


use Zend\Form\Form;


class ChangeUsername extends Form {
  
    
     public function __construct(\Application\Models\User\User $User ) {
        
        parent::__construct("moj-profil-promeni-username");
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        $this->setInputFilter(new \MojProfil\Models\MojiPodatci\ChangeUsername\Filter($User));
        

        #email-----------------------------------       
        $this->add(array(
            'name'=>'new_username',
            'attributes'=>array(
                'type'=>'text',
               ),
           ));
        #------------------------------------------------  
                      #password        
       $this->add(array(
            'name'=>'password',
            'attributes'=>array(
                'type'=>'password',
             
            ),
           ));
    #------------------------------------------------   

     #submit
      $this->add(array(
            'name'=>'change-username-submit',
          'type'=>'Zend\Form\Element\Button',
            'attributes'=>array(
                 'class'=>'btn btn-primary arrow-right',
                 'value'=>'Sacuvaj',
         ),
           ));
      
    
    
    
    
    
    
     }
    
    
    
    
    
}
<?php
namespace MojProfil\Models\MojiPodatci\ChangeUsername;

use Zend\InputFilter\InputFilter;




class Filter extends InputFilter {
   
    
    public function __construct(\Application\Models\User\User $User) {
        
        
                #username   
        $this->add(array(
            'name'=>'new_username',
            'requeried'=>true,
            'allow_empty'=>false,
               'validators' => array(
                      [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Username je obavezno!' 
                            ],
                        ],
                     ],  
                   array(
                    'name'=>'string_length',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'min'=>2,
                         'max' => 15,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Korisnicko ime mora da sadrzi najmanje dva karaktera!', 
                                 'stringLengthTooLong' => 'Korisnicko ime mora da sadrzi najvise 15 karaktera!' 
                            ),
                    ),
                ),
                 array(
                  'name'=>'Regex',
                     'break_chain_on_failure' => true,
                     'options'=>
                     array(
                     'pattern' => '/^[a-zA-Z\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Korisnicko ime nije validno!'
                     ))),
                                [
                    'name'=>'\Application\CustomValidators\Checkusername',
                    'break_chain_on_failure' => true,
                   'options'=>['user_class'=> $User]
                    ],
            
                   
            ),
                 ));
   #---------------------------------------------------------------------  
              
     #password    
        
            $this->add(array(
            'name'=>'password',
            'requeried'=>true,
            'validators'=>array(
                array(
                   'name'=>'not_empty',
                    'break_chain_on_failure' => true,
                     'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\NotEmpty::IS_EMPTY=>
                'Polje Lozinka je obavezno!'
                        )
                    )
                ),
                array(
                    'name'=>'string_length',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'min'=>5,
                         'max' => 32,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Polje Lozinka mora da sadrzi minimum 5 karaktera!', 
                                 'stringLengthTooLong' => 'Polje Lozinka mora da sadrzi najvise 32 karaktera!' 
                            ),
                    ),
                ),
            )
            
        ));    
        
      #---------------------------------------------------------------------     
             
        
    }
    
    
    
    
    
    
    
}
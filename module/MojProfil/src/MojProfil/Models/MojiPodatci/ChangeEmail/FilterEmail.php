<?php
namespace MojProfil\Models\MojiPodatci\ChangeEmail;

use Zend\InputFilter\InputFilter;




class FilterEmail extends InputFilter {
   
    
    
     public function __construct() {
        
  
        
      #email  
             $this->add(array(
            'name'=>'email',
            'requeried'=>true,
            'filters'=>[
                [ 'name'=>'StringTrim',]
                
             ],
            'validators'=>array(
                     [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Email je obavezno!' 
                            ],
                        ],
                     ], 
                array(
                    'name'=>'EmailAddress',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\EmailAddress::INVALID_FORMAT=>
                'Email adresa nije validna!'
                        )
                    )
                ),
             )
            
        ));
        
     #---------------------------------------------------------------------      
             
        
        
     #password    
        
            $this->add(array(
            'name'=>'password',
            'requeried'=>true,
            'validators'=>array(
                array(
                   'name'=>'not_empty',
                    'break_chain_on_failure' => true,
                     'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\NotEmpty::IS_EMPTY=>
                'Polje Lozinka je obavezno!'
                        )
                    )
                ),
                array(
                    'name'=>'string_length',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'min'=>5,
                         'max' => 32,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Polje Lozinka mora da sadrzi minimum 5 karaktera!', 
                                 'stringLengthTooLong' => 'Polje Lozinka mora da sadrzi najvise 32 karaktera!' 
                            ),
                    ),
                ),
            )
            
        ));    
        
      #---------------------------------------------------------------------     
             
  
        
        
    
  
     }
    
    
    
    
}
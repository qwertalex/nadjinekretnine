<?php
namespace MojProfil\Models\MojiPodatci\ChangeEmail;

use Zend\Form\Form;



class ChangeEmail extends Form{

     public function __construct( ) {
        
        parent::__construct("moj-profil-promeni-email");
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        $this->setInputFilter(new \MojProfil\Models\MojiPodatci\ChangeEmail\FilterEmail());
        

        #email-----------------------------------       
        $this->add(array(
            'name'=>'email',
            'attributes'=>array(
                'type'=>'text',
               ),
           ));
        #------------------------------------------------  
                      #password        
       $this->add(array(
            'name'=>'password',
            'attributes'=>array(
                'type'=>'password',
             
            ),
           ));
    #------------------------------------------------   

     #submit
      $this->add(array(
            'name'=>'change-email-submit',
          'type'=>'Zend\Form\Element\Button',
            'attributes'=>array(
                 'class'=>'btn btn-primary arrow-right',
                 'value'=>'Sacuvaj',
         ),
           ));
      
    
    
    
    
    
    
     }
    
    
    
    
    
    
}
<?php
namespace MojProfil\Models\MojiPodatci;


use Zend\InputFilter\InputFilter;


class FilterMojiPodatci extends InputFilter {
    
    public function __construct() {
      
        $this->add(array(
            'name'=>'mp_ime',
        'required'=> TRUE,
        'allow_empty' => true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------   
        
   
        
        
           $this->add(array(
            'name'=>'mp_ulica',
        'required'=> TRUE,
        'allow_empty' => true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------       
        
        
        
            $this->add(array(
            'name'=>'mp_mesto',
        'required'=> TRUE,
        'allow_empty' => true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
                   
 #mp_telefon    
        $this->add(array(
            'name'=>'mp_telefon',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'U polje telefon mozete uneti samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------    
        
        
        
              
                   
 #mp_mobilni   
        $this->add(array(
            'name'=>'mp_mobilni',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                 'U polje mobilni mozete uneti samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------     
        
        
        
                          
 #mp_dob   
        $this->add(array(
            'name'=>'mp_dob',
            'requeried'=>true,
            'allow_empty'=>TRUE,
            'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]{2}\/[\d]{2}\/[\d]{4}$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Polje dob nije validno!'
                     ))),
                   
            ),
                 ));
   #--------------------------------------------------------------------- 
        
        
    #mp_pol   
        $this->add(array(
            'name'=>'mp_pol',
            'requeried'=>FALSE,
            'allow_empty' => true,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 2,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
        
        
          
#mp_noviteti
    $this->add(array(
            'name'=>'noviteti',
            'requeried'=>FALSE,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
            array(
                'name' => 'Identical',
                'options' => array(
                    'token' => 1,
                    'messages' => array(
                         \Zend\Validator\Identical::NOT_SAME => 'Noviteti.',
                    ),
                ),
            ),
            ),
                 ));
   #---------------------------------------------------------------------   
        
        
     
    
    
                    
#mp_istekli_oglasi
    $this->add(array(
            'name'=>'istekli_oglasi',
            'requeried'=>FALSE,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
            array(
                'name' => 'Identical',
                'options' => array(
                    'token' => 1,
                    'messages' => array(
                         \Zend\Validator\Identical::NOT_SAME => 'Istekli oglasi.',
                    ),
                ),
            ),
            ),
                 ));
   #---------------------------------------------------------------------   
    
    
    
    
                    
#mp_poruke
    $this->add(array(
            'name'=>'poruke',
            'requeried'=>FALSE,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
            array(
                'name' => 'Identical',
                'options' => array(
                    'token' => 1,
                    'messages' => array(
                         \Zend\Validator\Identical::NOT_SAME => 'Istekli oglasi.',
                    ),
                ),
            ),
            ),
                 ));
   #---------------------------------------------------------------------   
    
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
}






<?php
namespace MojProfil\Models\MojiPodatci\ChangePass;


use Zend\Form\Form;


class ChangePassword extends Form{
    
    
    
    
    public function __construct( ) {
        
        parent::__construct("moj-profil-promeni-email");
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        $this->setInputFilter(new \MojProfil\Models\MojiPodatci\ChangePass\FilterPassword());
        
        #------------------------------------------------  
                      #password        
       $this->add(array(
            'name'=>'old_password',
            'attributes'=>array(
                'type'=>'password',
             
            ),
           ));
    #------------------------------------------------   
        
              #password        
       $this->add(array(
            'name'=>'password',
            'attributes'=>array(
                'type'=>'password',
            ),
           ));
    #------------------------------------------------     
       #repeat pass
      $this->add(array(
            'name'=>'retupepass',
            'attributes'=>array(
                'type'=>'password',
              ),
           ));
      #------------------------------------------------   
        
                    #submit
      $this->add(array(
            'name'=>'change-pass-submit',
          'type'=>'Zend\Form\Element\Button',
            'attributes'=>array(
                 'class'=>'btn btn-primary arrow-right',
                 'value'=>'Sacuvaj',
         ),
           ));
      
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
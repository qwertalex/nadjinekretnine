<?php
return array(
  
'controllers' => array(
     'invokables' => array(
'MojProfil\Controller\MojProfil' => 'MojProfil\Controller\MojProfilController',# <------HERE
          // <----- Module Controller
      ),
 ),   

    'router' => array(
   'routes' => array(
     'mojprofil' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/moj-profil[/:action][/strana/:strana][/po-strani/:po-strani][/kategorija/:kategorija][/nov-oglas/:nov-oglas]',# <------HERE
            // <---- url format module/action/id
           'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //  'id'     => '[0-9]+',
              'strana' => '[0-9]+',
               'po-strani'=>'[0-9]+',
               'kategorija'=>'[0-9]+',
               'nov-oglas'=>'[0-9]+',
               
            ),
            'defaults' => array(
               'controller' => 'MojProfil\Controller\MojProfil',# <------HERE
                // <--- Defined as the module controller
               'action'     => 'index',
                // <---- Default action
            //   'strana' => 1,
              //  'po-strani'=>10,
                
            ),
        ),
     ),
  ),
),
    
 
        'view_manager' => array(
    'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
                    'template_map' => array( 
            'paginator-slide' => __DIR__ . '/../view/layout/pagination-helper.phtml',
            'user_helper' => __DIR__ . '/../view/layout/user.phtml',
            'user_filter_helper'=>   __DIR__ . '/../view/layout/user_filter.phtml',        
            'modals'=>   __DIR__ . '/../view/layout/modals.phtml',              
             'dodatni_podatci'=>    __DIR__ . '/../view/layout/mojipodatci/kontakt-podatci.phtml',         
             'dodatni_podatci_modals'=>    __DIR__ . '/../view/layout/mojipodatci/modals.phtml',           
              'dodatni_podatci_pravna_lica'=>    __DIR__ . '/../view/layout/PravnaLica/MojiPodatci/pravna-lica-podatci.phtml',  
             'ponuda_tip_oglasa'=>    __DIR__ . '/../view/layout/ponuda-vrsta-oglasa.phtml',            
                'menu'=>    __DIR__ . '/../view/layout/menu.phtml', 
          
                        
        ),
    ),
);


 








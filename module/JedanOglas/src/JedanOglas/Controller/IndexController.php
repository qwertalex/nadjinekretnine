<?php

namespace JedanOglas\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

//local
use JedanOglas\Models\RenderOglas as Html;
use JedanOglas\Models\SideBar;
use JedanOglas\Models\Forms\Form as Prijavi;

class IndexController extends AbstractActionController 
{

    


    public function indexAction() {
    
        
    #load google map-------------------------------------------------------
     $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
     $renderer->headScript()->appendFile( '/js/jedan-oglas.js');
     $renderer->headLink()->appendStylesheet('/libraries/font-awesome-4.1.0/css/font-awesome.min.css');
     $renderer->headLink()->appendStylesheet('/css/bootstrap-social.css'); 
     $renderer->headScript()->appendFile( '/libraries/notify/jquery.noty.packaged.js');
      $renderer->headScript()->appendFile( '/libraries/pgw-slider/pgwslideshow.js');
      $renderer->headLink()->appendStylesheet('/libraries/pgw-slider/pgwslideshow_light.css'); 
   #-----------------------------------------------------------------------------------------------------    
        
        
     
   $routes=$this->params()->fromRoute();
 // var_dump($routes);
     $adapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
     $Html=new Html($adapter,$routes);
     $data=$Html->get_data();
     $SideBar=new SideBar($adapter);
     if (is_array($data)) {
         $Html->update_broj_pregleda();
     }
      
     $modal_prijavi=false;
     $Prijavi=new Prijavi();
       $request=$this->getRequest(); 
         if ($request->isPost()){            //if post
             $post= $request->getPost()->toArray();// get post
             if (isset($post['submit'])) {//if some form is submittet
                $Prijavi->setData($post);//set data to some form
                 if ($Prijavi->isValid($post)) {//activate form validation
                 	$post=$Prijavi->getData();
                        $sql=new \Zend\Db\Sql\Sql($adapter);
                        $insert=$sql->insert('prijavi_oglas');
                        $insert->values([
                            'id_oglasi_nk'      =>$post['id'],
                            'ime'               =>$post['ime'],
                            'email'             =>$post['email'],
                            'poruka'            =>$post['poruka'],
                            'refferer'          =>$post['refferer']
                        ]);
                        $sql->prepareStatementForSqlObject($insert)->execute();
                        $modal_prijavi=true;
                        
                 }
             }
         }
     
     
     
     
     
     
        
        return new ViewModel(array(
           'html'       =>$Html->get_data(),
            'slicni_oglasi' =>$SideBar->get_ads($Html->get_data(true), 10),
                'prijavi_oglas'=>$Prijavi,
                'modal_prijavi'=>$modal_prijavi
                
               ) );
        
    }


}



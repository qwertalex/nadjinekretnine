<?php
namespace JedanOglas\Models;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Db\Sql\Sql;

//local
use Application\Models\Oglasi\Render\RenderView as Html;
/**
 * Description of SideBar
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class SideBar {
    
    protected $adapter;
    
    public function __construct($adapter){
        $this->adapter=$adapter;
    }
    
    
    public function get_ads($filter,$limit){
       // var_dump($filter);
        $kat=$filter['kategorija'];
        $pro_izd=$filter['prodaja_izdavanje'];
        $mesto=$filter['mesto'];
        $where=new  \Zend\Db\Sql\Where();
           $sql=new Sql($this->adapter);
         $select = $sql->select();
         $select->from( 'oglasi_nk');       
         $select->columns(['naslov','id_oglasi_nk','kategorija']);
         $select->join("slike",'slike.id_oglasi_nk=oglasi_nk.id_oglasi_nk',['slika_1'],'left');
         $select->join("lokacija",'lokacija.id_oglasi_nk=oglasi_nk.id_oglasi_nk',['mesto']);
         $select->join("dodatno",'dodatno.id_oglasi_nk=oglasi_nk.id_oglasi_nk',['cena']);
         $select->join("vrsta_oglasa",'vrsta_oglasa.id_oglasi_nk=oglasi_nk.id_oglasi_nk',['vrsta_oglasa']);
         
         
         
         $select->where([
             'oglasi_nk.kategorija'         =>$filter['kategorija'],
             'oglasi_nk.prodaja_izdavanje'  =>$filter['prodaja_izdavanje'],
             'lokacija.mesto'               =>$filter['mesto'],
             $where->greaterThanOrEqualTo('vrsta_oglasa',1),
             $where->notEqualTo('oglasi_nk.id_oglasi_nk', $filter['id_oglasi_nk']),
         ]);
         
         
         
         
         $select->order("vrsta_oglasa.vrsta_oglasa DESC"); 
         $select->order("vrsta_oglasa.datum_postavljanja_oglasa DESC");
        if ($limit) {
            $select->limit($limit);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()) {
                    return $this->render_oglas(array_values(iterator_to_array($results)));
         }
          return false;        
        
        
        
    }
    
    
    protected function render_oglas($res){
        if (!$res) { return false; }
         foreach($res as $k=>$v):
              
            $res[$k]['mesto']=Html::get_mesto($v['mesto']);
            $res[$k]['cena']=Html::cena_valuta($v['cena']);
            $res[$k]['link']=Html::create_link($v['kategorija'], $v['id_oglasi_nk'], $v['naslov']);
            $res[$k]['slika_1']=Html::image($v['slika_1']);
            $res[$k]['vrsta_ogl']=Html::vrsta_oglasa($v['vrsta_oglasa']);
         endforeach;
        
        return $res;
        
        
    }
    
    
    
    
    
    
    
}

<?php

namespace JedanOglas\Models\Forms;

/**
 * Description of Filter
 *
 * @author aleksa
 */
use Zend\InputFilter\InputFilter;





class Filter  extends InputFilter{
    
    public function __construct() {
        
        
         $this->add(array(
            'name'=>'id',
            'requeried'=>true,
            'allow_empty'=>false,
              'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                      
                     ], 
                 [
                      'name'=>'Zend\Validator\Digits',
                      'break_chain_on_failure' => true,
                     ],
                   
            ),
                 ));
   #---------------------------------------------------------------------    
        
        
         #ulica   
        $this->add(array(
            'name'=>'ime',
        'required'=> true,
        'allow_empty' => false,
             'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------    
        
   
        
        
      $this->add(array(
            'name'=>'email',
            'requeried'=>true,
            'allow_empty'=>false,
            'validators'=>array(
                   [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Katgorija je obavezno!' 
                            ],
                        ],
                     ], 
                array(
                    'name'=>'EmailAddress',
                    'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\EmailAddress::INVALID_FORMAT=>
                'Email adresa nije validna!'
                        )
                    )
                ),
            )
        ));             
                
                
             #---------------------------------------------------------------------    
        
        
                $this->add(array(
            'name'=>'poruka',
        'required'=> true,
        'allow_empty' => false,
             'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
                [
                    'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '200',
                                ),
                    ]
            ),
                 ));
   #---------------------------------------------------------------------         
                
                
                
                
                
                
                
    }
    
    
    
    
    
    
    
}

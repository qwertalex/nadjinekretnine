<?php

namespace JedanOglas\Models\Forms;

/**
 * Description of Form
 *
 * @author aleksa
 */
 



class Form extends \Zend\Form\Form{
   
    
    public function __construct(){
        parent::__construct('prijavi-oglas');
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
         $this->setAttribute('class', 'form-horizontal');
         $this->setAttribute('id', 'prijavi-nepravilnosti-form');
        $this->setInputFilter(new \JedanOglas\Models\Forms\Filter());
        
        
        
        
        
        
        
        
        
        
        
        $this->add(array(
             'name' => 'id',
             'type' => 'Hidden',
         ));
        
         $this->add(array(
             'name' => 'ime',
             'type' => 'Text',
             'options' => [],
         ));
        $this->add(array(
             'name' => 'refferer',
             'type' => 'Hidden',
             'options' =>[],
         ));
        $this->add(array(
             'name' => 'email',
             'type' => 'Email',
             'options' =>[],
         ));
         
          $this->add(array(
             'name' => 'poruka',
             'type' => 'Textarea',
             'options' => [],
         ));
         
        $this->add(array(
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'Pošalji',
                 'id' => 'submitbutton',
             ),
         ));
    }
    
    
    
    
    
}

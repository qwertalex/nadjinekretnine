<?php

namespace JedanOglas\Models;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


use Application\Models\Url\UrlRewrite as Url;
use Zend\Db\Sql\Sql;

//local
use Application\Models\User\User;

/**
 * Description of RenderOglas
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class RenderOglas extends \Application\Models\Oglasi\Render\RenderView{
    
    protected $routes;
    protected $adapter;
    protected $kategorija;
    protected $id;
    protected $title;
    
    public function __construct($adapter,$routes) {
        $this->adapter=$adapter;
        $this->routes=$routes;
               
        $this->kategorija=Url::get_kategorija_id($routes['kategorija']);
        $this->id=$routes['id'];
        $this->title=$routes['naslov_oglasa'];
        
    }
    
   
    protected function sort_tables(){
        $required_tables=[/*'oglasi_nk',*/'vrsta_oglasa','lokacija','slike','kontakt_podaci'];
        $tables=[];
              
         switch((int)$this->kategorija){
              case 1: case 2:case 6:case 7: case 8:case 9:case 10:case 11:            
              case 14: case 15: case 16:case 17:
                  array_push($tables,'dodatni_podatci','dodatno');
               break;          
              case 4:$tables[]='dodatno';break;
              case 3:array_push($tables,'dodatni_podatci','dodatno','kuce');  break;
              case 5:array_push($tables,'dodatni_podatci','dodatno','plac'); break;
              case 12:case 13:$tables[]='dodatno'; break;
            }
          return array_merge($required_tables,$tables);
    }
    
    
    public function get_data($row=false){
        $sql=new Sql($this->adapter);
        $tables=$this->sort_tables();
    //    var_dump($tables);
        $where=new \Zend\Db\Sql\Where();
        $select=$sql->select('oglasi_nk');
       $select->where([
           'oglasi_nk.id_oglasi_nk'=>$this->id,
           $where->notEqualTo('vrsta_oglasa.vrsta_oglasa', 0)
   
       ]);
       $select->join('pravna_lica', 'pravna_lica.id_users_nk=oglasi_nk.id_users_nk','*','left');
        foreach($tables as $v):
            $select->join($v,$v.'.id_oglasi_nk=oglasi_nk.id_oglasi_nk','*','left');
        endforeach;
       $select->join('users_nk','users_nk.id_users_nk=oglasi_nk.id_users_nk',['logo_oglasivac']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count()>0) {
            if ($row) {
                return array_values(iterator_to_array($results))[0];
            }
            return  $this->set_data_view(array_values(iterator_to_array($results))[0]);
        }
        
          return false;
        
    }
    
    protected function set_data_view($data){
       $oreginal_mesto=$data['mesto'];
        $oreginal_deo_mesta=isset($data['deo_mesta'])?$data['deo_mesta']:"";
        
      //  var_dump($data);
        $data['title']=self::to_utf8($data['naslov']);//za title strane
        $data['meta_desc']=self::to_utf8($data['opis']);//za meta desc
        $data['prodaja_izdavanje']=  $this->oglas_pro_izd($data['prodaja_izdavanje']);
        $data['kategorija']=self::kategorija($data['kategorija']);
        $data['cena']=  self::cena_valuta($data['cena']);
        $data['mesto']=self::get_mesto($data['mesto']);
        if (isset($data['deo_mesta'])) {
              $data['deo_mesta']=self::get_deo_mesta($oreginal_mesto,$data['deo_mesta']);
        }else{
            $data['deo_mesta']="";
        }if (isset($data['lokacija'])) {
            $data['lokacija']=self::get_lokacija($oreginal_mesto,$oreginal_deo_mesta,$data['lokacija']);
        }else{
            $data['lokacija']="";
        }
              
     //   isset($data['slika_1'])?"":
        for($i=1;$i<11;$i++):
            isset($data['slika_'.$i])?$data['slika_'.$i]=self::image($data['slika_'.$i],false):"";
        endfor;
        
        $data['oglasivac']=self::oglasivac($data['oglasivac']);      
        if (isset($data['namestenost'])) {
            $data['namestenost']=  $this->namestenost($data['namestenost']);
        }
        
        $data['telefon']=  $this->telefon($data['telefon']);
        
        if (isset($data['dodatni_telefon'])) {
            $data['dodatni_telefon']=  $this->telefon($data['dodatni_telefon']);
        }
             
        
        if (isset($data['mapa'])&&$this->map($data['mapa'])) {
            $data['mapa']=  $this->map($data['mapa']);
        }
        
        if (isset($data['grejanje'])) {
            $data['grejanje']=  $this->grejanje($data['grejanje']);
        }
        if (isset($data['opis_objekta'])) {
            $data['opis_objekta']=  $this->opis_objekta($data['opis_objekta']);
        }
        if (isset($data['tip_objekta'])) {
            $data['tip_objekta']=  $this->tip_objekta($data['tip_objekta']);
        }
        
        
        //kuce
        if (isset($data['spratnost'])) {
            $data['spratnost']=  $this->spratnost_kuce($data['spratnost']);
        }
        if (isset($data['vrsta_kuce'])) {
            $data['vrsta_kuce']=$this->vrsta_kuce($data['vrsta_kuce']);
        }
        
        
        //samo za agencije logo,adresa i grad
        if (isset($data['logo_oglasivac'])) {
            $data['logo_agencije']=self::agencije_logo($data['logo_oglasivac']);
        } 
              
        
        
        if (!$data['id_kontakt_podaci']) {
            
            $User=new User($this->adapter,$data['id_users_nk']);
            $user_data=$User->get_by_id();
          //  var_dump($user_data);
            $data['telefon']=self::telefon($user_data['telefon_oglasivac']);
            $data['dodatni_telefon']=self::telefon($user_data['mobilni_oglasivac']);
            $data['mesto_kontakt_podaci']=$user_data['mesto_oglasivac'];
            $data['ulica_kontakt_podaci']=$user_data['ulica_oglasivac'];
            $data['ime']=$user_data['ime_oglasivac'];
            $data['email']=$user_data['email_nk'];
            
            
        }
        
        if (isset($data['naziv_firme'])) {
        //   $nf=self::to_utf8(self::only_letters_num($data['naziv_firme'], ture));
           $data['naziv_firme']=  implode('-',array_map('trim',array_filter(explode(" ",strtolower(self::only_letters_num(self::to_utf8($data['naziv_firme']),true))))));
        }
        
      // var_dump($data);
        
        
         return array_filter($data);
        
    }
    
    
    
    public function update_broj_pregleda(){
        $statement = $this->adapter->query("UPDATE oglasi_nk SET broj_pregleda=broj_pregleda+1 WHERE id_oglasi_nk=".$this->id); 
        return $statement->execute();
    }
    
    
    
}

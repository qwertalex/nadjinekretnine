<?php





return array(
  
'controllers' => array(
     'invokables' => array(
'JedanOglas\Controller\Index' => 'JedanOglas\Controller\IndexController',# <------HERE
         
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'jedanoglas' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/nekretnine-oglas/:kategorija/:id/:naslov_oglasa',# <------HERE
           'constraints' => array(
              'kategorija' => 'stanovi|kuce|sobe|garaze|placevi|dnevno-izdavanje|lokali|poslovni-prostori|hale|kiosci|stovarista|magacin|poljoprivredno-zemljiste|gradjevinsko-zemljiste|vikendice|apartmani|sobe|selidbe',
               'id'         =>'[0-9]+',
               'naslov_oglasa'     => '[a-zA-Z0-9-]+',
            ),
            'defaults' => array(
               'controller' => 'JedanOglas\Controller\Index',# <------HERE
               'action'     => 'index',
               
            ),
        ),
     ),
  ),
),
    
    'view_manager' => array(
    'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    
);













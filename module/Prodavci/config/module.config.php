<?php


return array(
  
'controllers' => array(
     'invokables' => array(
'Prodavci\Controller\Index' => 'Prodavci\Controller\ProdavciController', 
     ),
 ),    
    
    'router' => array(
   'routes' => array(
     'prodavci' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/prodavci[/:action]' 
            . '[/:id-agencije]'
            . '[/:agencija]'
            . '[/:kategorija]'
            . '[/:prodaja_izdavanje]'
            . '[/:lokacija]'
            . '[/:sa-slikom]'
            . '[/cena/:cena-val]'
            . '[/kvadratura/:kvadratura-val]'
            . '[/broj-soba/:sobe-val]'
            . '[/strana/:strana]'
            . '[/po-strani/:po-strani]', 
          
           'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              'id-agencije'            =>'[\d\-]+',
              'agencija'               =>'[a-z\-\_0-9]+',
              'kategorija'              => 'stanovi|kuce|sobe|graze|placevi|lokali|poslovni-prostori|hale|kiosci|stovarista|magacin|poljoprivredno-zemljiste|gradjevinsko-zemljiste|vikendice|apartmani|sobe|selidbe',
              'prodaja_izdavanje'       => 'prodaja|izdavanje',
              'lokacija'                => '[a-z\-\_0-9]+',
              'sa-slikom'               => 'samo-sa-slikom',
              'cena'                    => 'cena',
              'cena-val'                =>'[0-9\-]+',
              'kvadratura'              => 'kvadratura',
              'kvadratura-val'          => '[0-9\-]+',
              'broj-soba'               =>'broj-soba',
              'sobe-val'               =>'[\d\-]+',
              'strana'                  => '[0-9]+',
              'po-strani'               => '[a-zA-Z0-9_-]+', 
            ),
            'defaults' => array(
               'controller' => 'Prodavci\Controller\Index',  
               'action'     => 'index', 
            ),
        ),
     ),
  ),
),
    
    'view_manager' => array(
               'template_map'=>[
             'sidebar-prodavci'                 =>__DIR__ . '/../view/layout/helpers/sidebar.phtml',
             'paginacija-prodavci'                 =>__DIR__ . '/../view/layout/helpers/paginacija.phtml',
             'lista-oglasa-prodavci'                 =>__DIR__ . '/../view/layout/helpers/lista-oglasa.phtml',
        ],
         
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
         
);













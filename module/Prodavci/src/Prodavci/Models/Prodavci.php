<?php
namespace Prodavci\Models;
/**
 * Description of Prodavci
 *
 * @author aleksa
 */
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate;
use Zend\Db\Sql\Predicate\Operator;
use Application\Models\Url\UrlRewrite as Url;




class Prodavci extends \Pretraga\Models\FiltersCounter {
    protected $adapter;
    protected $get;
    protected $filter=[];
    protected $agency_id;

    public function __construct($adapter,$get) {
        $this->adapter=$adapter;
        $this->get=$get;
        $this->where=new \Zend\Db\Sql\Where();
        if (isset($get['id-agencije'])) {
            $this->filter['id_users_nk']=$get['id-agencije'];
            $this->agency_id=$get['id-agencije'];
        }
        
       // var_dump($get);
        $this->add_filters();
             //   var_dump($this->filter);
        parent::__construct($adapter,$this->filter);
    }
    
 
    public function get_prodavac_data(){
        if(!$this->agency_id) return false;
        $adapter=  $this->adapter;
         $sql=new \Zend\Db\Sql\Sql($this->adapter);
                 $select=$sql->select();
                 $select->from('pravna_lica');
                 $select->join('users_nk', 'users_nk.id_users_nk=pravna_lica.id_users_nk');
                $select->where([
                    'pravna_lica.id_users_nk'=>  $this->agency_id
                ]);
                 $selectString = $sql->getSqlStringForSqlObject($select);
                 $results = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                 return array_values(iterator_to_array($results)) ;
        
        
        
    }
    
    
     public function get_ads() {
         $sql=new Sql($this->adapter);
         $select=$sql->select(); 
         
         $select->from( 'oglasi_nk');
         $select->join('lokacija','lokacija.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['mesto','lokacija','deo_mesta','ulica','mapa','e_stan_frame']);
         $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[
                      'datum_postavljanja_oglasa','datum_isteka','vrsta_oglasa']);
         $select->join('slike','slike.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['slika_1'], $select::JOIN_LEFT);
         
        $select->join('dodatno','dodatno.id_oglasi_nk = oglasi_nk.id_oglasi_nk',
                     ['grejanje','spratovi','ukupno_spratovi','kvadratura','cena','broj_soba','tip_objekta','opis_objekta',
                         'nacin_placanja','namestenost','valuta'],
                     $select::JOIN_LEFT);
        
        $select->join('dodatni_podatci','dodatni_podatci.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[
                'internet','kablovska','interfon','klima','depozit','zamena','odmah_useljiv','za_pusace','za_nepusace','uknjizen',
            'lift','podrum','topla_voda','dodatni_podatci_telefon','dodatno_graza','terasa','parking'
                ], $select::JOIN_LEFT);
        
         $select->join('kuce','kuce.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['spratnost','vrsta_kuce'], $select::JOIN_LEFT);
        
         $select->join('plac','plac.id_oglasi_nk = oglasi_nk.id_oglasi_nk',[
                 'ostalo_plac','vrsta_plac','povrsina_plac','jedinica_mere_plac'
             ], $select::JOIN_LEFT);
        
         $select->order("vrsta_oglasa.vrsta_oglasa DESC");
        $select->order("vrsta_oglasa.datum_postavljanja_oglasa DESC");          
        $select->order("oglasi_nk.datum_kreiranja_oglasa DESC");   
     
        
             $select->where($this->filter);
       
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $res = array_values(iterator_to_array($results));//var_dump($res);
             return $res; 
          }
          return [];
       }
       
    
    
    
        
     /**
      * 
      * @return type
      * 
      * 
      * 
      * 
      * kreira filtere u zavisnosti od $this->get varaibile tj. od parametara 
      * u routes.
      * 
      * 
      */
     
     public function add_filters() {
         $get=$this->get;
         //  var_dump($get);
         $this->filter[]=$this->where->greaterThanOrEqualTo('vrsta_oglasa',1);
         if (isset($get['kategorija'])&&$get['kategorija']) {
               
              $this->filter['kategorija']=$get['kategorija'];
         }
         
         if (isset($get['prodaja_izdavanje'])&&$get['prodaja_izdavanje']) {
             $this->filter['prodaja_izdavanje']=$get['prodaja_izdavanje'];
        
         }
              //set mesto
         $this->filter_mesto();
         $this->filter_cena();
         //sobe
  
         $this->filter_sobe();
         //cena
         
         //kvadratura
         $this->filter_kvadratura();
     
       //  var_dump($this->filter);
         return $this->filter;
         
       }
       
       
       
          protected function filter_sobe(){
             $get= $this->get;
          if (isset($get['sobe_od'],$get['sobe_do'])&&$this->validate_int($get['sobe_od'])&&$this->validate_int($get['sobe_do']) ) {  
               $this->filter[]=$this->where->between('broj_soba', $get['sobe_od'],$get['sobe_do']); 
           }elseif (isset($get['sobe_od'])&&$this->validate_int($get['sobe_od'])) {
                $this->filter[]=$this->where->greaterThanOrEqualTo('broj_soba',$get['sobe_od']);
           }elseif (isset($get['sobe_do'])&&$this->validate_int($get['sobe_do'])) {
                $this->filter[]=$this->where->lessThanOrEqualTo('broj_soba',$get['sobe_do']);
           }
       }
       
       
       protected function filter_cena(){
           $get= $this->get;
          if (isset($get['cena_od'],$get['cena_do'])&&$this->validate_int($get['cena_od'])&&$this->validate_int($get['cena_do']) ) {  
               $this->filter[]=$this->where->between('cena', $get['cena_od'],$get['cena_do']); 
           }elseif (isset($get['cena_od'])&&$this->validate_int($get['cena_od'])) {
                $this->filter[]=$this->where->greaterThanOrEqualTo('cena',$get['cena_od']);
           }elseif (isset($get['cena_do'])&&$this->validate_int($get['cena_do'])) {
                $this->filter[]=$this->where->lessThanOrEqualTo('cena',$get['cena_do']);
           }
      }
       
       
       protected function validate_int($num){
           $validator = new \Zend\Validator\Digits();
           return $validator->isValid($num);
       }








       protected function filter_kvadratura(){
          $get= $this->get;
          if (isset($get['kvadratura_od'],$get['kvadratura_do'])&&$this->validate_int($get['kvadratura_od'])&&$this->validate_int($get['kvadratura_do']) ) {  
               $this->filter[]=$this->where->between('kvadratura', $get['kvadratura_od'],$get['kvadratura_do']); 
           }elseif (isset($get['kvadratura_od'])&&$this->validate_int($get['kvadratura_od'])) {
                $this->filter[]=$this->where->greaterThanOrEqualTo('kvadratura',$get['kvadratura_od']);
           }elseif (isset($get['kvadratura_do'])&&$this->validate_int($get['kvadratura_do'])) {
                $this->filter[]=$this->where->lessThanOrEqualTo('kvadratura',$get['kvadratura_do']);
           }
       }       
       
           
       /*
        * short cut za range values npr. 
        * cena od i cena do,kvadratura od do...
        * params:
        * $name=ime kolone iz baze;
        * $mm=je zapravo explode('-','10-2000')(npr. cena od 10 do 2000) ali zbog
        * mnogih konbinacija npr(0-0,100-0) tj. nemogucih kombinacija
        * ovaj paramertar se je uglavnom function $this->format_range_values($name,$od,$do);
        * 
        * 
        */
     protected function range_predicate($name,array $mm){
         if (count($mm)===1) {
            return $this->where->greaterThanOrEqualTo($name,$mm['min']);
         }elseif(count($mm)===2){
            return  $this->where->between($name, $mm['min'],$mm['max']);  
         }
         return false;
     }
              
 
       
       
           /**
         * 
         * pravi filter za mesto...
         * posto mesto u bazi moze da ima tri kolone mesto,deo_mesta  i lokacija
         * variabila $mesto moze da se dobije u vise formata:
         * $mesto=1; u tom slucaju pravi filer za bazu: mesto=1;
         * $mesto=1-2000; mesto=1 and deo_mesta=2000;
         * $mesto=1-2000-10003;  mesto=1 and deo_mesta=2000 and lokacija=10003;
         * to je to...
         * 
         * 
         */
       protected function filter_mesto(){
         if (!isset($this->get['lokacija'])) return false;
           $mesto= $this->get_location_id($this->get['lokacija']);
           if (count($mesto)===0) {
               return false;
           }
               $mes=[];$deo=[];$lok=[];
       //   if ($mesto&&count($mesto)>0) {
          //   if ($mesto['id']!=0) {
                 foreach ($mesto as $k => $v) {
                     if ($v!=0) {
                     if (strpos($v, '-')===false) {
                         $mes[]=$v;
                       /// $this->filter['mesto']=$v;
                    }elseif(count(explode('-', $v))===2){
                        $deo[]=explode('-', $v)[1];
                     //  $this->filter['deo_mesta']=explode('-', $v)[1];
                    }elseif(count(explode('-', $v))===3){
                        $lok[]=explode('-', $v)[2];
                        // $this->filter['lokacija']=explode('-', $v)[2];
                    }     
                 }
             }
        //}   
 
               
$all_predicates=[];

       if (count($mes)>0) { 
           foreach($mes as $k=>$v):
               $all_predicates[]=new Predicate\Operator('mesto', Operator::OPERATOR_EQUAL_TO, $v);
           endforeach;
       }if (count($deo)>0) {
           foreach($deo as $k=>$v):
               $all_predicates[]=new Predicate\Operator('deo_mesta', Operator::OPERATOR_EQUAL_TO, $v);
           endforeach;
        }if (count($lok)>0) {
           foreach($lok as $k=>$v):
              $all_predicates[]=new Predicate\Operator('lokacija', Operator::OPERATOR_EQUAL_TO, $v);
           endforeach;
        }
             
        
        
                    $where = new \Zend\Db\Sql\Where(array(
    // Other conditions...
    new Predicate\PredicateSet(
        $all_predicates,Predicate\PredicateSet::COMBINED_BY_OR
    ),
));
             
             $this->filter[]=$where;
        
        
        
        
        
        
        
        
        
        
        
        
             //  var_dump($this->filter); 
             
             
       }
       
       
       
       
       
     /**
      * 
      * OVO JE ZA FORMU FILER
      * 
      *
      * OPIS:
      * -----
      * Dinamicki setuje vrednosti u formu(jer se nevrsi validacija forme nego se validira u module.config)
      * 
      * 
      * 
      * 
      */ 
       
       public function set_form_values() {
           $get=$this->get;
                  //    var_dump($get);
        
 $data=[];
 if (isset($get['kategorija'])) {
     $data['kategorija']=$get['kategorija'];
 } if (isset($get['prodaja_izdavanje'])) {
     $data['prodaja_izdavanje']=$get['prodaja_izdavanje'];
 } if (isset($get['mesta_srbija'])) {
     $data['mesta_srbija']=$get['mesta_srbija'];
 }  
         
                
      
              if (isset($get['cena_od'])) {$data['cena_od']=$get['cena_od'];}
              if (isset($get['cena_do'])) {$data['cena_do']=$get['cena_do'];}
    
   
              if (isset($get['kvadratura_od'])) {$data['kvadratura_od']=$get['kvadratura_od'];}
              if (isset($get['kvadratura_do'])) {$data['kvadratura_do']=$get['kvadratura_do'];}
                  
              if (isset($get['sobe_od'])) {$data['sobe_od']=$get['sobe_od'];}
              if (isset($get['sobe_do'])) {$data['sobe_do']=$get['sobe_do'];}
                   
  
            
           return $data;
       }
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
}

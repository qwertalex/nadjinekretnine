<?php

namespace Prodavci\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

//local
use Prodavci\Models\Prodavci;
 use Pretraga\Models\Forms\FilterForm\Form as FilterForm;
 use Application\Models\Oglasi\Render\RenderView as Render;
class ProdavciController extends AbstractActionController {

    
    
    
    
    /**
     * 
     * @return ViewModel
     * 
     * vraca sva polja iz deklaracije
     * 
     * 
     */
    public function indexAction() {
        $adapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'); 
        
        
                 $sql=new \Zend\Db\Sql\Sql($adapter);
                 $select=$sql->select();
                 $select->from('pravna_lica');
                 $select->join('users_nk', 'users_nk.id_users_nk=pravna_lica.id_users_nk');
                
                 $selectString = $sql->getSqlStringForSqlObject($select);
                 $results = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE);
                 $res=array_values(iterator_to_array($results)) ;
   
return new ViewModel([
    'result'        =>$res,
    'render'        =>new Render()
]);
        
    }
 
    


public function agencijaAction(){
       //this is for performance
        $this->layout()->setTemplate('layout/layout');
     # -------------------------------------------------------
     $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');  
     $renderer->headLink()->appendStylesheet('/libraries/font-awesome-4.1.0/css/font-awesome.min.css');
     $renderer->headLink()->appendStylesheet('/css/bootstrap-social.css');
     $renderer->headScript()->appendFile( '/libraries/select2/select2.min.js');
     $renderer->headLink()->appendStylesheet('/libraries/select2/select2.css');
     $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
     $renderer->headLink()->appendStylesheet('/libraries/icheck/line/blue.css');
     $renderer->headLink()->appendStylesheet('/libraries/icheck/blue.css');
     $renderer->headScript()->appendFile( '/libraries/notify/jquery.noty.packaged.js');
     $renderer->headScript()->appendFile(/*Ver::pretraga_js()*/'/js/prodavci.lista.js');
   #-----------------------------------------------------------------------------------------------------       
        $request=$this->getRequest();
         $get= array_merge($this->params()->fromRoute(),$this->params()->fromQuery()) ;
         $adapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
         $Prodavci=new Prodavci($adapter,$get);
         
         
         
 
       $FilterForm=new FilterForm();
  
          
      if ($this->params()->fromRoute('po-strani')) {
    if ( $this->params()->fromRoute('po-strani')==10||
                $this->params()->fromRoute('po-strani')==20||
                        $this->params()->fromRoute('po-strani')==50){
                        $po_strani=$this->params()->fromRoute('po-strani'); 
            }
           else{ $po_strani=10; }
         }else{$po_strani=10;}
          
        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($Prodavci->get_ads()));
        $paginator->setCurrentPageNumber($this->params()->fromRoute('strana'))
                 ->setItemCountPerPage($po_strani);
      
        // var_dump($Prodavci->set_form_values());
      $FilterForm->setData($Prodavci->set_form_values());
        
 
      unset($get['controller']);
      $get['action']='agencija';
      
         $viewModel = new ViewModel([
             'prodavac'=>$Prodavci->get_prodavac_data(),
             'render'        =>new Render()
         ]);
    
         $lista_oglasa=new ViewModel([
            'paginator' =>$paginator,
             'get'      =>$get,
             'render_html'  => new \Application\Models\Oglasi\Render\RenderView(),
             'cookie'       =>!isset($request->getCookie()->favoriti)?false:$request->getCookie()->favoriti,
            
        ]);
        $lista_oglasa->setTemplate("lista-oglasa-prodavci");        
        $viewModel->addChild($lista_oglasa,"lista_oglasa");
        //var_dump($get);
         $sidebar=new ViewModel([
           'form'   =>$FilterForm,
           'get'    =>$get,
           
         ]);
        $sidebar->setTemplate("sidebar-prodavci");        
        $viewModel->addChild($sidebar,"sidebar");         
        return $viewModel;
   /*  
    */
    
    
    
  
}












}
<?php

namespace Ajax\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


//local
use Application\Models\User\SessionManipulation;
use Ajax\Model\AjaxLocationSearch as Ajax;
use Application\Models\Url\UrlRewrite;
use Application\Models\Form\MainSearch\SearchForm;

class AjaxController extends AbstractActionController {

    protected $ajax_check_email_var;
    protected $user_data_manipulation_var;





    public function indexAction() {
 die('only ajax calls');
    }
    
    
    
    
    
    
    public function newEmailAction() {
         $viewModel=new ViewModel();
         $check_email=  $this->ajax_check_email();
         $user_data=  $this->user_data_manipulation();
         $sess=new SessionManipulation();
         $email=$sess->get_user_session()['user'];
         
         
         $request=$this->getRequest(); 
         if ($request->isPost()){ 
             $post= $request->getPost()->toArray();
             $viewModel->setTerminal($request->isPost());
             $validate=$check_email->check_email($post['new_email']);
             if (is_string($validate)) {echo 'false';}
             elseif(is_int($validate)){
                     if ($validate==0) {
                         if ($user_data->update_email($email,$post['new_email'])) {
                             $sess->update_session_cookie($post['new_email']);
                             
                             echo 'ok';
                         }
                     }
                     else{
                      echo "false";
                     }
             };
             return $viewModel;
         } 
         else{
             die("only post");
         }
        
    }
    
    
    
    public function newPasswordAction() {
        $viewModel=new ViewModel();
         $request=$this->getRequest(); 
        if ($request->isPost()){
             $post= $request->getPost()->toArray();
             $viewModel->setTerminal($request->isPost());    
             if (!isset($post['old_pass'])||!isset($post['new_pass'])) {
                 die('not set $post[old_pas] and not set $post[new_pass]');
             }
             
             $user_data=  $this->user_data_manipulation();
             $sess=new SessionManipulation();
             $email=$sess->get_user_session()['user'];
             
             if ($user_data->update_password($email,$post['old_pass'],$post['new_pass'])) {
                 echo 'ok';
             }
             else{
                 echo 'false';
             }
             }
        
         else{
             die("only post");
         }
       return $viewModel;
    }
    
    
    
    
    public function lokacijaPretragaAction() {
         $get=$this->params()->fromQuery();
         if (isset($get['query'])) {
             echo Ajax::get_location($get['query']);
         }
         elseif(isset($get['id'])){ 
              echo Ajax::get_location_by_id($get['id']);
           }
        $viewModel=new ViewModel();
        $viewModel->setTerminal(TRUE);
        return $viewModel;    
        
       }
    
    
    
    
    
    
    
    
    
    
    
           public function ajax_check_email()
    {
        if (!$this->ajax_check_email_var) {
            $sm = $this->getServiceLocator();
            $this->ajax_check_email_var = $sm->get('Ajax_check_email');
        }
        return $this->ajax_check_email_var;
    }
    
        
           public function user_data_manipulation()
    {
        if (!$this->user_data_manipulation_var) {
            $sm = $this->getServiceLocator();
            $this->user_data_manipulation_var = $sm->get('UserDataManipulation');
        }
        return $this->user_data_manipulation_var;
    }
    
    
    
}























<?php
namespace Ajax\Model;
/**
 * Ovu class nasledjuje Pretraga\Model\Pretraga i Pretraga\Models\FilterCounter;
 * Takodje se i koristi za ajax call 
 * 
 * OPIS:
 * -----
 * Ova class sluzi za manipulaciju sa lokacijom i za count lokacije
 * ovo je novina posto se lokacije sada nalaze u bazi lokacija_counter;
 * 
 * 
 * 
 * 
 * 
 * 
 */
use Zend\Db\Sql\Sql;

//local
use Application\Models\LokacijaSr\Lokacija;
use Application\Models\Oglasi\Render\RenderView as Html;



class AjaxLocationSearch {
  

 

    public function __construct() {
    }
 
    
 
    
    
    /**
     * 
     * @param type $query
     * @return type
     * 
     * vraca lokaciju sa like poziva se u ajax_call(mozda i negde jos)
     * 
     */
    public static function get_location($query) {
        
        $all=Lokacija::$all_locations;
        $return=[];
        if ($query) {
            $query=explode(' ',strtolower(Html::only_letters_num(Html::to_utf8($query))));
            $query=implode('.+',$query);
            
            foreach($all as $k=>$v):
                    if (count($return)>=30) {break;}
                    $v_oreginal=$v;
                    $v=strtolower(Html::only_letters_num(Html::to_utf8($v)));
                    if(preg_match('/'.$query.'/', $v)){
                      $return[]=['id'=>$k,'text'=>$v_oreginal] ;
                    }
            endforeach;  
        //    var_dump('query: '.$query);  
         //   var_dump($return);
              
        }
       return json_encode($return);
    
     }
    
    
    
     
     /**
      * 
      * @param type $id
      * @return type
      * 
      * vraca lokaciju po id
      */
     
     public static function get_location_by_id($id){     
          if (!$id) {return json_encode(['id'=>0,'text'=>'srbija']);}
         $return=[];
         $all=Lokacija::$all_locations;
         $id=array_filter(explode(',', $id));
         
         foreach($id as $v):
             if (isset($all[$v])) {
                 $return[]=['id'=>$v,'text'=>$all[$v]];
             }
         endforeach;
         return json_encode($return);
     }
     
     
     
     /**
      * 
      * @param type $query
      * @return type
      * 
      * 
      * 
      * po lokaciji url npr: valjevo-okolno-mesto-zabari, vraca id
      * tako sto pretvara value iz Lokacija::$all_locations  u gore navedeni link 
      * i trazi da li je isto, ako jeste vraca array sa id lokacije
      */
         
    public static function get_location_id($query) {
      $all=Lokacija::$all_locations;
      $query=explode('_', $query);
     
      $return=[];
       foreach($query as $v):
           foreach($all as $k=>$av):
           if ($v===Html::location_to_url($av)) {
               $return[]=$k;
           }
           endforeach;
       endforeach;
      return $return;
      }
    
     
     
      
     
  
     
     
     
     
     
     
     
     
     
    
    
}

 

<?php





return array(
  
'controllers' => array(
     'invokables' => array(
'Magazin\Controller\Index' => 'Magazin\Controller\MagazinController',# <------HERE
          // <----- Module Controller
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'magazin' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/magazin-nekretnine[/:kategorija][/:id][/:clanak]',# <------HERE
            // <---- url format module/action/id
           'constraints' => array(
          //     'magazin-kategorija'=>'magazin-kategorija',
               'kategorija' => '[a-z]*',
               'id'         => '[\d]*',
               'clanak'     => '[a-z\-0-9]*',
               
            ),
            'defaults' => array(
               'controller' => 'Magazin\Controller\Index',# <------HERE
                // <--- Defined as the module controller
               'action'     => 'index',
                // <---- Default action
            ),
        ),
     ),
  ),
),
    
    'view_manager' => array(
              
         
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
         
);













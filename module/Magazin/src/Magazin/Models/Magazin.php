<?php
namespace Magazin\Models;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Db\Sql\Sql;

//local
use Application\Models\Oglasi\Render\RenderView as Html;
/**
 * Description of Magazin
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class Magazin {
    
    
    protected $adapter;


    public function __construct(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter=$adapter;
    }
    
    
    
    
    public function get_magazin($kategory=false,$id=false,$clanak=false,$limit=false) {
        if ($id) {
            return $this->get_one_article($id);
        }
        
        
        
        
        
        $sql=new Sql($this->adapter);
        $select=$sql->select('magazin');
        $where=[];
        if ($kategory) {
            $where['kategorija']=$kategory;
        }if ($clanak) {
             $naslov=strtolower(str_replace("-", " ", $clanak));
             $where['naslov']=$naslov; 
        }
   //    var_dump($where);
         $select->where($where);
        $select->order("datum DESC");
        if ($limit) {
            $select->limit($limit);
        }
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $arr_results = array_values(iterator_to_array($results));
             foreach($arr_results as $k=>$v):
                 $arr_results[$k]['link']=Html::location_to_url($v['naslov']);
             endforeach;
             
             
             
             
             return $arr_results;
         }
      return FALSE;
        
        
        
        
    }
    
    
    
    
    
    
    
    public function get_one_article($id){
        $sql=new Sql($this->adapter);
        $select=$sql->select('magazin');
        $select->where([
            'id_magazin'=>$id
        ]);
        $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             return array_values(iterator_to_array($results));
         }
        return false;
        
    }
    
    
    
    
    
    
    public function get_random_articles($limit=false){
        
        $sql=new Sql($this->adapter);
        $select=$sql->select('magazin');
        if ($limit) {
            $select->limit($limit);
        }
          $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $arr_results = array_values(iterator_to_array($results));
             foreach($arr_results as $k=>$v):
                 $arr_results[$k]['link']=Html::location_to_url($v['naslov']);
             endforeach;
             $r_keys=array_rand($arr_results,4);
        
        foreach($r_keys as $v):
            $return[]=$arr_results[$v];
        endforeach;
         return $return;
         }
      return FALSE;
        
        
    }
    
    
    
    
}

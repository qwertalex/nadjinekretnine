<?php





return array(
  
'controllers' => array(
     'invokables' => array(
'MiTrazimo\Controller\Index' => 'MiTrazimo\Controller\MiTrazimoController',# <------HERE
          // <----- Module Controller
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'mitrazimo' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/mi-trazimo-za-vas[/:kategorija][/:id]',# <------HERE
            // <---- url format module/action/id
           'constraints' => array(
          //     'magazin-kategorija'=>'magazin-kategorija',
               'kategorija' => '[a-z]*',
               'id'         => '[\d]*', 
               
            ),
            'defaults' => array(
               'controller' => 'MiTrazimo\Controller\Index',# <------HERE
                // <--- Defined as the module controller
               'action'     => 'index',
                // <---- Default action
            ),
        ),
     ),
  ),
),
    
    'view_manager' => array(
              
         
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
         
);













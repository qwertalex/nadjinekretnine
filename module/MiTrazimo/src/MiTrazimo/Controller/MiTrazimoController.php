<?php

namespace MiTrazimo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use MiTrazimo\Models\Forms\Form;
//local
use MiTrazimo\Models\InsertData;

class MiTrazimoController extends AbstractActionController {

    public function indexAction() {
         $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');   
         
         $renderer->headScript()->appendFile( '/libraries/select2/select2.min.js');
         $renderer->headLink()->appendStylesheet('/libraries/select2/select2.css');
         $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
         $renderer->headLink()->appendStylesheet('/libraries/icheck/line/blue.css');
         $renderer->headLink()->appendStylesheet('/libraries/icheck/blue.css');
         $renderer->headScript()->appendFile( '/js/mi-trazimo.js');
              $adapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
         $request=$this->getRequest();
       // var_dump($_POST);
         $modal="not submited";
         $form=new Form();
         if ($request->isPost()) {
          $post= $request->getPost()->toArray();
          if (isset($post['submit-filters'])) {
              $form->setData($post);
             // var_dump($post);
              if ($form->isValid($post)) {
                  $post=$form->getData();
                //  var_dump($post);
                //  var_dump('valide');
                  new InsertData($adapter, $post);
                  $modal=true;
              }else{
             //  var_dump($form->getMessages());
                  $modal=false;
              }
              
          }
          
          
          }
         
         
         
        
        
         
         
        return new ViewModel([
           'form'   =>$form,
            'modal' =>$modal
        ]);
        
    }
    
  
    
    
    
    
}



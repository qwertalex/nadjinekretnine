<?php

namespace MiTrazimo\Models;

/**
 * Description of InsertData
 *
 * @author aleksa
 */
use Zend\Db\Sql\Sql;



class InsertData {
  
    protected $adapter;
    protected $data;


    public function __construct($adapter,$data) {
        $this->adapter=$adapter;
        $this->data=$data;
        $this->insert_data();
    }
    
    
    protected function insert_data(){
        $data=  $this->data;
        $sql=new Sql($this->adapter);
        $insert=$sql->insert('mi_trazimo');
        $remote = new \Zend\Http\PhpEnvironment\RemoteAddress;
        $date=date ("Y-m-d H:i:s");
      
        $insert->values([
            'kategorija'        =>$data['kategorija'],
            'prodaja_izdavanje' =>$data['prodaja_izdavanje'],
            'mesta_srbija'      =>$data['mesta_srbija'],
            'slika'             =>$data['samo-sa-slikom']==1?1:NULL,
            'cena_od'           =>$data['cena_od']?$data['cena_od']:NULL,
            'cena_do'           =>$data['cena_do']?$data['cena_do']:NULL,
            'kvadratura_od'     =>$data['kvadratura_od']?$data['kvadratura_od']:NULL,
            'kvadratura_do'     =>$data['kvadratura_do']?$data['kvadratura_do']:NULL,
            'sobe_do'           =>$data['sobe_do'],
            'grejanje'          =>$this->grejanje()?$this->grejanje():NULL,
            'uknjizen'          =>$data['uknjizen'],
            'oglasivac'         =>  $this->oglasivac()?$this->oglasivac():NULL,
            'ime'               =>$data['ime'],
            'email'             =>$data['email'],
            'ip'                =>$remote->getIpAddress(),
            'datum_kreiranja'   =>$date,
            'datum_last_update' =>$date,
            'hash'              =>  uniqid(TRUE)
        ]);
        
        $statement = $sql->prepareStatementForSqlObject($insert);
$result = $statement->execute();
        
        
        
    }
    
    
    protected function oglasivac(){
        $arr=[];
        $data=  $this->data;
        if ($data['vlasnik']) {
            $arr[]=1;
        }if ($data['agencija']) {
            $arr[]=2;
        }if ($data['investitor']) {
            $arr[]=3;
        }if ($data['banka']) {
            $arr[]=4;
        }
        if (count($arr)>0&&  count($arr)<4) {
            return implode(',', $arr);
            
        }
        return false;
    }








    protected function grejanje(){
        $arr=[];
        $data=  $this->data;
        
        if ($data['cg']) {
            $arr[]=1;
        }if ($data['eg']) {
            $arr[]=2;
        }if ($data['ta']) {
            $arr[]=3;
        }if ($data['gas']) {
            $arr[]=4;
        }if ($data['podno']) {
            $arr[]=5;
        }if ($data['kaljeva']) {
            $arr[]=6;
        }if ($data['norveski']) {
            $arr[]=7;
        }if ($data['mermerni']) {
            $arr[]=8;
        }
        
        
        
        if (count($arr)>0) {
            return implode(',', $arr);
            
        }
        return false;
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
}

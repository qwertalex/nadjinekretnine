<?php

namespace MiTrazimo\Models\Forms;

/**
 * Description of Filter
 *
 * @author aleksa
 */



use Application\Models\LokacijaSr\Lokacija as Lok;



class Filter extends \Zend\InputFilter\InputFilter{
    
    
    
    
    
    public function __construct() {
 

         
          #kategorija    
        $this->add(array(
            'name'=>'kategorija',
            'allow_empty' => FALSE, 
            'requeried'=>true,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
                                       [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Katgorija je obavezno!' 
                            ],
                        ],
                     ], 
              [
               'name'=>'InArray',
               'options'=>[
                   'haystack'=>[1,2,3,6,7],
                   'messages' => [
                                \Zend\Validator\InArray::NOT_IN_ARRAY => 'Polje Katgorija je obavezno!' 
                            ],
               ]
           ]
            ),
                 ));
   #---------------------------------------------------------------------       
        
        
    $this->add(array(
            'name'=>'prodaja_izdavanje',
            'requeried'=>true,
            'allow_empty' => FALSE, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
                                         [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Izaberite prodaju ili izdavanje!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 2,
                       'messages' => [
                           \Zend\Validator\Between::NOT_BETWEEN  => 'Izaberite kategoriju!' 
                            ],
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------        
        
    #mesta_srbija    
        $this->add(array(
            'name'=>'mesta_srbija',
            'requeried'=>true,
          'allow_empty' => false, 
            'filters'  => array(
                     //  array('name' => 'Int'),
                    ),
               'validators' => [
                                             [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Izaberite lokaciju!' 
                            ],
                        ],
                     ],
                      [
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\,\-\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Izaberite lokaciju!'
                     ))
                     ], 
               ]
                 ));
   #---------------------------------------------------------------------  
 
        
               
        $this->add(array(
            'name'=>'samo-sa-slikom',
            'requeried'=>true,
            //'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
        
         
        
                $this->add(array(
            'name'=>'cena_od',
            'requeried'=>true,
            'allow_empty'=>true,
       'validators'=>[
                                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Cena je obavezno!' 
                            ],
                        ],
                     ], 
           
           
           
           [
               'name'=>'Zend\Validator\Digits',
               'options'=>[
                   'messages' => [
                   \Zend\Validator\Digits::NOT_DIGITS  => 'U polje cena unesite samo brojeve.' ,
                   \Zend\Validator\Digits::STRING_EMPTY  => 'U ovo polje unesite samo brojeve.' ,
                   \Zend\Validator\Digits::INVALID  => 'U ovo polje unesite samo brojeve.' ,
                            ],
               ]
           ]
       ]
                 ));
   #---------------------------------------------------------------------  
        
        
              $this->add(array(
            'name'=>'cena_do',
            'requeried'=>true,
            'allow_empty'=>true,
       'validators'=>[
           [
               'name'=>'Zend\Validator\Digits',
                  'options'=>[
                   'messages' => [
                   \Zend\Validator\Digits::NOT_DIGITS  => 'U polje cena unesite samo brojeve.' ,
                   \Zend\Validator\Digits::STRING_EMPTY  => 'U ovo polje unesite samo brojeve.' ,
                   \Zend\Validator\Digits::INVALID  => 'U ovo polje unesite samo brojeve.' ,
                            ],
               ]
           ]
       ]
                 ));
   #---------------------------------------------------------------------    
        
        
                $this->add(array(
            'name'=>'kvadratura_od',
            'requeried'=>true,
            'allow_empty'=>true,
       'validators'=>[
           [
               'name'=>'Zend\Validator\Digits',
                  'options'=>[
                   'messages' => [
                   \Zend\Validator\Digits::NOT_DIGITS  => 'U polje kvadratura unesite samo brojeve.' ,
                   \Zend\Validator\Digits::STRING_EMPTY  => 'U ovo polje unesite samo brojeve.' ,
                   \Zend\Validator\Digits::INVALID  => 'U ovo polje unesite samo brojeve.' ,
                            ],
               ]
           ]
       ]
                 ));
   #---------------------------------------------------------------------  
        
        
        
                $this->add(array(
            'name'=>'kvadratura_do',
            'requeried'=>true,
            'allow_empty'=>true,
       'validators'=>[
           [
               'name'=>'Zend\Validator\Digits',
                  'options'=>[
                   'messages' => [
                   \Zend\Validator\Digits::NOT_DIGITS  => 'U polje kvadratura unesite samo brojeve.' ,
                   \Zend\Validator\Digits::STRING_EMPTY  => 'U ovo polje unesite samo brojeve.' ,
                   \Zend\Validator\Digits::INVALID  => 'U ovo polje unesite samo brojeve.' ,
                            ],
               ]
           ]
       ]
                 ));
   #---------------------------------------------------------------------  
        
        
   $this->add([
       'name'=>'sobe_do',
       'required'=>true,
       'allow_empty'=>true,
       'validators'=>[
           [
               'name'=>'InArray',
               'options'=>[
                   'haystack'=>[1,2,3,4,5],
                  // 'strict'   => true,
               ]
           ]
       ],

    ]);
   
   
   
   
   
   
       $this->add(array(
            'name'=>'cg',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
   
   
   
       $this->add(array(
            'name'=>'eg',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
         $this->add(array(
            'name'=>'ta',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
           $this->add(array(
            'name'=>'gas',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
             $this->add(array(
            'name'=>'podno',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
               $this->add(array(
            'name'=>'kaljeva',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
                 $this->add(array(
            'name'=>'norveski',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
                   $this->add(array(
            'name'=>'mermerni',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
                   
                     $this->add(array(
            'name'=>'uknjizen',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
   
   
   
   
                      $this->add(array(
            'name'=>'vlasnik',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
                                         $this->add(array(
            'name'=>'agencija',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
                                                            $this->add(array(
            'name'=>'investitor',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
                                                                               $this->add(array(
            'name'=>'banka',
            'requeried'=>false,
             'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
   
   
   
   
     #--------------------------------------------------------------------- 
                   
         $this->add(array(
            'name'=>'email',
            'requeried'=>true,
             'allow_empty' => FALSE, 
            'filters'  => array(
                       //array('name' => 'String'),
                    ),
               'validators' => array(
                                           [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Molimo vas unesite odgovarajuci email.' 
                            ],
                        ],
                     ], 
             [
                 'name'     =>'EmailAddress',
                 'options'=>[
                     'messages'=>[
                         \Zend\Validator\EmailAddress::INVALID_FORMAT => 'Molimo vas unesite odgovarajuci email.' 
                     ]
                 ]
             ]
            ),
                 ));
   #--------------------------------------------------------------------- 
   
   
     #--------------------------------------------------------------------- 
                   
         $this->add(array(
            'name'=>'ime',
            'requeried'=>true,
             'allow_empty' => FALSE, 
            'filters'  => array(
                      array('name' => 'StripTags'),
                      array('name' => 'StringTrim'),
                    ),
               'validators' => array(
                                           [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Molimo vas unesite vase ime.' 
                            ],
                        ],
                     ], 
                    [
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[a-zA-Z\d\s]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Dozvoljeni karakteri su: slova , brojevi, razmak.'
                     ))
                     ], 
            ),
                 ));
   #--------------------------------------------------------------------- 
   
   
   
   
   
 
   
   
   
   
   

    }
    
}

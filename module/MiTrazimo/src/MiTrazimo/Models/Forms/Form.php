<?php

namespace MiTrazimo\Models\Forms;

/**
 * Description of Form
 *
 * @author aleksa
 */
class Form  extends \Zend\Form\Form{
   
    
    
    
    
      public function __construct() {
       
      
        parent::__construct('oglasi-filter');//ime forme
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'mi-trazimo-form');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setInputFilter(new \MiTrazimo\Models\Forms\Filter());//filter
        
         

                
#kategorija--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'kategorija',
             'attributes' => array(
               'id' => 'select_kategorija',
              'multiple'=>'multiple',
               'class'=>'select-submit',
                 'data-detect'          =>'kategorija'
            ),
   
             
     ));
      #----------------------------------------- 
     #-----------------------------------------   
        
                         
                
        #  radio buttons---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'prodaja_izdavanje',
            'attributes'=>array(
              // 'class'=>'radio_prodaja_izdavanje',
              //  "id"=>"radio_prodaja_izdavanje"
                  ),
                 'options' => array(
                  'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'Prodaja',
                            2=> 'Izdavanje',
                     ),
             ),
                 'attributes' => array(
                'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #----------------------------------------- 
        
    
       
#mesto--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'mesta_srbija',
           'attributes'=>array(
               "id"=>"mesta_srbija",
               'class'=>'select-submit',
               'style'=>'width:100%',
               'data-detect'            =>'mesto',
               //'value'=>'1'
           ),
           
     ));
      #-----------------------------------------        
   
  #sa slikom--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'samo-sa-slikom',
            'attributes'=>array(
          'id'=>'samo-sa-slikom'
               
                  ),
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
           
     ));
      #-----------------------------------------     
       

       
          $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'sobe_do',
              'attributes'=>[
                 'class'=>'span2',
             ],             
             'options' => array(
                     'label' => 'Which is your mother tongue?',
                    // 'empty_option'=>'do',
                     'value_options' => ['1'=>1,2,3,4,'5'=>'5+'],
             )
     ));   
                
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'cena_od',
            'attributes'=>array(
                'type'=>'text',
                 'class'=>'span2',
           'placeholder'=>"0",
                'style'=>" "
            ),
           
            
        ));
     #-----------------------------------------   
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'cena_do',
            'attributes'=>array(
                'type'=>'text', 'class'=>'span2',
            'placeholder'=>"do",
               'style'=>" "          
            ),
           
            
        ));
     #-----------------------------------------   
       
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'kvadratura_od',
            'attributes'=>array(
                'type'=>'text',
            'placeholder'=>"od", 'class'=>'span2',
              // 'style'=>"width: 45%;height:30px;border:1px solid silver;"       
            ),
           
            
        ));
     #-----------------------------------------   
       
                    
     #TEXT-------------------------------------         
        $this->add(array(
            'name'=>'kvadratura_do',
            'attributes'=>array(
                'type'=>'text',
             'placeholder'=>"do", 'class'=>'span2',
           //  'style'=>"width: 45%;height:30px;border:1px solid silver;"         
            ),
           
            
        ));
     #-----------------------------------------  
   
    
        
        #terasa--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'cg',
          'options' => array( 'label' => 'A checkbox','use_hidden_element' => false,'checked_value' => 1,'unchecked_value' => 0),
           'attributes'=>['value'=>1],
        ));
      #-----------------------------------------  
                #terasa--------------------------------        
         $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'eg',
          'options' => array( 'label' => 'A checkbox','use_hidden_element' => false,'checked_value' => 1,'unchecked_value' => 0),
           'attributes'=>['value'=>1],
        ));
               #terasa--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'ta',
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1],
        ));
      #-----------------------------------------  
               #terasa--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'gas',
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1],
        ));
      #-----------------------------------------  
               #terasa--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'podno',
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1],
        ));
      #-----------------------------------------  
               #terasa--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'kaljeva',
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1],
        ));
      #-----------------------------------------  
               #terasa--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'norveski',
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1],
        ));
      #-----------------------------------------  
               #terasa--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'mermerni',
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1],
        ));
      #-----------------------------------------  
        
                     #terasa--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'uknjizen',
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1],
        ));
      #-----------------------------------------  
        
        
        
        
        
        
                             # --------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'vlasnik','attributes'=>['checked'=>true],
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1,],
        ));
      #-----------------------------------------  
                            # --------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'agencija','attributes'=>['checked'=>true],
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1,],
        ));
      #-----------------------------------------  
                            # --------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'investitor','attributes'=>[],
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1,],
        ));
      #-----------------------------------------  
                            # --------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox','name'=> 'banka',
          'options' => array( 'use_hidden_element' => true,'checked_value' => 1,'unchecked_value' => 0), 'attributes'=>['value'=>1,],
        ));
      #-----------------------------------------  
        
        
        
      
                 #email-------------------------------------------        
          $this->add(array(
            'name'=>'email',
            'attributes'=>array(
                'type'=>'text',
             //  'class'=>'btn btn-primary  arrow-right pull-right',
               // 'value'=>'Prijavi se',
              // 'id'=>'filteri-form',
              // 'style'=>'display:none',
                
            ),
           ));
        
        #-----------------------------------------   
        
               #ime-------------------------------------------        
          $this->add(array(
            'name'=>'ime',
            'attributes'=>array(
                'type'=>'text',
             //  'class'=>'btn btn-primary  arrow-right pull-right',
               // 'value'=>'Prijavi se',
              // 'id'=>'filteri-form',
              // 'style'=>'display:none',
                
            ),
           ));
        
        #-----------------------------------------   
        
        
        
        
        
        
              #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit-filters',
            'attributes'=>array(
                'type'=>'submit',
               'class'=>'btn btn-primary  arrow-right pull-right',
                'value'=>'Prijavi se',
               'id'=>'filteri-form',
              // 'style'=>'display:none',
                
            ),
           ));
        
        #-----------------------------------------   
          
          
          
          
          
      }
    
    
    
    
    
    
    
    
    
    
    
    
}

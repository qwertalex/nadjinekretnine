<?php

namespace OstaviOglasForm;


use Zend\Mvc\MvcEvent;

//local import
use OstaviOglasForm\Models\DbInterpreter\DBInsertOglas;
use OstaviOglasForm\Models\DbInterpreter\Interpreter;
use Application\Models\User\SessionManipulation;
#-------------------------------------------------




class Module {
    
    
       public function onBootstrap(MvcEvent $e){
 
           
      }

    
    
 #default   includuje Helloworld/config/module.config.php  
public function getConfig() {return include __DIR__ . '/config/module.config.php';}
      
#autoload by the ModuleManager;
public function getAutoloaderConfig(){
return array(
   'Zend\Loader\StandardAutoloader' => array(
      'namespaces' => array( __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,))
         );
     }
      
  
     
     
     
     /*
 * this method add do manu things!
 * 
 * key invokables sadrzi key value madels. value je lokacija modela
 */
    public function getServiceConfig(){
        
        return array(
              'abstract_factories' => array(),
            'aliases' => array(),
            'factories' => array(
            'DB_insert_oglas' => function($serviceLocator) {
               $dbAdapter=$serviceLocator->get('Zend\Db\Adapter\Adapter');
               return new DBInsertOglas($dbAdapter);
            }, 
             'OstaviOglasInterpreter'=>function($sl){
               $dbAdapter=$sl->get('Zend\Db\Adapter\Adapter');
               $Oglasi=$sl->get('Oglasi');
               $User=$sl->get("User"); 
               return new Interpreter($Oglasi,$User);                
             },
                     
             ),
        
   #add new models---------------------------------------------------------
            'invokables' => array(
                'OstaviOglasForm' => 'OstaviOglasForm\Models\OstaviOglasForm',
                'FilterOstaviOglasForm'=>'OstaviOglasForm\Models\FilterOstaviOglasForm',
            ///    'ImgUpload'=>'OstaviOglasForm\Models\UploadFile',
                'FilterImgUpload'=>'OstaviOglasForm\Models\FilterUploadFile',
                'FormKontaktPodatci'=>'OstaviOglasForm\Models\FormKontaktPodatci',
                
                
        ), 
    #----------------------------------------------------------------------        
            
            'services' => array(),
            'shared' => array(),  
            
            
            
        );
        
        
        
        
    }
     
     
     
     
     
     
     
     
     
     
     
}














<?php
return array(
  
'controllers' => array(
     'invokables' => array(
'OstaviOglasForm\Controller\Index' => 'OstaviOglasForm\Controller\IndexController',# <------HERE
          // <----- Module Controller
      ),
 ),    
    
    'router' => array(
   'routes' => array(
     'ostavioglas' => array(# <------HERE
        'type'    => 'segment',
        'options' => array(
           'route'    => '/ostavi-oglas[/:action][/ad/:ad_id]',# <------HERE
  'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              'id'     => '[0-9]+',
              'ad'     => 'ad',
              'ad_id'     =>'[0-9]+',
      
            ),
          'defaults' => array(
               'controller' => 'OstaviOglasForm\Controller\Index',# <------HERE
                // <--- Defined as the module controller
               'action'     => 'index',
                // <---- Default action
            ),
        ),
     ),
  ),
),
    
     'view_manager' => array(
         'template_map'=>[
            'listaOglasa'             =>__DIR__ . '/../view/layout/helpers/form/first-tub.phtml',
              'ostavi-oglas-mapa-helper'             =>__DIR__ . '/../view/layout/helpers/mapa.phtml',
             'ostavi-oglas-slike'             =>__DIR__ . '/../view/layout/helpers/slike.phtml',
         ],
         'template_path_stack' => array(
            __DIR__ . '/../view',
         ),
     ),
    
    

    
    
    
    
    
    
    
    
    
    
    
);













<?php
namespace OstaviOglasForm\Models;
 
use Zend\Validator\AbstractValidator;
                     
class ValidatorValuta extends AbstractValidator
{
    const NOTSPECIAL = 'NOTSPECIAL';
 
    protected $messageTemplates = array(
        self::NOTSPECIAL => 'Izaberite valutu',
    );
     
    public function __construct(array $options = array())
    {
       parent::__construct($options);
    }
 
    public function isValid($value)
    {
        switch ($value) {
            case 'eur':
                case 'din':
                    return TRUE;
          default:
           $this->error(self::NOTSPECIAL);
           return false;
             
        }
    }
}  
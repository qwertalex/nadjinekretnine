<?php 
namespace OstaviOglasForm\Models\DbInterpreter;

use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Application\Models\User\User; 


class DBInsertOglas {
   
    
    protected $adapter;
    protected $ads_counter;
    protected $User;


    public function __construct(Adapter $adapter) {
        $this->adapter=$adapter;
    
        
    }

    
    /**
     * 
     * @param array $post
     * 
     * U $post param su indexi imena tabela a vrednosti su parovi key=>value
     * koji su zapravo kolone u bazi i vrednosti koje se insertuju
     * 
     * 
     * 
     * 
     */
    public function insert_data($post) {
      
        try{
             $this->adapter->getDriver()->getConnection()->beginTransaction();
             $sql = new Sql($this->adapter);
             $insert=$sql->insert('oglasi_nk');
             $insert->values($post['oglasi_nk']);
             $selectString = $sql->getSqlStringForSqlObject($insert);
             $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
             if ( $results->count() === 0 ) {die('Doslo je do greske, molimo vas probajte za nekoliko minuta!');}    
           
             unset($post['oglasi_nk']);
            $id= $this->adapter->getDriver()->getConnection()->getLastGeneratedValue(); 

         
    foreach ($post as $k=>$value) {
        $value['id_oglasi_nk']=$id; 
        $insert=$sql->insert($k);
     
        $insert->values($value);
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        if ( $results->count() === 0 ) {die('Doslo je do greske, molimo vas probajte za nekoliko minuta!');}
       }
       //commit transaction
      $this->adapter->getDriver()->getConnection()->commit();  
      return $id;
        }
 catch (Exception $exc){
     //rollback transaction
     $this->adapter->getDriver()->getConnection()->rollback();
 }  
 return false;
    }
    
  
    
    
    
    
    /**
     * 
     * @param type $post
     * @param type $ad_id
     * 
     * 
     * 
     * Update ads data
     * 
     */
      public function update_data($post,$ad_id) {
          $const_tables=[
       "dodatni_podatci",
       "dodatno"         ,
        "kontakt_podaci" ,
        "kuce"  ,
        "plac"  ,
     //   'selidbe' ,
        ];
        //  var_dump($post);
        try{
             $this->adapter->getDriver()->getConnection()->beginTransaction();
             $sql = new Sql($this->adapter);
             
             foreach ($post as $key => $v) {
                 if ($key==='vrsta_oglasa') {
                     unset($v['datum_postavljanja_oglasa']);
                     unset($v['datum_isteka']);
                     unset($v['vrsta_oglasa']);
                     $v['datum_azuriranja']=date("Y-m-d H:i:s");
                 }
                 if ($key==='oglasi_nk') {
                     unset($v['datum_kreiranja_oglasa']);
                 }
                   $all_tables[]=$key;
                
              $update=$sql->update($key);
             // var_dump($key);
            //  var_dump($v);
             $update->set($v);                
                 $update->where([
                     $key.'.id_oglasi_nk'=>$ad_id,
                 ]);
        $selectString = $sql->getSqlStringForSqlObject($update);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);   
        if ($results->count()==0) {
          //  $sql = new Sql($this->adapter);
            $select=$sql->select();
            $select->from($key)->where([
                     $key.'.id_oglasi_nk'=>$ad_id,
                 ]);
              $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);  
         if ($results->count()==0) {
          //   $sql = new Sql($this->adapter);
              $insert=$sql->insert($key);
              $v['id_oglasi_nk']=$ad_id;
             $insert->values($v);      
            
                $selectString = $sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);  
         //var_dump($results->count());   
         }              
          
        }

             }
             
            $resset_data=array_diff($const_tables, $all_tables);
            foreach ($resset_data as $tabl) {
                $delete=$sql->delete();
                $delete->from($tabl);
                $delete->where([
                    'id_oglasi_nk'=>$ad_id
                ]);
               $selectString = $sql->getSqlStringForSqlObject( $delete);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);  
         // var_dump($results->count());                  
            }
             
             
             
             
             
         //commit transaction
      $this->adapter->getDriver()->getConnection()->commit();  
        }
 catch (Exception $exc){
     //rollback transaction
     $this->adapter->getDriver()->getConnection()->rollback();
 }  
// var_dump($all_tables);
    }  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 
}
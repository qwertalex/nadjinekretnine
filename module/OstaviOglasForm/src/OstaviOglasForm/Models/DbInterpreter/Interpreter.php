<?php
namespace OstaviOglasForm\Models\DbInterpreter;


//local
//use Application\Models\LokacijaSr\Lokacija;
use Application\Models\User\User; 

class Interpreter {
   
    protected $post;
    protected $db_data=[];
    protected $user_id;
    protected $User;
    protected $Oglasi;

    public function __construct($Oglasi,$User) {
        $this->User=$User;
        $this->Oglasi=$Oglasi;
        
        $this->set_user_id();
     }
     
     
     protected function set_user_id() {
         $this->user_id=$this->User->get_user_id();
         
     }
     /**
      * 
      * @return type
      * 
      * Vraca tip usera
      * ako je 1 onda je fizicko lice
      * 2 pravno lice
      */
     protected function user_type() {
         return $this->User->user_type();
      }
      /*
       * Ukupan Broj oglasa koji je user postavio
       * 
       */
      protected function ads_counter() {
          return $this->Oglasi->count_active_ads();
          
      }
      protected function fizicko_lice(){
          if ($this->user_type()==1&&$this->ads_counter()>0) {
              return "0";
          }
          return '1';
      }
      protected function pravno_lice() {
         return "0";
          
      }
      protected function ads_activate() {
          switch((int)$this->user_type()){
              case 1:return $this->fizicko_lice(); 
              case 2:return $this->pravno_lice(); 
              
          }
          
          return false;
      }
     
     public function set_data($post){
         $this->post=$post; 
        $this->call_func();        
     }
    
     public function get_data() {
         return $this->db_data;
         
     }
     
     
     protected function call_func() {
         $post=  $this->post;
$kategorija=$post['kategorija'];
       
       
      // $this->kategorija();
       $this->oglasi_nk(); 
       $this->vrsta_oglasa();
       $this->kontakt_podatci();
       $this->lokacija_srbija();
       $this->slike();       
              
 
     //  $pi=(int)$this->post['prodaja_izdavanje'];       
       $col='stan';
        
          switch((int)$kategorija){
              case 1: case 2:case 6:case 7: case 8:case 9:case 10:case 11:            
              case 14: case 15: case 16:case 17:
              $this->dodatni_podatci();$this->dodatno();
              break;         
              case 4: $this->dodatno();break;
              case 3:$this->kuce(); $this->dodatni_podatci();$this->dodatno();  break;
              case 5:$this->plac();  $this->dodatni_podatci(); $this->dodatno(); break;
              case 12:$this->dodatno(); break;
              case 13:$this->dodatno(); break;
              

          }
          
     }
     
     
     
    
      
      
      public function lokacija_srbija() {
         $post=  $this->post;
         $table='lokacija';
         
         $mesto_deo_lokacija=$mesto=$post['mesta_srbija'];
         $deo=null;
         $lokacija=NULL;
         $ulica=NULL;
         $mapa=NULL;
         
          if (isset($post['deo_mesta'])&&(int)$post['deo_mesta']>0) {
             $deo=$post['deo_mesta'];$mesto_deo_lokacija.='-'.$deo;
         }        
         
         if (isset($post['lokacija'])&&(int)$post['lokacija']>0) {
             $lokacija=$post['lokacija'];$mesto_deo_lokacija.='-'.$lokacija;
         }
         
         if (isset($post['ulica'])&&$post['ulica']!="") {
             $ulica=$post['ulica'];
         }         
          if (isset($post['lat'],$post['lng'])) {
              if (trim($post['lat'])!=""&&  trim($post['lng'])!="") {
                 $mapa=$post['lat'].'|'.$post['lng']; 
                }
            
         }        
         
          $this->db_data[$table]=[
             'mesto'=>$mesto,
              'deo_mesta'=>$deo,
              'lokacija'=>$lokacija,
              'ulica'=>$ulica,
              'mapa'=>$mapa,
              'mesto_deo_lokacija'=>$mesto_deo_lokacija
              
          ];         
          
           }
      
      
      protected function is_set($p) {
          $post=$this->post;
          
          if (isset($post[$p])&&$post[$p]!="") {
              return $post[$p];
              
          }
          return NULL;
      }




      protected function oglasi_nk() {
          $table='oglasi_nk';

         $prodaja_izdavanje=  $this->is_set('prodaja_izdavanje');
         
          
          $this->db_data[$table]=[
            'id_users_nk'=>$this->user_id,
              'naslov'=>  $this->is_set('naslov'),
              'opis'=>  $this->is_set('opis'),
              'oglasivac'=> $this->is_set('oglasivac_nekretnine'),
              'prodaja_izdavanje'=>$prodaja_izdavanje,
              'kategorija'=>$this->post['kategorija'],
              'broj_pregleda'=>1,
              'datum_kreiranja_oglasa'=>$date=date("Y-m-d H:i:s")
          ];
      }
      
      
      
      
      protected function vrsta_oglasa(){
           $table="vrsta_oglasa";
           $datum_postavljanja_oglasa=null;
           $datum_isteka=date('Y-m-d H:i:s',strtotime('+1 months'));
           
           if ($this->ads_activate()!="0") {
              $adsInfo= \Application\Models\Oglasi\AdsInfo::standardni_ad_trajanje();              
              $datum_postavljanja_oglasa=$adsInfo['datum_postavljanja_oglasa'];
              $datum_isteka=$adsInfo['datum_isteka'];
           }

              
          $this->db_data[$table]=[
              'vrsta_oglasa'=>  $this->ads_activate(),
             //'paketi'=>NULL,
              'datum_postavljanja_oglasa'=>$datum_postavljanja_oglasa,
              'datum_isteka'=>$datum_isteka,
           ];
      }

      











      protected function dodatno() {
          $table="dodatno";
          $valuta=$this->is_set('selidbe_valuta');
          if (!$valuta) {
              $valuta="eur";
          }
          $this->db_data[$table]=[
              'grejanje'        =>$this->is_set('grejanje'),
              'spratovi'        =>$this->is_set('sprat'),
              'ukupno_spratovi' =>$this->is_set('ukupno_spratova'),
              'kvadratura'      =>$this->is_set('kvadratura'),
              'cena'            =>$this->is_set('cena'),
              'broj_soba'       =>$this->is_set('broj_soba'),
              'tip_objekta'     => $this->is_set('tip_objekta'),
              'opis_objekta'    =>$this->is_set('opis_objekta'),
              'nacin_placanja'  =>$this->is_set('nacin_placanja'),
              'namestenost'     =>$this->is_set('namestenost'),
              'valuta'          =>$valuta
              
          ];
          
          
          
          
          
      }
      
    
    
      
      protected function dodatni_podatci() {
          $table="dodatni_podatci";
          
          $this->db_data[$table]=[
              "interfon"=>$this->is_set('interfon'),
              "kablovska"=>$this->is_set('kablovska'),
              "internet"=>$this->is_set('internet'),
              "klima"=>$this->is_set('klima'),
              "depozit"=>$this->is_set('depozit'),
              "zamena"=>$this->is_set('zamena'),
              "odmah_useljiv"=>$this->is_set('odmah_useljiv'),
              "za_pusace"=>$this->is_set('za_pusace'),
              "za_nepusace"=>$this->is_set('za_nepusace'),
              "uknjizen"=>$this->is_set('uknjizen'),
              "lift"=>$this->is_set('lift'),
              "podrum"=>$this->is_set('podrum'),
              "topla_voda"=>$this->is_set('topla_voda'),
              "dodatni_podatci_telefon"=>$this->is_set('telefon'),
              "dodatno_graza"=>$this->is_set('graza'),
              "terasa"=>$this->is_set('terasa'),
              "parking"=>$this->is_set('parking'),
              'video_nadzor'=>  $this->is_set('video_nadzor'),
              
          ];
          
             
      }
                      
      
      protected function kontakt_podatci() {
          $table="kontakt_podaci"; 
          $this->db_data[$table]=[
              'telefon'=>$this->is_set('kontakt_telefon'),
              'dodatni_telefon'=>$this->is_set('kontakt_telefon2'),
              'email'=>$this->is_set('email'),
              'ime'       =>$this->is_set("kp_ime"),
              'mesto_kontakt_podaci'        =>$this->is_set('kp_mesto'),
              'ulica_kontakt_podaci'        =>$this->is_set('kp_ulica')
              
          ];
       
      }
      
      
    
      protected function kuce() {
         $table='kuce';
         $this->db_data[$table]=[
             'spratnost'=>$this->is_set('kuce_sparatnost'),
             'vrsta_kuce'=>$this->is_set('vrsta_objekta')
         ];
          
      }

     protected function plac() {
         $table='plac';
         $this->db_data[$table]=[
             'ostalo_plac'=>$this->is_set('ostalo_plac'),
             'vrsta_plac'=>$this->is_set('vrsta_plac'),
             'povrsina_plac'=>$this->is_set('plac_povrsina'),
             'jedinica_mere_plac'=>$this->is_set('ha_ar'),
             
             
             
         ];
         
     }
    
     protected function slike() {
         $post=  $this->post;
         $table='slike';
     function clean_img($sli){
            if (!$sli) {return null;}
            if (\strpos($sli,"/")!==false) { return $sli;}
            return date("Y-m-d")."/".$sli;            
      }  
         
                      $this->db_data[$table]=[
                          'slika_1'=>clean_img($this->is_set('slika_1')),
                          'slika_2'=>clean_img($this->is_set('slika_2')),
                          'slika_3'=>clean_img($this->is_set('slika_3')),
                          'slika_4'=>clean_img($this->is_set('slika_4')),
                          'slika_5'=>clean_img($this->is_set('slika_5')),
                          'slika_6'=>clean_img($this->is_set('slika_6')),
                          'slika_7'=>clean_img($this->is_set('slika_7')),
                          'slika_8'=>clean_img($this->is_set('slika_8')),
                          'slika_9'=>clean_img($this->is_set('slika_9')),
                          'slika_10'=>clean_img($this->is_set('slika_10'))
                      ];
         }
     
     
     /*
        protected function tip_oglasa() {
         $post=  $this->post;
         $table='tip_oglasa';
         $col="";
         $dana="";
         $tip=$post['tip_oglasa']; 
         
         switch($tip){
             case 'standardni_oglas':
                 $col='standardni';
                 $dana='select_standardni_oglas';
                 break;
              case 'istaknuti_oglas':
                 $col='istaknuti';
                 $dana='select_istaknuti_oglas';
                 break;
             case 'top_oglas':
                 $col='top';
                 $dana='select_top_oglas';
                 break;             
              case 'premium_oglas':
                 $col='premium';
                 $dana='select_premium_oglas';
                 break;            
         }
         
         
          $this->db_data[$table]=[
              $col=>1,
              'trajanje'=>$post[$dana],
              ];
         }
     
     

         protected function selidbe() {
             $post=  $this->post;
             $table='selidbe';
         $this->db_data[$table]=[
             'selidbe_cena'=>  $this->is_set('selidbe_cena'),
             'selidbe_dogovor'=>  $this->is_set('selidbe_cena_dogovor'),
             'selidbe_valuta'=>  $this->is_set('selidbe_valuta'),
             ];
         
         
         
             
         }
     
     
          */
     
     
     
     
     
     
      
    
    
    
}





<?php

namespace OstaviOglasForm\Models;

 //use Zend\Captcha;

use Zend\Form\Form;

//local
use Application\Models\LokacijaSr\Lokacija;


class OstaviOglasForm extends Form
{
    

    public function __construct() {
       
        
        
        
        parent::__construct('addNoviOglas');
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('id', 'form-novi-oglas');
        $this->setInputFilter(new \OstaviOglasForm\Models\FilterOstaviOglasForm());
        
        
        
        
                
#kategorija--------------------------------        
       $this->add(array(
             'type' => 'hidden',
             'name' => 'kategorija',
             'attributes' => array(
               'id' => 'e1',
               "class"=>"span3 ",
               'data-el'=>'kategorija'
              // 'value'=>'14',
            ),
   
             
     ));
#-----------------------------------------    
        
         #prodaja izdavanje radio buttons---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'prodaja_izdavanje',
            'attributes'=>array(
              // 'class'=>'span3',
                "id"=>"radio_prodaja_izdavanje",
                 'style'=>'font-weight: normal;',
                 'value' =>1 ,
                'data-el'=>'prodaja_izdavanje',
                  ),
                 'options' => array(
                     'value_options' => array(
                            1=> 'Prodaja',
                            2=> 'Izdavanje',
                     ),
             ),
           
           
     ));
      #-----------------------------------------  
   
                          
#mesto--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'mesta_srbija',
           
           'attributes'=>array(
               "id"=>"mesta_srbija",
               "class"=>"span3",
               'data-el'=>'mesta_srbija'
           ),
             'options' => array(
                     'value_options' => Lokacija::get_mesta_srbija(),
                  'empty_option' =>'--Izaberite--',
                 'disable_inarray_validator' => true,
             )
     ));
      #-----------------------------------------       
        
        
       
       
       
       
    #deo mesta--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'deo_mesta',
           'attributes'=>array(
              // "disabled"=>"disabled",
               'id'=>'deo_mesta',
               "class"=>"span3",
               'data-el'=>'deo_mesta'
           ),
             'options' => array(
                 'empty_option' =>'--Izaberite--',
                   'disable_inarray_validator' => true,
             )
     ));
      #-----------------------------------------        
       
       
       
        #lokacija--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'lokacija',
           'attributes'=>array(
               //"disabled"=>"disabled",
               'id'=>'select_lokacija',
               "class"=>"span3",
               'data-el'=>'lokacija'
           ),
             'options' => array(
              'disable_inarray_validator' => true,
             )
     ));
      #-----------------------------------------        
       
       
       
       
       
       
       
       
       
       
       
                
     #Ulica-------------------------------------         
        $this->add(array(
            'name'=>'ulica',
            'attributes'=>array(
                'type'=>'text',
           //  "style"=>"height:30px;",
             //  "id"=>"input_ulica",
                'class'=>'span4',
                
            ),
           
            
        ));
     #-----------------------------------------   
        
        
        
  #Kvadratura-------------------------------------         
        $this->add(array(
            'name'=>'kvadratura',
            'type'=>'text',
            'attributes'=>[
                'class'     =>'span2',
                'title'=>"U ovo polje unesite samo brojeve bez razmaka i zareza!"
            ],
  
        ));
     #-----------------------------------------     
        
        
 #cena-------------------------------------         
        $this->add(array(
            'name'=>'cena',
            'type'=>'text',
            'attributes'=>[
                  'class'     =>'span2',
              //  'data-toggle'=>"tooltip",
                'title'=>"U ovo polje unesite samo brojeve bez razmaka i zareza!"
            ],
           
            
        ));
     #-----------------------------------------    
        
        
        
        
        
                                
#broj soba--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'broj_soba',
              'attributes'=>array(
            //  "id"=>"select_broj_soba",  
             'class'=>'span2'
            ),
             'options' => array(
                  'empty_option' =>'--Izaberite--',
                     'value_options' => [1=>'1',2,3,4,5=>'5+'],
             )
     ));
      #-----------------------------------------    

       
       
      #cena-------------------------------------         
        $this->add(array(
            'name'=>'selidbe-cena',
            'type'=>'text',
            'attributes'=>[
                  'class'     =>'span2'
            ],
           
            
        ));
     #-----------------------------------------           

   
     #selidbe-cena-dogovor--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'selidbe_cena_dogovor',
            'attributes'=>array(
            //   'class'=>'radio_prodaja_izdavanje',
                'id'=>'selidbe_cena_dogovor',
               
                  ),
                   'options' => array(
                     'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
           
     ));
      #-----------------------------------------  
       
       
       
       
      /*
#selidbe valuta--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'selidbe_valuta',
              'attributes'=>array(
              "id"=>"selidbe_valuta",  
               'class'=>'span1',
                  'style'=>'float:left'
            ),
             'options' => array(
                     'value_options' => array(
                              'eur'=> '€',
                             'din'=> 'rsd',
                          
                     ),
             )
     ));
      #----------------------------------------- 
     */  
 
       
        
       
       
       /**
        * 
        * 
        * Google maps lng lat
        * 
        * 
        */
       
               $this->add(array(
            'name'=>'lat',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'input_lat',
                "style"=>"",
             ),
           
            
        ));
               $this->add(array(
            'name'=>'lng',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'input_lng',
               
            ),
         ));
       
       /*
        * ======================================
        * ======================================
        * ======================================
        * 
        * 
        */
       
       
        
        
        
        
        
        
      /*
       * 
       * 
       * 
       * DRUGI TAB
       * 
       * 
       * 
       * 
       * 
       * 
       * 
       */  
        
        
        
       
#naslov-------------------------------------         
        $this->add(array(
            'name'=>'naslov',
            'type'=>'text',
            'attributes'=>[
                'class'=>'input-block-level',
                'id'    =>'input_naslov',
                'title'=>"Unesite naslov vase usluge ili objekta. Duzina naslova moze biti najvise 50 karaktera, npr. Prodajem stan u naselju Medakovic!"
            ],
           
            
        ));
     #-----------------------------------------  
       
       
       
        
#opis--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Textarea',
             'name' => 'opis',
           'attributes'=>array(
               'cols'=>'8',
               'rows'=>'8',
              'id'=>'textarea-size' ,
               
            
            ),
             'options' => array(
               
               )
     ));
      #-----------------------------------------     
        
        
       
        
           
       
#oglasivac_nekretnine---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'oglasivac_nekretnine',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje ',
                'id'=>'oglasivac_nekretnine',
                 'value' => 1 
                  ),
                 'options' => array(
                  'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=>'Vlasnik',
                            2=>'Agencija',
                            3=>'Investitor',
                            4=>"Banka"
                     ),
             ),
        ));
      #-----------------------------------------      
        
  
       
               
#grejanje--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'grejanje',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje',
                
                  ),
                 'options' => array(
                  //'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'cg',
                            2=> 'eg',
                            3=>'TA',
                            4=>'gas',
                         5=>'podno',
                         6=>'kaljeva peć',
                         7=>'norveški radiatori',
                         8=>'mermerni radiatori',
                         
                     ),
             ),
                 'attributes' => array(
               // 'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #-----------------------------------------    
        
        
        
        
        
                 
#tip objekta--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'tip_objekta',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje',
                
                  ),
                 'options' => array(
                  //'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=>'standardni',
                            2=> 'stara gradnja',
                            3=> 'novogradnja',
                            4=>'u izgradnji',
                         
                           
                         
                     ),
             ),
                 'attributes' => array(
              //  'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #-----------------------------------------       
        
           
       
     /*  
              
  #kiosk_opis---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'kiosk_opis',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje ',
                
                  ),
                 'options' => array(
                  'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'renoviran',
                            2=> 'za renoviranje',
                         
                     ),
             ),
                 'attributes' => array(
              //  'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #-----------------------------------------        
       */

      
       
       
       
          
        
   #opis_objekta--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'opis_objekta',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje',
                ),
                 'options' => array(
                     'value_options' => array(
                         1=> 'izvorno stanje',
                         2=> 'renoviran',
                         3=> 'lux',
                         4=> 'salonski',
                         5=> 'duplex',
                         6=>'za renoviranje'
                      ),
             ),
      ));
      #-----------------------------------------       
       
       
       
             
        
        
#namestenost--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'namestenost',
            'attributes'=>array(
             //  'class'=>'radio_prodaja_izdavanje',
                
                  ),
                 'options' => array(
                  //'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'namešten',
                            2=> 'polunamešten',
                            3=> 'prazan',
                       
                           
                         
                     ),
             ),
                 'attributes' => array(
              //  'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #-----------------------------------------   
        
  
       
#ostalo plac---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'ostalo_plac',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje ',
                
                  ),
                 'options' => array(
                  'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'dozvoljena gradnja',
                            2=> 'započeta gradnja',
                            3=>'deljiv',
                         
                     ),
             ),
                 'attributes' => array(
              //  'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #-----------------------------------------    
               
  #plac povrsina-------------------------------------         
        $this->add(array(
            'name'=>'plac_povrsina',
            'attributes'=>array(
                'type'=>'text',
                        'class'=>'span2'
                
            ),
  
        ));
     #-----------------------------------------     
 /* #plac vrsta povrsine--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'ha_ar',
              'attributes'=>array(
              //"id"=>"selidbe_valuta",  
               'class'=>'span1',
                  'style'=>'float:left'
            ),
             'options' => array(
                     'value_options' => [
                         1=>'ha',
                         2=>'a',
                     ]
             )
     ));
      #-----------------------------------------      */
#vrsta_plac---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'vrsta_plac',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje ',
                
                  ),
                 'options' => array(
                  'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'plac',
                            2=> 'njiva',
                            3=>'voćnjak',
                            4=> 'vinograd',
                            5=>'šuma',
                     ),
             ),
                 'attributes' => array(
              //  'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #-----------------------------------------        
       
  
                
#vrsta objekta samo za kuce--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'vrsta_kuce',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje',
                
                  ),
                 'options' => array(
                  //'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'cela kuća',
                            2=> 'duplex/viseporodicne kuće', 
                            3=> 'vila',
                            4=> 'montažna kuća',
                            5=> 'stambeno-poslovna kuća',
                            6=> 'horizontala kuće',
                            7=> 'vertikala kuće', 
                     ),
             ),
                 'attributes' => array(
              //  'value' => '1' //set default checked value to key 1;
            )
           
     ));
#-----------------------------------------    
           
       
       
       
       
       
       
       
     
#spratnost (samo za kuce)--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'kuce_sparatnost',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje',
                ),
                 'options' => array(
                  //'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=> 'prizemna',
                            2=> 'spratna',
                      ),
             ),
                 'attributes' => array(
              //  'value' => '1' //set default checked value to key 1;
            )
           
     ));
      #-----------------------------------------      
       
     
        

        
         
       
    
      
               
#sprat--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'sprat',
             'options' => array(
                     'label' => 'Which is your mother tongue?',
                    'empty_option' => '--Izaberite--',
                     'value_options' => array(
                             '1'=>1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                         21,22,23,24,25,26,27,28,29,30,
                      ),
             )
     ));
      #-----------------------------------------   
       
     /*  
 #ukupan broj spratova-------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'ukupno_spratova',
             'options' => array(
                  //   'label' => 'Which is your mother tongue?',
                    'empty_option' => '--Izaberite--',
                     'value_options' => ['1'=>1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
                         21,22,23,24,25,26,27,28,29,30],
             )
     ));
      #-----------------------------------------    
      */ 
       
       
       
      
       
        
   
      
       
     
         /*
          * 
          * 
          * 
          * TAB KONTAKT PODATCI
          * 
          * 
          * 
          * 
          * 
          * 
          * 
          */
          
                          
     #Ulica-------------------------------------         
        $this->add(array(
            'name'=>'kp_ime',
            'attributes'=>array(
                'type'=>'text', 
              //  'class'=>'span4',
                
            ),
           
            
        ));
     #-----------------------------------------   
                        
     #Ulica-------------------------------------         
        $this->add(array(
            'name'=>'kp_ulica',
            'attributes'=>array(
                'type'=>'text', 
                
                //'class'=>'span4',
                
            ),
           
            
        ));
     #-----------------------------------------   
                        
     #Ulica-------------------------------------         
        $this->add(array(
            'name'=>'kp_mesto',
            'attributes'=>array(
                'type'=>'text',
             //   'class'=>'span4',
              ),
           
            
        ));
     #-----------------------------------------   
        
          
          
           
        #telefon-------------------------------------         
        $this->add(array(
            'name'=>'kontakt_telefon',
            'attributes'=>array(
                'type'=>'text',
            // "style"=>"height:30px;",
                "id"=>"kontakt_telefon",
                'title'=>"U ovo polje unesite samo brojeve bez razmaka i zareza, u FORMATU 0642223333"
            ),
           
            
        ));
     #----------------------------------------- 
        
        
             
        #telefon2-------------------------------------         
        $this->add(array(
            'name'=>'kontakt_telefon2',
            'attributes'=>array(
                'type'=>'text',
            // "style"=>"height:30px;",
                "id"=>"kontakt_telefon2",
               'title'=>"U ovo polje unesite samo brojeve bez razmaka i zareza, u FORMATU 0642223333"          
            ),
           
            
        ));
     #-----------------------------------------    
        
        
        
              
        #telefon-------------------------------------         
        $this->add(array(
            'name'=>'email',
            'attributes'=>array(
                'type'=>'text',
            // "style"=>"height:30px;",
              "id"=>"kontakt_email",
            ),
           
            
        ));
     #-----------------------------------------   
        
        
         
        
        
        
        /*
         * 
         * 
         * dnevno izdavanje
         * 
         * 
         * 
         */
        
        
        
        #oglasivac_nekretnine---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'di_tip',
            'attributes'=>array(
               'class'=>'radio_prodaja_izdavanje ',
                'id'=>'di_tip',
                 'value' => 1 
                  ),
                 'options' => array(
                  'label' => 'What is your gender ?',
                     'value_options' => array(
                            1=>'Apartman',
                            2=>'Stan',
                            3=>'Kuća',
                            4=>"Soba",
                            5=>'Vila',
                     ),
             ),
        ));
      #-----------------------------------------    
        
        
        #broj osoba--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'broj_osoba',
              'attributes'=>array(
            //  "id"=>"select_broj_soba",  
             'class'=>'span2'
            ),
             'options' => array(
                  'empty_option' =>'--Izaberite--',
                     'value_options' => [1=>'1',2,3,4,5,6,7,8,9,10=>'10+'],
             )
     ));
      #-----------------------------------------    
        
         #broj kreveta--------------------------------        
       $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'broj_kreveta',
              'attributes'=>array(
            //  "id"=>"select_broj_soba",  
             'class'=>'span2'
            ),
             'options' => array(
                  'empty_option' =>'--Izaberite--',
                     'value_options' => [1=>'1',2,3,4,5,6,7,8,9,10=>'10+'],
             )
     ));
      #-----------------------------------------          
    
            $chebx_arr=[
           'video_nadzor','za_nepusace','za_pusace', 'zamena','uknjizen','odmah_useljiv','depozit','depozit',
              'klima'  ,'kablovska','internet','lift','podrum','topla_voda','telefon','interfon','topla_voda','graza','parking','terasa',
                'kuhinja','pegla','fen','komp','zivotinje','masina_za_ves','televizor'
                
                
       ];
       
       
        for($i=0;$i<count($chebx_arr);$i++){
            $this->add([
             'type'=> 'Zend\Form\Element\Checkbox','name'=>$chebx_arr[$i],
             'attributes'=>['class'=>'radio_prodaja_izdavanje'],
             'options' => [
                         'label' => 'A checkbox',
                         'use_hidden_element' => true,
                         'checked_value' => 1,
                         'unchecked_value' => 0
             ]]);
        }
       

        
        
       
  /*
   * 
   * slike
   * 
   */     
       
#slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_1',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_1'
             ),
         ));
     #----------------------------------------- 
 #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_2',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_2'
             ),
         ));
     #-----------------------------------------     
       
 #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_3',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_3'
             ),
         ));
     #-----------------------------------------       
       
 #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_4',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_4'
             ),
         ));
     #-----------------------------------------       
       
    #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_5',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_5'
             ),
         ));
     #-----------------------------------------    
       
    #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_6',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_6'
             ),
         ));
     #-----------------------------------------    
       
   #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_7',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_7'
             ),
         ));
     #-----------------------------------------       
   #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_8',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_8'
             ),
         ));
     #-----------------------------------------     
       
   #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_9',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_9'
             ),
         ));
     #-----------------------------------------     
       

   #slike-------------------------------------         
        $this->add(array(
            'name'=>'slika_10',
            'attributes'=>array(
                'type'=>'hidden',
                'id'=>'slika_10'
             ),
         ));
     #-----------------------------------------     
          

        
        
        
        
        
          /*
           * 
           * 
           * 
           * 
           * END SLIKE
           * 
           * 
           * 
           * 
           * 
           * 
           */
          
#submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'submit',
               'class'=>'btn btn-primary  arrow-right pull-right',
                'value'=>'Objavi oglas',
               'id'=>'predaj-oglas',
               
            ),
           ));
        
        #-----------------------------------------   
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
        
    }
    
    
    
    
    
    
   
    
    
    
   
    
}


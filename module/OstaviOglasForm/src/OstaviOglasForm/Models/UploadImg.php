<?php

namespace OstaviOglasForm\Models;

use Zend\Filter\File\RenameUpload;

//local
use Application\Models\Oglasi\Images\ResizeImage;
class UploadImg {

    const DIR = "/img/oglasi_img/";
    const ROOT = '/var/www/nadjinekretnine.com/public';

    protected $dir;
    protected $fileTypes = array('jpg', 'jpeg', 'png');
    protected $max_file_size = 10485760;
    protected $id;
    protected $post;
    protected $error = [];

    public function __construct(Array $post) {
        $this->post = $post;
        $this->dir = self::ROOT . self::DIR . date('Y-m-d') . '/';
        // var_dump($post);
        $this->validate_ext();
        $this->validate_size();
       $this->verifyToken();
        $this->check_error_num();
        $this->rename_img();

        $this->return_ajax();
    }

    /**
     * 
     * @return boolean
     * 
     * 
     */
    protected function validate_ext() {
        $validator = new \Zend\Validator\File\Extension(['jpg','jpeg','png',"JPG",'JPEG','PNG']);
        if ($validator->isValid($this->post["Filedata"])) {
             return true;
        } 
        $this->error['ext'] = FALSE;
        return false;
    }

    protected function validate_size() {
        $validator = new \Zend\Validator\File\Size(['max' => '10MB']);
        if ($validator->isValid($this->post["Filedata"])) {
            return true;
        }
        $this->error['size'] = FALSE;
        return FALSE;
    }

    protected function verifyToken() {
        $token = $this->post['token'];
        $timestamp = $this->post['timestamp'];
        $verifyToken = md5('upload_za_slike' . $timestamp);

        if ($token == $verifyToken) {
            return TRUE;
        }
        $this->error['token'] = FALSE;
        return false;
    }

    protected function check_error_num() {
        $num = $this->post['Filedata']['error'];
        if ($num == 0) {
            return TRUE;
        }
        $this->error['num'] = FALSE;
        return FALSE;
    }

    protected function generate_id() {
        $this->id = uniqid();
    }

    /**
     * 
     * rename and upload file, resize img
     * 
     * 
     * 
     */
    protected function rename_img() {
        $this->generate_id();
        if (!file_exists($this->dir)) {
            mkdir($this->dir, 0755);
        }

        
        
       // var_dump($this->post['Filedata']);
       
   $filter = new RenameUpload(
                array(
            'target' => $this->dir . $this->id,
            'use_upload_extension' => TRUE,
            "randomize" => FALSE,
        ));
        $filter->filter($this->post['Filedata']);
         
     
        //get image name and location
       $img_name=$this->get_image();
     
       //validator za width
        $validator = new \Zend\Validator\File\ImageSize(['maxWidth' => 870]);
        if (!$validator->isValid($this->dir . $this->id)) {
             $resize = new ResizeImage($this->dir . $img_name);
             $resize->resizeTo(870, 540, 'maxWidth');
             $resize->saveImage($this->dir .$img_name);
         }  
        
        //validator za height
        $validatorh = new \Zend\Validator\File\ImageSize(['maxHeight' => 540]);
        if (!$validatorh->isValid($this->dir . $this->id)) {
             $resize = new ResizeImage($this->dir . $img_name);
             $resize->resizeTo(870, 540, 'maxHeight');
             $resize->saveImage($this->dir .$img_name);
         }  
         //za small images u listi oglasa
         $validator1 = new \Zend\Validator\File\ImageSize([ 'maxWidth' => 300, 'maxHeight' => 200]);
         if (!$validator1->isValid($this->dir . $this->id)) {
             $resize1 = new ResizeImage($this->dir . $img_name);
             $resize1->resizeTo(300, 200, 'exact');
             $resize1->saveImage($this->dir.'small_' .$img_name);
         }    
    }

    protected function get_image() {
        $dir = $this->dir;
        $arr = scandir($dir);
        foreach ($arr as $v) {
            if (strpos($v, $this->id) !== FALSE) {
                return $v;
            }
        }
        return false;
    }

    protected function return_ajax() {
        $ajax = [
            'status' => 'false',
            'dir' => self::DIR . date('Y-m-d') . '/',
        ];
        $error = count($this->error);
        if ($error === 0) {
            $ajax['status'] = 'ok';
            $ajax['img'] = $this->get_image();
        }

 
        echo json_encode($ajax);
    }


}

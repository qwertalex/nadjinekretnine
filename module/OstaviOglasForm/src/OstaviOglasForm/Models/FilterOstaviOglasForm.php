<?php


namespace OstaviOglasForm\Models;


use Zend\InputFilter\InputFilter;








class FilterOstaviOglasForm extends InputFilter
{



    public function __construct() {

        
       /*
        * 
        * 
        * 
        * validacija prvog taba
        * 
        * 
        * 
        * 
        */ 
        #kategorija    
        $this->add(array(
            'name'=>'kategorija',
            'requeried'=>true,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
                                       [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Katgorija je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 15,
                      'messages'=>[
                      \Zend\Validator\Between::NOT_BETWEEN=>'Izaberite kategoriju!'
                      ]
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
 
        #mesta_srbija    
        $this->add(array(
            'name'=>'mesta_srbija',
            'requeried'=>true,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje mesta je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 179,
                       'messages'=>[
                      \Zend\Validator\Between::NOT_BETWEEN=>'Izaberite mesto!'
                      ]
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------    
        
           #deo_mesta     
        $this->add(array(
            'name'=>'deo_mesta',
            'requeried'=>true,
          //  'allow_empty' =>$this->deo_mesta, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje deo mesta je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 2092,
                       'messages'=>[
                      \Zend\Validator\Between::NOT_BETWEEN=>'Izaberite deo mesta!'
                      ]
                  ),
              ),
                 
            ),
                 ));
   #---------------------------------------------------------------------    
        
             
        
 #lokacija    
        $this->add(array(
            'name'=>'lokacija',
            'requeried'=>true,
         //   'allow_empty'=>$this->lokacija,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje lokacija je obavezno!' 
                            ],
                        ],
                     ], 
                 [
                  'name' => 'Between',
                  'options' => [
                      'min' => 10000,
                      'max' => 12402,
                       'messages'=>[
                      \Zend\Validator\Between::NOT_BETWEEN=>'Izaberite lokaciju!'
                      ]
                      ]                   
                 ],
            ),
                 ));
   #---------------------------------------------------------------------     
        
        
   #ulica   
        $this->add(array(
            'name'=>'ulica',
        'required'=> false,
        'allow_empty' => true,
            
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
        
        
        
             
 #kvadratura    
        $this->add(array(
            'name'=>'kvadratura',
            'requeried'=>true,
           // 'allow_empty'=>$this->kvadratura,
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje kvadratura je obavezno!' 
                            ],
                        ],
                     ], 
                 [
                      'name'=>'Zend\Validator\Digits',
                      'break_chain_on_failure' => true,
                      'options'=>[
                          'messages'=>[
                              \Zend\Validator\Digits::NOT_DIGITS =>"Unesite samo brojeve, bez zareza!",
                              \Zend\Validator\Digits::STRING_EMPTY => 'Unesite samo brojeve, bez zareza!',
                              \Zend\Validator\Digits::INVALID => "Unesite samo brojeve, bez zareza!" ,
                          ]
                      ]
                    ],
                   
            ),
                 ));
   #---------------------------------------------------------------------    
        
        
#cena  
        $this->add(array(
            'name'=>'cena',
            'requeried'=>true,
          //  'allow_empty'=>$this->cena,
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje cena je obavezno!' 
                            ],
                        ],
                     ], 
                [
                      'name'=>'Zend\Validator\Digits',
                      'break_chain_on_failure' => true,
                      'options'=>[
                          'messages'=>[
                              \Zend\Validator\Digits::NOT_DIGITS =>"Unesite samo brojeve, bez zareza!",
                              \Zend\Validator\Digits::STRING_EMPTY => 'Unesite samo brojeve, bez zareza!',
                              \Zend\Validator\Digits::INVALID => "Unesite samo brojeve, bez zareza!" ,
                          ]
                      ]
                    ],
                   
            ),
                 ));
   #---------------------------------------------------------------------  
        
               
 #broj_soba   
        $this->add(array(
            'name'=>'broj_soba',
            'requeried'=>true,
          //  'allow_empty'=>$this->broj_soba,            
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje broj soba je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' =>5,
                       'messages'=>[
                      \Zend\Validator\Between::NOT_BETWEEN=>'Izaberite broj soba!'
                      ]                      
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------   
        
                   
 #prodaja_izdavanje  
        $this->add(array(
            'name'=>'prodaja_izdavanje',
            'requeried'=>true,
          //  'allow_empty'=>  $this->prodaja_izdavanje,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje prodaja izdavanje je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 2,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------       
        
        
        
        
        
        /*
         * 
         * SELIDBE
         **************************************************************
        *****/
 #selidbe_cena  
        $this->add(array(
            'name'=>'selidbe-cena',
            'requeried'=>true,
            'allow_empty'=>true,
               'validators' => array(   
                [
                      'name'=>'Zend\Validator\Digits',
                      'break_chain_on_failure' => true,
                      'options'=>[
                          'messages'=>[
                              \Zend\Validator\Digits::NOT_DIGITS =>"Unesite samo brojeve, bez zareza i razmaka!",
                              \Zend\Validator\Digits::STRING_EMPTY => 'Unesite samo brojeve, bez zareza i razmaka!',
                              \Zend\Validator\Digits::INVALID => "Unesite samo brojeve, bez zareza i razmaka!" ,
                          ]
                      ]
                    ],
                     [
                       'name'       =>'Zend\Validator\GreaterThan',
                       'break_chain_on_failure' => true,
                       'options'        =>[
                           'min'=>1,
                           'inclusive'=>true,
                            'messages'=>[
                                \Zend\Validator\GreaterThan::NOT_GREATER=>"Vrednost mora biti veca od nule!",
                                \Zend\Validator\GreaterThan::NOT_GREATER_INCLUSIVE=>"Vrednost mora biti veca od nule!",
                            ]
                           
                       ]
                       
                   ],
                   
            ),
                 ));
   #---------------------------------------------------------------------         
                        
        
         #selidbe_cena_dogovor 
        $this->add(array(
            'name'=>'selidbe_cena_dogovor',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 0,
                      'max' => 1,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------   
        /*
#selidbe_valuta
        $this->add(array(
            'name'=>'selidbe_valuta',
            'requeried'=>true,
            'allow_empty'=>TRUE,
           'validators' => array(
              array( 'name' => 'OstaviOglasForm\Models\ValidatorValuta',#custom validator valuta
                 ),
            ),
                 ));*/
   #---------------------------------------------------------------------  
        
        
           /*
         * 
         * END SELIDBE
         *******************************************************************/     
        
     
      
        
        /*
         *
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * D R U G I   T U B
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         */
        
        
        
        
        
        #naslov    
        $this->add(array(
            'name'=>'naslov',
            'requeried'=>true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
            'validators'=>array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Naslov je obavezno!' 
                            ],
                        ],
                     ], 
             array(
               'name'=>'Zend\Validator\StringLength',
                 'options'=>array(
                     'max'=>50
                 ),
             ),
           
            )
            
        ));
   #---------------------------------------------------------------------    
        
        
 #opis   
        $this->add(array(
            'name'=>'opis',
            'requeried'=>true,
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
            'validators'=>array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje opis je obavezno!' 
                            ],
                        ],
                     ], 
             array(
               'name'=>'Zend\Validator\StringLength',
                 'break_chain_on_failure' => true,
                 'options'=>array(
                     'max'=>200,
                     'messages'=>[
                     \Zend\Validator\StringLength::INVALID=>'Polje opis morze da sadrzi najvise 200 karaktera.',
                     \Zend\Validator\StringLength::TOO_LONG=>'Polje opis morze da sadrzi najvise 200 karaktera.'
                     ]
                 ),
             ),
           
            )
            
        ));
   #---------------------------------------------------------------------     
        
        
        
  #broj_soba   
        $this->add(array(
            'name'=>'ha_ar',
            'requeried'=>true,
           // 'allow_empty'=>  $this->ha_ar,            
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje jedinica mere je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' =>2,
                             'messages' => [
                                 \Zend\Validator\Between::NOT_BETWEEN => 'Nije izmedju' 
                            ],                      
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
        
        
        
        
        
        
        
        
        
        
    /*
     * 
     * 
     * 
     * dodatni podatci
     * 
     * 
     * 
     * 
     */   
#oglasivac_nekretnine
        $this->add(array(
            'name'=>'oglasivac_nekretnine',
            'requeried'=>true,
           // 'allow_empty'=>$this->oglasivac_nekretnine,
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje oglasivac je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 4,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
#grejanje
        $this->add(array(
            'name'=>'grejanje',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 8,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
#tip_objekta
        $this->add(array(
            'name'=>'tip_objekta',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 4,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------    

#opis_objekta
        $this->add(array(
            'name'=>'opis_objekta',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 6,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
#namestenost
        $this->add(array(
            'name'=>'namestenost',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 3,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------       

        
        
 #nacin_placanja       
    $this->add(array(
            'name'=>'nacin_placanja',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 4,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------      
        
#ostalo_plac       
    $this->add(array(
            'name'=>'ostalo_plac',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 3,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------  
    
    
#vrsta_plac      
    $this->add(array(
            'name'=>'vrsta_plac',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 5,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------  
    
    
    #cena  
        $this->add(array(
            'name'=>'plac_povrsina',
            'requeried'=>true,
          //  'allow_empty'=>$this->cena,
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje Povrsina je obavezno!' 
                            ],
                        ],
                     ], 
                    [
                      'name'=>'Zend\Validator\Digits',
                      'break_chain_on_failure' => true,
                      'options'=>[
                          'messages'=>[
                              \Zend\Validator\Digits::NOT_DIGITS =>"Unesite samo brojeve, bez zareza!",
                              \Zend\Validator\Digits::STRING_EMPTY => 'Unesite samo brojeve, bez zareza!',
                              \Zend\Validator\Digits::INVALID => "Unesite samo brojeve, bez zareza!" ,
                          ]
                      ]
                    ],
                   
            ),
                 ));
   #---------------------------------------------------------------------  
        
    
    
    
   #vrsta_objekta    (samo za kuce) 
    $this->add(array(
            'name'=>'vrsta_kuce',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 7,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------  
    
      
#kuce_sparatnost   (samo za kuce) 
    $this->add(array(
            'name'=>'kuce_sparatnost',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 2,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------    
    
    
    
    
 #sprat
    $this->add(array(
            'name'=>'sprat',
            'requeried'=>true,
            'allow_empty' => true, 
             'validators' => array(
                                  [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje sprat je obavezno!' 
                            ],
                        ],
                     ], 
               array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 30,
                  ),
              ),
              ),
            ));
   #---------------------------------------------------------------------  
    
     
             
        
#ukupno_spratova
    $this->add(array(
            'name'=>'ukupno_spratova',
            'requeried'=>true,
            'allow_empty' => true, 
            'filters'  => array(
                       array('name' => 'Int'),
                    ),
               'validators' => array(
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 30,
                  ),
              ),
            ),
                 ));
   #--------------------------------------------------------------------- 
    
    
    
    
    
    
    
    
       #ulica   
        $this->add(array(
            'name'=>'kp_ime',
        'required'=> false,
        'allow_empty' => true,
            
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------  
        
        
           #ulica   
        $this->add(array(
            'name'=>'kp_mesto',
        'required'=> false,
        'allow_empty' => true,
            
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------  
        
        
        
        
           #ulica   
        $this->add(array(
            'name'=>'kp_ulica',
        'required'=> false,
        'allow_empty' => true,
            
            'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
               'validators' => array(
              array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '100',
                                ),
                            ),
            ),
                 ));
   #---------------------------------------------------------------------  
    
    
    
    
    
  #telefon-------------------------------------         
        $this->add(array(
            'name'=>'kontakt_telefon',
            'requeried'=>true,
            'allow_empty'=>FALSE,
            
               'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d\-\/]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Polje telefon prima samo brojeve!'
                     ))),
                   
            ),
                 ));
   #--------------------------------------------------------------------- 
        
      #telefon2-------------------------------------         
        $this->add(array(
            'name'=>'kontakt_telefon2',
            'requeried'=>true,
            'allow_empty'=>TRUE,
               'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d\-\/]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Polje telefon prima samo brojeve!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------     
        
        
   $this->add(array(
            'name'=>'email',
            'requeried'=>FALSE,
            'allow_empty'=>TRUE,
            'validators'=>array(
                   [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje email je obavezno!' 
                            ],
                        ],
                     ], 
                array(
                    'name'=>'EmailAddress',
                    'options'=>array(
                        'messages'=>array(
                            \Zend\Validator\EmailAddress::INVALID_FORMAT=>
                'Email adresa nije validna!'
                        )
                    )
                ),
            )
        ));
        
       
        
        
        
        /*
         * 
         * 
         * Google maps lat,lng
         * 
         * 
         * 
         */
         $this->add(array(
            'name'=>'lat',
            'requeried'=>true,
            'allow_empty'=>true,
               'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Latituda nije u ispravnom formatu!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------  
        
              $this->add(array(
            'name'=>'lng',
            'requeried'=>true,
            'allow_empty'=>true,
               'validators' => array(
                 array(
                  'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Longituda nije u ispravnom formatu!'
                     ))),
                   
            ),
                 ));
   #---------------------------------------------------------------------    
        
        
        
        
        
        
        
        
        
        
    
    /*
     * 
     * 
     * SLIKE
     */
    
    
               
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_1',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
                   
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_2',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
        
                    
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_3',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
                       
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_4',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
                       
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_5',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
                       
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_6',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
                       
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_7',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
                       
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_8',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
                       
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_9',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
                       
   #------------------------------------------------ 
     $this->add(array(
         'name'=>'slika_10',
         'required'=> true,
         'allow_empty' => true,
         'filters'  => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                       ),
         'validators' => array(
              array(
                     'name' => 'StringLength',
                     'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => '0',
                                    'max' => '300',
                                  ),
              ),
         ),
     ));
   #---------------------------------------------------------------------      
        
        
           
    /*
     * 
     * END SLIKE
     * 
     * 
     */
   
        
        //'kuhinja','pegla','fen','komp','zivotinje','masina_za_ves','televizor'
    
         $chebx_arr=[
           'video_nadzor','za_nepusace','za_pusace', 'zamena','uknjizen','odmah_useljiv','depozit','depozit',
              'klima'  ,'kablovska','internet','lift','podrum','topla_voda','telefon','interfon','topla_voda','graza','parking','terasa',
                'kuhinja','pegla','fen','komp','zivotinje','masina_za_ves','televizor'
                
                
       ];
       
       
        for($i=0;$i<count($chebx_arr);$i++){
            $this->add([
             'name'=>$chebx_arr[$i],
             'requeried'=>true,
             'allow_empty' => true, 
             'filters'  => [['name' => 'Int']],
             'validators' => [[
                  'name' => 'Between',
                  'options' => [
                      'min' => 0,
                      'max' => 1,
               ]]]
            ]);
        }
        
        
        
                         
 #di_tip  
        $this->add(array(
            'name'=>'di_tip',
            'requeried'=>true,
            'allow_empty'=>TRUE,
            'filters'  => [['name' => 'Int']],
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje prodaja izdavanje je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 5,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------       
             $this->add(array(
            'name'=>'broj_osoba',
            'requeried'=>true,
            'allow_empty'=>TRUE,
            'filters'  => [['name' => 'Int']],
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje prodaja izdavanje je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 10,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------          
             $this->add(array(
            'name'=>'broj_kreveta',
            'requeried'=>true,
            'allow_empty'=>TRUE,
            'filters'  => [['name' => 'Int']],
               'validators' => array(                    [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Polje prodaja izdavanje je obavezno!' 
                            ],
                        ],
                     ], 
              array(
                  'name' => 'Between',
                  'options' => array(
                      'min' => 1,
                      'max' => 10,
                  ),
              ),
            ),
                 ));
   #---------------------------------------------------------------------
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}






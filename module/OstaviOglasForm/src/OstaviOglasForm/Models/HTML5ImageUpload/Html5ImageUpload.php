<?php

namespace OstaviOglasForm\Models\HTML5ImageUpload;

/**
 * Description of Html5ImageUpload
 *
 * @author aleksa
 */

use Zend\Filter\File\RenameUpload;
use Zend\Validator\File\Extension;
//local
use Application\Models\Oglasi\Images\ResizeImage;




class Html5ImageUpload {
    
    
     const DIR = "/img/oglasi_img/";
     const ROOT = '/var/www/nadjinekretnine.com/public';

    protected $dir;
    protected $fileTypes = array('jpg', 'jpeg', 'png');
    protected $max_file_size = 10485760;
    protected $error = [];
    protected $file;
    protected $path;
    protected $new_image;
    protected $image_id;
    protected $layout_path;


    protected $img;
    public function __construct($img) {
      $this->img=$img;
       $this->file=$img['imageHTML5'];
       $this->image_id=$img['imageHTML5_id'];
     $this->path=  self::ROOT.self::DIR. date('Y-m-d') . '/';
     $this->layout_path=  self::DIR.date('Y-m-d').'/';
     if ($this->validate_file()) {
         $rename_upload=$this->rename_upload();
         $this->modify_size($rename_upload);
     }
      
    }
    
    
    
    protected function validate_file(){
        
        if (!$this->validate_ext()) {
            $this->error['extesion']='extension is not valide';
        }if (!$this->validate_size()) {
            $this->error['size']='size is not valide';
        }if (!$this->check_error_num()) {
            $this->error['extesion']='error number error: '.$this->file['error'];
        }
        
        
        if (count($this->error)>0) {
            return false;
        }
        return true;
        
    }


    public function return_ajax(){
        if (count($this->error)>0) {
            return json_encode($this->error);
        }
            return json_encode(['img'=>  $this->layout_path.$this->new_image,'id'=>  $this->image_id]);   
    }

    




    /**
     * 
     * @return array image data
     * 
     * rename i upload image to $this->path
     */
    protected function rename_upload() {
             if (!file_exists($this->path)) {
                 mkdir($this->path, 0755);
            }
            $filter = new RenameUpload([
                 'target' => $this->path,
                 'use_upload_extension' => TRUE,
                 "randomize" => true, 
            ]);
                  return $filter->filter($this->file);
     }

    
    
    
    /**
     * 
     * @param type $tmp_name
     * @return boolean
     * 
     * MENJA  velicinu slike i cuva sliku sa novim dimenzijama
     * 
     * 
     */
    protected function modify_size($tmp_name){
          $tmp_name=$tmp_name['tmp_name'];
          $last=  strrpos($tmp_name, '/');
          $img_name=substr($tmp_name, $last+1);
          $this->new_image=$img_name;
    
       //validator za width
        $validator = new \Zend\Validator\File\ImageSize(['maxWidth' => 870]);
        if (!$validator->isValid($tmp_name)) {
             $resize = new ResizeImage($tmp_name);
             $resize->resizeTo(870, 540, 'maxWidth');
             $resize->saveImage($tmp_name);
         }  
        
        //validator za height
        $validatorh = new \Zend\Validator\File\ImageSize(['maxHeight' => 540]);
        if (!$validatorh->isValid($tmp_name)) {
             $resize = new ResizeImage($tmp_name);
             $resize->resizeTo(870, 540, 'maxHeight');
             $resize->saveImage($tmp_name);
         }  
         //za small images u listi oglasa
         $validator1 = new \Zend\Validator\File\ImageSize([ 'maxWidth' => 300, 'maxHeight' => 200]);
         if (!$validator1->isValid($tmp_name)) {
             $resize1 = new ResizeImage($tmp_name);
             $resize1->resizeTo(300, 200, 'exact');
             $resize1->saveImage($this->path.'small_' .$img_name);
         }  
         return true;
    }

    





    protected function check_error_num() {
         if ($this->file['error'] == 0) {return TRUE;}
         return FALSE;
    }

    
    
    
    
        protected function validate_ext() {
        $validator = new Extension(['jpg','jpeg','png',"JPG",'JPEG','PNG']);
        if ($validator->isValid($this->file)) {
             return true;
        } 
   
        return false;
    }

    protected function validate_size() {
        $validator = new \Zend\Validator\File\Size(['max' => '10MB']);
        if ($validator->isValid($this->file)) {
            return true;
        }
        return FALSE;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

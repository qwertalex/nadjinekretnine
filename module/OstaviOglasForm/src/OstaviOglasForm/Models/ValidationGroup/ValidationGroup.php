<?php

namespace OstaviOglasForm\Models\ValidationGroup;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of ValidationGroup
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class ValidationGroup {
    
    protected $data;
    
    public function __construct($data) {
        $this->data=$data;
    }
    protected $dodatno=[
      'depozit','odmah_useljiv','uknjizen','zamena','za_pusace','za_nepusace','klima','kablovska','internet','lift','podrum',
        'topla_voda','telefon','interfon','graza','parking','terasa'
    ];
    protected $moust_used=[
        'prodaja_izdavanje','mesta_srbija','deo_mesta','lokacija','ulica','cena','kvadratura',
    ];
    
    protected $slike=[
        'slika_1','slika_2','slika_3'
    ];
    protected $izdavanje_dodatno=[
      'kuhinja','pegla','fen','komp','zivotinje','masina_za_ves','televizor'  
    ];










    public function get_group(){
       $arr=['kategorija','naslov','opis','oglasivac_nekretnine'];
       if (!isset($this->data['kategorija'])) {
           return [];
       }
       switch((int)$this->data['kategorija']):
           case 1:array_push($arr,'broj_soba','grejanje','tip_objekta','opis_objekta','sprat');
                $arr=array_merge($arr,$this->moust_used,$this->dodatno);
               break;
           case 2:array_push($arr,'grejanje','sprat');
                $arr=array_merge($arr,$this->moust_used,$this->dodatno);
               break;
           case 3:array_push($arr,'grejanje','kuce_sparatnost','vrsta_kuce','broj_soba','tip_objekta');
                $arr=array_merge($arr,$this->moust_used,$this->dodatno);break;
           case 4://array_push($arr,'tip_objekta');
                $arr=array_merge($arr,$this->moust_used);
            break;
           case 5:array_push($arr,'plac_povrsina','vrsta_plac',
                   'prodaja_izdavanje','mesta_srbija','deo_mesta','lokacija','ulica','cena');
                $arr=array_merge($arr,$this->dodatno); break;
            
            
           //poslovni prostori
           case 6:case 7:array_push($arr,'grejanje','tip_objekta','opis_objekta','sprat');
                $arr=array_merge($arr,$this->moust_used,$this->dodatno);
                break;
           case 8:array_push($arr,'sprat');
                $arr=array_merge($arr,$this->moust_used,$this->dodatno);
               break;
           case 9:
               $arr=array_merge($arr,$this->moust_used,$this->dodatno);
                break;
           //zemljista
           case 10:case 11:
               $arr=array_merge($arr,$this->moust_used,$this->dodatno);
            break;
           //turisticki 
        case 12:array_push($arr,'broj_soba','grejanje','tip_objekta','opis_objekta');
                $arr=array_merge($arr,$this->moust_used,$this->dodatno);
                 break;
           case 13:
               array_push($arr,'broj_soba','grejanje','tip_objekta','opis_objekta');
                $arr=array_merge($arr,$this->moust_used,$this->dodatno);
               break;
           //selidbe
           case 14:
               unset($arr[3]);
               array_push($arr,'mesta_srbija','selidbe-cena','selidbe_cena_dogovor');
               break;
           case 15:
               array_push($arr,'di_tip','broj_osoba','broj_kreveta');
            $arr=array_merge($arr,$this->moust_used,$this->dodatno);
               break;
       endswitch;
       
       
if (isset($this->data['deo_mesta'])&&$this->data['deo_mesta']==2091||$this->data['deo_mesta']==2092) {
    if(($key = array_search('lokacija',$arr)) !== false) {
    unset($arr[$key]);
}
       }
       
       if (isset($this->data['prodaja_izdavanje'])&&$this->data['prodaja_izdavanje']==2) {
           $kat=(int)$this->data['kategorija'];
           if (!in_array($kat, [4,5,8])) {
              $arr[]='namestenost';
          }
          $arr=  array_merge($arr,  $this->izdavanje_dodatno);
           
           
       }
      
       
   //    var_dump($arr);
       return $arr;
       
   }
    
    
    
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

<?php

namespace OstaviOglasForm\Models;


use Zend\Validator\AbstractValidator;
/**
 * Description of ValidatroTipoglasa
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class ValidatorTipoglasa extends AbstractValidator
{
   
    const TIPOGLASA="tip oglasa";
    protected $messageTemplates=[
      self::TIPOGLASA=>"Izaberite tip oglasa!",  
    ];
    
    
    public function __construct(array $options=[]) {
        parent::__construct($options);
      }
    
 
    public function isValid($value) {
          
        switch ($value) {
              case 'standardni_oglas':
              case "istaknuti_oglas":
              case "top_oglas":         
              case "premium_oglas":
                  return TRUE;
             default:return false;
        }
          
          
          
          
          
    }
    
    
    
    
    
    
    
}







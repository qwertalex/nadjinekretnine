<?php

namespace OstaviOglasForm\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


#local include
use OstaviOglasForm\Models\OstaviOglasForm,
//OstaviOglasForm\Models\DbInterpreter\Interpreter,
Application\Models\LokacijaSr\Lokacija,
OstaviOglasForm\Models\UploadImg,
OstaviOglasForm\Models\ValidationGroup\ValidationGroup,
        OstaviOglasForm\Models\UploadFile;
  use OstaviOglasForm\Models\HTML5ImageUpload\Html5ImageUpload as Html5;
class IndexController extends AbstractActionController {

    
 

  
    public function indexAction() {
       \apc_clear_cache();
      // var_dump($_FILES);
        //----------------------------------------------------------------------
       #load google map and js--------------------------------------------------
       $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');   
       $renderer->headLink()->appendStylesheet('/libraries/select2/select2.css');
       $renderer->headScript()->appendFile( '/libraries/select2/select2.min.js');
       $renderer->headLink()->appendStylesheet('/libraries/icheck/flat/blue.css');
       $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
              
       $renderer->headScript()->appendFile( '/libraries/uplodify/jquery.uploadify.min.js');
       $renderer->headLink()->appendStylesheet('/libraries/uplodify/uploadify.css');
       //$renderer->headScript()->appendFile( '/js/uplodify.js');
       $renderer->headScript()->appendFile( '/js/ostavi-oglas.js');
       $renderer->headScript()->appendFile( '/libraries/js-bar/js-bar.js');
   #----------------------------------------------------------------------------------------------------- 
             $form= new OstaviOglasForm(); 
             $uploadImg= new UploadFile();         
             $User=$this->getServiceLocator()->get('User');
             $Oglasi=$this->getServiceLocator()->get('Oglasi');
             $update=false;
             $request = $this->getRequest();
             $is_update=false;
             
             //prvo deklaracija pa tek onda postavljanje oglasa
             if ($User->user_type()==2&&!$User->get_pravna_lica_data()) {
                  $this->redirect()->toUrl('http://nadjinekretnine.com/moj-profil/deklaracija?user_not_valid=true');
             }
             
             
 /*
  * 
  * 
  * 
  * 
  * 
  * 
  * 
  * 
  */
  
                // za update oglasa
            if ($this->params()->fromRoute('ad_id')) { 
                $update_set_dat=$Oglasi->prepare_oglas_update($this->params()->fromRoute('ad_id'));
                if ($update_set_dat) {
                  $form->setData($update_set_dat);
                  $is_update=true;  
                }
               
            }
  
   /*
    * 
    * 
    * 
    * 
    * 
    * 
    * 
    * 
    * 
    * 
    */         
   if (!$is_update) {
       

            $user_data=$User->get_by_id();
         //  var_dump($user_data);
            $default_dodatni_podatci=[
                'kontakt_telefon'       =>$user_data['telefon_oglasivac'],
                'kontakt_telefon2'      =>$user_data['mobilni_oglasivac'],
                'email'                 =>$user_data['email_nk'],
                'kp_ime'                =>$user_data['ime_oglasivac'],
                'kp_mesto'              =>$user_data['mesto_oglasivac'],
                'kp_ulica'              =>$user_data['ulica_oglasivac']
            ];
            
            $form->setData($default_dodatni_podatci);
       }    
       
       
       
       
    /*
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */   
       
             if ($request->isPost()) {
                $post = $request->getPost()->toArray();
         
            //   var_dump($post);
                
            $form= new OstaviOglasForm();
            $Create_validation_group=new ValidationGroup($post);
            // var_dump($Create_validation_group->get_group());
            $validation_group=$Create_validation_group->get_group();
            $add_val_group=[
              'lat','lng',  'kontakt_telefon','kontakt_telefon2','kp_ime','kp_mesto','kp_ulica',
           'slika_1','slika_2','slika_3','slika_4','slika_5','slika_6','slika_7','slika_8','slika_9','slika_10',
            ];
            $validation_group=array_merge($validation_group,$add_val_group);
          call_user_func([$form,'setValidationGroup'],$validation_group);
            $form->setData($post);//var_dump($post);
           if ($form->isValid($post)) {
                 $post=$form->getData();
               
           $interpreter=$this->getServiceLocator()->get('OstaviOglasInterpreter');
           $interpreter->set_data($post);
           $db= $this->getServiceLocator()->get('DB_insert_oglas');    
           
           if ($is_update) {
             // var_dump($interpreter->get_data());
               $db->update_data($interpreter->get_data(),$this->params()->fromRoute('ad_id'));
            $this->redirect()->toUrl('http://nadjinekretnine.com/moj-profil/moji-oglasi');
           }else{
             $oglas_id=$db->insert_data($interpreter->get_data());
             $this->redirect()->toRoute('mojprofil', array(
                    'controller' => 'mojiOglasi',
                    'action' =>  'moji-oglasi',
                    'strana' =>'1',
                    'po-strani'=>'10',
                    'nov-oglas'=>$oglas_id
                ));  
           }
           
           
           
           
            
           }else{
       //   var_dump($form->getMessages());
           }
       
            
           
            }

 $viewModel=new ViewModel([
     'form'=>$form, 
     'update_for_js'=>(isset($update_set_dat)?['deo_mesta'=>$update_set_dat['deo_mesta'],'lokacija'=>$update_set_dat['lokacija']]:false),
     'update'=>$is_update
 ]);
  
  
  
  $mapa_helper=new ViewModel();
          $mapa_helper->setTemplate("ostavi-oglas-mapa-helper");        
        $viewModel->addChild($mapa_helper,"mapa_helper"); 
  
   $slike_helper=new ViewModel([
       'file'       =>$uploadImg,
       
   ]);
          $slike_helper->setTemplate("ostavi-oglas-slike");        
        $viewModel->addChild($slike_helper,"slike_helper");  
  
  
  return $viewModel;
  
  
  
  
  
  
       
    
}  
    
    /**
     * 
     * @return ViewModel
     * @throws \Exception
     * 
     * 
     * NAMESTA VALIDATION GROUP ZA VALIDACIJU U DELOVIMA(prvi tub drugi, slike mapa, itd)
     * 
     * 
     * 
     * 
     */
    public function ajaxValidateFormAction(){
        $viewModel=new ViewModel();
        $viewModel->setTerminal(true);
        
        $request=$this->getRequest();
        if ($request->isPost()) {
            $post=$request->getPost()->toArray();
            $form= new OstaviOglasForm();
            $Create_validation_group=new ValidationGroup($post);
            // var_dump($Create_validation_group->get_group());
        
          call_user_func([$form,'setValidationGroup'],$Create_validation_group->get_group());
            $form->setData($post);//var_dump($post);
           if ($form->isValid($post)) {
                 $post=$form->getData();
                 //   var_dump('valide');
                    echo json_encode($post);
           }else{
              echo json_encode(['errors'=>$form->getMessages()]);
           }
       
            
        }else{
            throw new \Exception("Only Post is avaliable!");
        }
      
        
        return $viewModel;
    }
    




    public function delImageAction(){
        $request = $this->getRequest();
        if ($request->isPost()) {
                   $post = array_merge_recursive(
                        $request->getPost()->toArray(),
                        $request->getFiles()->toArray()
                    );
                    if (isset($post['token-delete'])) {
                        apc_dec('count');
                    }
                     $viewModel = new ViewModel();
                     $viewModel->setTerminal(TRUE);
                    return $viewModel;
        }else{
                  die('only post');
              }
        
        
    }

    



    /**
     * 
     * @return \Zend\View\Model\ViewModel
     * 
     * 
     * 
     * 
     * */
     
    public function uploadAction() {
       $request = $this->getRequest();
  
            if(extension_loaded('apc') && ini_get('apc.enabled'))
            {
            if (!apc_exists('count')) {apc_store('count',1);}
            else{ apc_inc('count');}
            }
              else{die('APC is not enabled!');}  
             // echo apc_fetch('count');   
            #--------------------------------------------   

              #if post is submited
              if ($request->isPost()) {
                   $post = array_merge_recursive(
                        $request->getPost()->toArray(),
                        $request->getFiles()->toArray()
                    );
                  
                   
                   
                  
                   
                 
                    if (apc_fetch('count')<=10) {
                        if (isset($post['imageHTML5'])) {
                            $HTML5= new Html5($post);
                           echo $HTML5->return_ajax();
                        }
                     }  else {
                         throw new Exception('APC LIMIT ERROR');    
                     }
                             
                                
                                
                                
                 $viewModel = new ViewModel();
                     $viewModel->setTerminal(TRUE);
                    return $viewModel;
              }
              else{
                  die('only post');
              }


  
  
        
    }
    
 
    public function ajaxGetLocationAction() {
               $viewModel = new ViewModel();
                 $viewModel->setTerminal(TRUE);
         $ajax=FALSE;
                 $request = $this->getRequest();
                 if ($request->isPost()){ 
                     $post=$request->getPost()->toArray();
                     if (isset($post['deo_mesta'])&&  \is_numeric($post['deo_mesta'])) {
                         $ajax=  Lokacija::get_deo_mesta($post['deo_mesta']);
                     }
                     elseif(isset($post['mesto'],$post['lokacija'])&&is_numeric($post['mesto'])&&is_numeric($post['lokacija'])){
                                                
                        $ajax= Lokacija::get_location($post['mesto'],$post['lokacija']);
                     }
                     else{
                         die("post[deo_mesta] is not set");
                     }
                              }
                 else{
                     die("post is required");
                 }
              return $viewModel->setVariables([
                          'ajax_result'=>  json_encode($ajax)
                     ]);
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
}



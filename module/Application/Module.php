<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\MvcEvent;
use Zend\Session\Config\SessionConfig;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Zend\Session\Validator\HttpUserAgent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface,
    Zend\ModuleManager\Feature\ConfigProviderInterface,
    Zend\ModuleManager\Feature\ViewHelperProviderInterface; 

//local
use Application\Models\User\RegisterNewUser as InsertDB;
use Application\Models\User\LoginUser;
use Application\Models\User\SessionManipulation;
use Application\Models\User\UserDataManipulation;
use Application\Models\User\User;
use Application\Models\Oglasi\PlAccounts;
use Application\Models\Oglasi\Render\RenderView;
use Application\Models\AssetsVersion\AutoVersion as Ver;

class Module  implements
    AutoloaderProviderInterface, 
    ConfigProviderInterface, 
    ViewHelperProviderInterface 
{
    
    
 
    protected $sm;
    protected $User;
    //route za registraciju i login koristi mehtod autorization
    protected $login_register_route='loginregister';
    //protected routs
    protected $protected_routes=[
        'mojprofil','ostavioglas',
    ];
    protected $curr_route;

    public function onBootstrap(MvcEvent $e){        

         $this->initSession(array(
             'name'=>"nadjinekretnine",
             'remember_me_seconds' => 43200,//12h
             'use_cookies' => true,
             'cookie_httponly' => true,
         ));
          $eventManager        = $e->getApplication()->getEventManager();
         $this->sm = $e->getApplication()->getServiceManager();        
         $this->User=$this->sm->get('User');
         //call function za  autorization
         $eventManager-> attach('route', array($this, 'autorization'));
         
         /*
          * 
          * NAMESTANJE ttitle i meta tagova
          */
         $eventManager-> attach(MvcEvent::EVENT_DISPATCH,function($e){
             $viewModel = $e->getViewModel();
             $setHeader=$e->getApplication()->getServiceManager()->get('setHeader');
             $setHeader::set_data( $e -> getRouteMatch()-> getParams());
             //----------------------------------------------------------
             
             $viewModel->setVariables([
                'title'=>$setHeader::get_title(),
                'meta_keywords'      =>$setHeader::get_meta_kewords(),
                'meta_desc'          =>$setHeader::get_meta_description(),
                'cache_main_css'     =>Ver::layout()['main_css'],
                'cache_custom_js'   =>Ver::layout()['custom_js'], 
             ]); 
         });
         #-----------------------------------------------------------  
         
         /*
          * 
          *
          * prikazuje poslednja tri oglasa iz baze i cache 
          * 
          * 
          */
          /*  $viewModel = $e->getViewModel();
          $viewModel_arr=[];
            

                    $cache = \Zend\Cache\StorageFactory::factory(array(
                       'adapter' => array(
                           'name' => 'filesystem',
                           'options' => array(
                               'cache_dir' => '/tmp',
                               'ttl' => 3600,
                           ),
                       ),
                       'plugins' => array(
                           'exception_handler' => array('throw_exceptions' => false),
                            'serializer'
                       ),
                   ));
                   $key    = 'zend-lastest-three-ads-fother-cache';
                   $viewModel_arr = $cache->getItem($key, $lastest);
                   if (!$lastest) {
                        $Oglasi= $e->getApplication()->getServiceManager()->get('Oglasi');
                        $poslednji_oglasi=$Oglasi->poslednji_oglasi(3);
                        if ($poslednji_oglasi) {
                        foreach($poslednji_oglasi as $k=>$v){
                            $poslednji_oglasi[$k]['mesto']=RenderView::get_mesto((int)$v['mesto']);
                            $poslednji_oglasi[$k]['cena_valuta']=RenderView::cena_valuta($v['cena'],$v['valuta']);
                            $poslednji_oglasi[$k]['slika_1']=RenderView::image($v['slika_1']);
                            $poslednji_oglasi[$k]['link']=RenderView::create_link($v['kategorija'], $v['id_oglasi_nk'], $v['naslov']);
                        }
                        //var_dump($poslednji_oglasi);
                               $viewModel_arr['POSLEDNJI_OGLASI']=$poslednji_oglasi;
                         
                        }
                       
                        $cache->setItem($key, $viewModel_arr);
                    }
              $viewModel->setVariables($viewModel_arr);   */
         //-------------------------------------------------------
        
     }

     
 
     
     
     
     /*
      * 
      * 
      * 
      * 
      * 
      * 
      */
public function initSession($config)
{
    $sessionConfig = new SessionConfig();
    $sessionConfig->setOptions($config);
    $sessionManager = new SessionManager($sessionConfig);
    if (isset($_POST['nadjinekretnine'])) {
  $sessionManager->setId($_POST['nadjinekretnine']); 
           }
     //user agent validation!
    $sessionManager->getValidatorChain()->attach('session.validate', array(new HttpUserAgent(), 'isValid'));
    $sessionManager->start();
    Container::setDefaultManager($sessionManager);
}


    
    
  /**
   * 
   * @param MvcEvent $e
   * @return void
   * 
   * OPIS:
   * Uzima route name i validira usera
   * ako je user validan(ako postoji sesija) ne moze da poseti route loginregister;
   * ako user nije validan ne moze da poseti vrednosti u protected $protected_route, redirecuje na route loginregister;
   * 
   * 
   */ 
     public function autorization(MvcEvent $e) {
         $route = $e -> getRouteMatch() -> getMatchedRouteName();
   
         if ($this->validate_user()) { 
                 $validation=true;
             if($route==$this->login_register_route){ 
                 $router= $e->getRouter();
                  $url= $router->assemble(array(), array('name' => 'mojprofil'));
                  $response = $e->getResponse();
                  $response->getHeaders()->addHeaderLine('Location', $url);
                  $response->setStatusCode(302); 
                  $response->sendHeaders();
             }                
         }
         else{
                 if (in_array($route, $this->protected_routes)) {
                         $router= $e->getRouter();
                         $url= $router->assemble(array(), array('name' => 'loginregister'));
                         $response = $e->getResponse();
                         $response->getHeaders()->addHeaderLine('Location', $url);
                         $response->setStatusCode(302);//var_dump($url);
                         $response->sendHeaders();
                 }  
             $validation=false;
         }   
       

                    $viewModel = $e->getViewModel();
                 $viewModel->setVariables(array(
                     'login_logout'=>  $this->User->render_html($validation)
                 ));
     }
    
    
/**
 * 
 * @return boolean | ako je validan user vraca true suprotno false;
 * OPIS:
 * Ako postoji session on proverava da li postoji kombinacija id i emaila u bazi 
 * ako postoji cookie validira ga i pravi session i vraca true
 * 
 */
protected function validate_user() {
         $user=$this->User;
         $data=  SessionManipulation::get_user_session();
         if ($data) {
             $id=isset($data['id'])?$data['id']:false;
             $username=isset($data['username'])?$data['username']:FALSE;
             if ($id&&$username&&$user->validate_user($id,$username)) {
                  return TRUE;
             }
          }
          if ($cookie=SessionManipulation::get_zapamti_me()) { 
              if ( $user->validate_cookie($cookie)) {
                  return TRUE;
              }
          }
         
         return false;
}


 


    
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
         );
    }
    
    
    
/*
 * this method add do manu things!
 * 
 * key invokables sadrzi key value madels. value je lokacija modela
 */
    public function getServiceConfig(){
        
        return array(
            'abstract_factories' => array(),
            'aliases' => array(),
            'factories' => array(
          'Register_new_user_insertDB' => function($serviceLocator) {
               $dbAdapter=$serviceLocator->get('Zend\Db\Adapter\Adapter');
               return new InsertDB($dbAdapter);
            },
            'Login_user' => function($serviceLocator) {
               $dbAdapter=$serviceLocator->get('Zend\Db\Adapter\Adapter');
               return new LoginUser($dbAdapter);
            }, 
            'UserDataManipulation' => function($serviceLocator) {
               $dbAdapter=$serviceLocator->get('Zend\Db\Adapter\Adapter');
               return new UserDataManipulation($dbAdapter);
            },  
          'User'=>function($serviceLocator) {
               $dbAdapter=$serviceLocator->get('Zend\Db\Adapter\Adapter');
                       $sess=SessionManipulation::get_user_session(); 
                       $id=$sess['id'];               
               return new User($dbAdapter,$id);
            },       
              'Oglasi' => function($serviceLocator) {
               $dbAdapter=$serviceLocator->get('Zend\Db\Adapter\Adapter');
                       $sess=SessionManipulation::get_user_session(); 
                       if (isset($sess['id'])) {
                           $id=$sess['id'];
                       }else{
                           $id=false;
                       }
                       
               return new Models\Oglasi\Oglasi($dbAdapter,$id);
            }, 
             'PlAccounts' => function($serviceLocator) {
               $dbAdapter=$serviceLocator->get('Zend\Db\Adapter\Adapter');
                       $sess=SessionManipulation::get_user_session(); 
                       $id=$sess['id'];
 
               return new PlAccounts($dbAdapter,$id);
            },  
        
            ),
        
   #add new models---------------------------------------------------------
            'invokables' => array(
               'LoginForm'=>'Application\Models\Form\LoginFormModels',
                'RegisterForm'=>'Application\Models\Form\RegisterFormModels',
                'FilterLoginForms'=>'Application\Models\Form\FilterLoginFormModels',
                 'BrziLinkovi'=>'Application\Helpers\Widgets\BrziLinkovi',
                 'change_email'=>'Application\Models\User\FormChangeEmail',
                'setHeader'     =>'Application\Models\Head\HtmlHeadHelpers'
                
        ), 
    #----------------------------------------------------------------------        
            
            'services' => array(),
            'shared' => array(),  
            
            
            
        );
        
        
        
        
    }
    
    
    
    
            public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'reklame' => function($sm) {
                  ///  $device=$sm->get("MobileDetect");
                    $helper = new \Application\Helpers\PrikaziReklame() ;
                    return $helper;
                }
            )
        );   
   }
    
    
    
    
    
    
    
    
}

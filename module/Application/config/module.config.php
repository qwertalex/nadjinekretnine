<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
  
    
       'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
             'Application\Controller\Form' => 'Application\Controller\FormController'
        ),
    ), 
    
    
    /*
     * DEFAULT CONTORLLER!
     */
    
        'router' => array(
   'routes' => array(
     'home' => array(
        'type'    => 'segment',
        'options' => array(
           'route'    => '/[:action][/:id]',
            // <---- url format module/action/id
           'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
              'id'     => '[0-9]+',
            ),
            'defaults' => array(
               'controller' => 'Application\Controller\Index',
                // <--- Defined as the module controller
               'action'     => 'index',
                // <---- Default action
            ),
        ),
     ),
       #-----------------------------------------------------------  
     
       
       /*
        * new controller for forms
        */
       
            'form' => array(
        'type'    => 'segment',
        'options' => array(
           'route'    => '/prijava-registracija[/:action]',
            // <---- url format module/action/id
           'constraints' => array(
              'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            ),
            'defaults' => array(
               'controller' => 'Application\Controller\Form',
                // <--- Defined as the module controller
               'action'     => 'login',
                // <---- Default action
            ),
        ),
     ),
    #-----------------------------------------------------------   
     ),
            
            
   
            
            
            
),
    
    
    
    
    
  /*
   * NE BRISATI!**************************************************
   */  
     'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
         
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    
    /*
     * ******************************************************
     */
    
    
    
    
    
    

    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'                => __DIR__ . '/../view/layout/layout.phtml',
            'layout/pretraga'              => __DIR__ . '/../view/layout/pretraga.phtml',
            'application/index/index'      => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'                    => __DIR__ . '/../view/error/404.phtml',
            'error/index'                  => __DIR__ . '/../view/error/index.phtml',
            'main_slider'                  => __DIR__ . '/../view/layout/helpers/sliders/main-slider.phtml',
            'premium_carousel'             =>__DIR__ . '/../view/layout/helpers/sliders/premium-carousel.phtml',
            'brzi_linkovi'             =>__DIR__ . '/../view/layout/helpers/brzi_linkovi/brzi-linkovi.phtml',
            'magazin'             =>__DIR__ . '/../view/layout/helpers/magazin/magazin.phtml',
            
            
            
            
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    
    
);

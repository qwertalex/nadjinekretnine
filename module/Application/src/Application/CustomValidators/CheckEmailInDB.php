<?php

namespace Application\CustomValidators;

use Zend\Validator\AbstractValidator;



class CheckEmailInDB extends AbstractValidator{
    

    
    
    
    
     protected $User;
    
    const NOTSPECIAL = 'NOTSPECIAL';
  //  protected $adapter;
    protected $messageTemplates = array(
        self::NOTSPECIAL => 'Korisnik sa ovom E-mail adresom vec postoji!',
    );
     
    public function __construct(array $options=[])
    {
        $this->User=$options['user_class'];
   
       
       parent::__construct([]);
    }
 
    public function isValid($value)
    { 
        
 
  if (!$this->User->get_by_email($value)) {
 
      return true;
  }
                 $this->error(self::NOTSPECIAL);
        return false;
        
   
        
    }
}

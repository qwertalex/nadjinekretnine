<?php
namespace Application\CustomValidators;

use Zend\Validator\AbstractValidator;

class Checkusername extends AbstractValidator {
  
    protected $User;
    const NOTSPECIAL = 'NOTSPECIAL'; 
    
    protected $messageTemplates = array(
        self::NOTSPECIAL => 'Korisnik sa ovim korisnickim imenom vec postoji!',
    );
     
    public function __construct(array $options=[]){
        $this->User=$options['user_class'];
        parent::__construct([]);
    }
 
    public function isValid($value){ 
      if ($this->User->check_username_availability($value)) {
                 return true;
        }
     $this->error(self::NOTSPECIAL);
      return false;
        
   
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
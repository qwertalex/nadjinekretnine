<?php
namespace Application\CustomValidators;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Regex;
use Zend\Validator\EmailAddress;


class UsernameOrPassword  extends AbstractValidator{
 
    // protected $User;
     const NOTSPECIAL = 'NOTSPECIAL';
  
    protected $messageTemplates = array(
        self::NOTSPECIAL => 'Korisnicko ime ili Email adresa nije validna!',
    );
     
    public function __construct(array $options=[]){
       //   $this->User=$options['user_class'];
          parent::__construct([]);
    }
 
    public function isValid($value){ 
         $username_validator = new Regex(['pattern'=>'/^[a-zA-Z\d]+$/']);
        $email_validator = new EmailAddress();
         if ($username_validator->isValid($value)) { //validate username
            return true;
         }elseif( $email_validator->isValid($value)){ //validate email
            return true;
         }
        $this->error(self::NOTSPECIAL);
         return false;
        
   
        
    }
    
    
    
    
    
    
    
}
<?php

namespace Application\Ajax;

use Zend\Db\Sql\Sql;
use Zend\Crypt\Password\Bcrypt;


class AjaxValidateUserLogin {
  
   
    protected $adapter;


    public function __construct($adapter) {
        $this->adapter=$adapter;
        
    }
    
    public function validate_user($email,$password) {
        $sql=new Sql($this->adapter);
        $select = $sql->select();
        $select->from('users_nk');
        $select->where(array('email_nk' => $email));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $arr_results = array_values(iterator_to_array($results));
        if ($results->count()==1) {
            
            
$bcrypt = new Bcrypt();
$pass_from_db=$arr_results[0]['password_nk'];

if ($bcrypt->verify($password, $pass_from_db)) {
   return true;
} else {
   return false;
}
        }       
return false;
        
    }
    
    
    
    
    
}




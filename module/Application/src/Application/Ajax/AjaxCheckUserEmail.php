<?php

namespace Application\Ajax;

use Zend\Db\Sql\Sql;



/**
 * return int
 * 
 * Provareava da li u bazi postoji email i vraca int
 * ako postoji vraca 1 u suprotnom vraca 0
 * 
 * 
 * 
 * 
 */
class AjaxCheckUserEmail {
   
    protected $adapter;
    public function __construct($adapter) {
        $this->adapter=$adapter;
    }
    
    public function check_email($email) {
      if(!filter_var($email, FILTER_VALIDATE_EMAIL))
  {
          return 'false';
  }
        $sql=new Sql($this->adapter);
        $select = $sql->select();
        $select->from('users_nk');
        $select->where(array('email_nk' => $email));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        //$results = array_values(iterator_to_array($results));
        return $results->count();
        
        
        
        
        
        
        
        
        
    }
    
    
    
}



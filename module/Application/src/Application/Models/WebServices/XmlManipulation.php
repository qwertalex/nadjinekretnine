<?php

namespace Application\Models\WebServices;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
/**
 * Description of XmlManipulation
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class XmlManipulation {
    
    protected $adapter;
    
    public function __construct($adapter) {
        $this->adapter=$adapter;
        
    }
    
    
    
    
    
    
    
      
     protected function insert_data($post) {
      // var_dump(count($post));
      foreach($post as $key_num=>$post_val):
          
    
        try{
             $this->adapter->getDriver()->getConnection()->beginTransaction();
             $sql = new Sql($this->adapter);
             $insert=$sql->insert('oglasi_nk');
             $insert->values($post_val['oglasi_nk']);
             $selectString = $sql->getSqlStringForSqlObject($insert);
             $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
             if ( $results->count() === 0 ) {die('Doslo je do greske, molimo vas probajte za nekoliko minuta!');}    
           
             unset($post_val['oglasi_nk']);
            $id= $this->adapter->getDriver()->getConnection()->getLastGeneratedValue(); 
           // var_dump($id);
         
    foreach ($post_val as $k=>$value) {
        $value['id_oglasi_nk']=$id; 
        $insert=$sql->insert($k);
       
        $insert->values($value);
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        if ( $results->count() === 0 ) {die('Doslo je do greske, molimo vas probajte za nekoliko minuta!');}
       }
       //commit transaction
      $this->adapter->getDriver()->getConnection()->commit();  
    //  var_dump($id);
    //  return $id;
        }
 catch (Exception $exc){
     //rollback transaction
     $this->adapter->getDriver()->getConnection()->rollback();
 }  
   endforeach;
 return false;
    }
    
    
    
    
    
      /**
     * 
     * @param type $data
     * 
     * 
     * 
     * 
     * 
     * 
     */
 protected function update_data($data){
  //   var_dump($data);
     
    $sql=new \Zend\Db\Sql\Sql($this->adapter);
     foreach($data as $k=>$v):
         $id=$v['oglasi_nk']['id_oglasi_nk'];
        // var_dump($id);
     
       try{
             $this->adapter->getDriver()->getConnection()->beginTransaction();
     
         foreach($v as $kk=>$vv):
            
          
              $update=$sql->update($kk);
            if (isset($vv['id_oglasi_nk'])) {
                unset($vv['id_oglasi_nk']);
            }
           
            $update->set($vv);
            $update->where([
                'id_oglasi_nk'  =>$id
            ]);
             $selectString = $sql->getSqlStringForSqlObject($update);
             $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
             
             endforeach;
           $this->adapter->getDriver()->getConnection()->commit(); 
            } catch (Exception $ex) {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
           
         
         
         
         
     endforeach;
    
     
     
 }
    
    
    
    
    
        protected function delete_data($oglas_id){
            if (!is_array($oglas_id)&&count($oglas_id)==0) {
                return false;
            }
          $sql=new \Zend\Db\Sql\Sql($this->adapter); 
            foreach($oglas_id as $v):
             $delete=$sql->delete('oglasi_nk');
        $delete->where(['id_oglasi_nk'=>$oglas_id]);
        $statement=$sql->prepareStatementForSqlObject($delete);
        $statement->execute();   
            endforeach;
        
        
        
        return true;
    }
    
    
    
    
    
}

<?php

namespace Application\Models\WebServices\NekretnineMojDom;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;


//local
use \Application\Models\WebServices\DownloadXml;
use Application\Models\LokacijaSr\Lokacija as Lok;
use Application\Models\Oglasi\Render\RenderView as Html;
/**
 * Description of NekretnineMojDom
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class NekretnineMojDom extends \Application\Models\WebServices\XmlManipulation{
    
     const DOWNLOAD_DIR="/var/www/nadjinekretnine.com/Xml/nekrentinemojdom/";
     const XML_FILE_STANOVI="stanovi.xml";
     const XML_FILE_KUCE="kuce.xml";
     const STANOVI_URL='http://www.nekretnine-mojdom.com/xml/stanovi.xml';
     const KUCE_URL="http://www.nekretnine-mojdom.com/xml/kuce.xml";
    const USER_ID=118;
     protected $adapter;

    
    public function __construct($adapter){
        $this->adapter=$adapter;
        $Xml=new DownloadXml();
       
        $download_stanovi=$Xml->download_page(self::STANOVI_URL);
        $download_kuce=$Xml->download_page(self::KUCE_URL);
       
        $Xml->save_file($download_stanovi, self::DOWNLOAD_DIR.self::XML_FILE_STANOVI);
        $Xml->save_file($download_kuce, self::DOWNLOAD_DIR.self::XML_FILE_KUCE);
       
     
      
        parent::__construct($adapter);
       $this->parse();
  
    }
    
    
    
    protected function parse(){
        
          $reader = new \Zend\Config\Reader\Xml();
          $stanovi=$reader->fromFile(self::DOWNLOAD_DIR.self::XML_FILE_STANOVI)['stan'];
          $kuce=$reader->fromFile(self::DOWNLOAD_DIR.self::XML_FILE_KUCE)['kuca'];
      //   var_dump($stanovi);
         //var_dump($kuce);
       
          $db_stanovi=[];
          $db_kuce=[];
          $get_db=  $this->get_db_moj_dom();
          $sifra_id=[];
               
               
           foreach($stanovi as $key=>$value):
             $sifra_id[]=$value['sifra'];
              if (in_array($value['sifra'], $get_db)) {
           
                  $db_stanovi['update'][]=$value;
                  continue;
              }
              
              $db_stanovi['insert'][]=$value;
           endforeach;
          
            foreach($kuce as $key=>$value):
             $sifra_id[]=$value['sifra'];
              if (in_array($value['sifra'], $get_db)) {
           
                  $db_kuce['update'][]=$value;
                  continue;
              }
              
              $db_kuce['insert'][]=$value;
           endforeach;
           
           
         $del=array_map(function($v){
              return $this->get_id_from_sifra($v);
          },array_diff($get_db,$sifra_id));
       
          $this->delete_data($del);
          
      
          if (isset($db_stanovi['insert'])) {
             $this->insert_stanovi($db_stanovi['insert']);
          }if (isset($db_stanovi['update'])) {
              $this->update_stanovi($db_stanovi['update']);
         }if (isset($db_kuce['insert'])) {
             $this->insert_kuce($db_kuce['insert']);
          }if (isset($db_kuce['update'])) {
              $this->update_kuce($db_kuce['update']);
        }
      }
    
      
      
    
      
      protected function insert_kuce($data){
          $this->insert_data($this->prepare_for_db($data, 3));
          
          
      }
      
       protected function update_kuce($data){
          $data=$this->prepare_for_db($data,3);
      
      foreach($data as $key=>$value):
          $data[$key]['oglasi_nk']['id_oglasi_nk']=$this->get_id_from_sifra($value['nekretnine_moj_dom']['id_moj_dom']);
      endforeach;
      
     $this->update_data($data);
          
          
      }
      
      
      
      /**
       * 
       * @param type $data
       * 
       * 
       * 
       * 
       * 
       * 
       */
    protected function update_stanovi($data){
      //  
      $data=$this->prepare_for_db($data,1);
      
      foreach($data as $key=>$value):
          $data[$key]['oglasi_nk']['id_oglasi_nk']=$this->get_id_from_sifra($value['nekretnine_moj_dom']['id_moj_dom']);
      endforeach;
      
     $this->update_data($data);
    }
    
    
    
    
    protected function prepare_for_db($data,$kategorija){
            $res=[];
        $strip_tags = new \Zend\Filter\StripTags();
      //  var_dump(1111);
        $num=0;
        foreach($data as $k=>$v):
           $lok=$this->get_lokacija_id($v['mesto'], $v['lokacija']);
            if (!$lok) {continue;} 
            $res[$k]['oglasi_nk']=[
                'id_users_nk'       =>self::USER_ID,
                'naslov'            =>$strip_tags->filter($v['naslov']),
                'kategorija'        =>$kategorija,
                'opis'              =>$strip_tags->filter($v['opis']),
                'oglasivac'         =>2,
                'prodaja_izdavanje' =>1,
                'datum_kreiranja_oglasa'=>$v['datum_azuriranja']." 02:00:12",
              ];
            
          $lok_exp=explode("-",$lok);
          $res[$k]['lokacija']=[
               'mesto'          =>$lok_exp[0],
               'deo_mesta'      =>$lok_exp[1],
               'lokacija'       =>$lok_exp[2],
               'mesto_deo_lokacija'=>$lok
           ];
            
           
            $res[$k]['vrsta_oglasa']=[
              'vrsta_oglasa'            =>1,
              'datum_postavljanja_oglasa'=>$v['datum_azuriranja']." 02:00:12",
                'datum_isteka'          =>date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s"). ' +1 months'))
             ];
             if (isset($v['slike']['slika'])&&is_array($v['slike'])) {
                 $res[$k]['slike']=  $this->slike($v['slike']['slika']);
             }
            
            $res[$k]['dodatno']=[
                'kvadratura'        =>(int)round($v['kvadratura']),
                'cena'              =>(int)round($v['cena']),
                'broj_soba'         =>(int)round($v['sobnost'])>5?5:round($v['sobnost']),
                'spratovi'          =>isset($v['sprat'])?$v['sprat']=="0"?NULL:$v['sprat']:NULL,
                'opis_objekta'      =>strtolower($v['zavrsen'])=='ne'?4:NULL,
                'grejanje'          =>  $this->grejanje($v['grejanje']),
                
                
            ];
            
            if (isset($v['lift'])&&strtolower($v['lift'])=='da') {
                $res[$k]['dodatni_podatci']=[
                  'lift'        =>1  
                ];
            }
            
            $res[$k]['nekretnine_moj_dom']=[
              'id_moj_dom'      =>$v['sifra']  
            ];
            
        endforeach;
        return $res;
        
    }
    
    
    
    
    
    
    public function insert_stanovi($data){
           $this->insert_data($this->prepare_for_db($data, 1));
               
    }
        
         protected function grejanje($str){
             if (!is_string($str)) {
                 return null;
             }
           switch(strtolower($str)):
               case 'cg':return 1;
               case 'et':return 2;
               case 'ta':return 3;
               case 'gs':return 4;
               case 'podno':return 5;
               case 'kaljeva pec':return 6;
               case 'norveski radiatori':return 7;
               case 'mermerni radiatori':return 8;
               default:return NULL;
             endswitch;
          }
             
            
    
    
    
    
    
    
    
    
    
    
    protected function slike($slike){
        $ret=[];
        $num=1;
        if (!is_array($slike)) {
           return ['slika_1'=>$slike];
        }
        foreach($slike as $v):
            if ($num>10) {break;}
           $ret['slika_'.$num]=$v;
           $num++;
        endforeach;
        return $ret;
    }
    
    
    protected function get_lokacija_id($mesto,$lokacija){
         $all_lok=Lok::$all_locations;
             
                foreach($all_lok as $k=>$v):
                    $all_lok[$k]=Html::to_utf8($v);
                endforeach;
        
        $search=preg_grep("/^.*{$mesto}.*({$lokacija}).*$/i",$all_lok);
        switch(count($search)):
            case 0:return false;
            case 1:return key($search);
            default:return key($search) ;
        endswitch;
        
        
        
    }
    
    
    
    
    protected function get_db_moj_dom(){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select("nekretnine_moj_dom");
        $select->columns(['id_moj_dom']);
        
        $statement=$sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
         $arr_results = array_values(iterator_to_array($results));
         $res=[];
         foreach($arr_results as $k=>$v):
             $res[]=$v['id_moj_dom'];
         endforeach;
         return $res;
        
        
    }
  
    protected function get_id_from_sifra($sifra){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select("nekretnine_moj_dom");
        $select->columns(['id_oglasi_nk']);
        $select->where([
           'id_moj_dom'     =>$sifra 
        ]);
        $statement=$sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
         return array_values(iterator_to_array($results))[0]['id_oglasi_nk'];
            
        
        
    }     
    
    
    

    
    
    
    
}

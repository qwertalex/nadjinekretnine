<?php
namespace Application\Models\Oglasi\Render;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Application\Models\LokacijaSr\Lokacija as Lok;
use Application\Models\Oglasi\Render\RenderView as Html;
/**
 * Description of RenderToDb:
 * --------------------------
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 *----------------------------
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class RenderToDb {
    
      public static function kategorija($str) {
             switch(strtolower($str)){
                     case 'stan': return 1;
                     case 'soba':return  2;
                     case 'kuca':return  3;
                     case 'garaza':return  4;
                     case 'plac':return  5;
                     case 'lokal':return  6;
                     case 'poslovni prostor':return 7 ;
                     case 'hala':return  8; 
                     case 'magacin':return 9 ;
                     case 'poljoprivredno zemljiste':return 10 ;
                     case 'gradjevinsko zemljiste':return  11;                    
                     case 'vikendica':return 12 ;
                     case 'apartmani':return  13; 
                     case 'selidbe':return  14;  
                     case 'dnevno-izdavanje':return 15;
             }
            return '';
     }
    
                     
            protected static function get_location_id($drzava,$grad,$opstina,$lokacija){
                if ($drzava!='SRBIJA') {return false;}
                
                $all_lok=Lok::$all_locations;
               $tt=false;
                foreach($all_lok as $k=>$v):
                    $all_lok[$k]=Html::to_utf8($v);
                endforeach;
                
               /*
                * 
                * Ovaj switch postoji za iz sale da bih pokupio sto vise oglasa prvi put
                * obavezno obrisati posle jer agencije pisu sve i svasta
                * 
                */ 
                switch($lokacija):
                    case "GOLF NASELJE":
                        $lokacija=str_replace(" NASELJE","",$lokacija);
                        break;
                    case "HRAM SV. SAVE":
                        $lokacija="hram svetog save";
                        break;
                    case "DJERAM":
                        $lokacija="deram pijaca";
                        break;
                    case "NAS.SAVA KOVACEVIC":
                        $lokacija="save kovacevica";
                        break;
                    case "HADZI POPOVAC":
                        $lokacija='HADZIPOPOVAC';
                        break;
                    case "MIRJEVO":
                        $lokacija='MIRiJEVO';
                        break;
                    case "GORNJI GRAD ZEMUN":
                        $lokacija="GORNJI GRAD";
                        break;
                    case "STARI MERKATOR-NBGD":
                        $lokacija="stari merkator";
                        break;
                    
                endswitch;
                 if (stripos($lokacija,'MILJAKOVAC')!==false) {
                    $lokacija="MILJAKOVAC";
                }
             //-----------------------------------------------------------   
                
               /* *
                *  
                * Menja rimske brojeve(ili veliko i(I)) zbog glupih agencija koje
                * npr:
                * Medakovic III menja u Medakovic 3
                *  
                */
                 if (\stripos($lokacija, "III")!==false) {
                    $lokacija=preg_replace('!\s+!', ' ', str_replace("III","3",$lokacija));
                 }elseif (\stripos($lokacija, "II")!==false) {
                     $lokacija=preg_replace('!\s+!', ' ', str_replace("II","2",$lokacija));
                 }else{
                     $lokacija=preg_replace('!\s+!', ' ',$lokacija);
                 }
                 //-----------------------------------------------------------------------------
                 
                 
                 
                 //search za regular exp. Jedini pravi search
                $search=preg_grep("/^.*{$grad}.*\s\/\s.*({$opstina}).*\s\/\s.*({$lokacija}).*$/i",$all_lok);
              //  var_dump($search);
                $count=count($search);
                if ($count===1) {
                    $full=array_keys($search)[0];
                    $exp=  \explode("-", array_keys($search)[0]);
                    
                   return [
                       'mesto'      =>$exp[0],
                       'deo_mesta'  =>$exp[1],
                       'lokacija'   =>$exp[2],
                       'full'       =>$full
                   ];
                }elseif($count>1){
                    
                    $search=preg_grep("/^.*{$grad}.*\s\/\s.*({$opstina}).*\s\/\s({$lokacija})$/i",$all_lok);
                    $count=count($search);
                    if ($count===1) {
                        $full=array_keys($search)[0];
                        $exp=  \explode("-", array_keys($search)[0]);
                        return [
                       'mesto'      =>$exp[0],
                       'deo_mesta'  =>$exp[1],
                       'lokacija'   =>$exp[2],
                       'full'       =>$full
                   ];
                    }
                }
                   
                
             
                
            }
    
    
    
    
            
            
         public static function grejanje($str){
             if (!is_string($str)) {
                 return null;
             }
           switch(strtolower($str)):
               case 'cg':return 1;
               case 'eg':return 2;
               case 'ta':return 3;
               case 'gas':return 4;
               case 'podno':return 5;
               case 'kaljeva pec':return 6;
               case 'norveski radiatori':case 'norveskiradiatori':return 7;
               case 'mermerni radiatori':case 'mermerniradiatori':return 8;
               default:return NULL;
             endswitch;
          }
             
            
            
            
            
            
            
            public static function e_stan_sprat($str){
                $arr=explode("/",$str);
                if (count($arr)===2&&is_numeric($arr[0])&&is_numeric($arr[1])) {
                    return ['sprat'=>$arr[0],'ukupno_spratovi'=>$arr[1]];
                }
                return ['sprat'=>NULL,'ukupno_spratovi'=>NULL];
                
            }
            
            
            
            public static function e_stan_broj_soba($str){
                if ($str&&is_numeric($str)) {
                    $str=round($str>5?5:$str);
                    return $str>0?$str:NULL;
                }
                return NULL;
            }
            
            
            
            public static function e_tip_objekta($novogradnja,$u_izgradnji){
                if ($novogradnja==="da") {
                    return 2;
                }if ($u_izgradnji==='da') {
                    return 4;
                 }
                return 1;
                
            }
            
            public static function e_opis_objekta($duplex){
                if ($duplex==='da') {
                    return 5;
                }
                return NULL;
             }
            
            
            public static function  e_prodaja_izdavanje($str){
                if (strtolower($str)==='p') {
                    return 1;
                }
                return 2;
            }
    
    
            public static function e_kontakt_podatci($ime,$tel,$foto,$email){
                $telefon=NULL;
                $dodatni=NULL;
               
                
                if ($tel) {
                   $tel=explode(",",$tel);
                    $telefon=Html::only_num($tel[0]); 
                    if (isset($tel[1])) {
                        $dodatni=Html::only_num($tel[1]);
                    }
                }
                
               $email_validator = new \Zend\Validator\EmailAddress();
               if (!$email_validator->isValid($email)) {
                   $email=NULL;
               }
                
                
                return [
                    'ime'       =>trim(Html::only_letters($ime,true)),
                    'telefon'   =>$telefon,
                    'dodatni_telefon'=>$dodatni,
                    'email'         =>$email,
                    'foto'          =>NULL
                  ];
                
                
               
            }
            
            
            
            public static function e_slike($slike){
                if (!$slike) {
                    return false;
                }
               $return=[];
               
                foreach($slike as $key=>$value):
                    foreach($value as $k=>$v):
                        if (!isset($v['url'])&&!isset($v['priority'])) {
                            continue;
                        }
                       $g="slika_".$v['priority'];
                       $return[$g]=$v['url'];
                    endforeach;
                endforeach;
                
                return $return;
              }
            
            
            
            
    
    
    
}

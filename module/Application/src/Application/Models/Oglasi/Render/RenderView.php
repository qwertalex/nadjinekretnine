<?php

namespace Application\Models\Oglasi\Render;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Application\Models\LokacijaSr\Lokacija as Lok,
 Application\Models\Url\UrlRewrite as Url;

/**
 * Description of RenderView
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class RenderView {
    
    const IMAGE_DIR="/img/oglasi_img/";
    public function __construct() {
        ;
    }
    
    /*
     * 
     * 
     * lokacija
     * 
     */
    
    public static function get_mesto($int) {
        return Lok::get_mesta_srbija_text($int);
    }
    public static function get_deo_mesta($mesto,$deo_mesta) {
        if ($lok=Lok::get_deo_mesta_srbija_text($mesto,$deo_mesta)) {
            return $lok;
        }
        return '';
    }    
    public static function get_lokacija($mesto,$deo_mesta,$lokacija) {
        if ($lok=Lok::get_lokacija_srbija_text($mesto,$deo_mesta,$lokacija)) {
            return $lok;
        }
        return '';
    }        
    
    public static function get_search_location($string){
        $lok=Lok::get_search_location();
         if (isset($lok[$string])) {
            return $lok[$string];
        }
        return '';       
    }
    
    public static function get_dm($deo){
        return Lok::get_dm($deo);
    }
    public static function get_lm($lok){
        return Lok::get_lm($lok);
    }
    
    
    
    
    /*
     * 
     * 
     * /lokacija
     * 
     * 
     * 
     * 
     */
    
    
    public static function vrsta_oglasa($num){
        switch((int)$num):
            case 1:return 'STANDARD';
            case 2:return "TOP";
            case 3:return "PREMIUM";
            case 0:return "neaktivan";
            default:return "iskljucen";
        endswitch;
    }
    
    
    public static function cena_valuta($cena,$valuta='eur') {
         if ($cena) {
             $valuta_="";
                 switch($valuta){
                    case 'eur':$valuta_='€';break;
                    case 'din':$valuta_='rsd';break;
                 }          
             return number_format( $cena, 0, '', '.') .' '.$valuta_;
         }
         return 'Dogovor';
     }
    
    
    public static function image($name,$small=true) {
        if (substr($name,0,4)=='http') {
            return $name;
        }elseif ($name) {
             if ($small) {
                $name_arr=explode("/",$name);
                if (file_exists('/home/nadjinek/public_html/public'.self::IMAGE_DIR.$name_arr[0].'/small_'.$name_arr[1])) {
                   return self::IMAGE_DIR.$name_arr[0].'/small_'.$name_arr[1];
                }}
            return self::IMAGE_DIR.$name;
        }
        return self::IMAGE_DIR.'nema-slike1.jpg';
     }
    
    public static function get_pro_izd($int){
        switch((int)$int):
            case 1:return 'Prodaja';
            case 2:return 'Izdavanje';
        endswitch;
    }
    public static function oglas_pro_izd($int){
         switch((int)$int):
            case 1:return 'Prodaju';
            case 2:return 'Izdavanje';
        endswitch;
    }
            
    
        public static function kategorija($int) {
             switch((int)$int){
                     case 1: return'Stan';
                     case 2:return  'Soba';
                     case 3:return  'Kuća';
                     case 4:return  'Garaža';
                     case 5:return  'Plac';
                     case 6:return  'Lokal';
                     case 7:return  'Poslovni prostor';
                     case 8:return  'Hala'; 
                     case 9:return  'Magacin';
                     case 10:return  'Poljoprivredno zemljiste';
                     case 11:return  'Gradjevinsko zemljiste';                    
                     case 12:return  'Vikendica';
                     case 13:return  'Apartmani'; 
                     case 14:return  'Selidbe';  
                     case 15:return  'Dnevno izdavanje';
             }
            return '';
     }
    
    
    
    public static function povrsina($kvadratura,$plac_povrsina=false,$plac_jedinica_mere=2){
        if ($kvadratura) {
            return $kvadratura.' m<sup>2</sup>';
        }
        if ($plac_jedinica_mere&&$plac_povrsina) {
            switch((int)$plac_jedinica_mere){
                case 1:return $plac_povrsina.' ha';
                case 2:return $plac_povrsina.' a';
            }
        }
        return false;
    }
    
    
    
        
    public static function oglasivac($int) {
       switch((int)$int){
            case 1:return'Vlasnik';
            case 2:return 'Agencija';
            case 3:return 'Investitor';
            case 4:return 'Banka';
          //  default:return false;
         }
       }
    
    
       
       
       
     protected function map($map){
        $map=  \explode("|", $map);
       if (count($map)===2) {
            return ['lat'=>$map[0],'lng'=>$map[1]];
        }
        return false;
     }
    
       
       protected function namestenost($int){
           switch((int)$int):
               case 1:return "namešten";
               case 2:return "polunamešten";
               case 3:return 'prazan';
           endswitch;
       }
       
       //prodavci!!!! poziva se public
       public function telefon($str){
           if (!$str) { return false; }
           if (\strlen($str)>7&&\strlen($str)<12) {
             $t="(".$str;
             return substr($t, 0,4).") ".  \substr($t, 4,3).'-'.substr($t,7);  
           }elseif(strlen($str)>5){
               return substr($str,0,3)." ".  substr($str, 3);  
           }
             return $str;
          }
               
       protected function grejanje($int){
           switch((int)$int):
               case 1:return 'CG';
               case 2:return 'EG';
               case 3:return 'TA';
               case 4:return 'gas';
               case 5:return 'podno';
               case 6:return 'kaljeva pec';
               case 7:return 'norveski radiatori';
               case 8:return 'mermerni radiatori';
             endswitch;
          }
       
       
       protected function tip_objekta($int){
           switch((int)$int):
               case 1:return 'standardni';
               case 3:return 'novogradnja';
               case 2:return 'sarogradnja';
               case 4:return 'u izgradnji';
           endswitch;
       }
       
       protected function opis_objekta($int){
           switch((int)$int):
               case 1:return 'izvorno stanje';
               case 2:return 'renoviran';
               case 3:return 'lux';
               case 4:return 'salonski';
               case 5:return 'duplex';
               case 6:return 'za renoviranje';
             endswitch;
       }
       
       
       
       protected function spratnost_kuce($int){
           switch((int)$int):
               case 1:return 'prizemna';
               case 2:return 'spratna';
           endswitch;
       }
       
               
       protected function vrsta_kuce($int){
           switch((int)$int):
               case 1:return 'cela kuća';
               case 2:return 'duplex';
               case 3:return 'vila';
               case 4:return 'montažna kuća';
               case 5:return 'stambeno poslovna kuća';
               case 6:return 'horizontala kuće';
               case 7:return 'vertikala kuće';
             endswitch;
       }
       
               

       protected static function kategorija_url($str) {
          switch((int)$str){
             case 1: $vrsta_objekta='stanovi';break;
             case 2: $vrsta_objekta='sobe';break;
             case 3: $vrsta_objekta="kuce";break;
             case 4: $vrsta_objekta="garaze";break;
             case 5:$vrsta_objekta='placevi';break;
           
             
             case 6:$vrsta_objekta='lokali'; break;
             case 7:$vrsta_objekta='poslovni-prostori';break;
             case 8:$vrsta_objekta='hale';break;
             case 9:$vrsta_objekta='magacin';break; 
             
             case 10:$vrsta_objekta='poljoprivredno-zemljiste';break;
             case 11:$vrsta_objekta='gradjevinsko-zemljiste';break;
             
             case 12:$vrsta_objekta='vikendice';break;
             case 13:$vrsta_objekta='apartmani';break;
             case 14:$vrsta_objekta='selidbe';break;
             case 15:return 'dnevno-izdavanje';
             
           }
  return $vrsta_objekta;
     }
    
    
     
     public static function agencije_logo($img){
         return '/img/logo/'.$img;
     }
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
        
    public static function create_link($kategorija,$id,$naslov) {
       return 'http://nadjinekretnine.com/nekretnine-oglas/'.self::kategorija_url($kategorija).'/'.$id.'/'.self::location_to_url($naslov,true);

             
       
    }
    
    public static function to_utf8($str){
             $cir=['č','ć','đ','š','ž','Č','Ć','Đ','Š','Ž'];
             $lat=['c','c','d','s','z','C','C','D','S','Z'];         
        return str_replace($cir,$lat,$str);
    }
    
    public static function only_letters($str){
        return \preg_replace("/[^a-zA-Z\s]/", '', $str);
    }
     public static function only_letters_num($str,$space=false){
         if ($space) {
           return \preg_replace("/[^a-zA-Z0-9\s]/", ' ', $str); 
         }
        return \preg_replace("/[^a-zA-Z0-9\s]/", '', $str);
    }   
    public static function only_num($str){
        return \preg_replace("/[^0-9]/", '', $str);
    }
    
    public static function location_to_url($lok,$space=false){
        return \implode('-',array_map('trim',array_filter(explode(" ",strtolower(self::only_letters_num(self::to_utf8($lok),$space))))));
    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

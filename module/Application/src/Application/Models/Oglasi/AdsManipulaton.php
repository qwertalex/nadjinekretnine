<?php

namespace Application\Models\Oglasi;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of AdsManipulaton
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;


class AdsManipulaton {
    
    protected $adapter;
    protected $user_id;
    public function __construct(\Zend\Db\Adapter\Adapter $adapter,$user_id) {
        $this->adapter=$adapter;
        $this->user_id=$user_id;
    }
    
    
    
        public function delete_oglas($oglas_id) {
          $gateway = new TableGateway('oglasi_nk', $this->adapter, new RowGatewayFeature('id_oglasi_nk'));
          $results = $gateway->delete([
              'id_users_nk' =>  $this->user_id,
              'id_oglasi_nk' => $oglas_id
          ]);
          if ($results) {
              return true;
          }
          return false;
    }
    
    
    
    
    
     
}
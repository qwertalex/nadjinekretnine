<?php

namespace Application\Models\Oglasi\Sliders;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Db\Sql\Sql;




/**
 * Description of MainSlider
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class Sliders extends \Application\Models\Oglasi\Render\RenderView{
    
    
    const DIRECTORY="/img/main_slider/";
    protected $adapter;
    
    public function __construct($adapter) {
        $this->adapter=$adapter;
        
    }
    
    
    
    
    public function main_slider() {
        
        
          $cache = \Zend\Cache\StorageFactory::factory(array(
    'adapter' => array(
        'name' => 'filesystem',
        'options' => array(
            'cache_dir' => '/tmp',
            'ttl' => 3600,
        ),
    ),
    'plugins' => array(
        'exception_handler' => array('throw_exceptions' => false),
         'serializer'
    ),
));
$key    = 'zend-main-slider-cache-def1';
        
$result = $cache->getItem($key, $success);
if (!$success) {
              
        $sql=new Sql($this->adapter);
        $select=$sql->select("main_slider");
        $select->columns(['img','title','text']);
        
        $where=new \Zend\Db\Sql\Where();
        $where->greaterThanOrEqualTo('datum_isteka', date('Y-m-d H:i:s'));
         $select->where($where);
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $arr_results = array_values(iterator_to_array($results));
             foreach ($arr_results as $key => $value) {
                $arr_results[$key]['img']=self::DIRECTORY.$value['img'];
             }
            return $arr_results;
         }
          $arr_results= [
             [
                 'img'      =>self::DIRECTORY.'def_small.jpg',
                 'title'    =>"Najbolji sajt za nekretnine u Srbiji!",
                'text'     =>'',
            ],
        /*  [
                 'img'      =>self::DIRECTORY.'dddefault.jpg',
                 'title'    =>"Veljiki izbor oglasa na sajtu NadjiNekretnine.com",
                 'text'     =>"",
             ],*/
              ];  
         $cache->setItem($key, $arr_results);
        return $arr_results;
}
        return $result;
        

        }
    
    
    /*
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * vraca sve premium oglase i kesira ih u filesys
     * 
     * 
     */
    
    public function premium_slider() {
        
        $cache = \Zend\Cache\StorageFactory::factory(array(
    'adapter' => array(
        'name' => 'filesystem',
        'options' => array(
            'cache_dir' => '/tmp',
            'ttl' => 3600,
        ),
    ),
    'plugins' => array(
        'exception_handler' => array('throw_exceptions' => false),
         'serializer'
    ),
));
$key    = 'premium-slider-cache12345';
$result = $cache->getItem($key, $success);
if (!$success) {
       $sql=new Sql($this->adapter);
        $select=$sql->select('vrsta_oglasa');
        $select->columns(['vrsta_oglasa']);
        $select->where([
           'vrsta_oglasa.vrsta_oglasa'=>3 
        ]);
        $select->join('oglasi_nk','oglasi_nk.id_oglasi_nk=vrsta_oglasa.id_oglasi_nk',['id_oglasi_nk','naslov','prodaja_izdavanje','kategorija','oglasivac']);
        $select->join('dodatno','dodatno.id_oglasi_nk=vrsta_oglasa.id_oglasi_nk',['cena','kvadratura']);
        $select->join('lokacija','lokacija.id_oglasi_nk=vrsta_oglasa.id_oglasi_nk',['mesto','deo_mesta']);
        $select->join('plac','plac.id_oglasi_nk=vrsta_oglasa.id_oglasi_nk',['povrsina_plac'],'left');
        $select->join('slike','slike.id_oglasi_nk=vrsta_oglasa.id_oglasi_nk',['slika_1'],'left');
        $select->order(['vrsta_oglasa.datum_postavljanja_oglasa DESC']);
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $pp = array_values(iterator_to_array($results));
             foreach($pp as $k=>$v):
                 $kat=$v['kategorija'];
             $pp[$k]['kategorija']=self::kategorija($v['kategorija']);
             if ($v['prodaja_izdavanje']) { $pp[$k]['prodaja_izdavanje']=self::get_pro_izd($v['prodaja_izdavanje']);}
             $pp[$k]['cena']=self::cena_valuta($v['cena']);
             if ($v['kvadratura']) {$pp[$k]['kvadratura']=self::povrsina($v['kvadratura']);}
            if ($v['povrsina_plac']) {
                $pp[$k]['povrsina_plac']=self::povrsina(false, $v['povrsina_plac']);
            }
             $pp[$k]['mesto']=self::get_mesto($v['mesto']);
             $pp[$k]['deo_mesta']=self::get_deo_mesta($v['mesto'], $v['deo_mesta']);
             $pp[$k]['oglasivac']=self::oglasivac($v['oglasivac']);
             $pp[$k]['slika_1']=self::image($v['slika_1']);
             $pp[$k]['link']=self::create_link($kat, $v['id_oglasi_nk'], $v['naslov']);
             endforeach;
            // var_dump($pp);
            //   $cache->setItem($key, $pp);
               return $pp;
          }
         return [];
    
  
}
return $result;
        
        
        
        
        
        

    }
    
    
    
    
    
    
    
}
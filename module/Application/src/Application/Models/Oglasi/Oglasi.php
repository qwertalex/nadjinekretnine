<?php
namespace Application\Models\Oglasi;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Application\Models\Oglasi\AdsInfo;



/*
 * 
 * 
 * 
 * 
 * 
 */




class Oglasi extends \Application\Models\Oglasi\AdsCounter{
     
    
    const dodatni_podatci="dodatni_podatci";
    const dodatno="dodatno";
    const kontakt_podatci="kontakt_podaci";
    const kuce="kuce";
    const lokacija='lokacija';
    const oglasi_nk="oglasi_nk";
    const plac="plac";
    const slike="slike";
    
    
    
    protected $oglas_id;
    
   // protected $adapter;
  //  protected $user_id;
    
    public function __construct( $adapter,$user_id) {
        $this->adapter=$adapter;
        $this->user_id=$user_id;
        parent::__construct($adapter,$user_id);
    }
    
    /**
     * 
     * @param type $ad_id
     * @return boolean
     * 
     * 
     * Vraca sve tabele iz baze vezane za oglase u array ili vraca false;
     * mora da postoji oglas u tabeli oglasi_nk koji ima user id=id i id_oglasi_nk=$ad_id sto znaci
     * da mogu da se vrate samo oglasi od tog korisnika, id usera se uzima u __construct()
     * 
     * 
     */
    public function get_all_ad_data($ad_id) {
        $sql=new Sql($this->adapter);
        $select = $sql->select();
        $select->from( 'oglasi_nk')->where(["oglasi_nk.id_users_nk=$this->user_id",'oglasi_nk.id_oglasi_nk'=>$ad_id]);
        $select->join('lokacija','lokacija.id_oglasi_nk = oglasi_nk.id_oglasi_nk');
        $select->join(self::kontakt_podatci,self::kontakt_podatci.".id_oglasi_nk = oglasi_nk.id_oglasi_nk");
        $select->join(self::dodatno,self::dodatno.'.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['*'], $select::JOIN_LEFT);
        $select->join('slike','slike.id_oglasi_nk = oglasi_nk.id_oglasi_nk',['*'], $select::JOIN_LEFT);
        $select->join(self::dodatni_podatci,self::dodatni_podatci.".id_oglasi_nk = oglasi_nk.id_oglasi_nk",['*'], $select::JOIN_LEFT);
        $select->join(self::kuce,self::kuce.".id_oglasi_nk = oglasi_nk.id_oglasi_nk",['*'], $select::JOIN_LEFT);
        $select->join(self::plac,self::plac.".id_oglasi_nk = oglasi_nk.id_oglasi_nk",['*'], $select::JOIN_LEFT);
        $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()==1) {
                    return array_values(iterator_to_array($results));
         }
          return false;
      }
    
    
  /**
   * 
   * @return type
   * 
   * OVE METODE BROJE OGLASE I 
   * nasledjuju abstracne metode iz \Application\Models\Oglasi\AdsCounter
   * 
   * broji sve oglase , broji oglase iz ponude:top, premium , standard,
   * i vraca broj aktivnih oglasa
   */
      //vraca sve oglase od odredjenog usera
    public function count_ads() {
        return $this->count_all_ads();
    }
    public function count_standard($int=1){
     return $this->count_one_ad_type($int);
    }
    public function count_top($int=2){
      return $this->count_one_ad_type($int);   
    }
    public function count_premium($int=3){
         return $this->count_one_ad_type($int);
    }
    //proverava da li postoje aktivni oglasi
    //vraca broj aktivnih oglasa ili 0
    public function count_active_ads(){
        return $this->active_ad();
    }
    
   /*
    * 
    * KRAJ COUNTERA OGLASA
    * 
    * 
    * 
    * 
    * 
    */ 
    
    /**
     * 
     * @param type $oglas_id
     * 
     * 
     * validaira odredjeni id oglasa da li je postavio ulogovan user
     * (sprecava XXS);
     * 
     */
    protected function validation_user_ad($oglas_id){
        $gateway = new TableGateway('oglasi_nk', $this->adapter);
        $this->oglas_id=$oglas_id;
        $rowset = $gateway->select(function ( $select) { 
                      $select->where([
                          'oglasi_nk.id_users_nk' =>  $this->user_id,
                          'oglasi_nk.id_oglasi_nk'=>$this->oglas_id,
                          ]);
         });
         $data = $rowset->current();
         if ($data) {
             return true;
         }
        return false;
     }
    
    /**
     * 
     * @param type $oglas_id
     * @param type $state
     * @return boolean
     * 
     * Aktivira ili gasi standardni oglas
     * 
     * 
     * 
     * 
     */
    public function on_off_free_standardni_oglas($oglas_id,$state) {
                var_dump(1);
        if (!$this->validation_user_ad($oglas_id)) {
           // throw new Exception("Kombinacija user, oglas nije validna!");
            return false;
        } 
        switch($state){
          case 'on':$vrsta_oglasa='0';break;
          case 'off':$vrsta_oglasa='1';break;
          default :return false;
      }
         
      //ako se oglas ukljucije
      if ($vrsta_oglasa==1) {
          if ($this->count_active_ads()>0) {
           //   throw new \Exception('Aktivni oglasi vec postoje!');
              return false;
          }
          $ads_info=AdsInfo::standardni_ad_trajanje();
          $dautm_postavljanja=$ads_info['datum_postavljanja_oglasa'];
          $datum_isteka=$ads_info['datum_isteka'];
          
      }else{
          $dautm_postavljanja=NULL;
          $datum_isteka=NULL;          
      }
      
      
      
      $ads=[
              'vrsta_oglasa'                =>  $vrsta_oglasa,
              'datum_postavljanja_oglasa'   =>  $dautm_postavljanja,
            //  'datum_isteka'                =>  $datum_isteka,
      ];
      if ($datum_isteka) {
          $ads['datum_isteka']=$datum_isteka;
      }
      
      
      
       $gateway = new TableGateway('vrsta_oglasa', $this->adapter);
          $gateway->update($ads,[
            'id_oglasi_nk'                  =>  $oglas_id,
          ]);
       
       
       
    }
    
    
    
    /*
     * 
     * 
     * vraca poslednje oglase po datumu i ima custom limit
     * 
     * 
     */
    public function poslednji_oglasi($limit=false){
         $sql=new Sql($this->adapter);
         $select = $sql->select();
         $select->from( 'oglasi_nk');       
         $select->columns(['naslov','id_oglasi_nk','kategorija']);
         $select->join("slike",'slike.id_oglasi_nk=oglasi_nk.id_oglasi_nk',['slika_1'],'left');
         $select->join("lokacija",'lokacija.id_oglasi_nk=oglasi_nk.id_oglasi_nk',['mesto']);
         $select->join("dodatno",'dodatno.id_oglasi_nk=oglasi_nk.id_oglasi_nk',['cena','valuta']);
         $select->join("vrsta_oglasa",'vrsta_oglasa.id_oglasi_nk=oglasi_nk.id_oglasi_nk',[]);
         
         $predicate = new  \Zend\Db\Sql\Where();
         $select->where($predicate->greaterThan('vrsta_oglasa.vrsta_oglasa','0'));
    



         $select->order("vrsta_oglasa.datum_postavljanja_oglasa DESC");
         
        if ($limit) {
            $select->limit($limit);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()) {
                    return array_values(iterator_to_array($results));
         }
          return false;        
        
        
        
        
    }
    
    
    
    
    
    
          
    public function prepare_oglas_update($oglas_id){
        if (!$this->validation_user_ad($oglas_id)) {
          //  throw new \Exception("Kombinacija user, oglas nije validna!");
            return false;
        }
        $data=  $this->get_all_ad_data($oglas_id)[0];
        $data['mesta_srbija']=$data['mesto'];
        
        $kat=$data['kategorija'];
        
        if ($data['mapa']) {
          $data['lat']=  \explode("|", $data['mapa'])[0];
        $data['lng']=explode("|", $data['mapa'])[1];  
        }
        if ($data['oglasivac']) {
            $data['oglasivac_nekretnine']=$data['oglasivac'];
            
        }
        if ($data['spratnost']) {
            $data['kuce_spratnost']=$data['spratnost'];
            
        }
        if ($data['spratovi']) {
            $data['sprat']=$data['spratovi'];
        }
        
        $data['kontakt_telefon']=$data['telefon'];
        $data['kontakt_telefon2']=$data['dodatni_telefon'];
        $data['telefon']=$data['dodatni_podatci_telefon'];
        $data['kp_ime']=$data['ime'];
        $data['kp_mesto']=$data['mesto_kontakt_podaci'];
        $data['kp_ulica']=$data['ulica_kontakt_podaci'];
        if (isset($data['povrsina_plac'])) {
            $data['plac_povrsina']=$data['povrsina_plac'];
        }
       
        
        unset($data['mesto'],$data['mapa'],$data['oglasivac'],$data['nacin_placanja'],$data['jedinica_mere_plac'],$data['spratnost'],
               $data['spratovi'],$data['doatni_telefon'] );
    //   var_dump($data);
        return $data;
    }
    
    
    
    
    
    
    
    
}
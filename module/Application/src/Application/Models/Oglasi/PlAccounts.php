<?php

namespace Application\Models\Oglasi;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of PlAccounts (pravna lica accounts)
 *
 * Vadi stanje na racunu, izvrsava naplatu i radi sve ostale stvari vezane za 
 * placanje oglasa za pravna lica
 * 
 * 
 * 
 * 
 * 
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;
use Application\Models\Oglasi\AdsInfo;

class PlAccounts extends \Application\Models\Oglasi\Oglasi{
    
    protected $adapter;
    protected $user_id;
    protected $post_data;
    protected $oglas_id;
    protected $dana;
    protected $tip_oglasa;
    protected $stanje;
    protected $br_tip_oglasa;

    public function __construct($adapter,$user_id) {
        $this->adapter=$adapter;
        $this->user_id=$user_id;
        parent::__construct($adapter, $user_id);
    }
    
    protected function cene(){
        return [
          'standard'    =>AdsInfo::get_pl_standard_cena(),
          'top'         =>AdsInfo::get_pl_top_cena(),
          'premium'     =>AdsInfo::get_pl_premium_cena()
        ];
    }
    
    public function set_data($post) {
        $this->post_data=$post;
        if (!$this->validate_post()) {
            throw new Exception('Podatci u postu nisu validni');
        }
        if (!$this->validation_user_ad($this->oglas_id)) {
            throw new Exception('User id se nepoklapa sa id oglasa.');
        }
        if ($post['tip_oglasa']!=='standard') {
        $this->stanje=  $this->get_stanje();
        if (!$this->provera_stanja()) {
            throw new Exception('Nema dovoljno novca na racunu.');
        }
        
         }else{
             $this->free_standard_oglas();
             return true;
         }
        
        
        
        $this->start_transaction();
    }

    /**
     * 
     * @return boolean
     * 
     * vraca trenutno stanje na racunu usera;
     */
    public function get_stanje() {
       $gateway = new TableGateway('pravna_lica_trenutno_stanje', $this->adapter); 
        $rowset =   $gateway->select([
                          'id_users_nk' =>  $this->user_id,
                          ]);
        $data = $rowset->current();
         if ($data) {
            return $data->trenutno_stanje;
         }        
         return false;
        
    }
    
    
    /**
     * 
     * @return type
     * 
     * izracunava cenu za odredjeni tip oglasa
     * 
     */
    protected function izracunaj_cenu() {
        switch($this->tip_oglasa){
            case 'standard': return ($this->cene()['standard']*$this->dana);
             case 'top':return ($this->cene()['top']*$this->dana);
              case 'premium':return ($this->cene()['premium']*$this->dana);
        }
     }
    
    /**
     * 
     * @return boolean
     * 
     * PROVERAVA DA LI USER IMA NOVACA ZA TRANSAKCIJU
     */
    protected function provera_stanja() {
         if ($this->stanje-$this->izracunaj_cenu()>=0) {
           return true;
        }
        return false;
    }
    
    /**
     * 
     * @param type $oglas_id
     * @return type
     * 
     * validira id oglasa sa user id 
     * detaljnije videti u parent classi
     */
    protected function validation_user_ad($oglas_id){
      return  parent::validation_user_ad($oglas_id);
    }
    
    
    
    
    /**
     * 
     * 
     * Kada pravno lice update oglas(izvrsi transakciju) 
     * update tri tabele:
     * -pravna_lica_trenutno_stanje-- udpate stanje na racunu
     * -pravna_lica_istorija_transakcija -- insert u istoriju (kolicina novca,datum)
     * -vrsta_oglasa -- update (tip_oglasa,datumi)
     */
    protected function start_transaction() {
            $date=date('Y-m-d H:i:s');
            $date_plus_days=date('Y-m-d H:i:s',strtotime($date. ' + '.$this->dana.' days'));
               $gateway = new TableGateway('pravna_lica_trenutno_stanje', $this->adapter,
                       new RowGatewayFeature('id_pravna_lica_trenutno_stanje'));
               $select=$gateway->select([
                    'pravna_lica_trenutno_stanje.id_users_nk'=>$this->user_id,
               ]);
               $data=$select->current();
               $data->trenutno_stanje= $this->stanje-$this->izracunaj_cenu();
               $data->save();
               /*
                * 
                * 
                * 
                * 
                * 
                * 
                */
                $gateway = new TableGateway('pravna_lica_istorija_transakcija', $this->adapter,
                       new RowGatewayFeature('id_pravna_lica_istorija_transakcija'));
                $insert=$gateway->insert([
                    'id_users_nk'=>  $this->user_id,
                    'iznos_transakcije'=>  $this->izracunaj_cenu(),
                    'vreme_transakcije'=>$date
                ]);

                $gateway = new TableGateway('vrsta_oglasa', $this->adapter);
                $gateway->update([
                    'vrsta_oglasa'=>  $this->br_tip_oglasa,
                    'datum_postavljanja_oglasa'=>$date,
                    'datum_isteka'=>$date_plus_days                    
                ],[
                    'id_oglasi_nk'=>  $this->oglas_id,
                ]);

    }
    
    
    public function iskljuci_oglas($oglas_id) {
        if (!$this->validation_user_ad($oglas_id)) {
            throw new \Exception('User id i oglas id se nepokalpaju.');
        }
                 $gateway = new TableGateway('vrsta_oglasa', $this->adapter);
                 $gateway->update([
                    'vrsta_oglasa'=>  0,
                    'datum_postavljanja_oglasa'=>null,
                    //'datum_isteka'=>null   ,
                      'osvezi_oglas'=> null,
                     'datum_azuriranja'=>NULL
                 ],[
                    'id_oglasi_nk'=> $oglas_id,
                 ]);       
       }
    
    /**
     * 
     * @param type $oglas_id
     * @return boolean
     * @throws \Exception
     * 
     * 
     * Ova opcija je iskljucena
    
       public function osvezi_oglas($oglas_id) {return false;
         if (!$this->validation_user_ad($oglas_id)) {
            throw new \Exception('User id i oglas id se nepokalpaju.');
        }
       
      $stanje=  $this->get_stanje();
        $cena=$this->get_refresh_cena($oglas_id);
            
        if (!$cena||$stanje-$cena<0) {
             throw new \Exception('Nema dovoljno novca na racunu.');
        }
                       $gateway_s = new TableGateway('pravna_lica_trenutno_stanje', $this->adapter,
                       new RowGatewayFeature('id_pravna_lica_trenutno_stanje'));
               $select=$gateway_s->select([
                    'pravna_lica_trenutno_stanje.id_users_nk'=>$this->user_id,
               ]);
               $data=$select->current();
               $data->trenutno_stanje= $stanje-$cena;
               $data->save();
        
        
                 $gateway = new TableGateway('vrsta_oglasa', $this->adapter);
                 $gateway->update([
                    'osvezi_oglas'=> date('Y-m-d H:i:s'),
                  ],[
                    'id_oglasi_nk'=> $oglas_id,
                 ]);         
        
        
       }
     */
       
       protected function get_refresh_cena($oglas_id) {
                 $gateway = new TableGateway('vrsta_oglasa', $this->adapter, new RowGatewayFeature('id_tip_oglasa'));
                 $select=$gateway->select([
                  'id_oglasi_nk'=> $oglas_id
                  ]);   
                 $data=$select->current();
                  if ($data) {
                      switch($data->vrsta_oglasa){
                          case 2:return AdsInfo::get_pl_top_refresh();
                          case 3:return AdsInfo::get_pl_premium_refresh();
                          default:return false;
                      }
            
                  }
                  return false;
       }
    
   /**
     * 
     * @return boolean
     * 
     * vaidira post i stavlja vrednosti u globalne variabile
     *  $this->oglas_id=$post['oglas_id'];
        $this->dana=$post['dana'];
        $this->tip_oglasa=$post['tip_oglasa'];
     */
    protected function validate_post() {
        $post=$this->post_data;
        if (isset($post['oglas_id'],$post['dana'],$post['tip_oglasa'])) {
            $valid_digit = new \Zend\Validator\Digits();
            $in_array = new \Zend\Validator\InArray();
            $in_array->setHaystack(['standard','top','premium']);
            $in_array->setStrict($in_array::COMPARE_STRICT);
            
            if ($valid_digit->isValid($post['oglas_id'])&&
                    $valid_digit->isValid($post['dana'])&&
                    $in_array->isValid($post['tip_oglasa'])) {
               $this->oglas_id=$post['oglas_id'];
               $this->dana=$post['dana'];
               $this->tip_oglasa=$post['tip_oglasa'];
               
               switch($this->tip_oglasa){
                   case "standard":
                       $this->br_tip_oglasa=1;
                       break;
                   case "top":
                        $this->br_tip_oglasa=2;
                       break;
                   case "premium":
                        $this->br_tip_oglasa=3;
                       break;
               }
               
               
               
               
               return true;
            }
                    
           
        } 
        return false;
               
    }
    
    
    
    /*
     * 
     * funkcija za preuzimanje vaucera tj.
     * za uplatu 1000 dingera na racun
     * 
     * 
     * 
     * 
     * 
     */
    public function vaucer(){
        $sql=new Sql($this->adapter);
        $update=$sql->update("pravna_lica_trenutno_stanje");
        
        $update->set([
            'trenutno_stanje'=>1000
        ],[
            'id_users_nk'=>$this->user_id
        ]);
        
            $statement = $sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
          
         if ($results->count()>0) {
             $this->istorija_uplate_update($this->user_id,1000);
                   return true;
         }
         return false;
        
    }
    
    
    
    /**
     * 
     * @param type $id
     * @param type $stanje
     * @return boolean
     * 
     * 
     * 
     * EVIDENCIJA ZA SVAKU UPLATU
     */
    protected function istorija_uplate_update($id,$stanje){
         $sql=new Sql($this->adapter);
        $insert=$sql->insert("istorija_uplate_pravna_lica");
        $insert->values([
            'id_users_nk'       =>$id,
            'stanje'            =>$stanje,
            'datum_vreme'       =>date('Y-m-d H:i:s')
        ]);
                   
        
            $statement = $sql->prepareStatementForSqlObject($insert);
            $results = $statement->execute();
         if ($results->count()>0) {
                   return true;
         }
         return false;
       }
    
       
       /**
        * 
        * @return boolean
        * 
        * 
        * vraca istoriju racuna iz baze
        */
    public function get_istorija_racuna(){
         $sql=new Sql($this->adapter);
        $select=$sql->select("istorija_uplate_pravna_lica");
        $select->where([
            'id_users_nk'   =>$this->user_id
        ]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count()>0) {
                   return array_values(iterator_to_array($results));
         }
         return false;
     }
    
    
    /**
     * 
     * @param type $post
     * @return boolean
     * @throws Exception
     * @throws Exeption
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    public function produzi_oglas($post){
                   $cena=false;
        if (is_numeric($post['id'])&&!$this->validation_user_ad($post['id'])) {
            throw new Exception('User id se nepoklapa sa id oglasa.');
        }elseif(!is_numeric($post['dana'])){
            throw new Exeption('$_POST[dana] nije numeric');
        }
        
          $sql=new Sql($this->adapter);
         $select=$sql->select("vrsta_oglasa");
         $select->columns(['vrsta_oglasa','datum_isteka']);
         $select->where([
            'id_oglasi_nk'   =>$post['id']
         ]);
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) { 
                   $res= array_values(iterator_to_array($results))[0];
                  // var_dump($res); 
                   $sve_cene=  $this->cene();
                   
                switch((int)$res['vrsta_oglasa']){
                   case 1: $cena=$sve_cene['standard']*(int)$post['dana'];break;
                    case 2:$cena= ($sve_cene['top']*(int)$post['dana']);break;
                     case 3:$cena= ($sve_cene['premium']*(int)$post['dana']);break;
               }           
           
      //besplatni standardni oglas         
     if ($res['vrsta_oglasa']>1) {
                   
            if (!$cena) { return false;}
               $stanje=(float)$this->get_stanje();
               if ($stanje-$cena<0) {return false;}
               $new_cena=$stanje-$cena;
  
               
                   
               
                
       $gateway = new TableGateway('pravna_lica_trenutno_stanje', $this->adapter,
                       new RowGatewayFeature('id_pravna_lica_trenutno_stanje'));
               $select=$gateway->select([
                    'pravna_lica_trenutno_stanje.id_users_nk'=>$this->user_id,
               ]);
               $data=$select->current();
               $data->trenutno_stanje= $new_cena;
               $data->save();
               /*
                * 
                * 
                * 
                * 
                * 
                * 
                */
                $gateway = new TableGateway('pravna_lica_istorija_transakcija', $this->adapter,
                       new RowGatewayFeature('id_pravna_lica_istorija_transakcija'));
                $insert=$gateway->insert([
                    'id_users_nk'=>  $this->user_id,
                    'iznos_transakcije'=> $cena,
                    'vreme_transakcije'=>date('Y-m-d H:i:s')
                ]);
   }
                
 //update stanje
                $date_plus_days=date('Y-m-d H:i:s',strtotime($res['datum_isteka']. ' + '.$post['dana'].' days'));               
                
                
                $gateway = new TableGateway('vrsta_oglasa', $this->adapter);
                $gateway->update([
                 //   'vrsta_oglasa'=>  $this->br_tip_oglasa,
                //    'datum_postavljanja_oglasa'=>$date,
                    'datum_isteka'=>$date_plus_days                   
                ],[
                    'id_oglasi_nk'=>  $this->oglas_id,
                ]);
                
                   
         }   
                   
                   
          return false;        
                   
     
        
    }
    
    
    
    
    
    
    
    protected function free_standard_oglas(){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $update=$sql->update('vrsta_oglasa');
          $date=date('Y-m-d H:i:s');
          $date_plus_days=date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'). ' + '.$this->dana.' days'));
        $update->set([
           'vrsta_oglasa'   =>1,
            'datum_postavljanja_oglasa'    =>date('Y-m-d H:i:s'),
            'datum_isteka'  =>$date_plus_days
        ]);
        $update->where([
            'id_oglasi_nk'  =>$this->oglas_id
        ]);
        $statement = $sql->prepareStatementForSqlObject($update);
         $statement->execute();
        
        return true;
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

<?php
namespace Application\Models\Oglasi;




 class  AdsInfo {

     const BROJ='1202';
     const PRETPLATA='NADJI';

    /*
   * 
   * 
   * CENE ZA SMS
   * 
   * 
   */  
    
     
    /*
     * FIZICKA LICA
     */
     const USER_STANDARD_KEYWORD="ST";
     const USER_STANDARD_CENA='110,64';
     const USER_TRAJANJE_DANA='30';
  //   const USER_STANDARD_REFRESH_KEYWORD='OSVEZISTANDARD';
   //  const USER_STANDARD_REFRESH_CENA='24,00';
    
     //TOP OGLAS TRAJE 10 DANA
     const USER_TOP_KEYWORD='TOP';
     const USER_TOP_CENA="122,64";
     const USER_TOP_TRAJANJE_DANA='30';
 //    const USER_TOP_REFRESH_KEYWORD='OSVEZITOP';
     const USER_TOP_REFRESH_CENA="49,99";
    
    //traje 7 dana
     const USER_PREMIUM_KEYWORD="PR";
     const USER_PREMIUM_CENA="242,64";//BRUTO CENA
     const USER_NETO_CENA="200";
     const USER_PREMIUM_REFRESH="50";
     const USER_PREMIUM_TRAJANJE_DANA="30";
     const USER_PREMIUM_REFRESH_KEYWORD="OSVEZIPREMIUM";
     const USER_PREMIUM_REFRESH_CENA="119,00";
 /*
      * 
      * CENE ZA UPLATNICE I ZA UPLATU PREKO RACUNA POPUST 10%
      * 
      * 
      */

     const RACUN_USER_STANDARD="STANDARDNI";
     const RACUN_USER_STANDARD_CENA=FALSE;
    
     const RACUN_USER_TOP_KEYWORD='TOP';
     const RACUN_USER_TOP_CENA="80,00";
     
    
     const RACUN_USER_PREMIUM_KEYWORD="PREMIUM";
     const RACUN_USER_PREMIUM_CENA="200,00";
    /*
     * 
     * -----------------------------------------
     */
  
     /*
      * 
      * PRAVNA LICA
      * 
      * 
      */
     
    
     const PRAVNA_LICA_STANDARD_TRAJANJE="1D";//mesecno=29.7;
     const PRAVNA_LICA_STANDARD_CENA="0";//0,99
     public static function get_pl_standard_cena() {
         return str_replace(",",".",self::PRAVNA_LICA_STANDARD_CENA);
     }
    
     
     const PRAVNA_LICA_TOP_TRAJANJE="24h";
     const PRAVNA_LICA_TOP_CENA="6,99";
     public static function get_pl_top_cena() {
         return str_replace(",",".",self::PRAVNA_LICA_TOP_CENA);
      }     
     const PRAVNA_LICA_TOP_REFRESH="3,49";
     public static function get_pl_top_refresh() {
          return str_replace(",",".",self::PRAVNA_LICA_TOP_REFRESH);
      }
     const PRAVNA_LICA_PREMIUM_TRAJANJE="24H";
     const PRAVNA_LICA_PREMIUM_CENA="24,99";  
     public static function get_pl_premium_cena() {
         return str_replace(",",".",self::PRAVNA_LICA_PREMIUM_CENA);
      }      
     const PRAVNA_LICA_PREMIUM_REFRESH="12,49";
      public static function get_pl_premium_refresh() {
          return str_replace(",",".",self::PRAVNA_LICA_PREMIUM_REFRESH);
      }
      
     const PRAVNA_LICA_EXTRA_TRAJANJE="5D";
     const PRAVNA_LICA_EXTRA_CENA="2000,00";
     const PRAVNA_LICA_EXTRA_TRAJANJE_DNEVNO="1D";
     const PRAVNA_LICA_EXTRA_CENA_DNEVNO="500,00";   
     const PRAVNA_LICA_EXTRA_REFRESH="200,00"; 
     
     //PAKETI ZA PRAVNA LICA
     /*
      * standardni oglasi:
      * 4040 oglasa
      * -----------------
      * top:
      * 
      * -------------------
      * premium:
      * 
      * --------------------
      * extra:
      * 
      */    
     const HIT4000="4000,00";

     const HIT6000='6000,00';    
     
     /*
      * 
      * 
      * END PRAVNA LICA
      * 
      * 
      */

     
   
   /*
    * 
    * 
    * 
    * INFO ZA FIRMU
    * 
    * 
    *    
    */
     
     
     const SVRHA_UPLATE="Uplata ";//preko js ubaciti tip oglasa
     const PRIMALAC="'Manasijevic'";
     const SIFRA_PLACANJA='242424342';//javice buraz
     const RACUN="1234667890";
     const MODEL="34356534";
     const POZIV="3434";//id oglasa
     
     
    const KONTAKT_TELEFON='060/0-333-807';
    const INFO_MAIL="info@nadjinekretnine.com";
     
     
     public static function standardni_ad_trajanje(){
          $date=date("Y-m-d H:i:s");
          $time = strtotime($date);
          $date_plus_2_mounth= date("Y-m-d H:i:s", strtotime("+1 month", $time));  
          return [
              'datum_postavljanja_oglasa'=>$date,
              'datum_isteka'=>$date_plus_2_mounth,
              ];
     }
     
     
     public static function top_ad_trajanje() {
         
         
     }
     
     public static function premium_ad_trajanje() {
         
         
     }
     
     
     
     
    
}
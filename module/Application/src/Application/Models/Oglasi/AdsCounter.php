<?php

namespace Application\Models\Oglasi;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * BROJI OGLASE 
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;



abstract class AdsCounter {
    
        protected $adapter;
        protected $user_id;
    
    public function __construct( $adapter,$user_id) {
        $this->adapter=$adapter;
        $this->user_id=$user_id;
        
    }
 
        abstract function count_ads();
        abstract function count_standard($int=1);
        abstract function count_top($int=2);
        abstract function count_premium($int=3);
        abstract function count_active_ads();
/*
    public function count_ads() {
        return $this->count_all_ads();
    }
    public function count_standard($int=1){
     return $this->count_one_ad_type($int);
      }
    public function count_top($int=2){
      return $this->count_one_ad_type($int);   
    }
    
    public function count_premium($int=3){
         return $this->count_one_ad_type($int);
    }
    */

    
    /**
     * 
     * @param type $ad_id
     * @return boolean
     * 
     * Proverava da li oglas postoji
     */
      
       public function ad_exist($ad_id) {
             $gateway = new TableGateway('oglasi_nk', $this->adapter, new RowGatewayFeature('id_oglasi_nk'));
             $results = $gateway->select([
                 'id_users_nk' =>  $this->user_id,
                 'id_oglasi_nk'=> $ad_id,
             ]);
             $data = $results->current();
             if ($data) {
                 return true;
             }
                  return false;
    }
    
    
    
    

    /**
     * 
     * @param type $int
     * @return type
     * 
     * Ovu funkciju koristim za prebrojavanje oglasa
     * abstrakne function je koriste ovako 
     * $this->count_one_ad_type(1);//vraca standardne oglase
     * ovaj method koriste abstract function
     */
    protected function count_one_ad_type($int=false) {
                 $sql = new Sql($this->adapter);
                $select = $sql->select();
                $select->from('oglasi_nk');
                $select->where(array('id_users_nk' => $this->user_id));
                $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk=oglasi_nk.id_oglasi_nk');
                
                if (in_array($int, range(0,3))) {
                $select->where([
                   'vrsta_oglasa.vrsta_oglasa'=>$int 
                ]);                    
                }
                $statement = $sql->prepareStatementForSqlObject($select);
                $results = $statement->execute();   
                return $results->count();    
        
        
        
    }
  
    


/**
 * 
 *  
 * vrac ukupan broj oglasa || false
 */

   protected function count_all_ads() {
             $gateway = new TableGateway('oglasi_nk', $this->adapter);

                $rowset = $gateway->select(function ( $select) {
                      $select->where([ 'id_users_nk' =>  $this->user_id,]);
                     $select->columns(['num' => new \Zend\Db\Sql\Expression('COUNT(*)')]);
                });

                $data=$rowset->current();
                if ($data) {
                    return $data->num;
                }
                return false;
}
    

    
    
    /**
     * 
     * BROJI DA LI POSTOJE AKTIVNI OGLASI (u koloni vrsta_oglasa.vrsta_oglasa
     * vrednost veca od 0 (nula je neaktivan oglas)
     * 
     */
        protected function active_ad() {
                $sql = new Sql($this->adapter);
                $select = $sql->select();
                $select->from('oglasi_nk');
                $select->where(array('id_users_nk' => $this->user_id));
                $select->join('vrsta_oglasa','vrsta_oglasa.id_oglasi_nk=oglasi_nk.id_oglasi_nk');
                
                 $predicate = new  \Zend\Db\Sql\Where();
                 $select->where([
                     $predicate->greaterThan('vrsta_oglasa.vrsta_oglasa',0 )
                 ]);
                $statement = $sql->prepareStatementForSqlObject($select);
                $results = $statement->execute();   
                return $results->count();    
         }
    
    
    
         
         
         
         
  
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
    
    
}

<?php
namespace Application\Models\Head;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of HtmlHeadHelpers
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class HtmlHeadHelpers {
    
   protected $url;
   protected static $data;
   protected static $title='Nekretnine - oglasi za nekretnine - srbija,beograd - izdavanje stanova,prodaja stanova - nadjinekretnine.com ';
   protected static $meta_keywords='';
   protected static $meta_description='';
/*    
   //Najposećeniji sajt za nekretnine u Srbiji. Pronađite stan, kuću, poslovni prostor. Prodaja i
   // izdavanje nekretnina, novogradnja, Beograd, Srbija, inostranstvo.
 * ---------------------------------------------------------- 
    //Najveci sajt za nekretnine. Oglasi za  izdavanje stanova, prodaja stanova, '
       //    . 'zemljište, lokali, kancelarije, poslovni prostor na jednom mestu.
 * 
 * Oglasni sajt na kom možete prodati, kupiti ili iznajmiti nekretnine u Srbiji.
 * 
 * 
 * 
 * 
 */
    public static function set_data($data){
        self::$data=$data;
     
      // var_dump($data);
        $con=$data['controller'];
        $action=$data['action'];
     
        
        
        switch($con):
            case 'Application\Controller\Index':
                switch($action):
                case "index":
                        self::$meta_description='Najbolji sajt za nekretnine u Srbiji. Oglasi za prodaju , izdavanje i novogradnju za beograd i celu srbiju.';
                        self::$meta_keywords="nekretnine,beograd,srbija,stanovi,kuce,placevi,graze,selidbe,prodaja,izdavanje,poslovni porstori,lokali,vikendice,apartmani";
                    break;
                endswitch;
                    
                break;
            case "Pretraga\Controller\Index":
                
                     self::$title=self::create_pretraga_title();
                     self::$meta_description=self::pretraga_data($data);
                     self::$meta_keywords=self::pretraga_keywords($data);
                
                break;
            case "LoginRegister\Controller\Index":
                
                     self::$title='Prijava / Registracija | Nekretnine';
                
                break;
            case "OstaviOglasForm\Controller\Index":
                
                     self::$title='Predaj oglas | Nekretnine';
                
                break;
            
            
            
            
            case "MojProfil\Controller\MojProfil":
                    switch($action):
                            case "index":
                                  self::$title='Moj Profil Pocetna | Nekretnine';
                                 break;
                             case "moji-podatci":
                                      self::$title='Moj Profil | Moji podatci | Nekretnine';
                                 break;
                             case "moji-oglasi":
                                          self::$title='Moj Profil | Moji oglasi | Nekretnine';
                                 break;
                             case "favoriti":
                                        self::$title='Moj Profil | Favoriti | Nekretnine';
                                 break;
                             case "poruke":
                                 self::$title='Moj Profil | Poruke | Nekretnine';
                                 break;
                             case "pomoc-podrska":
                                 self::$title='Moj Profil | Pomoc i podrska | Nekretnine';
                                 break;
                    endswitch;
                
                break;
            case "NadjiNekretnineInfo\Controller\Index":
               switch($action):
                 case "o-nama":
                     self::$title='O nama | Nekretnine';
                     break;
                 case "marketnig":
                     self::$title='Marketing | Nekretnine';
                     break;
                 case "impressum":
                     self::$title='Impressum | Nekretnine';
                     break;
                 case "uslovi-koriscenja":
                     self::$title='Uslovi koriscenja | Nekretnine';
                     break;
                 case "partnerski-sajtovi":
                     self::$title='Partneri | Nekretnine';
                     break;
                 case "cesta-pitanja":
                     self::$title='Cesto postavljanna pitanja | Nekretnine';
                     break;
                 case "kontakt":
                     self::$title='Kontaktirajte nas | Nekretnine';
                     break;
                 
                 case "mapa-sajta":
                     self::$title='Mapa sajta | Nekretnine';
                     break;
                 
             endswitch;
            break;
            
                 case 'Prodavci\Controller\Index':
                     switch ($action):
                     case "index":
                         self::$title="Agencije za nekretnine - Nekretnine";
                         self::$meta_description="Najveci izbor agencija za nekretnine u srbiji";
                         break;
                     case "agencija":
                         self::$title="Agencije za nekretnine | ". ucwords(str_replace('-', " ", $data['agencija']))." - Nekretnine";
                         self::$meta_description="Agencija za nekretnine | Oglasi od: ". ucwords(str_replace('-', " ", $data['agencija']))." - Nekretnine";
                         break;
                     endswitch;
                     break;
                     case "MiTrazimo\Controller\Index":
                         switch ($action):
                         case "index":
                             self::$title="Mi trazimo za vas - Nekretnine";
                             self::$meta_description="BESPLATNO se prijavite na servis Mi trazimo za vas i svakog dana ce vam stizati najnoviji oglasi za nekretnine";
                             break;
                         endswitch;
                         break;
            
        endswitch;
        
                 
        
        
        
        
    }
    
    
    
    
    
    
    public static function create_pretraga_title(){
        $data=self::$data;
        
        $str="";
        if ($data['kategorija']==='selidbe') {
            return "Selidbe | ".self::lokacija($data['lokacija']);
        }
        $str.=\ucfirst($data['prodaja_izdavanje'])." ";
        $str.=self::kategorija($data['kategorija'])." | ";
        $str.=self::lokacija($data['lokacija']);
        $str.=" | NadjiNekretnine.com";
        return $str;
    }
    
    
    
    
    
    
    
    protected static function kategorija($kat){
      switch($kat):
           default:return substr_replace($kat,"a",-1);
               case "magacin":return "magacina";
               case "vikendice":return "vikendica";
        endswitch;
    }
    
    
    protected static function lokacija($lok){
        return str_replace("-"," ",str_replace("_",",",$lok));
      }
    
    
    
    
    
    
    protected static function pretraga_data($data){
        $str="";
        $pro_izd=$data['prodaja_izdavanje'];
        switch($pro_izd):
            case "prodaja":$str.="Oglasi za prodaju ";break;
            case "izdavanje":$str.="Oglasi za izdavanje ";break;
        endswitch;
       $str.=self::kategorija($data['kategorija']).", ".self::lokacija($data['lokacija']).' ';
       $str.="| na portalu nadjinekretnine.com - ";
       $str.="najbolja ponuda i najpovoljnije cene nekretnina - ";
       if (isset($data['strana'])) {
           $str.='strana '.$data['strana'];
       }else{
           $str.='strana 1';
       }
        return $str;
        
    }
    
    protected static function pretraga_keywords($data){
        $str='nekretnine,'.$data['prodaja_izdavanje'].",srbija,".$data['kategorija'];
        return $str;
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static function get_title(){
        return self::$title;
    }
    public static function get_meta_kewords(){
        return self::$meta_keywords;
    }
    public static function get_meta_description(){
        return self::$meta_description;
    }
    
    
    
    
    
    
    
    
    
    
    
    
}

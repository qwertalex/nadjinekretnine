<?php

namespace Application\Models\Images;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Filter\File\RenameUpload;
/**
 * Description of ImageValidation
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class ImageValidation {
    
    protected $image;
    
    public function __construct($img) {
        $this->image=$img;
    }
    
    public function extension($ext){
           $validator = new \Zend\Validator\File\Extension($ext);
        if ($validator->isValid($this->image)) {
            return true;
        } 
        return false;
                 
    }
    /**
     * 
     * @param type $size
     * @return boolean
     * 
     * 
     * example $size="10MB";
     */
    public function size($size){
         $validator = new \Zend\Validator\File\Size(['max' => $size]);
        if ($validator->isValid($this->image)) {
            return true;
        }
        return false;
    }
    
    
    
    public function rename_move($dir,$file_name=false){
        $file_name=$file_name?$file_name:uniqid();
         $filter = new RenameUpload(
                array(
            'target' => $dir.'/'.$file_name,
            'use_upload_extension' => TRUE,
            "randomize" => FALSE,
        ));
        $filter->filter($this->image);
         return $this->get_image($dir, $file_name);
    }
    
    

    
    
    
    
    
    
    
    
    
    
     protected function get_image($dir,$image) {
      
        $arr = scandir($dir);
        foreach ($arr as $v) {
            if (strpos($v, $image) !== FALSE) {
                return $v;
            }
        }
        return false;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}

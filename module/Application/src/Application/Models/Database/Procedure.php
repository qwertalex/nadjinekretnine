<?php
namespace Application\Models\Database;




class Procedure {
    
    
    
        protected function call_procedure($adapter,$name,Array $params) {
          $driver = $adapter->getDriver();
          $connection = $driver->getConnection(); 
          $str=""; 
          foreach ($params as $v) {$str.="'".$v."',";}
          $str=  substr($str, 0,-1);
          
          $results = $connection->execute("call $name($str)");
          $results = array_values(iterator_to_array($results));
          return $results;         
    }
    
    
    
    
    
    
    
}

 

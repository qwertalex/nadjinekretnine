<?php
namespace Application\Models\User;

use Zend\Db\Sql\Sql;
use Zend\Crypt\Password\Bcrypt;
use Application\Models\User\SessionManipulation;
/**
 * 
 * Vadi podatke iz baze i setuje podatke o useru
 * ili o useriovim oglasima
 * 
 * 
 * 
 */

class UserDataManipulation {

    protected $adapter;
    protected $session;
    protected $email;
    public function __construct($adapter) {
        $this->adapter=$adapter;
        $this->session=new SessionManipulation();
         $sess=  $this->session;
         $this->email=  $sess->get_user_session()['user'];
        
    }   
    
    public function get_user_data($email) {
         $sql=new Sql($this->adapter);
         $select = $sql->select();
         $select->from('users_nk');
         $select->where(array('email_nk' => $email));
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()==1) {
             $arr_results = array_values(iterator_to_array($results));
                return $arr_results; 
         }
         return FALSE;
        
    }
    
    public function validate_session_user() {
        $email=  $this->email;
        if ($this->get_user_data($email)!==FALSE) {
            return TRUE;
         }
        return false;
        
    }
    
    public function get_id() {
       $sess=  $this->session; 
       $email=  $sess->get_user_session()['user'];
       if ($data=$this->get_user_data($email)) {
           return $data[0]['id_users_nk'];
       }
       return FALSE;
    }
    
    
    
    /**
     * 
     * @param type $email
     * @return type
     * 
     * 
     * 
     * 
     */
    public function get_user_newsletter($email) {
         $sql=new Sql($this->adapter);
         $select = $sql->select();
              $select->from(["n"=>'newsletter'])  // base table
             ->join(array('f' => 'users_nk'),     // join table with alias
             'f.id_users_nk = n.id_users_nk');         // join expression
        
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()==1) {
             $arr_results = array_values(iterator_to_array($results));
             $return=[
               'id_newsletter'=> $arr_results[0]['id_newsletter'],
               'id_users_nk'=>  $arr_results[0]['id_users_nk'],
               'poruke'=>  $arr_results[0]['poruke'],
               'noviteti'=>  $arr_results[0]['noviteti'],
               'istekli_oglasi'=> $arr_results[0]['istekli_oglasi']
                ];
            return $return;  
         }
 
         return false;
         
    }
    
    
    
    public function update_email($email,$new_email) {
        $sql=new Sql($this->adapter);
        $update=$sql->update();
        $update->table("users_nk");
        $update->set(['email_nk'=>$new_email]);
        $update->where(['email_nk'=>$email]);
         $statement = $sql->prepareStatementForSqlObject($update);
         $results = $statement->execute();
         if ($results->count()==1) {
             return TRUE;
         }    
        
        
        return FALSE;
        
    }
    
    
    
    public function update_password($email,$old_pass,$new_pass) {
         if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return FALSE;
         }
         if (strlen($old_pass)<5||strlen($new_pass)<5) {
            return false;
         }
         if ($this->check_password($email, $old_pass)) {
             $bcrypt = new Bcrypt();
             $sql=new Sql($this->adapter);
             $update=$sql->update();
             $update->table("users_nk");
             $update->set(['password_nk'=>$bcrypt->create($new_pass)]);
             $update->where(['email_nk'=>$email]);
             $statement = $sql->prepareStatementForSqlObject($update);
             $results = $statement->execute();
             if ($results->count()==1) {
                 return TRUE;
             }    
             return FALSE;
         }
        return false;
     }
    
    
    
    protected function check_password($email,$password) {
         $sql=new Sql($this->adapter);
         $select = $sql->select();
         $select->from('users_nk');
         $select->where(array('email_nk' => $email));
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         $arr_results = array_values(iterator_to_array($results));
        
         $bcrypt = new Bcrypt();
         $securePass = $arr_results['0']['password_nk'];

         if ($bcrypt->verify($password, $securePass)) {
             return TRUE;
         } 
         return FALSE;
     }
    
    
    
    
    
     
     public function update_user_data(Array $post) {
         $sess=  $this->session;
         
         $mp_ime=NULL;
         $mp_dob=NULL;
         $mp_tel=NULL;
         $mp_mob=NULL;
         $mp_pol=NULL;
         $mp_noviteti=0;
         $mp_istekli_oglasi=0;
         $mp_poruke=0;
         
         if (isset($post['mp_dob'])&&$post['mp_dob']!="") {
            $mp_dob=date('Y-m-d', strtotime($post['mp_dob']));
         }
         
         if (isset($post['mp_pol'])) {
             switch ($post['mp_pol']){
             case 1:$mp_pol='muski'; break;
             case 2:$mp_pol='zenski'; break;
             }
         }
         if (isset($post['mp_ime'])&&$post['mp_ime']!="") {
             $mp_ime=$post['mp_ime'];
         }
         if (isset($post['mp_telefon'])&&$post['mp_telefon']!="") {
             $mp_tel=$post['mp_telefon'];
         }         
         if (isset($post['mp_mobilni'])&&$post['mp_mobilni']!="") {
             $mp_mob=$post['mp_mobilni'];
         }
          if (isset($post['mp_noviteti'])&&$post['mp_noviteti']!="") {
             $mp_noviteti=$post['mp_noviteti'];
         }
         if (isset($post['mp_istekli_oglasi'])&&$post['mp_istekli_oglasi']!="") {
             $mp_istekli_oglasi=$post['mp_istekli_oglasi'];
         }
         if (isset($post['mp_poruke'])&&$post['mp_poruke']!="") {
             $mp_poruke=$post['mp_poruke'];
         }
         $email=  $sess->get_user_session()['user'];
             $sql=new Sql($this->adapter);
             $update=$sql->update();
             $update->table("users_nk");
             $update->set([
                 'ime'=>$mp_ime,
                 "datum_rodjenja"=>$mp_dob,
                 'telefon'=>$mp_tel,
                 "mobilni"=>$mp_mob,
                 "pol"=>$mp_pol,
             ]);
             $update->where(['email_nk'=>$email]);
             $statement = $sql->prepareStatementForSqlObject($update);
             $results = $statement->execute();
             $this->update_newsletter_data($mp_noviteti, $mp_istekli_oglasi, $mp_poruke);
             if ($results->count()==1) {
                    return TRUE;
             }    
             return FALSE;
         }
     
     
     
         protected function update_newsletter_data($noviteti,$istekli_oglasi,$poruke) {
             $email=  $this->email;
             $user_data=  $this->get_user_data($email);
             $id=$user_data[0]['id_users_nk'];
             $sql=new Sql($this->adapter);
             $update=$sql->update();
             $update->table("newsletter");
             $update->set([
                 'noviteti'=>$noviteti,
                 'istekli_oglasi'=>$istekli_oglasi,
                 "poruke"=>$poruke
             ]);
             $update->where(['id_users_nk'=>$id]);
             $statement = $sql->prepareStatementForSqlObject($update);
             $results = $statement->execute();
             if ($results->count()==1) {
                 return TRUE;
             }    
             return FALSE;
             
             
             
             
         }
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
    
    
}





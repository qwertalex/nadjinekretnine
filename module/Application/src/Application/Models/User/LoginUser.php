<?php

namespace Application\Models\User;


//use Zend\Db\Sql\Sql;
//use Zend\Crypt\Password\Bcrypt;
use Zend\Db\Adapter\Adapter;

//LOCAL
use Application\Models\User\SessionManipulation;
use Application\Models\User\User;

class LoginUser extends User{
    
   

    public function __construct(Adapter $adapter) {
       //  $this->adapter=$adapter;
          parent::__construct($adapter);
    }
    public function set_data($post) {
        parent::set_data($post);
    }
    
    
    
     public function zapamti_me() {
          $results=$this->get_by_email($this->data['login_email']);
          $id=$results['id_users_nk'];
          $token = uniqid('', true);
          $date=date ("Y-m-d", time() + 365 * 60 * 60 * 24);
          $this->call_procedure($this->adapter,'zapamti_me', [$id,$token,$date]);
          SessionManipulation::zapamti_me($id, $token);
     }
    
     public function validate_login() {
         if ($this->validate_password()) {
             return TRUE;
         }
        return FALSE;
    }
    
    

    
    
    
}


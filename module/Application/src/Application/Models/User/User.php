<?php
namespace Application\Models\User;

use Zend\Db\Adapter\Adapter,
Application\Models\Database\Procedure,
Zend\Crypt\Password\Bcrypt,
Zend\Mail\Message,
Zend\Mime\Message as MimeMessage,
Zend\Mime\Part as MimePart,
Zend\Mail\Transport\Sendmail,
Zend\Validator\Db\NoRecordExists,
Zend\Validator\EmailAddress;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

 
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\Feature\RowGatewayFeature;




//local
use Application\Models\User\SessionManipulation,
Application\Newsletters\Templates\ZaboravljenaLozinka;

/**
 * Ova classa radi sa podatcima o useru i o svemu sto ima veze sa 
 * logovanjem
 * --------------
 * Koristi procedure
 * ---------------------------
 * vraca podatke o useru na razne nacine update columns ,
 * tip_usera,pravi novog usera, validra login,zapamti me i jos mnogo toga
 * ovo je SUPER CLASS koja je vezana za prevna kao i za fizicka lica
 * 
 * 
 * 
 * 
 * 
 */
class User extends Procedure {
   
    protected $adapter;
    protected $data;
    protected $user_id;
    //email setings
    const INFO="info@nadjinekretnine.com";
    const PODRSKA="podrska@nadjinekretnine.com";
    const MARKETNING="marketing@nadjinekretnine.com";
    const PASS='qwert';
    const SMTP='smtpout.secureserver.net';
    const HOST="nadjinekretnine.com";

    public function __construct(Adapter $adapter,$user_id) {
        $this->adapter=$adapter;
        $this->user_id=$user_id;
               
     }
     public function get_user_id() {
         return $this->user_id;
       }
     /**
      * 
      * @param type $post
      * set $_POST data golobaly to variable protected $data;
      */
     public function set_data($post) {
        $this->data=$post;
     }
    
    /**
     * 
     * @param type $email
     * @return boolean
     * 
     * get user data by email
     */
    public function get_by_email($email) {
        $data=$this->call_procedure($this->adapter,'Get_by_email', [$email]);
        if (count($data)>0) {
            return $data[0];
        }
        return false;
    }
    
    /**
     * 
     * @param type $id
     * @return boolean
     * get user data by id
     * 
     */
            public function get_by_id() {
        $data=$this->call_procedure($this->adapter,'Get_user_by_id', [$this->user_id]);
        if (count($data)>0) {
            return $data[0];
        }
        return false;
    }
    
    
    /**
     * Tip usera (pravno ili fizicko lice)
     * 
     * return:
     * -------
     * vraca 1 za fizicka lica ili 2 za pravna lica
     * 
     * 
     */
    public function user_type(){
               $gateway = new TableGateway('users_nk', $this->adapter, new RowGatewayFeature('id_users_nk'));
          $results = $gateway->select(array('id_users_nk' =>  $this->user_id));
          $data = $results->current();
          if ($data) {
              return $data->user_oglasivac;
          }
        
        return false;
        
    }
    
    /**
     * 
     * @return boolean
     * 
     * PODATCI O PRAVNIM LICIMA (naziv firme, ime vlasnika...)
     */
    public function get_pravna_lica_data(){
                $gateway = new TableGateway('pravna_lica', $this->adapter, new RowGatewayFeature('id_pravna_lica'));
          $results = $gateway->select(array('id_users_nk' =>  $this->user_id));
          $data = $results->current();
          
          if ($data) {
              return $data;
          }
        
        return false;
               
    }
    /**
     * 
     * 
     * 
     * VRACA PODATKE O PRAVNIM LICIMA IZ TABELA:
     * -users_nk
     * -pravna_lica_trenutno_stanje
     * -istorija_uplate_pravna_lica
     */
    public function pravna_lica_all_data() {
       $gateway = new TableGateway('users_nk', $this->adapter);
        
        $select=$gateway->select(function($select){
            $select->where([
               'users_nk.id_users_nk'    => $this->user_id, 
            ]);
            $select->join('istorija_uplate_pravna_lica','istorija_uplate_pravna_lica.id_users_nk=users_nk.id_users_nk',[],'left');
            $select->join('pravna_lica_trenutno_stanje','pravna_lica_trenutno_stanje.id_users_nk=users_nk.id_users_nk',['trenutno_stanje'],'left');
        });
        $data=$select->current();
     //   var_dump($data);
        if ($data) {
            return $data;
        }
        return false;
        
        
    }
    
    
    
    public function on_off_user_validation($on_off=true){
          $gateway = new TableGateway('users_nk', $this->adapter, new RowGatewayFeature('id_users_nk'));
          $results = $gateway->select(array('id_users_nk' =>  $this->user_id));
          $data = $results->current();
           
          if ($data) {
                $data->user_validation = $on_off===true?1:0;
                $data->save();
                return true;
             }
          
            return false;
     }
    
    
    /**
     * 
     * @param type $post
     * @return boolean
     * 
     * 
     * 
     * Insert ili update deklaracija o oglasavanju
     * tabela: pravna_lica
     * 
     * 
     * 
     * 
     */
    
    public function pl_deklaracija($post){
        
        $validator = new \Zend\Validator\Db\NoRecordExists([
            'table'     =>'pravna_lica',
            'field'     =>'id_users_nk',
            'adapter'   =>$this->adapter,
        ]);
 $sql=new \Zend\Db\Sql\Sql($this->adapter);
$data=[
       'id_users_nk'        =>$this->user_id,
        'naziv_firme'       =>$post['naziv_firme'],
        'ulica'         =>  $post['ulica'],
        'pib'=>  $post['pib'],
        'maticni_broj'=>  $post['maticni_broj'],
        'ime_prezime'=>  $post['ime_prezime'],
        'telefon'=>  $post['telefon'], 
        'datum_postavljanja_deklaracije'    =>date("Y-m-d H:i:s")
    ];
if ($validator->isValid($this->user_id)) {
  //nema deklaracije
    $q=$sql->insert('pravna_lica');
    $q->values($data);  
   
} else {
    //deklaracija vec postoji
    $q=$sql->update('pravna_lica');
    unset($data['id_users_nk']);
    $q->set($data);
    $q->where([
        'id_users_nk'       =>$this->user_id
    ]);
}
        
  $statement = $sql->prepareStatementForSqlObject($q);
         $results = $statement->execute();
 return true; 
        
    }
    
    
    
    
    
    
    
    
    
    
    
    /**
     * 
     * @param type $email
     * @param type $pass
     * @return boolean
     * 
     *   
  'reg_uslovi_koriscenja' => string '1' (length=1)
  'reg_submit' => string 'Registruj se' (length=12)
  'user_ip' => string '127.0.0.1' (length=9)
     * 
     */
    public function create_user($data) {
         $bcrypt = new Bcrypt();
         $pass = $bcrypt->create($data['reg_password']);
        
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $insert=$sql->insert('users_nk');
        $insert->values([
            'user_name'                   =>$data['reg_username'],
            'email_nk'                    =>$data['reg_email'],
            'password_nk'                 =>$pass,
            'user_oglasivac'              =>$data['oglasivac_nekretnine'],
            'date_creation'              =>date("Y-m-d H:i:s"),
            'last_time_login_date'        =>date("Y-m-d H:i:s"),
            'user_validation'             =>($data['oglasivac_nekretnine']==1?1:2),
            'ip_address'                  =>$data['user_ip']
            
        ]);
      
         $statement = $sql->prepareStatementForSqlObject($insert);
         $results = $statement->execute();
         if ($results->count()==1) {
             return true;
         }
        
         return false;
      /*   if ($this->call_procedure($this->adapter,'Create_new_user', [$username,$email,$pass,$oglasivac])) {
             return true;
         }
        return false;*/
      }
  
    /**
     * 
     * @param type $id
     * @param type $email
     * OPIS:
     * koristi se za zabravljenu lozinku: salje email sa linkom i hashom koji se nalazi u bazi
     * u tabeli reset_pass...
     * 
     */
      public function reset_pass($id,$email) {
          $token=  md5(uniqid('', true));
          
          
          
          
    //-----------------------------------------      
       $html = new MimePart(ZaboravljenaLozinka::load_html($token));
         $html->type = "text/html";
         $body = new MimeMessage();
         $body->setParts(array($html));
         
         $message = new Message();
         $message->setEncoding("UTF-8");
         $message->setBody($body);

 $message->addTo($email, 'Zaboravljena lozinka')
        ->addFrom(self::PODRSKA)
        ->setSubject('Zaboravljena lozinka');     



    
// Setup SMTP transport using LOGIN authentication
$transport = new SmtpTransport();
 $options   = new SmtpOptions(array(
    'name'              => self::HOST,
    'host'              => self::SMTP,
    'port' => 80,
    'connection_class'  => 'login',
    'connection_config' => array(
        'username' => self::PODRSKA,
        'password' => self::PASS,
    ),
)); 
    $transport->setOptions($options);
    $transport->send($message);
//-------------------------------------------------
     
        
          $this->call_procedure($this->adapter,'Reset_pass', [$id,$token]);
     
      }
      
      /**
       * 
       * @param type $token
       * @return boolean
       * 
       * Trazi token koji je poslat emailom kada se aktivira zaboravljena lozinka
       */
      public function reset_pass_serach_token($token) {
        $search=  $this->call_procedure($this->adapter,'reset_pass_search_token', [$token]);
        if (count($search)>0) {
            return $search[0];
        }
        return false;
      }
  
      /**
       * 
       * @param type $id
       * @param type $pass
       * 
       * MENJA USER PASSWORD
       */
      public function new_password($id,$pass) {
             $bcrypt = new Bcrypt();
             $pass = $bcrypt->create($pass);
              $this->call_procedure($this->adapter,'Change_user_password', [$id,$pass]);
      }

      
      
      
      public function podrska($name,$message_text) {
         $html_mail="<h3>Ime: $name</h3>"
                 . "<h3>User id: $this->user_id</h3>"
                 . "<p>$message_text</p>"; 
          
         $html = new MimePart($html_mail);
         $html->type = "text/html";
         $body = new MimeMessage();
         $body->setParts(array($html));
         $message = new Message();
         $message->setEncoding("UTF-8");
         $message->setBody($body);


         $message->setFrom('no-replay@nadjinekretnine.com', 'NadjiNekretnine.com');
         $message->addTo('qwertalexa@yahoo.com', 'Pomoc i podrska');
         $message->addTo('alexa.manasijevic85@gmail.com', 'Pomoc i podrska');
         $message->setSubject('Pomoc i podrska');
        
            $transport = new Sendmail();
            $transport->send($message);
            
            
   $gateway = new TableGateway('pomoc_podrska', $this->adapter, new RowGatewayFeature('id_pomoc_podrska'));     
          $gateway->insert([
             'id_users_nk'      =>  $this->user_id,
              'name'            =>  $name,
              'message'         =>  $message_text
          ]);
          
          
          
          
      }
      
      
       

   
      /**
       * 
       * @param type $new_email
       * @param type $pass
       * @return boolean
       * 
       * UPDATE EMAIL NA STRANI moj-profil
       * 
       * 
       */
      public function update_email($new_email,$pass) {
          $gateway = new TableGateway('users_nk', $this->adapter, new RowGatewayFeature('id_users_nk'));
          $results = $gateway->select(array('id_users_nk' =>  $this->user_id));
          $data = $results->current();
          
          $validation=$this->login_validation_email_or_username_pass($data->email_nk,$pass);
          $check_email_availability=  $this->check_email_availability($new_email);
          
          if ($validation&&$check_email_availability&&$data) {
                $data->email_nk = $new_email;
                $data->save();
                return true;
             }
          
            return false;
     }
      public function update_username($new_username,$password) {
           $gateway = new TableGateway('users_nk', $this->adapter, new RowGatewayFeature('id_users_nk'));
          $results = $gateway->select(array('id_users_nk' =>  $this->user_id));
          $data = $results->current();
          
          $validation=$this->login_validation_email_or_username_pass($data->email_nk,$password);
          if ($validation&&$data) {
                     $data->user_name=$new_username;
                     $data->save();
                     
                     SessionManipulation::update_session($new_username);         
                     
                     
             return true;
          }
          
            return false;        
         
     }     
      
     public function update_password($new_password,$old_password) {
           $gateway = new TableGateway('users_nk', $this->adapter, new RowGatewayFeature('id_users_nk'));
          $results = $gateway->select(array('id_users_nk' =>  $this->user_id));
          $data = $results->current();
          
          $validation=$this->login_validation_email_or_username_pass($data->email_nk,$old_password);
          if ($validation&&$data) {
                $bcrypt = new Bcrypt();
                $pass = $bcrypt->create($new_password);             
                     $data->password_nk=$pass;
                     $data->save();
             return true;
          }
          
            return false;        
         
     }
      
      
    public function update_pl_data($post){
        $gateway = new TableGateway('users_nk', $this->adapter, new RowGatewayFeature('id_users_nk'));
          $results = $gateway->select(array('id_users_nk' =>  $this->user_id));
          $data = $results->current();
           
          if ($data) {
           
                   $data->ime_oglasivac=$post['mp_ime'];
                     $data->mesto_oglasivac=$post['mp_mesto'];
                     $data->ulica_oglasivac=$post['mp_ulica'];
                   $data->telefon_oglasivac=$post['mp_telefon'];
                     $data->mobilni_oglasivac=$post['mp_mobilni'];
                     $data->website=$post['mp_website'];
                     $data->logo_oglasivac=is_array($post['mp_logo'])?NULL:$post['mp_logo'];   /*  */
                     $data->save(); 
            }
        }
     
     
      
     
     
      
      /**
       * 
       * @param type $id
       * @return type
       * VRACA NEWSLETTER
       */
      public function get_newsletter() {
         return $this->call_procedure($this->adapter,'Get_newsletter', [$this->user_id]); 
      }
      /**
     * 
     * @return boolean
     * 
     * validate password
     */
      public function validate_password($email=false,$pass=false) {
         $post=  $this->data;
         if (!$email&&!$pass) {
         $email=$post['login_email'];
         $pass=$post['login_pass'];             
         }
        
         $data=$this->get_by_email($email);
         
         if ($data) {
             $bcrypt = new Bcrypt();
             $securePass = $data['password_nk'];
             if ($bcrypt->verify($pass, $securePass)) {
                 return true;
             }
         }
         return FALSE;
    }
    
    
    
    
    /**
     * 
     * @param type $email_or_username
     * @param type $pass
     * @return type
     * 
     * 
     * 
     * NA prijava-registracija STRANI VALIDIRA KADA USER HOCE DA SE LOGUJE 
     * 
     */
    public function login_validation_email_or_username_pass($email_or_username,$pass,$return_adapter=false) {
          $validator = new EmailAddress();
            if ($validator->isValid($email_or_username)) {
                $field="email_nk";
            } else {
                $field="user_name";
            }                   
                $authAdapter = new \Zend\Authentication\Adapter\DbTable\CallbackCheckAdapter(
               $this->adapter,
               'users_nk', // Table Name
               $field, // IdentityColumn
               'password_nk', // Credential Column
               function($securePass, $password){
               $bcrypt = new \Zend\Crypt\Password\Bcrypt();
               $authenticated = $bcrypt->verify($password, $securePass);
               return $authenticated;
               }
               );
               $authAdapter ->setIdentity($email_or_username)->setCredential($pass);  
                $authAdapter->authenticate();
                if ($return_adapter) {
                    return $authAdapter; 
                }
               return $authAdapter->getResultRowObject();                 
                    
    }
    
    
    

    
    
    
    
    
    
    
    
    /**
     * 
     * @param type $username
     * @return boolean
     * 
     * ako username nepostoji vraca TRUE;
     * 
     */
    public function check_username_availability($username) {
                   $validator = new NoRecordExists([
                            'table' => 'users_nk',
                            'field' => 'user_name',
                            'adapter'=>  $this->adapter,
                     ]);

                    if ($validator->isValid($username)) {
                        return TRUE;
                    } 
                    return false;
    }
    
    
    /**
     * 
     * @param type $email
     * @return boolean
     * AKO EMAIL NEPOSTOJI U BAZI ONDA RETURN TRUE;
     */
       public function check_email_availability($email) {
                   $validator = new NoRecordExists([
                            'table' => 'users_nk',
                            'field' => 'email_nk',
                            'adapter'=>  $this->adapter,
                     ]);

                    if ($validator->isValid($email)) {
                        return TRUE;
                    } 
                    return false;
    } 
    
    
    
    
    
    
    
    /**
     * 
     * @param type $id
     * @param type $email
     * @return boolean
     * 
     * Proverava da li je kombinacija id,email postoji u bazi
     * sesion cuva id i email pa proveerava da li su podatci u session validni.
     * Koristi se i prilikom validacije cookies
     */
    public function validate_user($id,$username) {
        if (!isset($id,$username)) {return false;}
        
 // Configure the instance with constructor parameters...
$authAdapter = new AuthAdapter($this->adapter,'users_nk', 'id_users_nk','user_name');
$authAdapter->setIdentity($id)->setCredential($username); 
       $authAdapter->authenticate();

return $authAdapter->getResultRowObject();   
       
    }
    
    
    /**
     * 
     * @param type $email
     * Kreira session
     */
        public function createSession($data,$sess_name='login') {
            if (is_object($data)) {
            SessionManipulation::create_user_session($data,$sess_name);
            $this->call_procedure($this->adapter,'Update_users_login_datetime', [$data->id_users_nk]);           
            return true;
            }
return false;
    }
    
    
    
       
    
    
    
    
    
    
    
    
    /**
     * 
     * @param type $cookie
     * @return boolean
     * 
     * Validira cookies tako sto poziva proceduru  get_zapami_me();
     * update cookie token u bazi i u browseru
     * 
     */
    public function validate_cookie($cookie) {
         if (!$cookie) {return false;}
         $id=$cookie['id'];
         $token=$cookie['token'];
         $zapamti=$this->call_procedure($this->adapter,'get_zapamti_me', [$id,$token]);
               
         if (count($zapamti)>0) {   
            $data=  $this->get_by_id($id);          
            if ($data&&$validate_all_data=$this->validate_user($data['id_users_nk'], $data['user_name'])) {
                $this->createSession($validate_all_data);
                //update cookie token------------------------------------------
                 $d=$data['id_users_nk'];
                 $tokenn = uniqid('', true);
                 $date=date ("Y-m-d", time() + 365 * 60 * 60 * 24);
                 $this->call_procedure($this->adapter,'zapamti_me', [$id,$tokenn,$date]);   
                 SessionManipulation::zapamti_me($id, $tokenn);
                 //-------------------------------------------------------------
                return TRUE;
            }
         }
         return false;
    }
    
    
    
         public function zapamti_me($id) {
          $token = uniqid('', true);
          $date=date ("Y-m-d", time() + 365 * 60 * 60 * 24);
          $this->call_procedure($this->adapter,'zapamti_me', [$id,$token,$date]);
          SessionManipulation::zapamti_me($id, $token);
     }
    
    
    
    
    
    /**
     * 
     * @param type $login_guest
     * @return string
     * 
     * vraca variabillu u zavisnosti da li je korisnik ulogovan u sustini menja
     * linkove u layout.phtml (prijava | registracija u Moj profil)
     * 
     */
    public function render_html($login_guest) {
         if ($login_guest) {
             $sess=SessionManipulation::get_user_session();
             
             
        $ulogovan='<li class="dropdown no-decoration"><a href="http://nadjinekretnine.com/moj-profil" class="dropdown-toggle btn btn-mini btn-link" data-toggle="dropdown" data-hover="dropdown" data-delay="200" data-close-others="false"> 
         <i class="fa fa-user"></i>   '.$sess["username"].' </a>    <ul class="dropdown-menu">
                                     <li><a id="" href="http://nadjinekretnine.com/moj-profil">
                                       <img   src="/img/tmp/user1.png"/>
                                            Moj profil</a>
                                    </li>
                                                        
                                    
                                     <li><a id="" href="http://nadjinekretnine.com/moj-profil/moji-podatci">
                                       <img  src="/img/tmp/list_moji_podatci.png"/>
                                            Moji podatci</a>
                                    </li>
                                    <li><a id="" href="http://nadjinekretnine.com/ostavi-oglas">
                                       <img  src="/img/tmp/dodaj_novi_oglas.png"/>
                                            Dodaj novi oglas</a>
                                    </li>
                                    <li><a id="" href="http://nadjinekretnine.com/moj-profil/moji-oglasi">
                                       <img src="/img/tmp/lista_oglasa.png"/>
                                            Moji oglasi</a>
                                    </li>
                                    <li><a id="" href="http://nadjinekretnine.com/moj-profil?action=logout">
                                       <img src="/img/tmp/exit.png"/>
                                           Odjavi se</a>
                                    </li>                 
                                    
                                    
                                    
                                        </ul></li>';      
        return $ulogovan;
             
        }

        $guest='<li><a class="btn btn-mini btn-link" href="http://nadjinekretnine.com/prijava-registracija">'
                . '<i class="fa fa-hand-o-right"></i> Prijava / Registracija</a></li>';
    
       
        return $guest;
        
    }
    
    

    
    
}

 

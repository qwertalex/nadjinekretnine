<?php
namespace Application\Models\User;

use Zend\Form\Form;



class FormChangeEmail extends Form {
    
    
    public function __construct() {
        
             
        parent::__construct('promeni_email');//ime forme
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        //$this->setInputFilter(new \OstaviOglasForm\Models\FilterOstaviOglasForm());//filter
        
                  
     #new_email-------------------------------------         
        $this->add(array(
            'name'=>'new_email',
            'attributes'=>array(
                'type'=>'text',
            // "style"=>"height:30px;",
                "id"=>"input_new_email",
                
            ),
           
            
        ));
     #-----------------------------------------    
           
        
      #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit_new_email',
            'attributes'=>array(
                'type'=>'submit',
               'class'=>'btn btn-warning arrow-right',
                'value'=>'Sacuvaj izmene',
                'id'=>'promena-email-addr',
               
            ),
           ));
        
        #-----------------------------------------     
        
        
    }
    
    
    
    
    
    
}




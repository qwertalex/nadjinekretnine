<?php

namespace Application\Models\User;

use Zend\Db\Sql\Sql;
use Zend\Crypt\Password\Bcrypt;


/**
 * 
 * Ova class ubacuje podatke u bazu prilikom registracije
 * novog korisnika!
 * 
 * 
 * 
 * 
 * 
 */
class RegisterNewUser {
   
    
    protected $adapter;


    public function __construct(Adapter $adapter) {
       $this->adapter=$adapter;
    }
    
    
    /**
     * 
     * @param array $post
     * 
     * OPIS:
     * INSERT u tabelu users_nk
     * prilikom registracije novog korisnika
     * 
     * 
     */
    public function insert($post) {
        $sql = new Sql($this->adapter);
        $insert=$sql->insert("users_nk");
        $bcrypt = new Bcrypt();
         
        $data=[
              'email_nk'=>$post['reg_email'],
              'password_nk'=>$bcrypt->create($post['reg_password']),
              'date_creation'=>date( 'Y-m-d H:i:s',time()),
              'last_time_login_date'=>date( 'Y-m-d H:i:s',time()),
              'user_oglasivac'=>$post['oglasivac']
        ];
          
        $insert->values($data);
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        if ( $results->count() === 0 ) {die('Doslo je do greske, molimo vas probajte za nekoliko minuta!');}
    
         
      }
      
      
      
      
      
      
      
      
      
      
      
      
      
      
    
    
    
    
}



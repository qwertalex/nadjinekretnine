<?php

namespace Application\Models\User;

use Zend\Session\Container;
use Zend\Http\Header\SetCookie;
//local

/**
 * 
 * Staticna classa koja pravi sesson za usera i cookie za usera,
 * brize session i cookies
 * vraca session i cookie
 * 
 */
class SessionManipulation {

    
    /**
     * 
     * @param string $sess_name
     * 
     * OPIS:
     * brise odredjenu session
     * 
     */
    public static function delete_session($sess_name) {
        $container=new Container($sess_name);
       $container->getManager()->getStorage()->clear($sess_name);
    }
    
    /**
     * 
     * @param type $sess_name
     * klasican logout
     */
    public static function logout($sess_name) {
        self::delete_session($sess_name);
        self::delete_zapamti_me();
        if (isset($_COOKIE['autolog'])) {
            unset($_COOKIE['autolog']);
        }
    }
    
    /**
     * 
     * @param string $email
     * 
     * OPIS:
     * pravi session sa key user i value email od usera
     * 
     * token i active se ne koriste
     * 
     */
    public static function create_user_session($data) {
         $container = new Container("login");
         $container->login = [
             'id'=>$data->id_users_nk,
             'username'=>$data->user_name,
             'token'=>  uniqid("", TRUE),
             'active'=>true
         ];
        
    }
    
    
    /**
     * 
     * @return session
     * 
     * 
     * 
     * 
     */
    public static function get_user_session() {
         $container = new Container("login");
          return $container->login;      
        
    }
    
    public static function update_session($username) {
         $container = new Container("login");
         $container->login['username'] =$username;
         var_dump(self::get_user_session());
    }
    /**
     * 
     * @param string $email
     * OPIS:
     * Pravi cookie sa name login_user i value email od usera
     * aktivira se kada se check checkbox prilikom registracije
     * 
     * 
 */
    public static function zapamti_me($id,$token) {
            setcookie('autolog',  json_encode(['user_id'=>$id,'token'=>$token]),time() + 365 * 60 * 60 * 24,'/','nadjinekretnine.com',false,TRUE);
     }
     public static function delete_zapamti_me() {
         setcookie('autolog', 'false',time()-3600,'/','nadjinekretnine.com',false,TRUE);
     }

     public static function get_zapamti_me() {
         if (isset($_COOKIE['autolog'])) {
                 $cookie=$_COOKIE['autolog'];       
                 $cookie=json_decode($cookie,TRUE);
                 if (is_array($cookie)&&isset($cookie['user_id'],$cookie['token'])) {
                         $id=(int)$cookie['user_id'];
                        $token=(string)$cookie['token'];    
                        return ['id'=>$id,'token'=>$token];
                 }
         }
         return false;
     }

    
    
}




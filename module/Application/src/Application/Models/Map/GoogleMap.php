<?php

namespace Application\Models\Map;

use Application\Models\Map\SearchForm;


class GoogleMap extends SearchForm
{
  
    
    public function mapOpenHtmlTags() {
        return '<div class="map-wrapper">
    <div class="map">
        <div id="map" class="map-inner"></div><!-- /.map-inner -->

        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="property-filter pull-right ">
                        <div class="content ">';
        
    }


    public function form() {
        return new SearchForm();
        
    }
    
    public function mapCloseHtmlTags() {
        return '</div><!-- /.content -->
                    </div><!-- /.property-filter -->
                </div><!-- /.span3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.map -->
</div><!-- /.map-wrapper -->';
        
    }
    
    
    
}







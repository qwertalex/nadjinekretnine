<?php
namespace Application\Models\PlaceniOglasi;

use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;


class PremiumOglasi {
    
    
    
     protected $adapter;
    
     public function __construct($adapter) {
        $this->adapter=$adapter;
       }
      
       
      /**
       * 
       * 
       * iz tabele tip_oplasa vadi id_oglasa_nk gde kolona premium=1
       * sve idjeve stavlja u array
       * 
       * return array || false
       * 
       */ 
       
       public function get_premium() {
          $sql = new Sql($this->adapter); 
           $select=$sql->select();
           $select->from("tip_oglasa")->where("premium=1");
             $selectString = $sql->getSqlStringForSqlObject($select);
             $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);           
          
            if ($results->count()>0){
                $results = array_values(iterator_to_array($results));
                foreach ($results as $key => $value) {
                    $id_oglasi[]=$value->id_oglasi_nk;
                }
               return $id_oglasi;
                }
             return FALSE;
        }
    
    
       

/**
 * 
 * @return array
 * 
 * prvo uzima sve idijeve u array iz funkcije $this->get_premium()
 * vraca array sa svim podatcima + slika iz tabele oglasi_nk i slika iz tabele slike
 * 
 * 
 * 
 * 
 */


       
       protected function get_oglasi_nk() {
           
             $id_oglasi=  $this->get_premium();
             if (!is_array($id_oglasi)) { return FALSE;}
             $sql = new Sql($this->adapter); 
             $return_arr=[];
             foreach ($id_oglasi as $id) {       
                 
                 $select=$sql->select();
                 $select->from("oglasi_nk")->where("id_oglasi_nk=".$id);
                 $selectString = $sql->getSqlStringForSqlObject($select);
                 $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);     
             
                 if ($results->count()>0){
                     $results = array_values(iterator_to_array($results));
                     $results[0]->slika= $this->get_slike($results[0]->id_oglasi_nk);
                     $return_arr[]=$results;
              
                 }
                }
      return $return_arr;
      }
       
       
     /**
      * vraca sliku ako postoji 
      * param je id (id_oglasi_nk) ako postoji taj id u tabeli onda
      * vraca sliku.
      * return string || false
      * 
      * 
      * 
      */ 
       
       protected function get_slike($id) {
           $slika="";
             $sql = new Sql($this->adapter); 
             $select=$sql->select();
             $select->from("slike")->where("id_oglasi_nk=".$id);
             $selectString = $sql->getSqlStringForSqlObject($select);
             $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE); 
            if ($results->count()>0){
                $results = array_values(iterator_to_array($results));
                
                if (!is_null($results[0]->slika_1)) {
                    return $results[0]->slika_1;
                }
                
                }           
               return false;
       }
       
       
       
       
       
       /**
        * 
        * @return string
        * 
        * prikazuje sliku u html
        * 
        */
       
       
       public function show_curasel_image() {
          $oglasi_nk=  $this->get_oglasi_nk();
          
        
           $str= '<div class="carousel-wrapper">
<div class="carousel">
<div class="content">
<h2 class="page-header">Premium oglasi</h2>
<a class="carousel-prev" href="detail.html" style="display: block;">Previous</a>
<a class="carousel-next" href="detail.html" style="display: block;">Next</a>
<div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 900px; height: 286px; margin: 0px 0px 10px 25px; overflow: hidden;"><ul style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 13500px; height: 286px; z-index: auto;">
';
           
        foreach ($oglasi_nk as $key => $v) {
             $prodaja_izdavanje="";
                    if ($v[0]->prodaja==1) {
                       $prodaja_izdavanje="prodaja";
                    } 
 else {$prodaja_izdavanje='izdavanje';}
//$this->img_resize($v[0]->slika);           
            if (!$v[0]->slika) {
                $v[0]->slika='no_images.png';
            }
 
           $str.='<li>
    <div title="'.$v[0]->naslov.'" class="image">
        <a href="'.$v[0]->link.'"></a>
        <img style="width:265px;height:200px;" src="/img/tmp/tmp/'.$v[0]->slika.'" alt="">
    </div>
    <!-- /.image -->
    <div class="title">
        <h3><a class="width_200" title="'.$v[0]->naslov.'" href="'.$v[0]->link.'">'.$v[0]->naslov.'</a></h3>
    </div>
    <!-- /.title -->
    <div class="izdavanje-prodaja">'.$prodaja_izdavanje.'</div>
    <!-- /.location-->
    
  <div style="padding:5px 10px 0 10px;">
  
<span style=" font-weight:bolder;float:left " class="key">'.$v[0]->kvadratura.'m<sup>2</sup></span> 
   <span class="premium-cena">€ '.number_format($v[0]->cena,0,'.','.').'</span><!-- .price -->
              
                
</div>       
    
</li>';         
        }
    $str.= '</ul></div>
</div>
<!-- /.content -->
</div>
<!-- /.carousel -->
</div>';
     
      return $str;   
           
       }
       
       
     
           
           
           
           
           
           
           
      
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
       
}



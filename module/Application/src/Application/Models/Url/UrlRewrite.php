<?php
namespace Application\Models\Url;

//local
use Application\Models\LokacijaSr\Lokacija;
use Application\Models\Oglasi\Render\RenderView as Html;

/*
 * 
 * 
 * kada se klikne na pretragu na naslovnoj strani onda se salje ajaxom
 * params i u osnovu parametara se pravi url
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
class UrlRewrite {
 
    protected $url_map='oglasi-mapa';
    protected $url_lista='oglasi-lista';
    protected $get;

    const URL='http://nadjinekretnine.com/pretraga-oglasa/';

    public function __construct($get) {
        $this->get=$get;
 
    }

/*
 * 
 * 
 * 
 * ovoo je obustavljeno jer je odluceno da se uradi redirekcija u javascriptu
 * tj. url rewrite
 */
    public function pretraga_rewrite() {
        $data= $this->get;
       // var_dump($data);
        $url=self::URL;
        $required=['kategorija','mesta_srbija','prodaja_izdavanje'];
        

        
        if (!isset($data[$required[0]],$data[$required[1]],$data[$required[2]])) {
            return 'No default params:kategorija,mesta_srbija,prodaja_izdavanje';
        }
        
        //KATEGORIJA
        $kategorija=$this->kategorija();
        if (!$kategorija) {
          return('========= KATEEGORIJA NIJE VALIDNA ======================');  
        }
        $url.=$kategorija;
        //--------------------------------------------
        
       //PRODAJA IZDAVANJE 
         $pro_izd=$this->prodaja_izdavanje($data['prodaja_izdavanje']);
         $url.='/'.$pro_izd;       
      //-----------------------------------------------------------------------  
        
         //MESTA SRBIJA
         if (strpos($data['mesta_srbija'],',')) {
            $ms=$data['mesta_srbija'];
            $ms=array_filter(explode(",",$ms));
            
            foreach ($ms as $k => $v) {
                $ms[$k]=self::ready_for_url(Html::get_search_location($v));
                if (!$ms[$k]) {
                  //  var_dump($ms[$k]);
                  return('========= Mesto NIJE VALIDNO ======================');   
                }
              }
            $ms=implode('_',$ms);
         }else{
            $ms=self::ready_for_url(Html::get_search_location($data['mesta_srbija']));
          }

         if (!$ms) {
            $ms='srbija';
         }
         $url.='/'.$ms;
         //   return $ms; 
          
         
         /*
          * 
          * 
          * optional route params
          * 
          * 
          * 
          * 
          * 
          * 
          * 
          * 
          */
         
         
         
         
         
         
         
         
         
         
         
         
         //SAMO SA LIKOM
         if (isset($data['samo-sa-slikom'])&&(int)$data['samo-sa-slikom']>0) {
             $url.='/samo-sa-slikom';
         }
         
         //---------------------------------------------------------
         
         //CENA 
         $url.=$this->min_max_url('/cena/',$data['cena_od'],$data['cena_do']);
   
          //kvadratura 
         $url.=$this->min_max_url('/kvadratura/',$data['kvadratura_od'],$data['kvadratura_do']);
             
       //------------------------------------------------------------------------          
         
         
         
    
        
        
             
        
        
        
        
        
      
        
        
        
        
        
        
        
        
        
       //  return $data;
       return $url; 
    }
    
    
    
    
    
    
    protected function min_max_url($route,$min=null,$max=null) {
             
             $min=is_numeric($min)?$min:0;
             $max=is_numeric($max)?$max:0;
        
            if ((int)$min<(int)$max||
                    (int)$min===(int)$max||(int)$min>0&&(int)$max===0
                    ||((int)$min!==0&&(int)$max!==0)) {
                
                    if ((int)$max>0) {
                       $route.=$min.'-'.$max;
                   }else{
                       if ($min==0) {
                           return "";
                       }
                       $route.=$min;
                   }
              return $route;
            }
             
       return "";
     }
    
    
    
    
    
    public static function ready_for_url($str) {
        if (trim($str)==="") {
            return false;
        }
         $str=Html::to_utf8($str);
         if (\strpos($str,'/')!==false) {
           $str= strtolower(str_replace("/","",$str));
         $str=array_filter(array_map('trim', explode(' ', $str))); 
         return \implode('-', $str);           
         }
         return \strtolower($str);
    }
    
    
    
    
    
    
    
    
    
    
    



    public function change_url() {
         $get=  $this->get;
         $url=self::URL;
         $url.=  $this->kategorija();
         $url.='/'.$this->prodaja_izdavanje();
         $url.='/'.$this->lokacija();
         $this->sa_slikom()!=""?$url.='/'.$this->sa_slikom():"";
         $this->cena()!==""?$url.='/'.$this->cena():"";
         $this->kvadratura()!==""?$url.='/'.$this->kvadratura():"";
         $url.=$this->po_strani();
         return $url;
     }
     

     protected function po_strani() {
         return '/po-strani-10';
     }

     
     
     
     
     
     
     
     /**
      * 
      * @return type
      * 
      * 
      * 
      * Opis:
      *-------
      * Kada user izabere lokaciju na naslovnoj strani moze da izabere mesto,deo mesta, lokaciju
      * -MESTO:dobije id od mesta npr Beograd(id=1)
      * -DEO MESTA: npr:Bograd / Opstina vozdovac(id=1-2000) od cega je id 1=beograd && Opstina Vozdovac=2000
      * - LOKACIJA: npr: Beograd / opstina vozdovac / brace jerkovic (id=1-2000-10332)
      *  od cega je id 1=beograd && vozdovac=2000 && 10332=brace jerkovic
      * -------------------------------------------------------------------------
      * Menja se cirilica u latinicu i zatim radi se explode za zarez(,) sto znaci da 
      * podrzava vise lokacija tj. user moze da izabere vise lokacija i one se salju u formatu: 1,2,1-2000-10000,3 (ovo su id lokacija)
      * -------------------------------------------------------------------
      * Zatim proverava da li ima u $get['lokacija'] postoji  karakter - zatim:
      * ako ne postoji onda je  u pitanju mesto  i poziva \Application\Models\LokacijaSr\Lokacija::get_mesta_srbija_text($v)
      * ako postoji karakter - onda ako ima length 2 onda je deo_mesta
      * ako je length 3 onda je lokacija
      * ------------------------------------
      * rezultat:
      * http://nadjinekretnine.com/pretraga-oglasa/sobe/prodaja/beograd-opstina-cukarica-nis-medijana-bozidar-adzija 
      * 
      * 
      */
     protected function lokacija() {
         $lok=  $this->get['lokacija'];
         if (!$lok) {
             return 'srbija';
         }
         $str="";
             $cir=['č','ć','đ','š','ž','Č','Ć','Đ','Š','Ž'];
             $lat=['c','c','d','s','z','C','C','D','S','Z'];    
             
             
             
             if (strpos($lok, ",")) {
                     $arr=  explode(',', $lok);
             }else {$arr[]=$lok;}             
             
             
             
            foreach ($arr as $v) {
                if (strpos($v, '-')===false) {
         if (in_array($v, range(1,179))) {
             $mesto=  \Application\Models\LokacijaSr\Lokacija::get_mesta_srbija_text($v);
             if ($mesto) {
                  $str.=  str_replace($cir,$lat,str_replace(" ","-",mb_strtolower($mesto,'UTF-8'))).'-';
             }                    
                }

         }else{
                $v=  \explode("-", $v);
                 if(count($v)==2&&in_array($v[1],range(2000,2092))){
                     $ddmesto=  \Application\Models\LokacijaSr\Lokacija::get_mesta_srbija_text($v[0]);
                    $deo_mesta=  \Application\Models\LokacijaSr\Lokacija::get_deo_mesta_srbija_text($v[1]);
                    $deo_mesta=$ddmesto." ".$deo_mesta;
                    if ($deo_mesta) {
                         $str.=  str_replace($cir,$lat,str_replace(" ","-",mb_strtolower($deo_mesta,'UTF-8'))).'-';
                    }
                }elseif(count($v)===3&&in_array($v[2],range(10000,12402))){
                     $lmesto=  \Application\Models\LokacijaSr\Lokacija::get_mesta_srbija_text($v[0]);
                    $ldeo_mesta=  \Application\Models\LokacijaSr\Lokacija::get_deo_mesta_srbija_text($v[1]);
                    $lokacija=  \Application\Models\LokacijaSr\Lokacija::get_lokacija_srbija_text($v[2]);
                    $lokacija=$lmesto.'-'.$ldeo_mesta.'-'.$lokacija;
                    if ($lokacija) {
                        $str.=  str_replace($cir,$lat,str_replace(" ","-",mb_strtolower($lokacija,'UTF-8'))).'-';
                    }
                    
                }
          }
          }
             
            $str=  substr($str, 0,-1);
            return $str;
     }

     
     
     
     
     
     protected function sa_slikom() {
         switch ((int)$this->get['slika']){
             default:$str='';break;
             case 1:$str='samo-sa-slikom';break;
         }
         return $str;
     }


     protected function cena() {
         $str="";
         if (is_numeric($this->get['cena'])&&(int)$this->get['cena']>0) {
             $str='cena-do-'.$this->get['cena'];
           }
        return $str;
     }



     protected function kvadratura() {
         $str="";
         if (is_numeric($this->get['kvadratura'])&&(int)$this->get['kvadratura']>0) {
             $str='kvadratura-do-'.$this->get['kvadratura'];
           }
        return $str;
     }



     protected function prodaja_izdavanje($pro_izd=false) {
         if (!$pro_izd) {
            $pro_izd= $this->get['prodaja_izdavanje'];
         }
         switch((int)$pro_izd){
           //  case 1:$str='prodaja';break;
             case 2:$str='izdavanje';break;
             default:$str='prodaja';break;
         }
         return $str;
     }
     
     protected function kategorija() {
         switch((int)$this->get['kategorija']){
           
                case 1: return'stanovi';
                     case 2:return  'sobe';
                     case 3:return  'kuce';
                     case 4:return  'garaze';
                     case 5:return  'placevi';
                     case 6:return  'lokali';
                     case 7:return  'poslovni-prostori';
                     case 8:return  'hale'; 
                     case 9:return  'magacin';
                     case 10:return  'poljoprivredno-zemljiste';
                     case 11:return  'gradjevinsko-zemljiste';                    
                     case 12:return  'vikendice';
                     case 13:return  'apartmani'; 
                     case 14:return  'selidbe';  
                     case 15:return  'dnevno-izdavanje';
                default:return false;
         }
         
                
     }
     
     
     
             public static function get_kategorija_id($str) {
                 switch($str){
                    case  'stanovi':return 1; 
                     case 'sobe':return 2; 
                     case "kuce":return 3; 
                     case "graze":case "garaze":return 4; 
                     case 'placevi':return 5; 

                     case 'lokali':return 6;  
                     case 'poslovni-prostori':return 7; 
                     case 'hale':return 8; 
                     case 'magacin':return 9; 

                     case 'poljoprivredno-zemljiste':return 10; 
                     case 'gradjevinsko-zemljiste':return 11; 

                     case 'vikendice':return 12; 
                     case 'apartmani':return 13; 
                     case 'selidbe':return 14; 
                     case 'dnevno-izdavanje':return 15;
                 }
             }
     
     
         
         
         
         
         
         
         
         
         
         
         
    
    
}




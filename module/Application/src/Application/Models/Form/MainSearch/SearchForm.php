<?php

namespace Application\Models\Form\MainSearch;


use Zend\Form\Form;

//local



class SearchForm extends Form
{
 
    
    public function __construct() {
        parent::__construct('MainSearch');
        $this->setAttribute('action', 'http://nadjinekretnine.com/pretraga-oglasa/stanovi/prodaja/srbija');
        $this->setAttribute('method', 'get');
        $this->setAttribute('class', 'form-inline');
        $this->setAttribute('style', 'margin-bottom:0;');
        $this->setAttribute('id', 'MainSearch');
        $this->setInputFilter(new \Application\Models\Form\MainSearch\FilterSearchForm());
        
        
        
        
    #kategorija--------------------------------        
       $this->add(array(
             
             'name'=> 'kategorija',
               'attributes'=>array(
                'class'=>'span3',
                   'id'=>'e1',
                  'type'=> 'hidden',
                  'style'=>'margin-top:5px;margin-left:0;'
                  
                 ),
             'options'=> array(
                  'value_options'=>array(),
             )
     ));
      #-----------------------------------------   
        
        


        
         #prodaja izdavanje radio buttons---------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Radio',
             'name'=> 'prodaja_izdavanje',
            'attributes'=>array(
               'class'=>'radio-line-icheck span2',
                "id"=>"radio_prodaja_izdavanje",
                 'value' =>1,
                'style'=>'display:none'
                  ),
                 'options' => array(
                     'value_options' => array(
                            1=> 'Prodaja',
                            2=> 'Izdavanje',
                          //  3=>'U izgradnji'
                     ),
             ),
      ));
      #-----------------------------------------  
       
       
       
       #lokacija--------------------------------        
       $this->add(array(
             'type'=> 'hidden',
             'name'=> 'lokacija',
            'attributes'=>array(
              ///  'class'=>'span5',
                'id'=>'lokacija-index',
               // 'multiple'=>'multiple',
                'style'=>'width:100%'
                
                 ),
             'options'=> array(
                 // 'value_options'=> Lokacija::get_mesta_srbija(),
             )
     ));
      #-----------------------------------------      
       
       
            
  #sa slikom--------------------------------        
       $this->add(array(
             'type'=> 'Zend\Form\Element\Checkbox',
             'name'=> 'sa_slikom',
             'attributes'=>array(
                'id'=>'sa-slikom',
                'class'=>'radio-line-icheck',
                 'style'=>''
                  ),
                              'options' => array(
                                  'label' => 'A checkbox',
                     'use_hidden_element' => true,
                     'checked_value' => 1,
                     'unchecked_value' => 0
             ),
           
     ));
      #-----------------------------------------   
  
       
       
       
       
       
  
       
                          
     #cena do-------------------------------------------
       $this->add(array(
            'name'=>'cena_do',
            'attributes'=>array(
                'type'=>'text',
                'class'=>'span1',
                'placeholder'=>'cena do',
              //  'style'=>'height: 20px;',
                 'style'=>'height:15px;'
                
            ),
         
        ));
        
       #-----------------------------------------     
       
       

       
       
                          
     #kvadratura do-------------------------------------------
       $this->add(array(
            'name'=>'kvadratura_do',
            'attributes'=>array(
                'type'=>'text',
                'class'=>'span2',
                'placeholder'=>'kvadratura do',
               'style'=>'height:15px;'
                
            ),
         
        ));
        
       #----------------------------------------- 
       
       
       
       

       
       
       
       
       
       
       
       
       
       /*
          
      #submit-------------------------------------------        
          $this->add(array(
            'name'=>'submit',
            'attributes'=>array(
                'type'=>'\Zend\Form\Element\Button',
                'class'=>'btn btn-primary',
                'value'=>'Nađite oglas',
               'id'=>'submit-index',
              //  'style'=>'margin-right:20px;width:150px; ',
           // 'class'=>'map-button-submit',
                
               
            ),
           ));
        
        #-----------------------------------------   
       */   
          $this->add(array(
        'type' => 'Button',
        'name' => 'submit-main-search',
        'options' => array(
            'label' => 'Nađite oglas',
            'label_options' => array(
              //  'disable_html_escape' => true,
            )
        ),
        'attributes' => array(
            'type'  => 'submit',
            'class' => 'btn btn-primary ',
            'id'    =>'submit-index'
          //  'value'=>'<i class="icon icon-foo"></i> Submit'
         )
    ));
 
          
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}







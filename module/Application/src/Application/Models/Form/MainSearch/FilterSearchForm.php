<?php
namespace Application\Models\Form\MainSearch;


use Zend\InputFilter\InputFilter;
use Application\Models\LokacijaSr\Lokacija;

class FilterSearchForm extends InputFilter {
  
    
    
    public function __construct() {
       #broji ukupan broj lokacija u srbiji
           //  $lok=new Lokacija();
            // $count=count($lok->get_search_location())-1;
                          
        //----------------------------------------------       
         $this->add(array(
             'name'=>'kategorija',
             'requeried'=>true,
             'filters'  => [
                 ['name' => 'Int'],
             ],
             'validators' => [
                 [
                     'name' => 'Between',
                     'options' => [
                      'min' => 1,
                      'max' => 17,
                     ]
                 ]
             ]
         )); 
        
          $this->add(array(
             'name'=>'prodaja_izdavanje',
             'requeried'=>true,
             'filters'  => [
                 ['name' => 'Int'],
             ],
             'validators' => [
                 [
                     'name' => 'Between',
                     'options' => [
                      'min' => 1,
                      'max' => 2,
                     ]
                 ]
             ]
         )); 
        
          $this->add(array(
             'name'=>'lokacija',
             'requeried'=>true,
             'filters'  => [
                 ['name' => 'stringTrim','name'=>'stripTags'],
             ],
           /*  'validators' => [
                 [
                     'name' => 'Between',
                     'options' => [
                      'min' => 1,
                      'max' => $count,
                     ]
                 ]
             ]*/
         )); 
        
         $this->add(array(
             'name'=>'sa_slikom',
             'requeried'=>true,
             'filters'  => [
                 ['name' => 'Int'],
             ],
             'validators' => [
                 [
                     'name' => 'Between',
                     'options' => [
                      'min' => 0,
                      'max' => 1,
                     ]
                 ]
             ]
         )); 
        
           
          
         $this->add(array(
             'name'=>'cena_do',
             'requeried'=>true,
             'filters'  => [
                 ['name' => 'Int'],
             ],
             'validators' => [
                 [
                     'name' => 'Regex', 
                      
                       'options' => array(
                           'pattern' => '/^\d+$/',
                         ),
                     
                 ]
             ]
         )); 
        
                  
          
          
          
              
         $this->add(array(
             'name'=>'kvadratura_do',
             'requeried'=>true,
             'filters'  => [
                 ['name' => 'Int'],
             ],
             'validators' => [
                 [
                'name'=>'Regex',
                     'options'=>
                     array(
                     'pattern' => '/^[\d]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Lokacija nije ispravna!'
                     ))
                 ]
             ]
         )); 
        
        
                 
          
          
  
          
          
          
          
          
          
          
          
          
          
        
    }
    
    
    
    
    
    
    
    
    
    
}



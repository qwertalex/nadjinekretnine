<?php
namespace Application\Models\Form\KontaktPodrska;


use Zend\Form\Form;


class KontaktPodrska  extends Form {
    
    
    
    public function __construct() {
        
        parent::__construct("kontakt-podrska");
        $this->setAttribute('action', '');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'contact-form');
        $this->setInputFilter(new \Application\Models\Form\KontaktPodrska\KontaktPodrskaFilter());
        
      
        #-----------------------------------       
        $this->add(array(
            'name'=>'name',
            'attributes'=>array(
                'type'=>'text',
               ),
           ));
        #------------------------------------------------  
                        
       $this->add(array(
            'name'=>'message',
            'attributes'=>array(
                'type'=>'Zend\Form\Element\Textarea',
             
            ),
           ));


     #submit
      $this->add(array(
            'name'=>'submit',
          'type'=>'Zend\Form\Element\Button',
            'attributes'=>array(
                 'class'=>'btn btn-primary arrow-right',
                 'value'=>'Posalji',
         ),
           ));
      
    
    
    
    
    
    
     }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
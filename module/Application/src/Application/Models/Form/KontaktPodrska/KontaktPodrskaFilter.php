<?php
namespace Application\Models\Form\KontaktPodrska;


use Zend\InputFilter\InputFilter;


class KontaktPodrskaFilter extends InputFilter {
   
    
    public function __construct() {
        
        
       
        $this->add(array(
            'name'=>'name',
            'requeried'=>true,
            'allow_empty'=>false,
               'validators' => array(
                      [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Niste uneli vase Ime!' 
                            ],
                        ],
                     ],  
                   array(
                    'name'=>'string_length',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'min'=>2,
                         'max' => 50,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Ime mora da sadrzi najmanje dva karaktera!', 
                                 'stringLengthTooLong' => 'Ime mora da sadrzi najvise 50 karaktera!' 
                            ),
                    ),
                ),
                 array(
                  'name'=>'Regex',
                     'break_chain_on_failure' => true,
                     'options'=>
                     array(
                     'pattern' => '/^[a-zA-Z\d\s]+$/',
                     'messages'=>array(
                     \Zend\Validator\Regex::NOT_MATCH=>
                'Ime moze da sadrzi samo slova i eventualno brojeve!'
                     ))),
                  ),
                 ));
   #---------------------------------------------------------------------  
              
        
        
             
        $this->add(array(
            'name'=>'message',
            'requeried'=>true,
            'allow_empty'=>false,
            'filters'  => array(
               array('name' => 'StripTags'),
               array('name' => 'StringTrim'),
           ),           
               'validators' => array(
                      [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => 'Niste napisali poruku!' 
                            ],
                        ],
                     ],  
                   array(
                    'name'=>'string_length',
                    'break_chain_on_failure' => true,
                    'options'=>array(
                        'min'=>2,
                         'max' => 255,
                             'messages' => array(
                                 'stringLengthTooShort' => 'Poruka mora da sadrzi najmanje dva karaktera!', 
                                 'stringLengthTooLong' => 'Polje Poruka moze da sadrzi najvise 255 karaktera!' 
                            ),
                    ),
                ),
                
                  ),
   
                 ));
   #---------------------------------------------------------------------  
              
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
}
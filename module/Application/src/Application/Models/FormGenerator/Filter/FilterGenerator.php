<?php
namespace Application\Models\FormGenerator\Filter;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of FilterGenerator
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class FilterGenerator {
    
    protected function not_empty($label){
    return [
                      'name' =>'NotEmpty', 
                      'break_chain_on_failure' => true,
                        'options' =>[
                             'messages' => [
                                \Zend\Validator\NotEmpty::IS_EMPTY => "Polje $label je obavezno!" 
                            ],
                        ],
                     ];
    }
    
   /**
    * 
    * @param array $data
    * @param type $err_msg_template
    * @return type
    * 
    * 
    * example:
    *   $this->add($filter->digits(['name'=> 'maticni_broj'],[
             'not_empty'=>'Polje Maticni broj je obavezno!',
             'not_digits'=>'Polje Maticni broj prima samo brojeve!'
         ]));
    */ 
    
    public function digits(Array $data,$label,$empty=true){
       $err_msg_template=[
             'not_empty'=>'Polje '.$label.' je obavezno!',
             'not_digits'=>"Polje $label prima samo brojeve!"          
       ];
        return [
             'name'=>$data['name'],
             'required'=> isset($data['required'])?$data['required']:true,
             'allow_empty' => isset($data['allow_empty'])?$data['allow_empty']:false,
            'filters'  => array(
                 ['name' => 'StripTags'],
                 ['name' => 'StringTrim']
                    ),
             'validators' => array(
                  [
                      'name'=>'Zend\Validator\Digits',
                      'break_chain_on_failure' => true,
                      'options'=>[
                          'messages'=>[
                              \Zend\Validator\Digits::NOT_DIGITS =>"Polje $label prima samo brojeve!",
                              \Zend\Validator\Digits::STRING_EMPTY => 'Polje '.$label.' je obavezno!',
                              \Zend\Validator\Digits::INVALID => "Polje $label prima samo brojeve!" ,
                          ]
                      ]
                      ],
                  
                 $this->not_empty($label), 
                
                ), 
        ];
    }
    
    public function string_length(Array $data,Array $options,$label){
         
        return [
       'name'=>$data['name'],
             'required'=> isset($data['required'])?$data['required']:true,
             'allow_empty' => isset($data['allow_empty'])?$data['allow_empty']:false,
        'filters'  => [
                 ['name' => 'StripTags'],
                 ['name' => 'StringTrim']
             ],
             'validators' => array(
                  $this->not_empty($label), 
                    array(
                        'name'    => 'StringLength',
                         'break_chain_on_failure' => true,
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => $options['min'],
                            'max'      => $options['max'],
                            'messages'=>[
                                 \Zend\Validator\StringLength::TOO_SHORT =>"Polje $label ima manje od ".$options['min']." karaktera",
                                 \Zend\Validator\StringLength::TOO_LONG =>"Polje $label ima vise od ".$options['max']." karaktera",
                                
                            ]
                        ),
                    ),
             
                ), 
        ];
    }
    
    public function identical($data,$token,$text){
        
           return [
       'name'=>$data['name'],
             'required'=> isset($data['required'])?$data['required']:true,
             'allow_empty' => isset($data['allow_empty'])?$data['allow_empty']:false,
        'filters'  => [
                 ['name' => 'StripTags'],
                 ['name' => 'StringTrim']
             ],
             'validators' => array(
                //  $this->not_empty($text), 
                  [
                   'name'    => 'Identical',
                   'options' =>[
                       'token'=>$token,
                       'messages'=>[
                      \Zend\Validator\Identical::NOT_SAME=>$text,
                           \Zend\Validator\Identical::MISSING_TOKEN=>$text,
                      ]                  
                   ],

                  ]
             
                ), 
        ];
        
        
    }
    
    
    
       public function bettwen(Array $data,Array $options,$label){
         
        return [
       'name'=>$data['name'],
             'required'=> isset($data['required'])?$data['required']:true,
             'allow_empty' => isset($data['allow_empty'])?$data['allow_empty']:false,
        'filters'  => [
                 ['name' => 'StripTags'],
                 ['name' => 'StringTrim']
             ],
             'validators' => array(
                  $this->not_empty($label), 
                    array(
                        'name'    => 'Zend\Validator\Between',
                         'break_chain_on_failure' => true,
                        'options' => array( 
                            'min'      => $options['min'],
                            'max'      => $options['max'],
                            'messages'=>[
                             //    \Zend\Validator\StringLength::TOO_SHORT =>"Polje $label ima manje od ".$options['min']." karaktera",
                              //   \Zend\Validator\StringLength::TOO_LONG =>"Polje $label ima vise od ".$options['max']." karaktera",
                                
                            ]
                        ),
                    ),
             
                ), 
        ];
    }
    
  
    
    
}

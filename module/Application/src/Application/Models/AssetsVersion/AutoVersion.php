<?php
namespace Application\Models\AssetsVersion;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of AutoVersion
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class AutoVersion {
    
    
    
    const MAIN_CSS="22";
    
    
    const CUSTOM_JS="25";
    
    const INDEX_PAGE_JS="14";
    const PRETRAGA_JS='13';
    
    public static function layout(){
        return [
         //   'custom_js'            =>'/js/custom.__'.self::CUSTOM_JS.".js",
           // 'main_css'              =>'/css/main.__'.self::MAIN_CSS.".css"
            'custom_js'            =>'/js/custom.js',
              'main_css'              =>'/css/main.css'
        ];
    }
    public static function index__js(){
       // return "/js/index.page.__".self::INDEX_PAGE_JS.".js";
        return '/js/index.page.js';
    }
    
 
    public static function pretraga_js(){
       // return '/js/pretraga.lista.__'.self::PRETRAGA_JS.'.js';
         return '/js/pretraga.lista.js';
    }
    
    
    
    
    
    
    
    
    
    
    
}


/*
 * RewriteCond %{HTTP_HOST} ^www\.(.+) [NC]
RewriteRule ^ http://%1%{REQUEST_URI} [L,R=301]
RewriteCond %{HTTP_HOST} ^50\.62\.62\.4
RewriteRule (.*) http://nadjinekretnine.com/$1 [R=301,L]
RewriteEngine On
# The following rule tells Apache that if the requested filename
# exists, simply serve it.
RewriteCond %{REQUEST_FILENAME} -s [OR]
RewriteCond %{REQUEST_FILENAME} -l [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^.*$ - [NC,L]
# The following rewrites all other queries to index.php. The 
# condition ensures that if you are using Apache aliases to do
# mass virtual hosting, the base path will be prepended to 
# allow proper resolution of the index.php file; it will work
# in non-aliased environments as well, providing a safe, one-size 
# fits all solution.
RewriteCond %{REQUEST_URI}::$1 ^(/.+)(.+)::\2$
RewriteRule ^(.*) - [E=BASE:%1]
RewriteRule ^(.*)$ %{ENV:BASE}index.php [NC,L]
RewriteRule !\.(js|ico|txt|gif|jpg|png|css)$ index.php
#custom limitira upload fajlova do 10MB
LimitRequestBody 10485760



RewriteCond %{HTTP_HOST} ^www\.(.+) [NC]
RewriteRule ^ http://%1%{REQUEST_URI} [L,R=301]
RewriteCond %{HTTP_HOST} ^50\.62\.62\.4
RewriteRule (.*) http://nadjinekretnine.com/$1 [R=301,L]
RewriteEngine On


 */












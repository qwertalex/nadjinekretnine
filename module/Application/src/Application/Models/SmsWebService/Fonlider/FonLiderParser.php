<?php

namespace Application\Models\SmsWebService\Fonlider;

/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Http\Client;
use Application\Models\Oglasi\AdsInfo;


/**
 * Description of FonLiderParser
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class FonLiderParser {
    const FONLIDER_SERVER='212.200.146.130';
    protected $adapter;
    protected $oglas_id;
    protected $get;
    const ERROR_MSG='Uneli ste pogresnu sifru oglasa.';
    const OK_MSG="Transakcija je uspesno obavljena. Hvala vam sto ste poslali SMS.";
    
    
    
    
    
    
    
    
    public function __construct($adapter,$get) {
        $this->adapter=$adapter;
        $this->get=$get;
     //  $this->id_validation();
    }


protected function return_message($ok){
    if ($ok) {
        return[
             'status'       =>true,
             'msg'          =>self::OK_MSG
         ];
    }
    return[
             'status'       =>false,
             'msg'          =>self::ERROR_MSG
         ];
    
    
    
}







    public function id_validation(){
         $get=$this->get;
         if (!isset($get['sender'],$get['keyword'],$get['message'])) {
             return $this->return_message(false);
         }
         
         
         //keyword validation
         $keyword=strtolower($get['keyword']);
         if (!in_array($keyword,['st','top','pr'])) {
             return $this->return_message(false);
             return false;
         } 
         
         
         
         //id vlaidation
        $mes=strtolower($this->get['message']);
       // $mes=trim(str_replace(['pr','st','top'],"",$mes));
        $this->oglas_id = preg_replace('/[^0-9+]/', '', $mes);
        if ($this->search_oglas()) {
            $this->update_oglas($keyword);
            return $this->return_message(true);
        }
        
        return $this->return_message(false);
        return false;
    }
    
  
    protected function set_oglas_data($keyword){
        $date=date("Y-m-d H:i:s");
        $arr=[];
        switch($keyword):
            case "pr":
               $arr['vrsta_oglasa']=3;
               $arr['datum_isteka']=date("Y-m-d H:i:s", strtotime($date ." + 30 days"));
                 break;
             case "st":
                 $arr['vrsta_oglasa']=1;
                 $arr['datum_isteka']=date("Y-m-d H:i:s", strtotime($date ."+ 30 days"));
                 break;
              case "top":
                  $arr['vrsta_oglasa']=2;
                  $arr['datum_isteka']=date("Y-m-d H:i:s", strtotime($date ."+ 30 days"));
                  break;
            default:return false;
        endswitch;
        $arr['datum_postavljanja_oglasa']=$date;
        return $arr;
        
        
    }
    
    
    
    protected function update_oglas($keyword){
        $data=$this->set_oglas_data($keyword);
        if (!$data) {
            return false;
        }
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $update=$sql->update('vrsta_oglasa');
        $update->set($data);
        $update->where([
            'id_oglasi_nk'      =>  $this->oglas_id
        ]);
        $statement = $sql->prepareStatementForSqlObject($update);
         $results = $statement->execute();
         if ($results->count()==1) {
             $this->insert_data();
                   return true;
         }
        return false;
     }
    
    
    
    
    
    
    
    
    
    
    
    
   protected function search_oglas(){
         $validator = new \Zend\Validator\Db\RecordExists([
            'table'   => 'oglasi_nk',
            'field'   => 'id_oglasi_nk',
            'adapter' => $this->adapter
         ]);

        if ($validator->isValid($this->oglas_id)) {return true;}
        return false;
        
        
    }
    
    
    
    
    
    /**
     * 
     * @param array $message
     * 
     * Don't work at the moment
     */
    public static function send_message(Array $message){
         
        $client = new Client(self::FONLIDER_SERVER,[
          'maxredirect' =>2,
          'timeout'     =>30,
          'useragent'   =>'nadjinekretnine.com'
      ]);
       $client->setParameterGet($message);
       $response = $client->send();
  }
    
    
    
    
    /**
     * 
     * 
     * 
     * insert in fonlider table for statistics
     */
    public function insert_data($valid=true){
        $get=$this->get;
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $insert=$sql->insert('fonlider');
        $insert->values([
            'id_oglasi_nk'          =>  $this->oglas_id,
            'sender'                =>$get['sender'],
            'keyword'               =>$get['keyword'],
            'message'               =>$get['message'],
            'datetime'              =>date("Y-m-d H:i:s"),
            'is_valid'              =>$valid?1:0,
            'ip'                    =>$get['ip']
        ]);
        $statement = $sql->prepareStatementForSqlObject($insert);
        $statement->execute();
    }
    
    
    
    
    
    
    
    
    
}

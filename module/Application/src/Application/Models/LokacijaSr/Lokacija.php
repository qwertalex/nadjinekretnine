<?php
namespace Application\Models\LokacijaSr;





class Lokacija  extends \Application\Models\LokacijaSr\LocationArray{
    
   
    

    
public static function get_mesta_srbija_text($int) {
    if (array_key_exists($int,self::$mesta_srbija)) {
        return self::$mesta_srbija[$int];
    }
    return false;
    }    
    
public static function get_deo_mesta_srbija_text($mesto,$deo_mesta) {
    if (isset(self::$deo_mesta_new[$mesto][$deo_mesta])) {
        return self::$deo_mesta_new[$mesto][$deo_mesta];
    }elseif($deo_mesta==1||$deo_mesta==2091){
        return "Gradska lokacija";
    }elseif($deo_mesta==2||$deo_mesta==2092){
        return "Okolno mesto";
    }
    return false;
    }
public static function get_lokacija_srbija_text($mesto,$deo_mesta,$lokacija) {
    if (isset(self::$lokacija_new[$mesto][$deo_mesta][$lokacija])) {
        return self::$lokacija_new[$mesto][$deo_mesta][$lokacija];
    }
    return false;
    }    
    
    public static function get_dm($deo_mesta){
        $deo_mesta=(int)$deo_mesta;
     if($deo_mesta==1||$deo_mesta==2091){
        return "Gradska lokacija";
    }elseif($deo_mesta==2||$deo_mesta==2092){
        return "Okolno mesto";
    }
        $d=self::$deo_mesta_new;
        foreach(self::$deo_mesta_new as $k=>$v):
            if (isset($v[$deo_mesta])) {
                return $v[$deo_mesta];
            }
        endforeach;
        return false;
    }
    public static function get_lm($lok){
        $lok=(int)$lok;
        foreach(self::$lokacija_new as $k=>$val):
           foreach ($val as $kk=>$v):
               if (isset($v[$lok])) {
                   return $v[$lok];
               };
           endforeach;
        endforeach;
        return false;
    }
    
   /*  
        public static function get_deo_lokacija_range(){
        return [
            'min'=>array_keys(self::$lokacija)[0],
            'max'=>max(array_keys(self::$lokacija))
            
            ];
    }
    public static function get_deo_mesta_range(){
        return [
            'min'=>array_keys(self::$deo_mesta_srbija)[0],
            'max'=>max(array_keys(self::$deo_mesta_srbija))
            
            ];
    }

   */ 


 
public static function merge_locations(){
    $m=self::$mesta_srbija;
    $deo=self::$deo_mesta_new;
    $lok=self::$lokacija_new;
    
    $return=[];$KEY;
    foreach($m as $k=>$v):
        $return[$k]=$v;
        if (isset($deo[$k])) {
            foreach($deo[$k] as $dk=>$dv):
                
                $return[$k.'-'.$dk]=$v.' / '.$dv;
                if (isset($lok[$k][$dk])) {
                   foreach($lok[$k][$dk] as $lk=>$lv):
                        $return[$k.'-'.$dk.'-'.$lk]=$v.' / '.$dv.' / '.$lv;
                   endforeach;
                }
              endforeach;
         }else{
              if (\in_array($k,array_keys(Lokacija::$lokacija_new))) {
                 $return[$k.'-1']=$v.' / Gradska lokacija';
                 
                 foreach($lok[$k][1] as $lokko=>$lokkv):
                   $return[$k.'-1-'.$lokko]=$v.' / Gradska lokacija /'.$lokkv;  
                 endforeach;
                 $return[$k.'-2']=$v.' / Okolno mesto';
                 foreach($lok[$k][2] as $lokko=>$lokkv):
                   $return[$k.'-2-'.$lokko]=$v.' / Okolno mesto /'.$lokkv;  
                 endforeach;
                // $return[$k.'-2']=$v.' / Okolno mesto';
             }else{
                  $return[$k.'-2091']=$v.' / Gradska lokacija';
                 $return[$k.'-2092']=$v.' / Okolno mesto';                
             }
             
         }
      endforeach;
    
    return $return;
}




    public static function get_mesta_srbija() {
       return self::$mesta_srbija;
    }    

   public static function get_deo_mesta($mesto) {
      if (isset(self::$deo_mesta_new[$mesto])) {
       return self::$deo_mesta_new[$mesto];
   }
    if (\in_array($mesto,array_keys(Lokacija::$lokacija_new))) {
    return [
            1=>'Gradska lokacija' ,
            2 =>  'Okolno mesto' ,        
    ];       
   }
    return [
            2091=>'Gradska lokacija' ,
            2092 =>  'Okolno mesto' ,        
    ];
 }
    

        
  public static function get_location($mesto,$deo_mesta) {
       return self::$lokacija_new[$mesto][$deo_mesta];
   }
    
   




    
    
}
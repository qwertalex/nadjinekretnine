<?php

namespace Application\Models\Cron;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of Cleanup
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class Cleanup {
    protected $adapter;
    const IMG_DIR="/var/www/nadjinekretnine.com/public/img/oglasi_img";
    const CAPTCHA_DIR="/var/www/nadjinekretnine.com/public/img/captcha";
    
    
    public function __construct($adapter) {
        $this->adapter=$adapter;
    }
    
    
    
    
    
    
    
    /**
     * 
     * @return boolean
     * 
     * 
     * 
     * 
     * Brise sve slike iz foldera koje se ne nalaze u bazi
     */
    public function cleanup_ads_images(){
        
        $scandir=array_diff(scandir(self::IMG_DIR),['..', '.']);
       $dir_img=[];
        foreach($scandir as $k=>$v):
            if (is_dir(self::IMG_DIR.'/'.$v)) {
                $imgs=array_diff(scandir(self::IMG_DIR.'/'.$v),['..', '.']);
               foreach($imgs as $val):
                if (0 === strpos($val, 'small')) {
                   continue;
                 }
               $dir_img[]=$v.'/'.$val;
               
                
                   
               endforeach;
            }
        endforeach;
    
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select("slike");
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
         if ($results->count()>0) {
             $arr_results = array_values(iterator_to_array($results));
             $images=[];
             foreach($arr_results as $key=>$v):
                 \array_push($images,
                         $v['slika_1'],
                         $v['slika_2'],
                         $v['slika_3'],
                         $v['slika_4'],
                         $v['slika_5'],
                         $v['slika_6'],
                         $v['slika_7'],
                         $v['slika_8'],
                         $v['slika_9'],
                         $v['slika_10']
                         
                         );
             endforeach;
             
             $images=array_filter($images);
             
             //get image to delete
             //vraca aarray onig slika kkojih ima u folderu a nema u bazi(one slike koje se brisu);
             $img_to_delete=array_diff($dir_img,$images);
       
         
             foreach($img_to_delete as $v): 
                 unlink(self::IMG_DIR.'/'.$v);
             $pos=strpos($v, '/')+1;
                 $img_name='small_'.substr($v,$pos);
            $st=substr($v,0,$pos);
             
                 if (file_exists(self::IMG_DIR.'/'.$st.$img_name)) {
                                         
                     unlink(self::IMG_DIR.'/'.$st.$img_name);
                 }
             endforeach;
             
             foreach($scandir as $k=>$v):
            if (is_dir(self::IMG_DIR.'/'.$v)) {
                $is_empty=array_diff(scandir(self::IMG_DIR.'/'.$v),['..', '.']);
                if (count($is_empty)===0) {
                    rmdir(self::IMG_DIR.'/'.$v);
                }
            }
        endforeach;
             
             
             
             
             return true;
         }
         
         return false;
     
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function delete_capcha_imgs(){
         $captcha=array_diff(scandir(self::CAPTCHA_DIR),['..', '.']);
         
         foreach($captcha as $v):
             unlink(self::CAPTCHA_DIR.'/'.$v);
         endforeach;
         
         
         
    }
    
    
    
    /**
     * 
     * 
     * 
     * 
     * 
     * 
     */
    public function delete_old_ads(){
      $minus_m=date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"). ' -1 months'));
      
      $sql=new \Zend\Db\Sql\Sql($this->adapter); 
       $select=$sql->select('vrsta_oglasa');
       $where=new \Zend\Db\Sql\Where();
       $where->lessThan("datum_isteka", $minus_m);
     //  $where->addPredicate(new \Zend\Db\Sql\Predicate\Expression('datum_isteka <'.$minus_m));
       $select->where($where);
     
       $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
         if ($results->count()>0) {
             $arr_results = array_values(iterator_to_array($results));
             foreach($arr_results as $k=>$v):
                 $delete=$sql->delete("oglasi_nk");
                 $delete->where([
                    'id_oglasi_nk'  =>$v['id_oglasi_nk'] 
                 ]);
                 $statement = $sql->prepareStatementForSqlObject($delete);
                 $statement->execute();
             endforeach;
             return true;
         }
       
       return false;
       
       
    }
    
    
    
    
    
    
    
    
    
    
}

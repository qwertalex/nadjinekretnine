<?php
namespace Application\Models\Estan;
/*
 * The MIT License
 *
 * Copyright 2014 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;

//local
 
/**
 * Description of XmlParser
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class XmlParser extends \Application\Models\Oglasi\Render\RenderToDb{
    
    protected $adapter;
    protected $xml;
    protected $user_id;
    protected $id_agencije;//id agencije iz estana
    protected $agency_data=[];
    
    public function __construct($adapter,$xml,$user_id,$id_agencije) {
        $this->adapter=$adapter;
        $this->xml=$xml['Item'];
        $this->user_id=$user_id;
        $this->id_agencije=$id_agencije;
        $this->agency_data=$this->get_db_estan();
        $this->sort_data();
    }
    
    
    
    protected function get_user_data(){
        $sql=new Sql($this->adapter);
        $select=$sql->select('users_nk');
        $select->columns(['mesto_oglasivac','ulica_oglasivac']);
        $select->where([
           'id_users_nk'    =>  $this->user_id 
        ]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count()>0) {
           return  array_values(iterator_to_array($results));
        }
        return false;
    }
    
    
    
    
    
    
    
    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * STRUKTURA,
     * 
     * 
     * 
     */
    
    public function parse_data(){
     $users_location=$this->get_user_data()[0];
     $kat=  $this->xml;
     $return=[];
     $strip_tags = new \Zend\Filter\StripTags();
     $ok=0;
     $not=0;
   //  var_dump($kat);
        foreach($kat as $k=>$v):
            if ($v['DRZAVA']!='SRBIJA'||$v['LOKACIJA']==="DUBAI") {
                continue;
            }elseif(is_array($v['CENA'])||$v['CENA']<=0||!\is_numeric($v['CENA'])){
                continue;
            }
            
            $return[$k]['oglasi_nk']=[
                'id_users_nk'           =>$this->user_id,
                'kategorija'            =>self::kategorija($v['VRSTAOBJEKTA']),
                'naslov'                =>$strip_tags->filter($v['NASLOV']),
                'opis'                  =>$strip_tags->filter($v['OPIS']),
                'oglasivac'             =>2,
                'prodaja_izdavanje'     =>self::e_prodaja_izdavanje($v['VRSTAPRODAJARENTA']),
                'datum_kreiranja_oglasa'=>$v['DATUM']." 00:00:09",
             ];
    
          $kontakt=self::e_kontakt_podatci($v['AGENTIME'], $v['AGENTTELEFON'], $v['AGENTFOTO'], $v['AGENTMAIL']);
            $return[$k]['kontakt_podaci']=[
                'telefon'               =>$kontakt['telefon'],
                'dodatni_telefon'       =>$kontakt['dodatni_telefon'],
                'email'                 =>$kontakt['email'],
                'slika_agenta'          =>$kontakt['foto'],
                'ime'                   =>$kontakt['ime'],
                'mesto_kontakt_podaci'  =>$users_location['mesto_oglasivac'],
                'ulica_kontakt_podaci'  =>$users_location['ulica_oglasivac']
                
            ];
            
            
            
          $mes=self::get_location_id($v['DRZAVA'],$v['GRAD'],$v['OPSTINA'],$v['LOKACIJA']);
            if ($mes) {++$ok;
                $return[$k]['lokacija']=[
               'mesto'          =>$mes['mesto'],
               'deo_mesta'      =>$mes['deo_mesta'],
               'lokacija'       =>$mes['lokacija'],
                'ulica'         =>$v['ADRESA'],    
                'e_stan_frame'  =>(isset($v['HTMLDETMAPA'])?$v['HTMLDETMAPA']:NULL),
                'mesto_deo_lokacija'=>$mes['full']
                    
                    
             //  'full'           =>$mes['full'],
              
             ];
            }else{
                ++$not;
                unset($return[$k]);
                continue;
            }
        //  $return[$k]['oreginal']=$v['GRAD'].' / '.$v['OPSTINA'].' / '.$v['LOKACIJA'];
            
            
      
            
            $return[$k]['vrsta_oglasa']=[
              'vrsta_oglasa'            =>1,
              'datum_postavljanja_oglasa'=>$v['DATUM']." 00:00:00",
                'datum_isteka'          =>date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s"). ' +1 months'))
                
                
            ];
            
               
        if ($v['VRSTAOBJEKTA']==='KUCA') {
            $return[$k]['kuce']=[
              'vrsta_kuce'  =>NULL,
              'spratnost'   =>NULL
            ];
        }
            
          if ($v['VRSTAOBJEKTA']==='PLAC') {
            $return[$k]['plac']=[
              'ostalo_plac'         =>NULL,
                'vrsta_plac'        =>NULL,
                'povrsina_plac'     =>(isset($v['ARI'])?$v['ARI']:NULL),
                'jedinica_mere_plac'=>NULL
            ];
        }  
            
            $images=isset($v['images'])?$v['images']:null;
            $slike=self::e_slike($images);
       $return[$k]['slike']=[
                          'slika_1'=>(isset($slike['slika_1'])?$slike['slika_1']:NULL),
                          'slika_2'=>(isset($slike['slika_2'])?$slike['slika_2']:NULL),
                          'slika_3'=>(isset($slike['slika_3'])?$slike['slika_3']:NULL),
                          'slika_4'=>(isset($slike['slika_4'])?$slike['slika_4']:NULL),
                          'slika_5'=>(isset($slike['slika_5'])?$slike['slika_5']:NULL),
                          'slika_6'=>(isset($slike['slika_6'])?$slike['slika_6']:NULL),
                          'slika_7'=>(isset($slike['slika_7'])?$slike['slika_7']:NULL),
                          'slika_8'=>(isset($slike['slika_8'])?$slike['slika_8']:NULL),
                          'slika_9'=>(isset($slike['slika_9'])?$slike['slika_9']:NULL),
                          'slika_10'=>(isset($slike['slika_10'])?$slike['slika_10']:NULL),
       ];
          
          
          $return[$k]['dodatno']=[
            'kvadratura'        =>$v['KVADRATURA'],
            'grejanje'          =>self::grejanje($v['GREJANJE']),
            'spratovi'          =>self::e_stan_sprat($v['SPRAT'])['sprat'],
            'ukupno_spratovi'   =>self::e_stan_sprat($v['SPRAT'])['ukupno_spratovi'], 
            'godina_izgradnje'  =>(is_numeric($v['GODINAIZGRADNJE'])?$v['GODINAIZGRADNJE']:NULL),
            'cena'              =>$v['CENA'],
            'broj_soba'         =>self::e_stan_broj_soba($v['STRUKTURA']),
            'tip_objekta'       =>self::e_tip_objekta($v['NOVOG'], $v['UIZGRADNJI']),
            'opis_objekta'      =>self::e_opis_objekta($v['DUPLEKS']),
         ];
         $return[$k]['dodatni_podatci']=[
            'dodatni_podatci_telefon'           =>($v['TELEFON']==='da'?1:NULL),
            'lift'              =>($v['LIFT']==='da'?1:NULL),  
            'interfon'          =>($v['INTERFON']==='da'?1:NULL),
            'topla_voda'        =>($v['TOPLAVODA']==='da'?1:NULL),
            'odmah_useljiv'     =>($v['USELJIVOST']==='da'?1:NULL),
            'podrum'            =>($v['PODRUM']==='da'?1:NULL), 
            'dodatno_graza'             =>($v['GARAZA']==='da'?1:NULL),
            'uknjizen'          =>($v['UKNJ']==='da'?1:NULL),
         ];
          
          $return[$k]['e_stan']=[
            'e_stan_oglas_id'       =>$v['ID'],
            'id_agencije'           =>$v['IDAGENCIJA']
          ];
          
          
          
        endforeach;
     //    var_dump($return);
        
      /*    $this->insert_data($return);
        
        
        
        
  
          var_dump($ok);
          var_dump($not);
         
    */    
       return $return;

    }
    
    
    
   // public function delete_
    
    /*
     * 
     * 
     * VADI PODATKE IZ BAZE VEZANE ZA ESTAN
     * 
     * 
     * 
     */
    protected function get_db_estan(){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select('e_stan');
        $select->columns([
            'id_oglasi_nk','e_stan_oglas_id','id_agencije'
        ]);
        $select->where([
            'id_agencije'   =>  $this->id_agencije
        ]);
        
         $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if ($results->count()>0) {
             $arr_results = array_values(iterator_to_array($results));
           //  var_dump($arr_results);
             return $arr_results;
         }
        return [];
        
        
    }
    
    
    /**
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    protected function sort_data(){
        $new_xml=$this->parse_data();//novi xml file
        $db_xml=$this->agency_data;//baza
        $update=[];
        $insert=[];
        $delete=[];
        $delete_xml=[];
        $delete_db=array_column($db_xml,'e_stan_oglas_id');
     //   var_dump($new_xml);
    // var_dump($db_xml);
     if (count($new_xml)>0&&count($db_xml)>0) {
         
         //loop new xml file
      foreach($new_xml as $k=>$v):
            $estan_id=$v['e_stan']['e_stan_oglas_id'];
            $id_agencije=$v['e_stan']['id_agencije'];
            
            
         $delete_xml[]=$estan_id;
            
            
            //loop from database
             foreach($db_xml as $kk=>$vv):
                 //ako vec postoji u bazi
              //    $delete_db[]=$vv['e_stan_oglas_id']; 
                
                   
                   //prepare for update
                 if ($estan_id==$vv['e_stan_oglas_id']) {
                     $v['oglasi_nk']['id_oglasi_nk']=$vv['id_oglasi_nk'];
                    $update[]=$v;
                    unset($new_xml[$k]);break;
                 }
                 
                   
               endforeach;
        endforeach;
        
        
        }
        //----------------------------------------------------------------
        
        
        //check for update and update data
        if (count($update)>0) {$this->update_data($update);}
        //check for insert data and insert
        if (count($new_xml)>0) {$this->insert_data($new_xml);}
       
   $diff=array_unique(array_diff($delete_db,$delete_xml));

   if (count(array_unique($diff))>0) {
       $this->delete_data(array_unique($diff));
   }

    }
    
    
    
    
    /**
     * 
     * @param type $data
     * 
     * 
     * 
     * 
     * 
     * 
     */
 protected function update_data($data){
  //   var_dump($data);
     
    $sql=new Sql($this->adapter);
     foreach($data as $k=>$v):
         $id=$v['oglasi_nk']['id_oglasi_nk'];
        // var_dump($id);
     
       try{
             $this->adapter->getDriver()->getConnection()->beginTransaction();
     
         foreach($v as $kk=>$vv):
            
          
              $update=$sql->update($kk);
            if (isset($vv['id_oglasi_nk'])) {
                unset($vv['id_oglasi_nk']);
            }
            if ($id==1943) {
              //  var_dump($vv);
            }
            $update->set($vv);
            $update->where([
                'id_oglasi_nk'  =>$id
            ]);
             $selectString = $sql->getSqlStringForSqlObject($update);
             $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
               if ($id==1943) {
              //  var_dump($results->count());
            }
             endforeach;
           $this->adapter->getDriver()->getConnection()->commit(); 
            } catch (Exception $ex) {
                $this->adapter->getDriver()->getConnection()->rollback();
            }
           
         
         
         
         
     endforeach;
    
     
     
 }
    
    
    
    
    
    
   protected function insert_data($post) {
      // var_dump(count($post));
      foreach($post as $key_num=>$post_val):
          
    
        try{
             $this->adapter->getDriver()->getConnection()->beginTransaction();
             $sql = new Sql($this->adapter);
             $insert=$sql->insert('oglasi_nk');
             $insert->values($post_val['oglasi_nk']);
             $selectString = $sql->getSqlStringForSqlObject($insert);
             $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
             if ( $results->count() === 0 ) {die('Doslo je do greske, molimo vas probajte za nekoliko minuta!');}    
           
             unset($post_val['oglasi_nk']);
            $id= $this->adapter->getDriver()->getConnection()->getLastGeneratedValue(); 
           // var_dump($id);
         
    foreach ($post_val as $k=>$value) {
        $value['id_oglasi_nk']=$id; 
        $insert=$sql->insert($k);
       
        $insert->values($value);
        $selectString = $sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        if ( $results->count() === 0 ) {die('Doslo je do greske, molimo vas probajte za nekoliko minuta!');}
       }
       //commit transaction
      $this->adapter->getDriver()->getConnection()->commit();  
    //  var_dump($id);
    //  return $id;
        }
 catch (Exception $exc){
     //rollback transaction
     $this->adapter->getDriver()->getConnection()->rollback();
 }  
   endforeach;
 return false;
    }
    
    
    
    
    
    protected function delete_data(Array $estan_oglas_id){
        $sql=new \Zend\Db\Sql\Sql($this->adapter);
        $select=$sql->select('e_stan');
        $select->where([
            'e_stan_oglas_id'      =>$estan_oglas_id
        ]);
        $deleteString = $sql->getSqlStringForSqlObject($select);
        $results = $this->adapter->query($deleteString, Adapter::QUERY_MODE_EXECUTE);
        if ($results->count()>0) {
             $e_stan=array_values(iterator_to_array($results));
             foreach($e_stan as $k=>$v):
                 $arr[]=$v['id_oglasi_nk'];
             endforeach;
             $delete=$sql->delete('oglasi_nk');
             $delete->where([
                'id_oglasi_nk'      =>$arr 
             ]);
             $del = $sql->getSqlStringForSqlObject($delete);
        $results = $this->adapter->query($del, Adapter::QUERY_MODE_EXECUTE);
      //  var_dump($results->count());
        }
       
        return true;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

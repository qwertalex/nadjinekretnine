<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

//local
use Application\Models\Oglasi\Sliders\Sliders;
//use Application\Helpers\Widgets\BrziLinkovi;
use Application\Models\Url\UrlRewrite;
use Pretraga\Models\Pretraga;
use Application\Models\Form\MainSearch\SearchForm;
use Magazin\Models\Magazin;
use Application\Models\AssetsVersion\AutoVersion as Ver;
use Application\Models\Oglasi\Render\RenderView as Html;
use Application\Models\Estan\XmlParser as Xml;
use Application\Models\User\SessionManipulation as Sess;
use Application\Models\Estan\Agencije;
use Application\Models\Cron\Cleanup;

 
class IndexController extends AbstractActionController{
   
       /**
 * 
 * @return \Zend\View\Model\ViewModel
 * 
 * 
 * 
 * 
 */
    public function indexAction(){
    # -------------------------------------------------------
     $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');   
     $renderer->headScript()->appendFile( '/libraries/select2/select2.min.js');
     $renderer->headLink()->appendStylesheet('/libraries/select2/select2.css');
     $renderer->headScript()->appendFile( '/libraries/icheck/icheck.js');
     $renderer->headLink()->appendStylesheet('/libraries/icheck/line/blue.css');
     $renderer->headScript()->appendFile(Ver::index__js());
   #-----------------------------------------------------------------------------------------------------
     
     $adapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
     $Sliders=new Sliders($adapter);
        
         $request =$this->getRequest();
        $get=$request->getQuery()->toArray();        
     $search_form=new SearchForm();
 
        if (isset($get['submit'])) {
           $search_form->setData($get);
            if ($search_form->isValid($get)) {
                 $pretraga=new Pretraga();
                 $pretraga->set_pretraga($get);
           
                 $url_rewrite = new UrlRewrite();
                 $this->redirect()->toUrl($url_rewrite->change_url($get));
                  
                
            }
           }
        
          
 
           
           
           /*
            * 
            * 
            * ------------------------------------------------
            * 
            * VIEW MODELS
            * 
            * -------------------------------------------
            */
        
        $viewModel=new ViewModel([
              'search_form'          =>$search_form,
                             
        ]);        
  
        $mainSliderHelper=new ViewModel([
            'slider'=>$Sliders->main_slider(),
            
        ]);
        $mainSliderHelper->setTemplate("main_slider");        
        $viewModel->addChild($mainSliderHelper,"main_slider");
        
        $premiumCarouselHelper=new ViewModel([
            'carousel'=>$Sliders->premium_slider(),
            'html'      =>new Html()
            
        ]);
        $premiumCarouselHelper->setTemplate("premium_carousel");        
        $viewModel->addChild($premiumCarouselHelper,"premium_carousel");        
        
        
        $brzi_linkovi_helper=new ViewModel([
           // 'brzi_linkovi'=>new BrziLinkovi($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'))
        ]);
        $brzi_linkovi_helper->setTemplate('brzi_linkovi');
        $viewModel->addChild($brzi_linkovi_helper,'brzi_linkovi');

        $Magazin=new Magazin($adapter);
        $magazin=new ViewModel([
             'magazin'=>$Magazin->get_random_articles(false)
        ]);
        $magazin->setTemplate('magazin');
        $viewModel->addChild($magazin,'magazin');        
        
        
        
        
        return $viewModel;
    }
    
    
    
 
    
    
    
    
    
    
    
    
    
    /**
     * 
     * @return strirng | json_encode($response)
     * 
     * 
     * OPIS:
     * prima samo post parametre i poziva class AjaxCheckUserEmail.php
     * koja gleda da li u bazi ima vec neko registrovan sa tim emailom.
     * koristi se prilikom registracije novog korisnika!
     * Ako vrati true onda nepostoji u bazi taj email, a false onda 
     * je neki user vec registorvan sa tim emailom
     * 
     */
    
    
    public function ajaxCheckEmailAction() {
        $viewModel = new ViewModel();
        $check_email= $this->getServiceLocator()->get('Ajax_check_email');
        $request   = $this->getRequest();   
        $response=TRUE;
        if ($request->isPost()){ 
        $post= $request->getPost()->toArray();
        if (isset($post['check_email'])) {
           $ajax_response= $check_email->check_email($post['check_email']);
           if ($ajax_response>0) {
               $response=false;
            }             
        $viewModel->setTerminal($request->isPost());
        return $viewModel->setVariables([
           'ajax_response'=>  json_encode($response),
        ]);
        }
        else{die('Missing: $_POST[check_email]');}
        }
        die("samo post ili sta vec!");
     
   
    }
    
    
    
    
    
    public function AjaxValidateUserLoginAction() {
        $viewModel=new ViewModel();
        $check_login=  $this->ajax_validate_user_login();
        $request   = $this->getRequest(); 
        if ($request->isPost()){ 
           $post= $request->getPost()->toArray();
           
           if (isset($post['check_email'])&&isset($post['check_password'])) {
            $ajax_response= $check_login->validate_user($post['check_email'],$post['check_password']);
            $viewModel->setTerminal($request->isPost());
            return $viewModel->setVariables([
              'ajax_response'=>  json_encode($ajax_response),
            ]);
           }
           else{die('Missing: $_POST[check_email] && $_POST[check_password]');}
        }
        die("samo post ili sta vec!");
    }
    
    
    
    
    
    
    
        
    
    public function sendMessageAction(){
        
       $request=$this->getRequest();
        
        if ($request->isPost()) {
          $post= $request->getPost()->toArray();
          if (isset($post['isuservalid'])) {
              if (Sess::get_user_session()) {
                  echo "true";
              }else{
                  echo "false";
              }
          } }
        $viewModel=new ViewModel();
         $viewModel->setTerminal(true);
        return $viewModel;
        
        
        
    }
    
    
    
    
    
    

    
    
    /*
     * 
     * 
     * 
     * 
     * ovaj controller  je za CRON JOBS
     * Nazalost zove se estan
     * 
     * 
     * 
     * 
     * 
     */
    
    public function estanAction(){
        ini_set('max_execution_time', 300);
        $adapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
         //-------------------------------
        //upload estan xml file
       function download_page($path){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$path);
	curl_setopt($ch, CURLOPT_FAILONERROR,1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	$retValue = curl_exec($ch);			 
	curl_close($ch);
	return $retValue;
}
         $Agencije=Agencije::$agencije_id;
         $url=Agencije::URL;
         $dir=Agencije::DOWNLOAD_DIR;
        
        foreach($Agencije as $key=>$v):
               $sXML = download_page($url.$v['file']);
                $reader = new \Zend\Config\Reader\Xml();       
                $doc = new \DOMDocument();
                $doc->loadXML($sXML);
                $doc->save($dir.$v['file']);
                $Xml=new Xml($adapter,$reader->fromFile($dir.$v['file']),$v['id_users_nk'],$v['estan_id_agencije']);
                $Xml->parse_data();               
        endforeach;
    //-------------------------------------------------------------------
         //clean server
       $Clean=new Cleanup($adapter);
        $Clean->cleanup_ads_images();
        $Clean->delete_capcha_imgs();
        $Clean->delete_old_ads();
        //-------------------------------
        
        //MOJ DOM XML
      $MojDom=new \Application\Models\WebServices\NekretnineMojDom\NekretnineMojDom($adapter);
      //------------------------------------------------
      
      
      $sql=new \Zend\Db\Sql\Sql($adapter);
      $insert=$sql->insert('cronjobs');
      $insert->values([
         'datetime'     =>date("Y-m-d H:i:s") 
      ]);
        $statement = $sql->prepareStatementForSqlObject($insert);
        $statement->execute();
        
        
     
        $viewModel=new ViewModel();
        $viewModel->setTerminal(true);
       return $viewModel;
    }
    
    
    
    
    
    
    /*
     * 
     * 
     * 
     * 
     * 
     * 
     * 
     */
    public function fonliderSmsAction(){
       
        $adapter=$this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        $get = $this->getRequest()->getQuery();
       //  var_dump($get); 
   $get['ip']=$_SERVER['REMOTE_ADDR'];
       $Fonlider=new \Application\Models\SmsWebService\Fonlider\FonLiderParser($adapter, $get);
      
     if ($_SERVER['REMOTE_ADDR'] !==$Fonlider::FONLIDER_SERVER) {
      //  die('Pogresni parametri');
}
        
        
         $this->getResponse()->getHeaders()->addHeaders([
             'Content-type' => 'text/plain',
             'X-Powered-By' =>'express',
         ]);  
      
         
         $return_msg=$Fonlider->id_validation();
         echo $return_msg['msg'];
         if ($return_msg['status']===false) {
             $Fonlider->insert_data(false);
         }
         
         
         
        $viewModel=new ViewModel([
            
        ]);
        
        
        $viewModel->setTerminal(true);
        return $viewModel;  
    }
     
  
    
    
    
    
}

<?php

namespace Application\Helpers\Widgets;


use Application\Models\Database\CustomQuery;


class NajnovijiOglasi extends CustomQuery 
{
    




    public function najnovijiOglasiHtml($num) {
    
        
        $str=' <div class="widget properties last">
    <div class="title">
        <h2>Njnoviji Oglasi</h2>
    </div><!-- /.title -->

    <div class="content">';
        
        foreach ($this->customQuery('CALL najnoviji_oglasi('.$num.')') as $v) {
                 if (strlen($v['title'])>23) {
                $v['title']=  substr($v['title'], 0,20);
                $v['title'].='...';
            }
            
            $str.='      
                <div class="property">
            <div class="image">
                <a href="'.$v['link'].'"></a>
                <img src="/img/tmp/'.$v['img'].'" alt="">
            </div><!-- /.image -->

            <div class="wrapper">
                <div class="title">
                    <h3>
                        <a style="font-size:11px;" href="'.$v['link'].'">'.$v['title'].' </a>
                    </h3>
                </div><!-- /.title -->
  <div style="font-size:11px;"  class="location">'.strtoupper($v['prodaja_izdavanje']).' | '.strtoupper($v['grad']).'</div><!-- /.location -->
 <div class="price">€'.number_format($v['cena'],0,'.','.').' <span style="float: right;margin-right: 2px; ">'.$v['kvadratura'].'m<sup>2</sup></span></div><!-- /.price -->
             
          </div><!-- /.property -->   </div><!-- /.wrapper -->';
        
     
            
        }
        
        
        
        $str.=' </div><!-- /.content --></div><!-- /.properties -->';
        
        
        
        
        return $str;
        
       }
    
    
    
    
    
    
    
    
    
    
    
    
    
}




<?php

namespace Application\Helpers;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
use Zend\View\Helper\AbstractHelper;
use Detection\MobileDetect;
/**
 * Description of PrikaziReklame
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class PrikaziReklame extends AbstractHelper{
     
    protected $google_ads=[
         '728x90'=>[
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- jedan Oglas --><ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-7076386533829894" data-ad-slot="8777028762"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>',
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Lista oglasa --><ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-7076386533829894" data-ad-slot="4381488761"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>',
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Main page prvi slider --><ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-7076386533829894" data-ad-slot="8047488763"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>',
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Naslovna --><ins class="adsbygoogle" style="display:inline-block;width:728px;height:90px" data-ad-client="ca-pub-7076386533829894" data-ad-slot="3511701161"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>',
         ],
     
         '320x100'=>[
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Big mobile --><ins class="adsbygoogle" style="display:inline-block;width:320px;height:100px" data-ad-client="ca-pub-7076386533829894" data-ad-slot="5416276368"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>',
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- long mobile --><ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-7076386533829894" data-ad-slot="5276675568"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>',
        ],
        '336x280'=>[
                  'pravougaonik'     =>'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- pravougaonik --><ins class="adsbygoogle"style="display:inline-block;width:336px;height:280px"data-ad-client="ca-pub-7076386533829894"data-ad-slot="8230141964"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>',
       
        ],
        '270x600'=>[
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- soliter270x600 --><ins class="adsbygoogle" style="display:inline-block;width:270px;height:600px" data-ad-client="ca-pub-7076386533829894" data-ad-slot="5782876366"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>'
        ],
        'responsive'=>[
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- Reduktivna --><ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-7076386533829894" data-ad-slot="9336615164" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>',
            '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- responsive1 --><ins class="adsbygoogle" style="display:block"data-ad-client="ca-pub-7076386533829894" data-ad-slot="1813348364" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>'
    
            ]
        
        
        
      
    ];
            
    
    protected $wrapper='<div class="bottom-wrapper">';
    protected $wrapper_end='</div>';
    
    
    
    
    protected $reklamni_baner=[
      'desktop'=>"<a href='http://nadjinekretnine.com/info/marketing#baneri'><img style='margin-bottom:2px' src='/img/baneri/reklamni_baner.jpg'/></a>"  ,
        'phone'=>"<a href='http://nadjinekretnine.com/info/marketing#baneri'><img style='margin-bottom:2px' src='/img/baneri/reklamni_baner_mobile.jpg'/></a>"  ,
        'tablet'=>"<a href='http://nadjinekretnine.com/info/marketing#baneri'><img style='margin-bottom:2px' src='/img/baneri/reklamni_baner.jpg'/></a>"  ,
    ];
    protected $flajer_baner=[
      'desktop'=>"<div class=\"bottom-wrapper\"><a href='#'><img style='' src='/img/baneri/flajer2.jpg'/></a></div>"  ,
        'phone'=>"<a href='#'><img style='margin-bottom:2px' src='/img/baneri/banermob.jpg'/></a>"  ,
        'tablet'=>"<a href='#'><img style='margin-bottom:2px' src='/img/baneri/banermob.jpg'/></a>"  ,
    ];
    
    
    
    
    protected function get_baner($name) {
        $detect_device=  $this->detect_device();
        switch ($name):
            case 'reklamni_baner':
                if ($this->reklamni_baner[$detect_device]) {
                    return $this->reklamni_baner[$detect_device];
                }
                return "";
            case 'flajer_baner': 
                    return $this->flajer_baner[$detect_device];
              return "";
            default:return "";
        endswitch;
    }


    protected function ready_ad($ad,$wrapper=false){
        $ad=$this->google_ads[$ad];
        $count=count($ad)-1;
        if ($this->count>$count) {
            $this->count=0;
        }
        if ($wrapper) {
            return $ad[$this->count++];
        }
       
        return $this->wrapper.$ad[$this->count++].$this->wrapper_end;
}
    

    protected function detect_device(){
        $device=new MobileDetect;
        if( $device->isMobile() && !$device->isTablet() ){
                return 'phone';
           }elseif($device->isTablet()){
               return 'tablet';
           }
           return 'desktop';
    }


    protected $count=0;
    public function __invoke(Array $conf=[],$size=false,$wrapper=false){   
        $device=  $this->detect_device();
        if ($size) {
            if (!in_array($device,$conf)) {
                 return "";
            }
            return $this->ready_ad($size,$wrapper);
        }
        
        if (count($conf)>0) {
            if (isset($conf['banner'])) {
                return $this->get_baner($conf['banner']['name']);
            }
            
            
            
            
            
            
             if (!in_array($device,$conf)) { return ""; }
        }
        
        switch($device):
            case 'desktop':return $this->ready_ad('728x90',$wrapper);
            case "tablet":return $this->ready_ad('320x100',$wrapper);
            case 'phone':return $this->ready_ad('responsive',$wrapper);
        endswitch;
        
        
        
    }
    
    
    
}

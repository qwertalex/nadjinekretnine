<?php
namespace Application\Newsletters\Templates;



class ZaboravljenaLozinka {
   
    
    public static function load_html($token) {
        return '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>[SUBJECT]</title>
      <style type="text/css">
      body {
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       margin:0 !important;
       width: 100% !important;
       -webkit-text-size-adjust: 100% !important;
       -ms-text-size-adjust: 100% !important;
       -webkit-font-smoothing: antialiased !important;
     }
     .tableContent img {
       border: 0 !important;
       display: block !important;
       outline: none !important;
     }
     a{
      color:#382F2E;
    }

    p, h1{
      color:#382F2E;
      margin:0;
    }
 p{
      text-align:left;
      color:#999999;
      font-size:14px;
      font-weight:normal;
      line-height:19px;
    }

    a.link1{
      color:#382F2E;
    }
    a.link2{
      font-size:16px;
      text-decoration:none;
      color:#ffffff;
    }

    h2{
      text-align:left;
       color:#222222; 
       font-size:19px;
      font-weight:normal;
    }
    div,p,ul,h1{
      margin:0;
    }


    </style>
<script type="colorScheme" class="swatch active">
{
    "name":"Default",
    "bgBody":"ffffff",
    "link":"ffffff",
    "color":"999999",
    "bgItem":"ffffff",
    "title":"222222"
}
</script>
  </head>
  <body paddingwidth="0" paddingheight="0" bgcolor="#ffffff"  style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center" bgcolor="#ffffff" style="font-family:Helvetica, Arial,serif;">

      
       

      <tr>
        <td>
          <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td width="40"></td>
              <td width="520">
                <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">

<!-- =============================== Header ====================================== -->           
                  

                  <tr><td height="25"></td></tr>
<!-- =============================== Body ====================================== -->

                  <tr>
                    <td class="movableContentContainer" valign="top">

                      <div lass="movableContent">
                        <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr>
                            <td valign="top" align="center">
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                  <p style="text-align:center;margin:0;font-family:Georgia,Time,sans-serif;font-size:26px;color:#222222;">
                                 <span style="color:#DC2828;">NadjiNekretnine.com</span></p>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </div>

                      <div lass="movableContent">
                        <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr>
                            <td valign="top" align="center">
                              <div class="contentEditableContainer contentImageEditable">
                                <div class="contentEditable">
                                  <img src="http://nadjinekretnine.com/img/newsletter/zaboravljena_lozinka/line.png" width="251" height="43" alt="" data-default="placeholder" data-max-width="560">
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </div>

                      <div class="movableContent">
                        <table width="520" border="0" cellspacing="0" cellpadding="0" align="center">
                          <tr><td height="55"></td></tr>
                          <tr>
                            <td align="left">
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable" align="center">
                                  <h2 >Promena lozinke</h2>
                                </div>
                              </div>
                            </td>
                          </tr>

                          <tr><td height="15"> </td></tr>

                          <tr>
                            <td align="left">
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable" align="center">
                                  <p  style="text-align:left;color:#999999;font-size:14px;font-weight:normal;line-height:19px;">
                                    Klikni te na sledeci link da bih ste promenili lozinku: 
                                    <a style="color:blue" href="http://nadjinekretnine.com/reset-password/'.$token.'" >
                                        http://nadjinekretnine.com/reset-password/'.$token.'</a>.
                                    <br>
                                  Ovaj link je aktivan sledeca 24h!
                                    
                                    <br>
                                    <br>
                                    Za sva pitanja u vezi sa registracijom ili tehničkim problemima,
pišite nam na  <br>e-mail:    <a href="mailto:info@nadjinekretnine.com">info@nadjinekretnine.com</a> 

                                
                               
                                  </p>
                                </div>
                              </div>
                            </td>
                          </tr>

                     

                          <tr>
              
                          </tr>
                      
                        </table>
                      </div>

              

                    </td>
                  </tr>

                  

<!-- =============================== footer ====================================== -->
                  


                </table>
              </td>
              <td width="40"></td>
            </tr>
          </table>
        </td>
      </tr>

      <tr><td height="88"></td></tr>


    </table>

    <!--Default Zone

        

        -->
        <!--Default Zone End-->

      </body>
      </html>

';
    }

    



   
    
    
}




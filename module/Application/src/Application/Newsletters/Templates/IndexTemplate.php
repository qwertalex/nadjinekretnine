<?php

namespace Application\Newsletters\Templates;

/*
 * The MIT License
 *
 * Copyright 2015 aleksa.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Description of indexTemplate
 *
 * @author aleksa
 * 
 * contact <qwertalex@yahoo.com>
 */
class IndexTemplate {
    
    
    
    
    
    public static function template(){
        return '
    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>[SUBJECT]</title>
    <style type="text/css">
      body {background: #ffffff; font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif; font-size:16px; line-height:150%; color:#444; margin:0; padding:0;}
        p,li {margin-bottom:14px; margin-top:0;font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;font-size:16px;color:#313131; line-height:150%;}
        td, div {font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;font-size:13px; }
        a {color:#013275;text-decoration: none;font-weight: bold;}
        h1 {margin-top:15px;padding:0; color:#D70000; font-size:30px;font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight: normal;}
        h2 {margin-top:0; color:#D70000; font-size:18px;font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight:normal;}
        h3 {margin:0;font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;}
        .bgItem{background: #D70000;}   
        ul {list-style-image: url(images/bullet.png);}
        .hero-unit {
padding: 60px;
margin-bottom: 30px;
font-size: 18px;
font-weight: 200;
line-height: 30px;
color: inherit;
background-color: #eeeeee;
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
}
      </style>
 
  </head>
    <body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4"  style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
    <!-- main container -->
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%"><!-- MASTER TABLE; controls the movable content container! -->
      <tr>
        <td align="center" class="movableContentContainer ">               
          
          <!-- Header: Logo; company name; website nav bar - Image + text -->
          <div class="movableContent">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
              <tbody>
                <tr>
                  <td align="center" colspan="9" height="20">
                  </td>
                </tr>
                <tr>
                  <td valign="top" height="39" width="250">
                    <div class="contentEditableContainer contentImageEditable">
                      <div class="contentEditable">
                      <a href="http://nadjinekretnine.com">
                        <img data-default="placeholder" data-max-width="75" data-max-height="60" src="http://nadjinekretnine.com/img/logo-black1.png" width="250" height="" >
                        </a>
                        </div>
                    </div>
                  </td>
                  <td width="15">
                  </td>
                 
                  <td width="20">
                  </td>
                  <td width="65" valign="middle">
                    <div class="contentEditableContainer contentTextEditable">
                      <div class="contentEditable">
                        <a href="http://nadjinekretnine.com" style="text-align:right;">
                          Home</a>
                      </div>
                    </div>
                  </td>
                  <td width="10">
                  </td>
                  <td width="65" valign="middle">
                    <div class="contentEditableContainer contentTextEditable">
                      <div class="contentEditable">
                        <a href="http://nadjinekretnine.com/info/marketing" style="text-align:right;">
                          Marketing</a>
                      </div>
                    </div>
                  </td>
                  <td width="10">
                  </td>
                  
                </tr>
               
              </tbody>
            </table>
          </div>
          <!-- END: Header: Logo; company name; website nav bar - Image + text -->
  
          <!-- END: Featured Image -->
   <!-- Section Title: Benefits-->
          <div class="movableContent">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
              <td align="center">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                  <!-- Spacing-->
                  <tr>
                    <td colspan="5" height="20">
                    </td>
                  </tr>
                  <!-- .Spacing-->
                  <tr>
                    <td width="180"> 
                      <!--h1Line-->
                      <div class="movableContent">
                        <div class="contentEditableContainer contentImageEditable">
                          <div class="contentEditable">
                            <img data-default="placeholder" data-max-width="180" src="http://nadjinekretnine.com/img/newsletter/newsletter_line.png" width="180" height="30"/>
                          </div>
                        </div>
                      </div>
                      <!--.h1Line-->
                    </td>
                   
                    <td width="180" align="center"> 
                      <div class="contentEditableContainer contentTextEditable">
                      <div class="contentEditable">
                    <h1>Besplatni Oglasi</h1>
                    </div></div>
                    </td>
                   
                    <td width="180" height="30">
                      <!--h1Line-->
                      <div class="movableContent">
                        <div class="contentEditableContainer contentImageEditable">
                          <div class="contentEditable">
                            <img data-default="placeholder" data-max-width="180" src="http://nadjinekretnine.com/img/newsletter/newsletter_line.png" width="180" height="30"/>
                          </div>
                        </div>
                      </div>
                      <!--.h1Line-->
                      </td>
                    </tr>
                    <tr>
                     
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </div>
          <!-- .Section Title: Benefits-->
   <!-- Title -->
          <div class="movableContent">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
             
              <tr>
                <td>
                  <div class="contentEditableContainer contentTextEditable">
                    <div class="contentEditable">
                    
                      <p style="color:#666666;text-align:left;">
                      Na nasem portalu mozete besplatno da postavite vase oglase.

                       Za vise informacija posetite nasu stranicu <a href="http://nadjinekretnine.com/info/marketing">marketning</a>.
</p>
                    </div>
                  </div>
                </td>
              </tr>
              
            </table>
          </div>
          <!-- END: Title -->

         
 

          <!-- Section Title: Benefits-->
          <div class="movableContent">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
              <td align="center">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                  <!-- Spacing-->
                  <tr>
                    <td colspan="5" height="20">
                    </td>
                  </tr>
                  <!-- .Spacing-->
                  <tr>
                    <td width="180"> 
                      <!--h1Line-->
                      <div class="movableContent">
                        <div class="contentEditableContainer contentImageEditable">
                          <div class="contentEditable">
                            <img data-default="placeholder" data-max-width="180" src="http://nadjinekretnine.com/img/newsletter/newsletter_line.png" width="180" height="30"/>
                          </div>
                        </div>
                      </div>
                      <!--.h1Line-->
                    </td>
                   
                    <td width="180" align="center"> 
                      <div class="contentEditableContainer contentTextEditable">
                      <div class="contentEditable">
                    <h1>Stedite novac i zaradite</h1>
                    </div></div>
                    </td>
                   
                    <td width="180" height="30">
                      <!--h1Line-->
                      <div class="movableContent">
                        <div class="contentEditableContainer contentImageEditable">
                          <div class="contentEditable">
                            <img data-default="placeholder" data-max-width="180" src="http://nadjinekretnine.com/img/newsletter/newsletter_line.png" width="180" height="30"/>
                          </div>
                        </div>
                      </div>
                      <!--.h1Line-->
                      </td>
                    </tr>
                    <tr>
                     
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </div>
          <!-- .Section Title: Benefits-->
 
          <!--Featured Product -->
          <div class="movableContent">
                <table align="center" width="600" valign="middle" cellpadding="0" style="margin:0;" cellspacing="0" border="0">
                  <tr>
                   
                  </tr>
                  <tr>
                    <td valign="top">
                      <div class="contentEditableContainer contentTextEditable">
                        <div class="contentEditable">
                          <h2 style="text-align:left;">
                            Specijalna ponuda za AGENCIJE, prijavite se za nas servis PRVI KUPAC  
 postanite nas partner i zaradite.</h2>
                         <ul style="text-align:left;color:black" >

<li>Vas klijent se putem SMS-a prijavi na servis.</li>
<li>Vi vasim klijentima saljete ekskluzivne informacije.</li>
<li>Informacije se prijavlenom korisniku isporucuju dok se ne odjavi</li>
<li>Svaki SMS se primaocu naplacuje po ceni od 70 din + PDV</li>

</ul>

                         <h2 style="text-align:left;">
    Uz servis prvi kupac potrebno je da:</h2>
                         <ul style="text-align:left;color:black" >

<li>Unesete detalje poruke u aplikaciju. </li>
 <li>Stisnete OK </li>
</ul>



                         <h2 style="text-align:left;">
     Na 50 POSLATIH PORUKA PRIHOD izgleda ovako:</h2>
 
 <ul>
 	<li>Cena poruke je 70 din </li>
 <li>21 do 35 din za mobilne operatere </li>
 <li>50% prihoda * 50 poruka </li>

<li><b>Oko 3000 din PRIHODA</b></li>
 </ul>
  
 <p style=" ">
Za vise informacija kliknite <a href="http://nadjinekretnine.com/info/marketing">ovde</a>.

 </p>
   </div>
                      </div>
                    </td>
                   
                   
                  </tr>
                  <tr>
                    <td colspan="3" height="20">
                    </td>
                  </tr>
                </table>
          </div>
          <!-- END: Featured Product -->

          

          <!-- Contact Us: Phone & Email -->
          <div class="movableContent">
                <table align="center" width="600" valign="middle" cellpadding="0" style="margin:0;" cellspacing="0" border="0">
                  <tr>
                    <td colspan="3" height="20">
                    </td>
                  </tr>
                  
                  <tr>
                    <td valign="top" width="">
                <div class="hero-unit">
    <p>
        Ako imate  pitanja slobodno nas kontaktirajte.
    </p>
         <div class="row">
                    <div class="span3">
                                <h3 class="call-us">Marketing mendzer</h3>
                                <p class="content-icon-spacing">
                                    +381 65 831-5363<br>
                                     
                                </p>
                            </div>        
                            <div class="span3">
                                <h3 class="call-us">Tehnicka podrska</h3>
                                <p class="content-icon-spacing">
                                    +381 60 0-333-807<br>
                                     
                                </p>
                            </div>
                            <div class="span3">
                                <h3 class="email">Email</h3>
                                <p class="content-icon-spacing">
                                    <a href="mailto:info@nadjinekretnine.com">info@nadjinekretnine.com</a><br>
                         
                                </p>
                            </div>
                        </div>
            </div>
                    </td>
                    
                 
                </tr>
 
                </table>
          </div>
          <!-- END: Contact Us: Phone & Email -->
  

        </td>
      </tr>
    </table>
 

  </body>
  </html>
    ';
        
        
        
        
    }
        
        
     
    
    
    
    
    
    
    
}

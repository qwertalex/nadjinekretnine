<?php
namespace Application\Newsletters\Templates;





class MysticalBlue {
 
    public static $Theme_blue="<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
  <title>Mystical template</title>
  <style type='text/css'>
  body {
   padding-top: 0 !important;
   padding-bottom: 0 !important;
   padding-top: 0 !important;
   padding-bottom: 0 !important;
   margin:0 !important;
   width: 100% !important;
   -webkit-text-size-adjust: 100% !important;
   -ms-text-size-adjust: 100% !important;
   -webkit-font-smoothing: antialiased !important;
 }
 .tableContent img {
   border: 0 !important;
   display: block !important;
   outline: none !important;
 }
 a{
  color:#382F2E;
}

p, h1{
  color:#382F2E;
  margin:0;
}

div,p,ul,h1{
  margin:0;
}


</style>


</head>
<body paddingwidth='0' paddingheight='0' bgcolor='#d1d3d4'  style='padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;' offset='0' toppadding='0' leftpadding='0'>
  <table width='100%' border='0' cellspacing='0' cellpadding='0' class='tableContent' align='center' bgcolor='#F6F6F6'>
    <tr>
      <td valign='top' >
        <table  width='600' border='0' cellspacing='0' cellpadding='0' align='center' style='font-family:helvetica, sans-serif;' bgcolor='#F6F6F6'>
          <tr>
            <td width='20'></td>
            <td>
              <table width='560' border='0' cellspacing='0' cellpadding='0' align='center'>
                <!--  =========================== The header ===========================  -->                
                <tr><td height='25'></td></tr>

                <tr>
                  <td>
                    <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' >
                      <tr>

                        <td align='left' valign='middle' >
                          <div class='contentEditableContainer contentImageEditable'>
                            <div class='contentEditable' >
                              <img src='http://nadjinekretnine.com/img/newsletter/mystical_blue/logo.png' alt='Compagnie logo' data-default='placeholder' data-max-width='300' width='107' height='27'>
                            </div>
                          </div>
                        </td>

                        <td align='right' valign='top' >
                          <div class='contentEditableContainer contentTextEditable' style='display:inline-block;'>
                            <div class='contentEditable' >
                              <a href='[SHOWEMAIL]' style='color:#A8B0B6;font-size:13px;text-decoration:none;'>Open in your browser</a>
                            </div>
                          </div>
                        </td>
                        <td width='18' align='right' valign='top'>
                          <div class='contentEditableContainer contentImageEditable' style='display:inline-block;'>
                            <div class='contentEditable' >
                              <a href='[SHOWEMAIL]'><img src='http://nadjinekretnine.com/img/newsletter/mystical_blue/openBrowser.png' alt='open in browser image' data-default='placeholder' width='15' height='15' style='padding-left:10px;'></a>
                            </div>
                          </div>
                        </td>

                      </tr>
                    </table>
                  </td>  
                </tr>

                <tr><td height='25'></td></tr>

                <tr>
                  <td bgcolor='#52619E' align='center' valign='middle' width='560' height='184' style='background:url(\'/bigImg.png\') #52619E no-repeat;background-size:560px 184px;'>
                    <div class='contentEditableContainer contentTextEditable'>
                      <div class='contentEditable' >
                        <h1 style='color:#ffffff;font-size:32px;font-weight:normal;'>This is a background image</h1>
                        <p style='color:#ffffff;font-size:14px;line-height:19px;'>You can change it in the html but, be sure to have the exact size : 560px by 184px.</p>
                      </div>
                    </div>
                  </td>
                </tr>

                <tr>
                  <td bgcolor='#2D3867' align='right' style='padding:20px;' align='center'>
                    <div class='contentEditableContainer contentTextEditable' >
                      <div class='contentEditable' >
                        <p style='color:#ffffff;font-size:16px;line-height:19px;'>Rates and Services.</p>
                      </div>
                    </div>
                  </td>
                </tr>

                <!--  =========================== The body ===========================  -->
                <tr>
                  <td class='movableContentContainer'>
                    <!--  ==== movable content in the body ===  -->
                    
                    <div class='movableContent'>
                      <table width='560' border='0' cellspacing='0' cellpadding='0'>
                        <tr><td height='25'></td></tr>
                        <tr>
                          <td>
                            <table width='558' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd'>
                              <tr><td colspan='3' height='16'></td></tr>
                              <tr>
                                <td width='16'></td>
                                <td>
                                  <table width='526' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff'>
                                    <tr>
                                      <td align='left' valign='top'>
                                        <div class='contentEditableContainer contentTextEditable' >
                                         <div class='contentEditable' >
                                          <h1 style='color:#555555;font-size:21px;font-weight:normal;'>About us
                                          </h1>
                                        </div>
                                      </div>
                                    </td>
                                  </tr>
                                  <tr><td height='13'></td></tr>
                                  <tr>
                                    <td align='left' valign='top'>
                                      <div class='contentEditableContainer contentTextEditable' >
                                       <div class='contentEditable' >
                                        <p style='color:#999999;font-size:13px;line-height:19px;'>Give information about your company, the services you offer and your vision. Link back to your website.</p>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr><td height='20'></td></tr>
                                <tr>
                                  <td align='right' valign='top' style='padding-bottom:8px;'>
                                    <div class='contentEditableContainer contentTextEditable' >
                                     <div class='contentEditable' >
                                      <a href='#' style='padding:8px;background:#52619E;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;'>Find out more → </a>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            </table>

                          </td>
                          <td width='16'></td>
                        </tr>
                        <tr><td colspan='3' height='16'></td></tr>
                      </table>
                    </td>
                  </tr>
                </table>

              </div>

              <div class='movableContent'>
                <table width='560' border='0' cellspacing='0' cellpadding='0'>
                  <tr><td height='25'></td></tr>
                  <tr>
                    <td>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr>
                          <td width='269' style='border:1px solid #dddddd;' bgcolor='ffffff'>
                            <table width='269' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='9' colspan='3'></td></tr>

                              <tr>
                                <td width='9'></td>
                                <td width='251'>
                                  <div class='contentEditableContainer contentImageEditable'>
                                    <div class='contentEditable'>
                                      <img src='http://nadjinekretnine.com/img/newsletter/mystical_blue/leftImg.png' alt='product image' data-default='placeholder' data-max-width='251' width='251' height='142' >
                                    </div>
                                  </div>
                                </td>
                                <td width='9'></td>
                              </tr>

                              <tr><td height='9' colspan='3'></td></tr>
                            </table>
                          </td>

                          <td width='18'></td>

                          <td width='269' style='border:1px solid #dddddd;' bgcolor='ffffff'>
                            <table width='269' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='9' colspan='3'></td></tr>

                              <tr>
                                <td width='9'></td>
                                <td width='251'>
                                  <div class='contentEditableContainer contentImageEditable'>
                                    <div class='contentEditable'>
                                      <img src='http://nadjinekretnine.com/img/newsletter/mystical_blue/rightImg.png' alt='product image' data-default='placeholder' data-max-width='251' width='251' height='142' >
                                    </div>
                                  </div>
                                </td>
                                <td width='9'></td>
                              </tr>

                              <tr><td height='9' colspan='3'></td></tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </div>

              <div class='movableContent'>
                <table width='560' border='0' cellspacing='0' cellpadding='0'>
                  <tr><td height='25'></td></tr>
                  <tr>
                    <td>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr>
                          <td width='175' valign='top'>
                            <table width='175' border='0' cellspacing='0' cellpadding='0' align='left' valign='top' bgcolor='#52619E'>
                              <tr><td colspan='3' height='18'></td></tr>

                              <tr>
                                <td width='18'></td>

                                <td width='139'>
                                  <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                      <h1 style='color:#ffffff;font-size:21px;font-weight:normal;'>Free estimate</h1>
                                    </div>
                                  </div>
                                </td>

                                <td width='18'></td>
                              </tr>

                              <tr><td colspan='3' height='15'></td></tr>

                              <tr>
                                <td width='18'></td>

                                <td width='139'>
                                  <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                      <p style='color:#C0CAED;font-size:13px;line-height:19px;'>Thinking of revamping your website?</p>
                                    </div>
                                  </div>
                                </td>

                                <td width='18'></td>
                              </tr>

                              <tr><td colspan='3' height='18'></td></tr>

                              <tr><td colspan='3' height='18' bgcolor='#2D3867'></td></tr>

                              <tr>
                                <td width='18' bgcolor='#2D3867'></td>
                                <td width='139' bgcolor='#2D3867'>
                                  <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                      <a href='#' style='color:#C0CAED;font-size:13px;text-decoration:none;'>Get a free estimate → </a>
                                    </div>
                                  </div>
                                </td>
                                <td width='18' bgcolor='#2D3867'></td>
                              </tr>

                              <tr><td colspan='3' height='18' bgcolor='#2D3867'></td></tr>
                            </table>
                          </td>

                          <td width='18'></td>

                          <td width='365' style='border:1px solid #dddddd;' bgcolor='ffffff' valign='top'>
                            <table width='365' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='15' colspan='3'></td></tr>

                              <tr>
                                <td width='20'></td>
                                <td width='325'>
                                  <table width='327' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Why you should revamp your website</h1>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr><td height='13'></td></tr>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <p style='color:#999999;font-size:13px;line-height:19px;'>What’s the value of the services you offer? Why should people consider hiring your rather than doing the work internally?
                                            </p>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr><td height='16'></td></tr>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <a href='#' style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <td width='20'></td>
                              </tr>

                              <tr><td height='15' colspan='3'></td></tr>
                            </table>
                          </td>
                        </tr>

                      </table>
                    </td>
                  </tr>
                </table>
              </div>

              <div class='movableContent'>
                <table width='560' border='0' cellspacing='0' cellpadding='0'>
                  <tr><td height='25'></td></tr>
                  <tr>
                    <td>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr>
                          <td width='316' style='border:1px solid #dddddd;' bgcolor='ffffff' valign='top'>
                            <table width='316' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='15' colspan='3'></td></tr>

                              <tr>
                                <td width='20'></td>
                                <td width='276'>
                                  <table width='276' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Customer testimonial</h1>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr><td height='13'></td></tr>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <p style='color:#999999;font-size:13px;line-height:19px;'>Have a happy customer? A case study? White paper? Share the information. Hint at the story, but give the full picture on your website</p>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr><td height='16'></td></tr>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <a href='#' style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <td width='20'></td>
                              </tr>

                              <tr><td height='15' colspan='3'></td></tr>
                            </table>
                          </td>

                          <td width='18'></td>

                          <td width='224' style='border:1px solid #dddddd;' bgcolor='ffffff' valign='top'>
                            <table width='224' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='15' colspan='3'></td></tr>

                              <tr>
                                <td width='20'></td>
                                <td width='180'>
                                  <table width='180' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Statistics
                                            </h1>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr><td height='13'></td></tr>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <ul style='padding-left: 20px;'>
                                              <li style='color:#999999;font-size:13px;line-height:19px;'>
                                                Proven data and analysis
                                              </li>
                                              <li style='color:#999999;font-size:13px;line-height:19px;'>
                                                Can help people make
                                              </li>
                                              <li style='color:#999999;font-size:13px;line-height:19px;'>
                                                Rational decisions
                                              </li>
                                            </ul>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                    <tr><td height='16'></td></tr>
                                    <tr>
                                      <td>
                                        <div class='contentEditableContainer contentTextEditable'>
                                          <div class='contentEditable'>
                                            <a href='#' style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a>
                                          </div>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <td width='20'></td>
                              </tr>

                              <tr><td height='15' colspan='3'></td></tr>
                            </table>
                          </td>
                        </tr>

                      </table>
                    </td>
                  </tr>
                </table>
                
              </div>




            </td>
          </tr>
          <!--  =========================== The footer ===========================  -->

          <tr><td height='30'></td></tr>

          <tr>
            <td bgcolor='#52619E'>
              <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#52619E'>
                <tr>
                  <td bgcolor='#52619E' width='416' align='left' valign='top'>
                    <table width='416' border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#52619E'>
                      <tr><td height='28' colspan='3'></td></tr>
                      <tr>
                        <td width='30'></td>
                        <td valign='middle'>
                          <div class='contentEditableContainer contentTextEditable'>
                            <div class='contentEditable'>
                              <p style='color:#ffffff;font-size:14px;line-height:19px;'>
                                Stay in Touch! 
                              </p>
                            </div>
                          </div>
                        </td>
                        <td width='10'></td>
                      </tr>
                      <tr><td height='28' colspan='3'></td></tr>
                    </table>
                  </td>
                  <td bgcolor='#3E4A7F' width='72' valign='top' align='center'>
                    <table width='72' width='72' border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#3E4A7F'>
                      <tr>
                        <td bgcolor='#3E4A7F' width='72' height='72' align='center' valign='middle'>
                          <div class='contentEditableContainer contentFacebookEditable'>
                            <div class='contentEditable' valign='middle'>
                              <a href=''><img src='http://nadjinekretnine.com/img/newsletter/mystical_blue/facebook.png' alt='facebook link' data-default='placeholder' data-max-width='37' width='37' height='37' ></a>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td bgcolor='#2D3867' width='72' valign='top' align='center'>
                    <table width='72' height='72' border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#2D3867'>
                      <tr>
                        <td bgcolor='#2D3867' width='72' height='72' align='center' valign='middle'>
                          <div class='contentEditableContainer contentTwitterEditable'>
                            <div class='contentEditable' valign='middle'>
                              <a href=''><img src='http://nadjinekretnine.com/img/newsletter/mystical_blue/twitter.png' alt='twitter link' data-default='placeholder' data-max-width='37' width='37' height='37' ></a>
                            </div>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>

          <tr><td height='28'></td></tr>

          <tr>
            <td valign='top' align='center'>
              <div class='contentEditableContainer contentTextEditable'>
                <div class='contentEditable' >
                  <p style='color:#A8B0B6; font-size:13px;line-height: 16px;'>This email was sent to [email] when you signed up on [CLIENTS.WEBSITE] Please add us to your contacts to ensure the newsletters land in your inbox.
                  </p>
                </div>
              </div>
            </td>
          </tr>

          <tr><td height='28'></td></tr>

          <tr>
            <td valign='top' align='center'>
              <div class='contentEditableContainer contentTextEditable'>
                <div class='contentEditable' >
                  <p style='color:#A8B0B6; font-weight:bold;font-size:13px;line-height: 30px;'>[CLIENTS.COMPANY_NAME]</p>
                </div>
              </div>
              <p style='color:#A8B0B6;font-size:13px;line-height: 15px;'>[CLIENTS.ADDRESS]</p>
              <div class='contentEditableContainer contentTextEditable'>
                <div class='contentEditable' >
                  <a href='[UNSUBSCRIBE]' style='line-height: 20px;color:#A8B0B6; font-size:13px;'>Unsubscribe</a>
                </div>
              </div>
            </td>
          </tr>

          <tr><td height='28'></td></tr>

        </table>
      </td>
      <td width='20'></td>
    </tr>
  </table>
</td>
</tr>
</table>

<!--Default Zone

      <div class='customZone' data-type='image'>
          <div class='movableContent'>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr>
                          <td width='269' style='border:1px solid #dddddd;' bgcolor='ffffff'>
                            <table width='269' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='9' colspan='3'></td></tr>

                              <tr>
                                <td width='9'></td>
                                <td width='251'>
                                  <div class='contentEditableContainer contentImageEditable'>
                                    <div class='contentEditable'>
                                      <img src='http://nadjinekretnine.com/img/newsletter/mystical_blue/leftImg.png' alt='product image' data-default='placeholder' data-max-width='251' width='251' height='142' >
                                    </div>
                                  </div>
                                </td>
                                <td width='9'></td>
                              </tr>

                              <tr><td height='9' colspan='3'></td></tr>
                            </table>
                          </td>

                          <td width='18'></td>
                          
                          <td width='269' style='border:1px solid #dddddd;' bgcolor='ffffff'>
                            <table width='269' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='9' colspan='3'></td></tr>

                              <tr>
                                <td width='9'></td>
                                <td width='251'>
                                  <div class='contentEditableContainer contentImageEditable'>
                                    <div class='contentEditable'>
                                      <img src='http://nadjinekretnine.com/img/newsletter/mystical_blue/rightImg.png' alt='product image' data-default='placeholder' data-max-width='251' width='251' height='142' >
                                    </div>
                                  </div>
                                </td>
                                <td width='9'></td>
                              </tr>

                              <tr><td height='9' colspan='3'></td></tr>
                            </table>
                          </td>
                        </tr>
                        
                      </table>
                    </div>
      </div>
      
      <div class='customZone' data-type='text'>
          <div class='movableContent'>
                      <table width='558' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd'>
                        <tr><td colspan='3' height='16'></td></tr>
                        <tr>
                          <td width='16'></td>
                          <td>
                              <table width='526' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff'>
                                <tr>
                                  <td align='left' valign='top'>
                                    <div class='contentEditableContainer contentTextEditable' >
                             <div class='contentEditable' >
                                    <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Cinnamon Roll</h1>
                                    </div>
                            </div>
                                  </td>
                                </tr>
                                <tr><td height='13'></td></tr>
                                <tr>
                                  <td align='left' valign='top'>
                                    <div class='contentEditableContainer contentTextEditable' >
                             <div class='contentEditable' >
                                    <p style='color:#999999;font-size:13px;line-height:19px;'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                    </div>
                            </div>
                                  </td>
                                </tr>
                                <tr><td height='20'></td></tr>
                                <tr>
                                  <td align='right' valign='top' style='padding-bottom:8px;'>
                                    <div class='contentEditableContainer contentTextEditable' >
                             <div class='contentEditable' >
                                    <a href='#' style='padding:8px;background:#52619E;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;'>Find out more →</a>
                                    </div>
                            </div>
                                  </td>
                                </tr>
                              </table>

                          </td>
                          <td width='16'></td>
                        </tr>
                        <tr><td colspan='3' height='16'></td></tr>
                      </table>
                    </div>
      </div>
      
      <div class='customZone' data-type='imageText'>
          <div class='movableContent'>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr>
                          <td width='175' valign='top'>
                                <table width='175' border='0' cellspacing='0' cellpadding='0' align='left' valign='top' bgcolor='#77529E'>
                                  <tr><td colspan='3' height='18'></td></tr>

                                  <tr>
                                    <td width='18'></td>

                                    <td width='139'>
                                      <div class='contentEditableContainer contentTextEditable'>
                              <div class='contentEditable'>
                                      <h1 style='color:#ffffff;font-size:21px;font-weight:normal;'>50% Off</h1>
                                      </div>
                            </div>
                                    </td>

                                    <td width='18'></td>
                                  </tr>

                                  <tr><td colspan='3' height='15'></td></tr>

                                  <tr>
                                    <td width='18'></td>

                                    <td width='139'>
                                      <div class='contentEditableContainer contentTextEditable'>
                              <div class='contentEditable'>
                                      <p style='color:#DCC5F4;font-size:13px;line-height:19px;'>Lorem Ipsum is simply dummy text of the printing.</p>
                                      </div>
                            </div>
                                    </td>

                                    <td width='18'></td>
                                  </tr>

                                  <tr><td colspan='3' height='18'></td></tr>

                                  <tr><td colspan='3' height='18' bgcolor='#533474'></td></tr>

                                  <tr>
                                    <td width='18' bgcolor='#533474'></td>
                                    <td width='139' bgcolor='#533474'>
                                      <div class='contentEditableContainer contentTextEditable'>
                              <div class='contentEditable'>
                                      <a href='#' style='color:#52619E;font-size:13px;text-decoration:none;'>Find out more →</a>
                                      </div>
                            </div>
                                    </td>
                                    <td width='18' bgcolor='#533474'></td>
                                  </tr>

                                  <tr><td colspan='3' height='18' bgcolor='#533474'></td></tr>
                                </table>
                          </td>

                          <td width='18'></td>
                          
                          <td width='365' style='border:1px solid #dddddd;' bgcolor='ffffff' valign='top'>
                            <table width='365' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='15' colspan='3'></td></tr>

                              <tr>
                                <td width='20'></td>
                                <td width='325'>
                                      <table width='327' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Sesame Snaps</h1>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                        <tr><td height='13'></td></tr>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <p style='color:#999999;font-size:13px;line-height:19px;'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has conquered the planet earth.</p>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                        <tr><td height='16'></td></tr>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <a href='#' style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                      </table>
                                </td>
                                <td width='20'></td>
                              </tr>

                              <tr><td height='15' colspan='3'></td></tr>
                            </table>
                          </td>
                        </tr>
                        
                      </table>
                    </div>
      </div>
      
      <div class='customZone' data-type='Textimage'>
          <div class='movableContent'>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr>
                          <td width='365' valign='top' style='border:1px solid #dddddd;' bgcolor='ffffff' valign='top'>
                            <table width='365' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='15' colspan='3'></td></tr>

                              <tr>
                                <td width='20'></td>
                                <td width='325'>
                                      <table width='327' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Sesame Snaps</h1>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                        <tr><td height='13'></td></tr>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <p style='color:#999999;font-size:13px;line-height:19px;'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has conquered the planet earth.</p>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                        <tr><td height='16'></td></tr>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <a href='#' style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                      </table>
                                </td>
                                <td width='20'></td>
                              </tr>

                              <tr><td height='15' colspan='3'></td></tr>
                            </table>
                            
                          </td>

                          <td width='18'></td>
                          
                          <td width='175' >
                            
                                <table width='175' border='0' cellspacing='0' cellpadding='0' align='left' valign='top' bgcolor='#77529E'>
                                  <tr><td colspan='3' height='18'></td></tr>

                                  <tr>
                                    <td width='18'></td>

                                    <td width='139'>
                                      <div class='contentEditableContainer contentTextEditable'>
                              <div class='contentEditable'>
                                      <h1 style='color:#ffffff;font-size:21px;font-weight:normal;'>50% Off</h1>
                                      </div>
                            </div>
                                    </td>

                                    <td width='18'></td>
                                  </tr>

                                  <tr><td colspan='3' height='15'></td></tr>

                                  <tr>
                                    <td width='18'></td>

                                    <td width='139'>
                                      <div class='contentEditableContainer contentTextEditable'>
                              <div class='contentEditable'>
                                      <p style='color:#DCC5F4;font-size:13px;line-height:19px;'>Lorem Ipsum is simply dummy text of the printing.</p>
                                      </div>
                            </div>
                                    </td>

                                    <td width='18'></td>
                                  </tr>

                                  <tr><td colspan='3' height='18'></td></tr>

                                  <tr><td colspan='3' height='18' bgcolor='#533474'></td></tr>

                                  <tr>
                                    <td width='18' bgcolor='#533474'></td>
                                    <td width='139' bgcolor='#533474'>
                                      <div class='contentEditableContainer contentTextEditable'>
                              <div class='contentEditable'>
                                      <a href='#' style='color:#52619E;font-size:13px;text-decoration:none;'>Find out more →</a>
                                      </div>
                            </div>
                                    </td>
                                    <td width='18' bgcolor='#533474'></td>
                                  </tr>

                                  <tr><td colspan='3' height='18' bgcolor='#533474'></td></tr>
                                </table>

                          </td>
                        </tr>
                        
                      </table>
                    </div>
      </div>

      <div class='customZone' data-type='textText'>
          <div class='movableContent'>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr>
                          <td width='316' style='border:1px solid #dddddd;' bgcolor='ffffff' valign='top'>
                            <table width='316' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='15' colspan='3'></td></tr>

                              <tr>
                                <td width='20'></td>
                                <td width='276'>
                                      <table width='276' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Sesame Snaps</h1>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                        <tr><td height='13'></td></tr>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <p style='color:#999999;font-size:13px;line-height:19px;'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has conquered the planet earth.</p>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                        <tr><td height='16'></td></tr>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <a href='#' style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                      </table>
                                </td>
                                <td width='20'></td>
                              </tr>

                              <tr><td height='15' colspan='3'></td></tr>
                            </table>
                          </td>

                          <td width='18'></td>
                          
                          <td width='224' style='border:1px solid #dddddd;' bgcolor='ffffff' valign='top'>
                            <table width='224' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                              <tr><td height='15' colspan='3'></td></tr>

                              <tr>
                                <td width='20'></td>
                                <td width='180'>
                                      <table width='180' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Sesame Snaps</h1>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                        <tr><td height='13'></td></tr>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <p style='color:#999999;font-size:13px;line-height:19px;'>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has conquered the planet earth.</p>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                        <tr><td height='16'></td></tr>
                                        <tr>
                                          <td>
                                            <div class='contentEditableContainer contentTextEditable'>
                                    <div class='contentEditable'>
                                            <a href='#' style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a>
                                            </div>
                                  </div>
                                          </td>
                                        </tr>
                                      </table>
                                </td>
                                <td width='20'></td>
                              </tr>

                              <tr><td height='15' colspan='3'></td></tr>
                            </table>
                          </td>
                        </tr>
                        
                      </table>
                    </div>
      </div>

      <div class='customZone' data-type='qrcode'>
          <div class='movableContent'>
                      <div >
                        <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd;'>
                            <tr><td height='15' colspan='4'></td></tr>

                            <tr>
                              <td width='20'></td>
                              <td valign='top' valign='top' width='75' style='padding:0 20px;'>
                                  <div class='contentQrcodeEditable contentEditableContainer'>
                                      <div class='contentEditable'>
                                          <img src='/applications/Mail_Interface/3_3/modules/User_Interface/core/v31_campaigns//neweditor/default/qr_code.png' width='75' height='75'>
                                      </div>
                                  </div>
                              </td>
                              <td valign='top' valign='top' style='padding:0 20px 0 20px;'>
                                <div class='contentTextEditable contentEditableContainer'>
                                  <div class='contentEditable'>
                                    <p style='color:#999999;font-size:13px;line-height:19px;'>Etiam bibendum nunc in lacus bibendum porta. Vestibulum nec nulla et eros ornare condimentum. Proin facilisis, dui in mollis blandit. Sed non dui magna, quis tincidunt enim. Morbi vehicula pharetra lacinia. Cras tincidunt, justo at fermentum feugiat, eros orci accumsan dolor, eu ultricies eros dolor quis sapien.</p>
                                    <p style='color:#999999;font-size:13px;line-height:19px;'>Integer in elit in tortor posuere molestie non a velit. Pellentesque consectetur, nisi a euismod scelerisque.</p>
                                    <p style='text-align:right;margin:0;f'><a style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a></p>
                                    
                                  </div>
                                </div>
                              </td>
                              <td width='20'></td>
                            </tr>
                            <tr><td height='15' colspan='4'></td></tr>

                        </table>
                      </div>
                    </div>
      </div>
      
      <div class='customZone' data-type='gmap'>
          <div class='movableContent'>
              <div >
                 <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd;'>
                      <tr><td height='15' colspan='4'></td></tr>
                      <tr>
                        <td width='20'></td>
                          <td valign='top' style='padding:0 20px;'>
                              <div class='contentEditableContainer contentGmapEditable'>
                                  <div class='contentEditable'>
                                      <img src='/applications/Mail_Interface/3_3/modules/User_Interface/core/v31_campaigns//neweditor/default/gmap_example.png' width='175' data-default='placeholder'>
                                  </div>
                              </div>
                          </td>
                          <td valign='top' style='padding:0 20px 0 20px;'>
                              <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                      <h2 style='color:#555555;font-size:21px;'>This is a subtitle</h2>
                                      <p style='color:#999999;font-size:13px;line-height:19px;'>Etiam bibendum nunc in lacus bibendum porta. Vestibulum nec nulla et eros ornare condimentum. Proin facilisis, dui in mollis blandit. Sed non dui magna, quis tincidunt enim. Morbi vehicula pharetra lacinia. Cras tincidunt, justo at fermentum feugiat, eros orci accumsan dolor, eu ultricies eros dolor quis sapien.</p>
                                      <p style='color:#999999;font-size:13px;line-height:19px;'>Integer in elit in tortor posuere molestie non a velit. Pellentesque consectetur, nisi a euismod scelerisque.</p>
                                      <p style='text-align:right;margin:0;'><a href='#' style='color:#52619E;font-size:13px;font-weight:bold;text-decoration:none;'>Read more</a></p>
                                      
                                  </div>
                              </div>
                          </td>
                          <td width='20'></td>
                      </tr>
                      <tr><td height='15' colspan='4'></td></tr>
                  </table>
              </div>
          </div>
      </div>

      <div class='customZone' data-type='social'>
          <div class='movableContent'>
              <div >
                  <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd;'>
                      <tr><td height='15' colspan='4'></td></tr>
                      <tr>
                          <td valign='top' colspan='4' style='padding:0 20px;'>
                              <div class='contentTextEditable contentEditableContainer'>
                                  <div class='contentEditable'>
                                      <h2 style='color:#555555;font-size:21px;margin:0;'>This is a subtitle</h2>
                                  </div>
                              </div>
                          </td>
                      </tr>
                      <tr><td height='15' colspan='4'></td></tr>
                      <tr>
                          <td width='62' valign='top' valign='top' width='62' style='padding:0 0 0 20px;'>
                              <div class='contentEditableContainer contentTwitterEditable'>
                                  <div class='contentEditable'>
                                      <img src='/applications/Mail_Interface/3_3/modules/User_Interface/core/v31_campaigns//neweditor/default/icon_twitter.png' width='42' height='42' data-customIcon='false' data-noText='false'>
                                  </div>
                              </div>
                          </td>
                          <td width='216' valign='top' >
                              <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                      <p style='color:#999999;font-size:13px;line-height:19px;'>Follow us on twitter to stay up to date with company news and other information.</p>
                                  </div>
                              </div>
                          </td>
                          <td width='62' valign='top' valign='top' width='62'>
                              <div class='contentEditableContainer contentFacebookEditable'>
                                  <div class='contentEditable'>
                                      <img src='/applications/Mail_Interface/3_3/modules/User_Interface/core/v31_campaigns//neweditor/default/icon_facebook.png' width='42' height='42' data-customIcon='false' data-noText='false'>
                                  </div>
                              </div>
                          </td>
                          <td width='216' valign='top' style='padding:0 20px 0 0;'>
                              <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                      <p style='color:#999999;font-size:13px;line-height:19px;'>Like us on Facebook to keep up with our news, updates and other discussions.</p>
                                  </div>
                              </div>
                          </td>
                      </tr>
                      <tr><td height='15' colspan='4'></td></tr>
                  </table>
              </div>
          </div>
      </div>

      <div class='customZone' data-type='twitter'>
          <div class='movableContent'>
              <div '>
                  <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd;'>
                      <tr><td height='15' colspan='4'></td></tr>
                      <tr>
                          <td width='20'></td>
                          <td valign='top' valign='top' width='62' style='padding:0 0 0 20px;'>
                              <div class='contentEditableContainer contentTwitterEditable'>
                                  <div class='contentEditable'>
                                      <img src='/applications/Mail_Interface/3_3/modules/User_Interface/core/v31_campaigns//neweditor/default/icon_twitter.png' width='42' height='42' data-customIcon='false' data-noText='false'>
                                  </div>
                              </div>
                          </td>
                          <td valign='top' style='padding:0 20px 0 0;'>
                              <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                      <p style='color:#999999;font-size:13px;line-height:19px;'>Follow us on twitter to stay up to date with company news and other information.</p>
                                  </div>
                              </div>
                          </td>
                          <td width='20'></td>
                      </tr>
                      <tr><td height='15' colspan='4'></td></tr>
                  </table>
              </div>
          </div>
      </div>
      
      <div class='customZone' data-type='facebook'>
          <div class='movableContent'>
              <div >
                  <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd;'>
                      <tr><td height='15' colspan='4'></td></tr>
                      <tr>
                        <td width='20'></td>
                          <td valign='top' valign='top' width='62' style='padding:0 0 0 20px;'>
                              <div class='contentEditableContainer contentFacebookEditable'>
                                  <div class='contentEditable'>
                                      <img src='/applications/Mail_Interface/3_3/modules/User_Interface/core/v31_campaigns//neweditor/default/icon_facebook.png' width='42' height='42' data-customIcon='false' data-noText='false'>
                                  </div>
                              </div>
                          </td>
                          <td valign='top' style='padding:0 20px 0 0;'>
                              <div class='contentEditableContainer contentTextEditable'>
                                  <div class='contentEditable'>
                                      <p style='color:#999999;font-size:13px;line-height:19px;'>'Like us on Facebook to keep up with our news, updates and other discussions.</p>
                                  </div>
                              </div>
                          </td>
                          <td width='20'></td>
                      </tr>
                      <tr><td height='15' colspan='4'></td></tr>
                  </table>
              </div>
          </div>
      </div>

      <div class='customZone' data-type='line'>
          <div class='movableContent'>
                      <table width='560' border='0' cellspacing='0' cellpadding='0'><tr><td height='18' bgcolor='#F6F6F6'></td></tr></table>
                    </div>
      </div>


      <div class='customZone' data-type='colums1v2'><div class='movableContent'>
                          <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' >
                            <tr><td height='10'>&nbsp;</td></tr>
                            <tr>
                              <td align='left' valign='top'>
                                <table width='100%'  border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd' >
                                  <tr><td colspan='3' height='15'>&nbsp;</td></tr>
                                  <tr>
                                    <td width='25'>&nbsp;</td>
                                    <td class='newcontent'>
                                  
                                    </td>
                                    <td width='25'>&nbsp;</td>
                                  </tr>
                                  <tr><td colspan='3' height='25'>&nbsp;</td></tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                    </div>
      </div>

      <div class='customZone' data-type='colums2v2'><div class='movableContent'>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr><td height='10' colspan='3'>&nbsp;</td></tr>
                        <tr>
                          <td width='275'  valign='top' >
                            <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd' >
                              <tr><td colspan='3' height='15'>&nbsp;</td></tr>
                              <tr>
                                <td width='25'>&nbsp;</td>
                                <td class='newcontent'>
                              
                                </td>
                                <td width='25'>&nbsp;</td>
                              </tr>
                              <tr><td colspan='3' height='25'>&nbsp;</td></tr>
                            </table>
                          </td>

                          <td width='10'>&nbsp;</td>
                          
                          <td width='275' valign='top' >
                            <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd' >
                              <tr><td colspan='3' height='15'>&nbsp;</td></tr>
                              <tr>
                                <td width='25'>&nbsp;</td>
                                <td class='newcontent'>
                              
                                </td>
                                <td width='25'>&nbsp;</td>
                              </tr>
                              <tr><td colspan='3' height='25'>&nbsp;</td></tr>
                            </table>
                          </td>
                        </tr>
                        
                      </table>
                    </div>
      </div>

      <div class='customZone' data-type='colums3v2'><div class='movableContent'>
                      <table width='560' border='0' cellspacing='0' cellpadding='0' align='center' valign='top'>
                        <tr><td height='10' colspan='5'>&nbsp;</td></tr>
                        <tr>
                          <td width='182'  valign='top' >
                            <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd' >
                              <tr><td colspan='3' height='15'>&nbsp;</td></tr>
                              <tr>
                                <td width='25'>&nbsp;</td>
                                <td class='newcontent'>
                              
                                </td>
                                <td width='25'>&nbsp;</td>
                              </tr>
                              <tr><td colspan='3' height='25'>&nbsp;</td></tr>
                            </table>
                          </td>

                          <td width='7'>&nbsp;</td>
                          
                          <td width='182'  valign='top' >
                            <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd' >
                              <tr><td colspan='3' height='15'>&nbsp;</td></tr>
                              <tr>
                                <td width='25'>&nbsp;</td>
                                <td class='newcontent'>
                              
                                </td>
                                <td width='25'>&nbsp;</td>
                              </tr>
                              <tr><td colspan='3' height='25'>&nbsp;</td></tr>
                            </table>
                          </td>

                          <td width='7'>&nbsp;</td>
                          
                          <td width='182'  valign='top' >
                            <table width='100%' border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff' style='border:1px solid #dddddd' >
                              <tr><td colspan='3' height='15'>&nbsp;</td></tr>
                              <tr>
                                <td width='25'>&nbsp;</td>
                                <td class='newcontent'>
                              
                                </td>
                                <td width='25'>&nbsp;</td>
                              </tr>
                              <tr><td colspan='3' height='25'>&nbsp;</td></tr>
                            </table>
                          </td>
                        </tr>
                        
                      </table>
                    </div>
      </div>

      <div class='customZone' data-type='textv2'>
        <table  border='0' cellspacing='0' cellpadding='0' align='center' valign='top' bgcolor='#ffffff'>
                                <tr>
                                  <td align='left' valign='top'>
                                    <div class='contentEditableContainer contentTextEditable' >
                             <div class='contentEditable' >
                                    <h1 style='color:#555555;font-size:21px;font-weight:normal;'>Cinnamon Roll</h1>
                                    </div>
                            </div>
                                  </td>
                                </tr>
                                <tr><td height='13'></td></tr>
                                <tr>
                                  <td align='left' valign='top'>
                                    <div class='contentEditableContainer contentTextEditable' >
                             <div class='contentEditable' >
                                    <p style='color:#999999;font-size:13px;line-height:19px;'>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    </div>
                            </div>
                                  </td>
                                </tr>
                                <tr><td height='20'></td></tr>
                                <tr>
                                  <td align='right' valign='top' style='padding-bottom:8px;'>
                                    <div class='contentEditableContainer contentTextEditable' >
                             <div class='contentEditable' >
                                    <a href='#' style='padding:8px;background:#52619E;font-size:13px;color:#ffffff;text-decoration:none;font-weight:bold;'>Find out more →</a>
                                    </div>
                            </div>
                                  </td>
                                </tr>
                              </table>
      </div>



    -->
    <!--Default Zone End-->

  </body>
  </html>
";
    
    
    
    
}






